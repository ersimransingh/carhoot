/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.carhoot;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.carhoot";
  public static final String BUILD_TYPE = "release";
  public static final int VERSION_CODE = 9;
  public static final String VERSION_NAME = "9.0";
}
