import React from 'react';
import 'react-native-gesture-handler';

import Navigation from './src/screens/navigation'
import { Provider } from 'react-redux'
import store from './src/redux/store'
import { setJSExceptionHandler, getJSExceptionHandler } from 'react-native-exception-handler';
import { Alert } from 'react-native';
export default function App() {
  const exceptionhandler = (e, isFatal) => {
    if (isFatal) {
      Alert.alert(
        'Unexpected error occurred',
        `
          Error: ${(isFatal) ? 'Fatal:' : ''} ${e.name} ${e.message}
          We have reported this to our team ! Please close the app and start again!
          `,
        [{
          text: 'Close'
        }]
      );
    } else {
      console.log(e); // So that we can see it in the ADB logs in case of Android if needed
    }
  };
  setJSExceptionHandler(exceptionhandler, true);
  return (
    <Provider store={store}>
      <Navigation />
    </Provider>
  );
}
