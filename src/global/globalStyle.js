export const global = {
    greenColorText: '#0AC97E'
}


export const fontFamilyRegular = "SofiaProRegular";
export const fontFamilyLight = "SofiaProLight";
export const fontFamilyBold = "SofiaProBold";
export const fontFamilyMedium = "SofiaProMedium";
export const fontFamilySemiBold = "Poppins-SemiBold";
export const buttonColor = "#276EF1";
export const bluecolor = "#276EF1";
export const textColor = "#34495E"
