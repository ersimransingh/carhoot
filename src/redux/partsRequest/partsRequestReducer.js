import { SET_PARTS_LOCATION, SET_PART_CATEGORY_REQUEST, SET_TO_DEFAULT_PARTS_STATE, SET_PART_CATEGORY_TYPE, SET_PART_EDITABLE, SET_CAR_NAME_PART, SET_PART_NAME, SET_PART_NAMES, SET_PART_SUB_CATEGORY_REQUEST } from './partsRequestTypes';

const initialState = {
    partCategory1: "eg : engine",
    partCategoryId1: "",
    partSubCategory1: "eg : Gasket and Seal",
    partSubCategoryId1: "",
    partName: "eg : Crankshaft seal",
    partNames: [],
    longitude: 36.8219813,
    latitude: -1.2851404,
    // longitude: 76.717873,
    // latitude: 30.704648,
    locationName: "",
    carName: "Choose your car",
    selectedCarId: "",
    isEditable: false,
    editId: "",
    requestType: "",
    requestService: ""
}

const partsRequestReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_PART_CATEGORY_REQUEST:
            return {
                ...state,
                partCategory1: action.data,
                partCategoryId1: action.id
            }
        case SET_PART_CATEGORY_TYPE:
            return {
                ...state,
                requestType: action.type,
                requestService: action.service
            }
        case SET_PART_SUB_CATEGORY_REQUEST:
            return {
                ...state,
                partSubCategory1: action.data,
                partSubCategoryId1: action.id
            }
        case SET_PART_NAME:
            return {
                ...state,
                partName: action.data
            }
        case SET_CAR_NAME_PART:
            return {
                ...state,
                carName: action.data,
                carId: action.id
            }
        case SET_PARTS_LOCATION:
            return {
                ...state,
                longitude: action.long,
                latitude: action.lat,
                locationName: action.name
            }
        case SET_PART_NAMES:
            return {
                ...state,
                partNames: action.data
            }
        case SET_PART_EDITABLE:
            return {
                ...state,
                isEditable: action.val,
                editId: action.id
            }
        case SET_TO_DEFAULT_PARTS_STATE:
            return {
                ...state,
                partCategory1: "eg : engine",
                partCategoryId1: "",
                partSubCategory1: "eg : Gasket and Seal",
                partSubCategoryId1: "",
                partName: "eg : Crankshaft seal",
                partNames: [],
                longitude: 76.717873,
                latitude: 30.704649,
                locationName: "",
                carName: "Choose your car",
                selectedCarId: "",
                isEditable: false,
                editId: "",
                requestType: "",
                requestService: ""
            }
        default: return state
    }
}
export default partsRequestReducer;