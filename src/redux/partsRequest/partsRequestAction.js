import { SET_PARTS_LOCATION, SET_PART_CATEGORY_REQUEST, SET_TO_DEFAULT_PARTS_STATE, SET_PART_EDITABLE, SET_PART_CATEGORY_TYPE, SET_CAR_NAME_PART, SET_PART_NAME, SET_PART_NAMES, SET_PART_SUB_CATEGORY_REQUEST } from './partsRequestTypes';
export const setParCategory = (data, id) => {
    return {
        type: SET_PART_CATEGORY_REQUEST,
        data: data,
        id
    }
}
export const setPartSubCategory = (data, id) => {
    return {
        type: SET_PART_SUB_CATEGORY_REQUEST,
        data: data,
        id
    }
}
export const setPartName = (data) => {
    return {
        type: SET_PART_NAME,
        data: data
    }
}
export const setPartCarName = (data, id) => {
    return {
        type: SET_CAR_NAME_PART,
        data: data,
        id: id
    }
}
export const setLocationPart = (long, lat, name) => {
    return {
        type: SET_PARTS_LOCATION,
        long: long,
        lat: lat,
        name: name
    }
}
export const setPartNames = (data) => {
    return {
        type: SET_PART_NAMES,
        data: data
    }
}
export const setPartEditable = (val, id) => {
    return {
        type: SET_PART_EDITABLE,
        val: val,
        id: id
    }
}
export const setCategoryType = (type, service) => {
    return {
        type: SET_PART_CATEGORY_TYPE,
        type,
        service
    }
}
export const setPartsRequestAllDefault = () => {
    return {
        type: SET_TO_DEFAULT_PARTS_STATE
    }
}