import { SET_CAR_PARTS, SET_PART_CATEGORY, SET_PART_SUB_CATEGORY, SET_PART_NAMES, SET_SERVICE_PARTS, UPDATE_SERVICE_PARTS, SET_PARTS_CATEGORIES } from './importPartsTypes'

const initialState = {
    serviceParts: [],
    partsCategory: [],
    partsSubCategory: [],
    partsName: [],
    carParts: [],
    partsCategories: [],
}

const importPartsReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_CAR_PARTS:
            return {
                ...state,
                carParts: action.data
            }
        case SET_PART_CATEGORY:
            return {
                ...state,
                partsCategory: action.data
            }
        case SET_PART_SUB_CATEGORY:
            return {
                ...state,
                partsSubCategory: action.data
            }
        case SET_PART_NAMES:
            return {
                ...state,
                partsName: action.data
            }
        case SET_SERVICE_PARTS:
            return {
                ...state,
                serviceParts: action.data
            }
        case SET_PARTS_CATEGORIES:
            return {
                ...state,
                partsCategories: action.data
            }
        case UPDATE_SERVICE_PARTS:
            const index = action.index;
            const newArray = [...state.serviceParts]
            newArray[index].status = action.value
            return {
                ...state,
                serviceParts: newArray
            }

        default: return state
    }
}
export default importPartsReducer;