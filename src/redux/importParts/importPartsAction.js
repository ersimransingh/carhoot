import { SET_CAR_PARTS, SET_PART_CATEGORY, SET_PART_SUB_CATEGORY, SET_PART_NAMES, SET_SERVICE_PARTS, UPDATE_SERVICE_PARTS, SET_PARTS_CATEGORIES } from './importPartsTypes'

export const setCarPartsRedux = (data) => {
    return {
        type: SET_CAR_PARTS,
        data: data
    }
}
export const setPartCategoryRedux = (data) => {
    return {
        type: SET_PART_CATEGORY,
        data: data
    }
}
export const setPartSubCategoryRedux = (data) => {
    return {
        type: SET_PART_SUB_CATEGORY,
        data: data
    }
}
export const setPartNamesRedux = (data) => {
    return {
        type: SET_PART_NAMES,
        data: data
    }
}
export const setServicePartsRedux = (data) => {
    return {
        type: SET_SERVICE_PARTS,
        data: data
    }
}
export const setPartsCategoriesAll = (data) => {
    return {
        type: SET_PARTS_CATEGORIES,
        data: data
    }
}

export const updateServicePart = (index, value) => {
    return {
        type: UPDATE_SERVICE_PARTS,
        index: index,
        value: value
    }
}