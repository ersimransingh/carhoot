import { UPDATE_USER_DATA, UPDATE_USER_CAR_DATA, Get_requests, SET_CONFIRM_PIN } from './userType'
const initialState = {
    userData: {},
    carsData: null,
    requests: [],
    confirmPin: null
}
const userReducer = (state = initialState, action) => {

    switch (action.type) {
        case UPDATE_USER_DATA:
            return {
                ...state,
                userData: action.data
            }

        case UPDATE_USER_CAR_DATA:
            return {
                ...state,
                carsData: action.data
            }
        case Get_requests:
            return {
                ...state,
                requests: action.data
            }
        case SET_CONFIRM_PIN:
            return {
                ...state,
                confirmPin: action.data
            }
        default: return state;
    }
}

export default userReducer;