import { UPDATE_USER_DATA, UPDATE_USER_CAR_DATA, Get_requests, SET_CONFIRM_PIN } from './userType';

export const updateUserData = (data) => {
    return {
        type: UPDATE_USER_DATA,
        data: data
    }
}

export const updateUserCarData = (data) => {
    return {
        type: UPDATE_USER_CAR_DATA,
        data: data
    }
}
export const updateRequests = (data) => {
    return {
        type: Get_requests,
        data: data
    }
}
export const confirmPin = (data) => {
    return {
        type: SET_CONFIRM_PIN,
        data: data
    }
}