import { GET_DATA_Models, GET_DATA_EngineSize, GET_DATA_Make } from './carDataTypes';

export const getCarModels = (data) => {
    return {
        type: GET_DATA_Models,
        data: data
    }
}
export const getCarMake = (data) => {
    return {
        type: GET_DATA_Make,
        data: data
    }
}
export const getCarEngine = (data) => {
    return {
        type: GET_DATA_EngineSize,
        data: data
    }
}