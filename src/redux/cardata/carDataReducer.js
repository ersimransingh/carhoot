import { GET_DATA_Models, GET_DATA_EngineSize, GET_DATA_Make } from './carDataTypes'
const initialState = {
    // navigationPage: 'Dashboard',
    carModels: [],
    carEngine: [],
    carMake: []

}
const carDataReducer = (state = initialState, action) => {

    switch (action.type) {
        case GET_DATA_Models:
            return {
                ...state,
                carModels: action.data
            }
        case GET_DATA_Make:
            return {
                ...state,
                carMake: action.data
            }
        case GET_DATA_EngineSize:
            return {
                ...state,
                carEngine: action.data
            }
        default: return state;
    }
}

export default carDataReducer;