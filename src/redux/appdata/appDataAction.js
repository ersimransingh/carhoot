import { GET_Rescue_Data, GET_AutoService_Data, SET_SECONDS_TOWING, ALL_SERVICES_SEARCHABLE, SERVICE_DETAILS, UPDATE_SERVICE_DETAILS, UPDATE_SERVICE_TRIM, SET_SERVICE_TRIM, SET_IS_EDITABLE } from './appDataTypes';

export const getRescueData = (data) => {
    return {
        type: GET_Rescue_Data,
        data: data
    }
}
export const updateTowingSeconds = (data) => {
    return {
        type: SET_SECONDS_TOWING,
        data: data
    }
}
export const getAutoServiceData = (data) => {
    return {
        type: GET_AutoService_Data,
        data: data
    }
}
export const setServicesSearchable = (data) => {
    return {
        type: ALL_SERVICES_SEARCHABLE,
        data: data
    }
}
export const setServiceDetails = (data) => {
    return {
        type: SERVICE_DETAILS,
        data: data
    }
}
export const setServiceTrim = (data) => {
    return {
        type: SET_SERVICE_TRIM,
        data: data
    }
}
export const updateServiceDetails = (index, value) => {
    return {
        type: UPDATE_SERVICE_DETAILS,
        index: index,
        value: value
    }
}
export const updateServiceTrim = (index, value) => {
    return {
        type: UPDATE_SERVICE_TRIM,
        index: index,
        value: value
    }
}
export const setEditableTowing = (status, id) => {
    return {
        type: SET_IS_EDITABLE,
        data: status,
        id: id
    }
}
