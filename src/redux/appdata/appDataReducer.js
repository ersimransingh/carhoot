import { GET_Rescue_Data, GET_AutoService_Data, SET_SECONDS_TOWING, ALL_SERVICES_SEARCHABLE, SERVICE_DETAILS, UPDATE_SERVICE_DETAILS, UPDATE_SERVICE_TRIM, SET_SERVICE_TRIM, SET_TOWING_ID, SET_IS_EDITABLE } from './appDataTypes'
const initialState = {
    data: [],
    autoservice: [],
    servicesSearchable: [],
    serviceDetails: [],
    serviceTrim: [],
    isEditing: false,
    towingId: "",
    secondsTowing: 0

}
const rescueDataReducer = (state = initialState, action) => {


    switch (action.type) {
        case SET_SECONDS_TOWING:
            return {
                ...state,
                secondsTowing: action.data
            }
        case GET_Rescue_Data:
            return {
                ...state,
                data: action.data
            }
        case GET_AutoService_Data:
            return {
                ...state,
                autoservice: action.data
            }
        case ALL_SERVICES_SEARCHABLE:
            return {
                ...state,
                servicesSearchable: action.data
            }
        case SERVICE_DETAILS:
            return {
                ...state,
                serviceDetails: action.data
            }
        case SET_SERVICE_TRIM:
            return {
                ...state,
                serviceTrim: action.data
            }
        case UPDATE_SERVICE_DETAILS:
            const index = action.index;
            const newArray = [...state.serviceDetails]
            newArray[index].status = action.value
            return {
                ...state,
                serviceDetails: newArray
            }
        case UPDATE_SERVICE_TRIM:
            const index1 = action.index;
            const newArray1 = [...state.serviceTrim]
            newArray1[index1].status = action.value
            return {
                ...state,
                serviceTrim: newArray1
            }
        case SET_IS_EDITABLE:
            return {
                ...state,
                isEditing: action.data,
                towingId: action.id
            }
        default: return state;
    }
}

export default rescueDataReducer;