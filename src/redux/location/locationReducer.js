import { UPDATE_LOCATION_PLACE_DATA } from './locationTypes'
const initialState = {
    locationPlaces: []
}
const locationReducer = (state = initialState, action) => {

    switch (action.type) {
        case UPDATE_LOCATION_PLACE_DATA:
            return {
                ...state,
                locationPlaces: action.data
            }
        default: return state;
    }
}

export default locationReducer;