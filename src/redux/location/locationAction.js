import { UPDATE_LOCATION_PLACE_DATA } from './locationTypes'

export const updateLocationsData = (data) => {
    return {
        type: UPDATE_LOCATION_PLACE_DATA,
        data: data
    }
}