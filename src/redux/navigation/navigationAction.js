import { UPDATE_STATE } from './navigationTypes';

export const updateState = (pageName) => {
    return {
        type: UPDATE_STATE,
        data: pageName
    }
}