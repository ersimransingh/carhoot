import { UPDATE_STATE } from './navigationTypes'
const initialState = {
    // navigationPage: 'Dashboard',
    navigationPage: 'splash',

}
const navigationReducer = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_STATE:
            return {
                ...state,
                navigationPage: action.data
            }
        default: return state;
    }
}

export default navigationReducer;