import { combineReducers } from 'redux'
import navigationReducer from './navigation/navigationReducer'
import userReducer from './user/userReducer';
import carDataReducer from './cardata/carDataReducer';
import rescueDataReducer from './appdata/appDataReducer'
import locationReducer from './location/locationReducer';
import stepReducer from './steps/stepReducer';
import importCarReducer from './importCar/importCarReducer'
import importPartsReducer from './importParts/importPartsReducer'
import partsRequestReducer from './partsRequest/partsRequestReducer'
const rootReducer = combineReducers({
    navigation: navigationReducer,
    user: userReducer,
    cardata: carDataReducer,
    servicedata: rescueDataReducer,
    location: locationReducer,
    steps: stepReducer,
    importCar: importCarReducer,
    importParts: importPartsReducer,
    parts: partsRequestReducer
})

export default rootReducer;