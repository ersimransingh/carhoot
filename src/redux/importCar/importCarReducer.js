import {
    SET_CAR_DATA,
    SET_CAR_IMPORT_DATA,
    SET_DONT_KNOW_CAR_MAKES,
    SET_DONT_BODY_TYPE,
    SET_DONT_FUEL_TYPE,
    SET_CAR_COLOR_REDUX,
    SET_CAR_ENGINE_REDUX,
    SET_CAR_MODELS_REDUX,
    SET_CAR_YEAR_REDUX,
    SET_DONT_BUDGET,
    SET_DONT_MILAGE,
    SET_ALL_CARTS_ARR,
    SET_USER_CARS
} from './importCarTypes'
const initialState = {
    carsData: {},
    importCarMakes: [],
    dknowImportMakeCar: [],
    dknowBodyType: [],
    dknowFuelType: [],
    dknowminmaxMilage: {},
    dknowminMaxBudget: {},
    allCars: [],
    knowcarModel: [],
    knwoCarEngine: [],
    knowYearofManufacture: [],
    knowColor: [],
    userCars: []
}

const importCarReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_CAR_DATA:
            return {
                ...state,
                importCarMakes: action.data.knoTheCar.car
            }
        case SET_USER_CARS:
            return {
                ...state,
                userCars: action.data
            }
        case SET_CAR_IMPORT_DATA:
            return {
                ...state,
                importCarMakes: action.data.knoTheCar.car,
                // dknowImportMakeCar:action.data.dontKnowCar.
            }
        case SET_DONT_KNOW_CAR_MAKES:
            return {
                ...state,
                dknowImportMakeCar: action.data
            }
        case SET_DONT_BODY_TYPE:
            return {
                ...state,
                dknowBodyType: action.data
            }
        case SET_ALL_CARTS_ARR:
            return {
                ...state,
                allCars: action.data
            }
        case SET_DONT_FUEL_TYPE:
            return {
                ...state,
                dknowFuelType: action.data
            }
        case SET_CAR_COLOR_REDUX:
            return {
                ...state,
                knowColor: action.data
            }
        case SET_CAR_ENGINE_REDUX:
            return {
                ...state,
                knwoCarEngine: action.data
            }
        case SET_CAR_YEAR_REDUX:
            return {
                ...state,
                knowYearofManufacture: action.data
            }
        case SET_CAR_MODELS_REDUX:
            return {
                ...state,
                knowcarModel: action.data
            }
        case SET_DONT_BUDGET:
            return {
                ...state,
                dknowminMaxBudget: action.data
            }
        case SET_DONT_MILAGE:
            return {
                ...state,
                dknowminmaxMilage: action.data
            }
        default: return state
    }
}

export default importCarReducer;