import {
    SET_CAR_DATA,
    SET_CAR_IMPORT_DATA,
    SET_DONT_KNOW_CAR_MAKES,
    SET_DONT_BODY_TYPE,
    SET_DONT_FUEL_TYPE,
    SET_CAR_COLOR_REDUX,
    SET_CAR_ENGINE_REDUX,
    SET_CAR_MODELS_REDUX,
    SET_CAR_YEAR_REDUX,
    SET_DONT_MILAGE,
    SET_DONT_BUDGET,
    SET_ALL_CARTS_ARR,
    SET_USER_CARS
} from './importCarTypes'

export const setImportCarData = (data) => {
    return {
        type: SET_CAR_DATA,
        data: data
    }
}
export const setUserCars = (data) => {
    return {
        type: SET_USER_CARS,
        data: data
    }
}
export const setCarImport = (data) => {
    return {
        type: SET_CAR_IMPORT_DATA,
        data: data
    }
}
export const setAllCars = (data) => {
    return {
        type: SET_ALL_CARTS_ARR,
        data: data
    }
}
export const setDontKnowCarMakes = (data) => {
    return {
        type: SET_DONT_KNOW_CAR_MAKES,
        data: data
    }
}

export const setBodyType = (data) => {
    return {
        type: SET_DONT_BODY_TYPE,
        data: data
    }
}

export const setFuelType = (data) => {
    return {
        type: SET_DONT_FUEL_TYPE,
        data: data
    }
}

export const setCarColorRedux = (data) => {
    return {
        type: SET_CAR_COLOR_REDUX,
        data: data
    }
}
export const setCarEngineRedux = (data) => {
    return {
        type: SET_CAR_ENGINE_REDUX,
        data: data
    }
}
export const setCarModelRedux = (data) => {
    return {
        type: SET_CAR_MODELS_REDUX,
        data: data
    }
}
export const setCarYearlRedux = (data) => {
    return {
        type: SET_CAR_YEAR_REDUX,
        data: data
    }
}
export const setDontMilageRedux = (data) => {
    return {
        type: SET_DONT_MILAGE,
        data: data
    }
}
export const setDontBudgetRedux = (data) => {
    return {
        type: SET_DONT_BUDGET,
        data: data
    }
}