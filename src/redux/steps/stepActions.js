import { SET_ACTIVE_STEPS, ADD_ACTIVE_STEPS, OPEN_PAGE_STEPS } from './stepTypes'

export const updateSteps = (data) => {
    return {
        type: SET_ACTIVE_STEPS,
        data: data
    }
}
export const addSteps = (data) => {
    return {
        type: ADD_ACTIVE_STEPS,
        data: data
    }
}
export const updateOpenPageSteps = (data) => {
    return {
        type: OPEN_PAGE_STEPS,
        data: data
    }
}