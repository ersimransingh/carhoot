import { SET_ACTIVE_STEPS, ADD_ACTIVE_STEPS, OPEN_PAGE_STEPS } from './stepTypes'

const innitialState = {
    activeSteps: [],
    openPageSteps: []
}

const stepReducer = (state = innitialState, action) => {
    switch (action.type) {
        case SET_ACTIVE_STEPS:
            return {
                ...state,
                activeSteps: action.data
            }
        case ADD_ACTIVE_STEPS:
            return {
                ...state,
                activeSteps: [...state.activeSteps, action.data]
            }
        case OPEN_PAGE_STEPS:
            return {
                ...state,
                openPageSteps: action.data
            }
        default: return state;
    }
}
export default stepReducer;