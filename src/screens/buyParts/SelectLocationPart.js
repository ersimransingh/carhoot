import React from 'react';
import { View, Text, StatusBar, Image, Dimensions, SafeAreaView, StyleSheet, Alert, Linking } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import Geolocation from '@react-native-community/geolocation';
import SelectLocationNew from '../../components/SelectLocationNew'
import MapView, { Marker, Overlay, PROVIDER_GOOGLE, PROVIDER_DEFAULT } from 'react-native-maps'
import Button from '../../components/Button';
import { MapStyle } from '../../global/mapStyle'
import { useRoute } from '@react-navigation/native';
import { updateUserData } from '../../redux';
import axios from 'axios'
import Api from '../../api';
import { useSelector, useDispatch } from 'react-redux';
import { updateStates, updateRequests, setEditableTowing, setLocationPart, setCategoryType } from '../../redux/'
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment';


export default function SelectLocationPart(props) {
    const dispatch = useDispatch()
    const lastData = useSelector(state => state.parts)
    const partsRedux = useSelector(state => state.importParts.serviceParts);
    const partId = "5f734c1ff77c365a6c1106b3";
    const [longitude, setLongitude] = React.useState(lastData.longitude)
    const [latitude, setLatitude] = React.useState(lastData.latitude)
    const [selectedData, setSelectedData] = React.useState(lastData.locationName)
    const [lat, setLat] = React.useState(31)
    const [long, setLong] = React.useState(75)
    const [longitudeDelta, setLongitudeDetla] = React.useState(0.0022208690643310547);
    const [latitudeDelta, setlatitudeDetla] = React.useState(0.0035607585813046683);
    const mapRef = React.createRef()
    const route = useRoute();

    const _gotoNextPage = async () => {
        if (route.name != "SelectLocationPartEdit") {
            dispatch(setLocationPart(longitude, latitude, selectedData))
            props.navigation.navigate("SearchPageParts", props.route.params);
        } else {
            updateLocation()
        }
    }
    const getLocationFromLatLong = (lat, long) => {
        console.log('https://maps.googleapis.com/maps/api/geocode/json?address=' + lat + ',' + long + '&key=AIzaSyAB7cS3Wb_fyoN_GObjGhud-3PMNNRmfwc')
        fetch('https://maps.googleapis.com/maps/api/geocode/json?address=' + lat + ',' + long + '&key=AIzaSyAB7cS3Wb_fyoN_GObjGhud-3PMNNRmfwc')
            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.results[0].formatted_address) {
                    setSelectedData(responseJson.results[0].formatted_address)
                    dispatch(setLocationPart(long, lat, responseJson.results[0].formatted_address))
                } else {
                    setSelectedData("Current Location")
                    dispatch(setLocationPart(long, lat, "Current Location"))
                }
            })
    }
    React.useEffect(() => {

        Geolocation.getCurrentPosition((x) => {
            if (route.name != "SelectLocationPartEdit") {
                setLatitude(x.coords.latitude)
                setLongitude(x.coords.longitude)
                getLocationFromLatLong(x.coords.latitude, x.coords.longitude)
                console.log(x.coords.latitude)
            } else {
                setLatitude(lastData.latitude)
                setLongitude(lastData.longitude)
                setSelectedData(lastData.locationName)
            }
            setLat(x.coords.latitude)
            setLong(x.coords.longitude)
        },
            err => {
                if (err.code == 2) {
                    if (route.name != "SelectLocationPartEdit") {
                        setLatitude(-1.2851404)
                        setLongitude(36.8219813)
                        getLocationFromLatLong(-1.2851404, 36.8219813)
                    }
                    Alert.alert("Alert", "GPS not enabled so we are not able to get your current location automatically.",
                        [
                            {
                                text: "Cancel",
                                onPress: () => console.log("Cancel Pressed"),
                                style: "cancel"
                            },
                            { text: "Open Setting", onPress: () => Linking.openSettings() }
                        ])
                } else if (err.code == 3) {
                    Alert.alert("Unable to get location please activate High Accuracy Location from settings.")
                } else {
                    Alert.alert(err.message)
                }

            },
            { enableHighAccuracy: false, timeout: 50000 }
        )
    }, [false])
    const _UpdateAllData = async () => {
        try {
            const value = await AsyncStorage.getItem('token')
            axios.post(Api + "/api/getRescueRequest", {}, {
                headers: { 'authorization': value }
            })
                .then((res) => {
                    if (res.data.status == true) {
                        dispatch(updateRequests(res.data.data))
                        props.navigation.goBack();
                    }
                })
                .catch((error) => {
                    console.log(error)
                })
        }
        catch (err) {
            console.log(err)
        }
    }
    const updateLocation = async () => {
        try {
            const value = await AsyncStorage.getItem('token')
            let d = {}
            let partItems = [];
            for (let i in partsRedux) {
                if (partsRedux[i].status) {
                    partItems.push(partsRedux[i].name);
                }
            }
            dispatch(setLocationPart(longitude, latitude, selectedData))
            if (lastData.requestType == 'import') {
                d = {
                    id: lastData.editId,
                    importType: "part",
                    type: "import",
                    carMake: lastData.carName,
                    partsName: partItems,
                    importService: partId,
                    startTime: moment().add(60, 'm').toDate(),
                    endTime: moment().add(90, 'm').toDate(),
                    location: {
                        long: longitude,
                        lat: latitude
                    },
                    locationName: selectedData,
                    price: 0
                }
                dispatch(setCategoryType("import", "part"))
            } else {
                d = {
                    id: lastData.editId,
                    importType: "part",
                    type: "get",
                    carMake: lastData.carName,
                    subCategory: lastData.partSubCategory1,
                    partCategory: lastData.partCategory1,
                    partName: lastData.partName,
                    importService: partId,
                    startTime: moment().add(60, 'm').toDate(),
                    endTime: moment().add(90, 'm').toDate(),
                    location: {
                        long: longitude,
                        lat: latitude
                    },
                    locationName: selectedData,
                    price: 0
                }
                dispatch(setCategoryType("get", "part"))
            }
            axios.post(Api + "/api/editimportservice", d, {
                headers: { 'authorization': value }
            })
                .then((res) => {
                    _UpdateAllData()
                })
                .catch((err) => {
                    console.log(err)
                })


        } catch (err) {
            console.log(err)
        }
    }
    const _backCurrentLocation = () => {
        Geolocation.getCurrentPosition((x) => {
            setLatitude(x.coords.latitude)
            setLongitude(x.coords.longitude)
            if (mapRef.current) {
                mapRef.current.animateToRegion({
                    latitude: x.coords.latitude,
                    longitude: x.coords.longitude,
                    latitudeDelta: latitudeDelta,
                    longitudeDelta: longitudeDelta
                })
                getLocationFromLatLong(x.coords.latitude, x.coords.longitude)
            } else {
                _backCurrentLocation()
            }
        },
            err => {
                console.log(err)
            },
        )
    }
    return (
        <View style={styles.mainContainer}>
            <StatusBar
                backgroundColor="#276EF1"
                barStyle="light-content"
            />
            <View style={styles.topBar}></View>
            <View style={styles.mainHeader}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ justifyContent: 'center' }}>
                        <TouchableOpacity
                            style={{ padding: 15, paddingLeft: 0 }}
                            onPress={() => props.navigation.goBack()}
                        >
                            <Image
                                source={require('../../assets/images/Left.png')}
                                style={{ width: 24, height: 24 }}
                            />
                        </TouchableOpacity>
                    </View>
                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <SelectLocationNew
                            label={props.route.params.service != 'auto' ? "What is the delivery location?" : ""}
                            placeholder={selectedData}
                            // nonAbsolute={true}
                            onSelect={(t) => {
                                mapRef.current.animateToRegion({
                                    latitude: t.cords.lat,
                                    longitude: t.cords.long,
                                    latitudeDelta: latitudeDelta,
                                    longitudeDelta: longitudeDelta
                                }, 1000)
                                setLongitude(t.cords.long)
                                setLatitude(t.cords.lat);
                                setSelectedData(t.name)
                            }}
                        />
                    </View>
                </View>
            </View>
            <View style={styles.bottomButtonView}>
                <Button
                    name={"Select delivery location"}
                    onPress={() => {
                        _gotoNextPage()
                    }}
                />
            </View>
            <View style={styles.currentLocationIcon}>
                <TouchableOpacity
                    style={styles.currentLocationTouch}
                    onPress={() => _backCurrentLocation()}
                >
                    <Image
                        source={require('../../assets/images/currentLocation.png')}
                        style={{ width: 26, height: 26 }}
                    />
                </TouchableOpacity>
            </View>
            {latitude != -1.2851404 &&
                <SafeAreaView style={{ flex: 1, zIndex: 0 }}>
                    <Image
                        source={require('../../assets/images/currentLocationIcon.png')}
                        style={styles.mapcurrentlocation}
                    />
                    <MapView
                        style={{ flex: 1, zIndex: -2 }}
                        provider={PROVIDER_GOOGLE}
                        ref={mapRef}
                        initialRegion={{
                            latitude: parseFloat(latitude),
                            longitude: parseFloat(longitude),
                            latitudeDelta: latitudeDelta,
                            longitudeDelta: longitudeDelta,
                        }}
                        region={{
                            latitude: parseFloat(latitude),
                            longitude: parseFloat(longitude),
                            latitudeDelta: latitudeDelta,
                            longitudeDelta: longitudeDelta,
                        }}
                        customMapStyle={MapStyle}
                        onRegionChangeComplete={(d) => {
                            setLatitude(d.latitude)
                            setLongitude(d.longitude)
                            setLongitudeDetla(d.longitudeDelta)
                            setlatitudeDetla(d.latitudeDelta)
                            getLocationFromLatLong(d.latitude, d.longitude)


                        }}
                        cacheEnabled={false}
                    >
                        <Marker
                            coordinate={{
                                latitude: lat,
                                longitude: long,
                            }}
                            pinColor="#0AC97E"
                            title={"Your Location"}
                        >
                            <View style={styles.markerView}>
                            </View>
                        </Marker>
                    </MapView>
                </SafeAreaView>
            }
        </View>
    )
}
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1
    },
    topBar: {
        backgroundColor: "#276EF1", height: 40
    },
    mainHeader: {
        padding: 15, backgroundColor: '#F8F8F8', zIndex: 103
    },
    bottomButtonView: {
        position: 'absolute', width: '100%', bottom: 0, padding: 20, zIndex: 101
    },
    currentLocationIcon: {
        position: 'absolute', bottom: 100, padding: 20, right: 0, zIndex: 100
    },
    currentLocationTouch: {
        padding: 10, backgroundColor: '#FFFFFF', borderRadius: 50, zIndex: 34343
    },
    mapcurrentlocation: {
        width: 36, height: 45, position: 'absolute', alignSelf: 'center', marginTop: windowHeight / 2.9
    },
    markerView: {
        width: 14, height: 14, backgroundColor: "#276EF1", borderColor: 'rgba(255, 196, 50, 0.3)', borderWidth: 3, borderRadius: 7
    }
});