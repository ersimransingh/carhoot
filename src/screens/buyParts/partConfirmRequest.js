import React from 'react';
import { View, Dimensions, Text, KeyboardAvoidingView, Platform, Image, SafeAreaView, ScrollView, StatusBar, TouchableOpacity } from 'react-native';

import MapView, { PROVIDER_GOOGLE, PROVIDER_DEFAULT, Marker } from 'react-native-maps'
import { fontFamilyMedium, fontFamilyRegular, fontFamilyLight, fontFamilyBold } from '../../global/globalStyle'
import Header from '../../components/Header'
import Button from '../../components/Button'
import Input from '../../components/Input'
import SelectOption from '../../components/SelectOption'
import Api from '../../api';
import axios from 'axios';
import { useSelector, useDispatch } from 'react-redux';
import { updateLocationsData, updateRequests, setEditableTowing } from '../../redux'
import moment from 'moment'
import AsyncStorage from '@react-native-community/async-storage';
import { updateUserCarData, updateUserData, setPartEditable, setPartsRequestAllDefault } from '../../redux'
import MapViewDirections from 'react-native-maps-directions';

import Toast from 'react-native-simple-toast';
import { MapStyle } from '../../global/mapStyle';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;


export default function PartConfirmRequest(props) {

    const dispatch = useDispatch()
    const [loader, setLoader] = React.useState(false)
    const dd = useSelector(state => state.user.requests);
    const [activeData, setActiveData] = React.useState('[]')
    const _submitReq = async () => {
        try {
            setLoader(true)
            const value = await AsyncStorage.getItem('token')
            axios.post(Api + "/api/getRescueRequest", {

            }, {
                headers: { 'authorization': value }
            }
            )
                .then((res) => {
                    if (res.data.status == true) {
                        dispatch(updateRequests(res.data.data))
                        setLoader(false)
                    }
                })
                .catch((error) => {
                    console.log(error.message)
                    setLoader(false)
                    Toast.show(JSON.stringify(error))
                })
        }
        catch (err) {
            setLoader(false)
            console.log(err)
        }
    }

    const _submitRequest = async () => {
        let data = {}
        for (let i in dd) {
            if (dd[i]._id == props.route.params.oid) {
                data = dd[i]
            }
        }
        let d = {
            type: data.rescuetype + '  Request',
            price: data.price,
            id: props.route.params.id,
            oid: props.route.params.oid,
            name: data.requestType,
            latitude: parseFloat(props.route.params.latitude),
            longitude: parseFloat(props.route.params.longitude),
        }

        try {
            setLoader(true)
            const value = await AsyncStorage.getItem('token')
            axios.post(Api + "/api/changeStatus", {
                "type": "Import",
                "id": props.route.params.oid,
                "status": "Pending"

            }, {
                headers: { 'authorization': value }
            }
            )
                .then((res) => {
                    if (res.data.status == true) {
                        dispatch(setPartsRequestAllDefault())
                        Toast.show('Your request has been received')
                        _submitReq()
                        setLoader(false)
                        props.navigation.navigate('ImportPartProgress', d)
                    }
                })
                .catch((error) => {
                    setLoader(false)
                    Toast.show(error)
                })
        }

        catch (err) {
            setLoader(false)
            console.log(err)
        }

    }
    const [location, setLocation] = React.useState('');
    const [carMakes, setCarMakes] = React.useState('Select Car');
    let whereTo = props.route.params.whereto ? JSON.parse(props.route.params.whereto) : ''
    return (
        <View style={{ flex: 1 }} >
            <StatusBar
                barStyle="dark-content"
            />
            <View style={{ flex: 0.8 }}>

                <TouchableOpacity
                    onPress={() => props.navigation.navigate("Tabs")}
                    style={{ position: "absolute", zIndex: 103, padding: 20, marginTop: 30 }}
                >
                    <Image
                        source={require("../../assets/images/home_icon.png")}
                        style={{ width: 50, height: 50 }}
                    />
                </TouchableOpacity>

                <MapView
                    style={{ flex: 1 }}
                    provider={PROVIDER_GOOGLE}
                    loadingEnabled={true}
                    region={{
                        latitude: props.route.params.latitude,
                        longitude: props.route.params.longitude,
                        latitudeDelta: 0.0035607585813046683,
                        longitudeDelta: 0.0022208690643310547,
                    }}
                    cacheEnabled={true}
                    customMapStyle={MapStyle}
                >
                    <Marker
                        coordinate={{
                            latitude: props.route.params.latitude,
                            longitude: props.route.params.longitude,
                        }}
                        pinColor="#0AC97E"
                        title={"Location"}
                    >
                        <Image
                            source={require('../../assets/images/currentLocationIcon.png')}
                            style={{ width: 36, height: 45 }}
                        />
                    </Marker>
                    {props.route.params.whereto != "" &&
                        <Marker
                            coordinate={{
                                latitude: whereTo.lat,
                                longitude: whereTo.long,
                            }}
                            pinColor="#276EF1"
                            title={"Pick Up Location"}
                        >
                            <Image
                                source={require('../../assets/images/whereToPin.png')}
                                style={{ width: 36, height: 45 }}
                            />
                        </Marker>
                    }
                    {props.route.params.whereto != "" &&
                        <MapViewDirections
                            origin={{ longitude: props.route.params.longitude, latitude: props.route.params.latitude }}
                            destination={{ longitude: whereTo.long, latitude: whereTo.lat }}
                            apikey={"AIzaSyANEd-gpHYzA7Mc2hq9r9PgYxpWuUNeHL0"}
                            strokeWidth={3}
                            strokeColor="#276EF1"
                        />
                    }
                </MapView>
            </View>

            {dd.map((data, key) => {
                if (data._id == props.route.params.oid) {
                    var startTime = moment();
                    var endTime = moment(data.startTime);
                    var duration = moment.duration(endTime.diff(startTime));
                    var hours = parseInt(duration.asHours());
                    var minutes = parseInt(duration.asMinutes()) % 60;
                    return (
                        <View style={{ flex: 1, backgroundColor: 'white' }} key={key}>
                            <View style={{ backgroundColor: '#276EF1', padding: 8 }}>
                                <Text style={{ color: 'white', fontSize: 13, fontFamily: fontFamilyRegular, textAlign: 'center' }}>
                                    Import delivery in 2-3 weeks
                                </Text>
                            </View>
                            <ScrollView>
                                <View style={{ flexDirection: 'row', padding: 15, borderBottomWidth: 1, borderBottomColor: "#E0E6ED" }}>
                                    <View>
                                        <Image
                                            source={require('../../assets/images/money.png')}
                                            style={{ width: 45, height: 45 }}
                                        />
                                    </View>
                                    <View style={{ justifyContent: 'center', paddingLeft: 15 }}>
                                        <Text style={{ color: "#34495E", fontSize: 18, fontFamily: fontFamilyBold }}>KES {data.price}</Text>
                                    </View>
                                </View>
                                <View style={{ flexDirection: 'row', padding: 15, borderBottomWidth: 1, borderBottomColor: "#E0E6ED" }}>
                                    <View>
                                        <Image
                                            source={require('../../assets/images/locationPriceIcon.png')}
                                            style={{ width: 45, height: 45 }}
                                        />
                                    </View>
                                    <View style={{ justifyContent: 'center', paddingLeft: 15, flex: 1 }}>
                                        <Text style={{ color: "#34495E", fontSize: 15, fontFamily: fontFamilyRegular }}>{data.locationName}</Text>
                                    </View>
                                    <TouchableOpacity
                                        style={{ justifyContent: 'center' }}
                                        onPress={() => {
                                            let oldParams = props.route.params
                                            oldParams['orderData'] = data
                                            dispatch(setPartEditable(true, data._id))
                                            props.navigation.navigate("SelectLocationPartEdit", oldParams)

                                        }}
                                    >
                                        <Text style={{ color: "#276EF1", fontFamily: fontFamilyRegular }}>Change</Text>
                                    </TouchableOpacity>
                                </View>

                                <View style={{ flexDirection: 'row', padding: 15, borderBottomWidth: 1, borderBottomColor: "#E0E6ED" }}>
                                    <View>
                                        <Image
                                            source={require('../../assets/images/serviceIcon.png')}
                                            style={{ width: 45, height: 45 }}
                                        />
                                    </View>
                                    <View style={{ justifyContent: 'center', paddingLeft: 15, flex: 1 }}>
                                        <Text style={{ color: "#34495E", fontSize: 15, fontFamily: fontFamilyRegular }}>{data.partCategory}</Text>
                                    </View>
                                </View>
                                <View style={{ flexDirection: 'row', padding: 15, borderBottomWidth: 1, borderBottomColor: "#E0E6ED" }}>
                                    <View>
                                        <Image
                                            source={require('../../assets/images/serviceIcon.png')}
                                            style={{ width: 45, height: 45 }}
                                        />
                                    </View>
                                    <View style={{ justifyContent: 'center', paddingLeft: 15, flex: 1 }}>
                                        <Text style={{ color: "#34495E", fontSize: 15, fontFamily: fontFamilyRegular }}>{data.type == 'import' ? data.partsName.map(d => d + ", ") : data.partName}</Text>
                                    </View>
                                    {data.requestType != "Towing service" &&
                                        <TouchableOpacity
                                            style={{ justifyContent: 'center' }}
                                            onPress={() => {
                                                let oldParams = props.route.params
                                                oldParams['orderData'] = data
                                                dispatch(setPartEditable(true, data._id))
                                                props.navigation.navigate("EditImportServiceParts", oldParams)
                                            }}
                                        >
                                            <Text style={{ color: "#276EF1", fontFamily: fontFamilyRegular }}>Change</Text>
                                        </TouchableOpacity>
                                    }
                                </View>
                                <View style={{ flexDirection: 'row', padding: 15, borderBottomWidth: 1, borderBottomColor: "#E0E6ED" }}>
                                    <View>
                                        <Image
                                            source={require('../../assets/images/locationPriceIcon.png')}
                                            style={{ width: 45, height: 45 }}
                                        />
                                    </View>
                                    <View style={{ justifyContent: 'center', paddingLeft: 15 }}>
                                        <Text style={{ color: "#34495E", fontSize: 15, fontFamily: fontFamilyRegular }}>{moment(data.updatedAt).format("DD-MMM-YYYY")}</Text>
                                    </View>
                                </View>
                                <View style={{ flexDirection: 'row', padding: 15, borderBottomWidth: 1, borderBottomColor: "#E0E6ED" }}>
                                    <View>
                                        <Image
                                            source={require('../../assets/images/servicePayment.png')}
                                            style={{ width: 45, height: 45 }}
                                        />
                                    </View>
                                    <View style={{ justifyContent: 'center', paddingLeft: 15, flex: 1 }}>
                                        <Text style={{ color: "#34495E", fontSize: 15, fontFamily: fontFamilyRegular }}>M-Pesa</Text>
                                    </View>
                                    <TouchableOpacity
                                        style={{ justifyContent: 'center' }}
                                        disabled={true}
                                    >
                                        <Text style={{ color: "#E0E6ED", fontFamily: fontFamilyRegular }}>Change</Text>
                                    </TouchableOpacity>
                                </View>

                            </ScrollView>
                            <View style={{ marginBottom: 15 }}>
                                <View style={{ padding: 15 }}>
                                    <Button
                                        name="Confirm part order"
                                        onPress={() => {
                                            _submitRequest()
                                        }}
                                        loading={loader}
                                    />
                                </View>
                            </View>
                        </View>
                    )
                }
            })}
        </View>
    )
}