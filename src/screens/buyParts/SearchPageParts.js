import React from 'react';
import { View, Dimensions, Text, KeyboardAvoidingView, Platform, SafeAreaView, ScrollView, StatusBar, TouchableWithoutFeedback, Image } from 'react-native';
import MapView, { Marker, PROVIDER_GOOGLE, PROVIDER_DEFAULT } from 'react-native-maps'
import { fontFamilyMedium, fontFamilyRegular, fontFamilyLight } from '../../global/globalStyle'
import { TouchableOpacity } from 'react-native-gesture-handler';
import Header from '../../components/Header'
import Button from '../../components/Button'
import Input from '../../components/Input'
import SelectOption from '../../components/SelectOptionNew'
import SelectLocation from '../../components/SelectLocation'
import SelectLocationNew from '../../components/SelectLocationNew'
import navigationReducer from '../../redux/navigation/navigationReducer';
import Geolocation from '@react-native-community/geolocation';
import { useSelector, useDispatch } from 'react-redux';
import { originLatLong } from '../../globalVariable'
import { updateLocationsData, setUserCars, updateRequests, setEditableTowing, setPartCarName, setLocationPart, setCategoryType } from '../../redux'
import Api from '../../api';
import axios from 'axios';
import moment from 'moment'
import AsyncStorage from '@react-native-community/async-storage'
import Toast from 'react-native-simple-toast';
import { useRoute } from '@react-navigation/native';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
import { useFocusEffect } from '@react-navigation/native';
function Garage(props) {
    return (
        <TouchableOpacity style={{
            flexDirection: 'row',
            borderBottomColor: props.selected ? "#276EF1" : '#E0E6ED',
            borderBottomWidth: 1,
            padding: 15,
            backgroundColor: "transparent",
            borderColor: '#276EF1',
            borderWidth: props.selected ? 1 : 0
        }}
            onPress={() => props.onPress(props.id)}

        >
            <View style={{ paddingRight: 20, justifyContent: 'center', alignItems: 'center' }}>
                <Image
                    source={require('../../assets/images/garageNew.png')}
                    style={{ width: 45, height: 45 }}
                />
            </View>
            <View style={{ flex: 1, justifyContent: 'center' }}>
                <Text style={{ fontFamily: fontFamilyRegular, fontSize: 14, color: '#313030' }}>{props.name}</Text>
                <Text style={{ fontFamily: fontFamilyLight, fontSize: 14, color: '#313030' }}>{props.address}</Text>
            </View>
        </TouchableOpacity>
    )
}

export default function SearchPageParts(props) {
    const dispatch = useDispatch()
    const locationsData = useSelector(state => state.location.locationPlaces)
    const partsData = useSelector(state => state.parts)
    const [longitude, setLongitude] = React.useState(partsData.longitude)
    const [latitude, setLatitude] = React.useState(partsData.latitude)
    const [wlongitude, setwLongitude] = React.useState("")
    const [wlatitude, setwLatitude] = React.useState("")
    const [search, setSearch] = React.useState()
    const [lt, setLt] = React.useState()
    const [selectedData, setSelectedData] = React.useState(partsData.locationName)
    const [wselectedData, setwSelectedData] = React.useState("")
    const [defPlace, setDefplace] = React.useState()
    const [deflocation, setdefLocation] = React.useState()
    const [data, setData] = React.useState([])
    const [garages, setGarages] = React.useState('[]')
    const userCars = useSelector(state => state.importCar.userCars)

    const [loader, setLoader] = React.useState(false)
    const [selectedGarage, setSelectedGarage] = React.useState(-1)
    const isEditable = useSelector(state => state.servicedata.isEditing)
    const towingId = useSelector(state => state.servicedata.towingId)
    const partId = "5f734c1ff77c365a6c1106b3";
    const route = useRoute();
    const _getDefaultPlace = async (latitude, longitude) => {
        try {
            axios.post("https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + latitude + "," + longitude + "&radius=1500&key=AIzaSyAB7cS3Wb_fyoN_GObjGhud-3PMNNRmfwc")
                .then((res) => {
                    setData([])
                    if (res.data.results.length != 0) {
                        let places = res.data.results;
                        for (let y in places) {
                            let a = { name: places[y].name + places[y].vicinity, value: places[y].place_id, show: true, long: JSON.stringify(places[y].geometry.location) };
                            data.push(a);
                            setDefplace(a.value)
                        }
                        dispatch(updateLocationsData(data))
                    }
                })
                .catch((error) => {
                    console.log(error)
                })
        }
        catch (err) {
            Toast.show(err)
        }
    }


    const arr = []
    const [location, setLocation] = React.useState('');
    const carMakesRedux = useSelector(state => state.cardata.carMake)
    const myCars = useSelector(state => state.user.carsData)
    const [selectedCarId, setSelectedCarId] = React.useState(partsData.selectedCarId)
    const [selectedCarName, setSelectedCarName] = React.useState(partsData.carName)

    React.useEffect(() => {
        let dta = [];
        for (let i in myCars) {
            let a = { name: myCars[i].make + "[ " + myCars[i].regno + " ]", value: myCars[i]._id, show: true }
            dta.push(a)
        }
        dispatch(setUserCars(dta));
    }, [false])
    const [carname, setcarName] = React.useState(arr.length != 0 ? arr[0].name : '')
    const [carMakes, setCarMakes] = React.useState(arr.length != 0 ? arr[0].value : '');
    const _submitReq = async () => {
        try {
            setLoader(true)
            const value = await AsyncStorage.getItem('token')
            axios.post(Api + "/api/getRescueRequest", {

            }, {
                headers: { 'authorization': value }
            }
            )
                .then((res) => {
                    if (res.data.status == true) {
                        dispatch(updateRequests(res.data.data))
                        setLoader(false)
                    }
                })
                .catch((error) => {
                    console.log(error.message)
                    setLoader(false)
                    Toast.show(JSON.stringify(error))
                })
        }
        catch (err) {
            setLoader(false)
            console.log(err)
        }
    }

    const submitData = async () => {

        let cd = {};
        for (let i in myCars) {
            if (myCars[i]._id == selectedCarId) {
                cd = myCars[i]
            }
        }
        dispatch(setLocationPart(longitude, latitude, selectedData))
        dispatch(setPartCarName(selectedCarName, selectedCarId))
        try {
            let distance = 1000;
            let getTimeandDistance = await axios.get("https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=" + originLatLong.lat + "," + originLatLong.long + "&destinations=" + latitude + "," + longitude + "&key=AIzaSyAB7cS3Wb_fyoN_GObjGhud-3PMNNRmfwc")

            if (getTimeandDistance.data.rows[0].elements[0].status == "OK") {
                distance = parseInt(getTimeandDistance.data.rows[0].elements[0].distance.value / 1000);
            }
            let type = props.route.params.type
            if (selectedCarName == 'Choose your car') {
                Toast.show("Please choose your car.")
            } else if (distance > 100) {
                Toast.show("We are not providing service at your location.")
            } else {
                setLoader(true);
                const value = await AsyncStorage.getItem('token')
                let d = {}
                if (type == 'carParts') {
                    d = {
                        importType: "part",
                        type: "import",
                        carMake: selectedCarName,
                        partsName: props.route.params.selectedPartsArr,
                        importService: partId,
                        startTime: moment().add(30, 'm').toDate(),
                        endTime: moment().add(60, 'm').toDate(),
                        location: {
                            long: longitude,
                            lat: latitude
                        },
                        locationName: selectedData,
                        price: 0
                    }
                    dispatch(setCategoryType("import", "part"))
                } else {
                    let cd = {};
                    for (let i in myCars) {
                        if (myCars[i]._id == selectedCarId) {
                            cd = myCars[i]
                        }
                    }

                    let price = 0;

                    d = {
                        importType: "part",
                        type: "get",
                        carMake: selectedCarId,
                        subCategory: partsData.partSubCategory1,
                        partCategory: partsData.partCategory1,
                        partName: partsData.partName,
                        importService: partId,
                        startTime: moment().add(30, 'm').toDate(),
                        endTime: moment().add(60, 'm').toDate(),
                        location: {
                            long: longitude,
                            lat: latitude
                        },
                        locationName: selectedData,
                        price: price,
                        make: cd.make,
                        year: cd.year,
                        model: cd.model,
                        category: partsData.partCategory1,
                        part: partsData.partName
                    }
                    dispatch(setCategoryType("get", "part"))
                }
                axios.post(Api + "/api/importservice", d, {
                    headers: { 'authorization': value }
                })
                    .then((res) => {
                        setLoader(false);
                        _submitReq()
                        props.navigation.navigate("PartConfirmRequest", { oid: res.data.carAdded._id, id: partId, whereto: "", longitude: longitude, latitude: latitude })
                    })
                    .catch((err) => {
                        setLoader(false);
                        console.log(err)
                    })
            }
        }

        catch (err) {
            setLoader(false)
            console.log(err)
        }
    }
    return (
        <View style={{ flex: 1, backgroundColor: 'white' }} >
            <StatusBar
                backgroundColor="#276EF1"
                barStyle="light-content"
            />
            <View style={{ backgroundColor: "#276EF1", height: 40 }}></View>

            <View style={{ backgroundColor: '#F8F8F8', zIndex: 100 }}>
                <View style={{ flexDirection: 'row' }}>
                    <View>
                        <TouchableOpacity
                            style={{ padding: 15 }}
                            onPress={() => props.navigation.goBack()}
                        >
                            <Image
                                source={require('../../assets/images/Left.png')}
                                style={{ width: 24, height: 24 }}
                            />
                        </TouchableOpacity>

                    </View>
                    <View style={{ flex: 1 }}>
                        <View style={{ padding: 15, marginTop: 5 }}>
                            <Text style={{ fontFamily: fontFamilyRegular }}>Confirm delivery address and car</Text>
                            <View style={{ height: 20 }} />
                            <View style={{ zIndex: 20 }}>
                                <SelectLocationNew
                                    label=""
                                    placeholder={selectedData}
                                    onSelect={(t) => {
                                        setLongitude(t.cords.long)
                                        setLatitude(t.cords.lat);
                                        setSelectedData(t.name)
                                    }}
                                />
                            </View>


                            <View style={{ height: 20 }} />
                            <View style={{ zIndex: 18 }} >
                                <SelectOption
                                    label=""
                                    placeholder={selectedCarName}
                                    getId={true}
                                    selectedId={selectedCarId}
                                    onChangeText={(t) => {
                                        setSelectedCarId(t.value)
                                        setSelectedCarName(t.name)
                                    }}
                                    data={userCars}
                                />
                            </View>
                        </View>
                    </View>
                </View>
            </View>
            <View style={{ height: 15 }} />
            <View style={{ flex: 1, zIndex: 0 }}>
                <ScrollView>

                    <View style={{ zIndex: 0, position: "relative" }}>
                        <TouchableOpacity style={{ flexDirection: 'row', padding: 5, backgroundColor: '#FFFFFF', zIndex: -1 }}
                            onPress={() => props.navigation.navigate('SelectLocationPart', props.route.params)}
                        >
                            <View>
                                <Image
                                    source={require('../../assets/images/mapLocationReturn.png')}
                                    style={{ width: 45, height: 45 }}
                                />
                            </View>
                            <View style={{ justifyContent: "center", paddingLeft: 20 }}>
                                <Text style={{ color: '#276EF1', fontSize: 15, fontFamily: fontFamilyRegular }}>Set location from map</Text>
                            </View>
                        </TouchableOpacity>
                    </View>

                </ScrollView>
            </View>
            <View style={{ padding: 20 }}>
                <Button
                    name="Get car part quote"
                    onPress={() => submitData()}
                    loading={loader}

                />
            </View>
        </View>

    )
}