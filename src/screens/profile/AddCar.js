import React, { useEffect } from 'react';
import { View, Text, StatusBar, SafeAreaView, KeyboardAvoidingView, Platform, Image, TouchableOpacity, Dimensions } from 'react-native'
import Header from '../../components/Header'
import Input from '../../components/Input'
import SelectOption from '../../components/SelectOption'
import Devider from '../../components/Devider'
import Button from '../../components/Button'
import { ScrollView } from 'react-native-gesture-handler';
import axios from 'axios';
import Api from '../../api'
import Toast from 'react-native-simple-toast';
import { useSelector, useDispatch } from 'react-redux';
import {
    getCarMake, getCarModels, getCarEngine, updateUserCarData, updateUserData, setDontKnowCarMakes,
    setCarEngineRedux,
    setCarYearlRedux,
    setCarModelRedux,
    setCarColorRedux
} from '../../redux'
import AsyncStorage from '@react-native-community/async-storage';
import { set } from 'react-native-reanimated';
import { fontFamilyBold } from '../../global/globalStyle';
export default function AddCarProfile(props) {
    const windowWidth = Dimensions.get('window').width;
    const windowHeight = Dimensions.get('window').height;
    const [registrationNumber, setRegistrationNumber] = React.useState(props.route.params.type == 'edit' ? props.route.params.regno : '');
    const [vregistrationNumber, setvRegistrationNumber] = React.useState(false);
    const [chassiNumber, setChassiNumber] = React.useState(props.route.params.chassisno != undefined && props.route.params.chassisno != null ? props.route.params.chassisno : ' ');
    const [vchassiNumber, setvChassiNumber] = React.useState(false);
    const [carMakes, setCarMakes] = React.useState(props.route.params.type == 'edit' ? props.route.params.carmake : 'Select Car')
    const [carModels, setCarModels] = React.useState(props.route.params.type == 'edit' ? props.route.params.modelyear : 'ex: Swift')
    const [carEngines, setCarEngines] = React.useState(props.route.params.type == 'edit' ? props.route.params.enginesize : 'ex: 1000CC')
    const users = useSelector(state => state);
    const [carModel, setCarModel] = React.useState(props.route.params.type == 'edit' ? props.route.params.modelyear : 'Select Model')
    const [carMake, setCarMake] = React.useState(props.route.params.type == 'edit' ? props.route.params.carmake : 'Select Car')
    const [carEngine, setEngine] = React.useState(props.route.params.type == 'edit' ? props.route.params.enginesize : null)
    const [year, setYear] = React.useState("ex. 2007")
    const [loader, setLoader] = React.useState(false)
    const [flag, setFlag] = React.useState(false)
    const [title, setTitle] = React.useState(props.route.params.type == 'edit' ? 'Update Car' : 'Add new car')
    const carModelsRedux = useSelector(state => state.cardata.carModels)
    const carEngineRedux = useSelector(state => state.cardata.carEngine)
    const [loading, setLoading] = React.useState(false);
    const dknowImportMakeCar = useSelector(state => state.importCar.dknowImportMakeCar);
    const dispatch = useDispatch()
    const knowcarModel = useSelector(state => state.importCar.knowcarModel);
    const knwoCarEngine = useSelector(state => state.importCar.knwoCarEngine);
    const allCars = useSelector(state => state.importCar.allCars);
    const [manufactureYear, setManufactureYear] = React.useState(props.route.params.type == 'edit' ? props.route.params.year : "ex. 2007")
    const knowYearofManufacture = useSelector(state => state.importCar.knowYearofManufacture);
    _checkToken = async (value) => {

        if (value != 'false' && value != null && value != undefined) {
            axios.post(Api + "/api/checkToken/", {}, {
                headers: {
                    'authorization': value
                }
            })
                .then(res => {
                    if (res.data.status == true) {

                        let a = res.data.data.user

                        dispatch(updateUserData(a))
                    }
                    else {
                        console.log('Session Expired Login Again')
                    }
                })
                .catch(error => { throw (error) });

        }
    }
    const createSearchableArray = (array) => {
        let arr = [];
        for (let i in array) {
            let a = { name: array[i], value: array[i], show: true }
            arr.push(a);
        }
        return arr;
    }
    React.useEffect(() => {
        setLoading(true);
        axios.post(Api + "/api/car-models", { carMake: carMakes }, {})
            .then((res) => {
                if (res.data.status) {
                    dispatch(setCarModelRedux(createSearchableArray(res.data.models)));
                    if (props.route.params.type != 'edit') {
                        if (!res.data.models.includes(carMakes)) {
                            setCarModels('ex: Swift')
                        }
                    }
                }
                setLoading(false);
            })
            .catch((err) => {
                console.log(err);
                setLoading(false);
            })
        // dispatch(setCarEngineRedux(engine));
        // dispatch(setCarModelRedux(model));


    }, [carMakes])
    React.useEffect(() => {
        setLoading(true);
        axios.post(Api + "/api/engine-capacity", { carMake: carMakes, carModel: carModels }, {})
            .then((res) => {
                if (res.data.status) {
                    dispatch(setCarEngineRedux(createSearchableArray(res.data.engines)));
                    if (props.route.params.type != 'edit') {
                        if (!res.data.engines.includes(carEngines)) {
                            setCarEngines('ex: 1000CC')
                        }
                    }
                }
                setLoading(false);
            })
            .catch((err) => {
                console.log(err);
                setLoading(false);
            })
        // dispatch(setCarEngineRedux(engine));
        // dispatch(setCarModelRedux(model));


    }, [carModels])
    const _getuserData = async (token) => {

        axios.post(Api + "/api/getcars/", {}, {
            headers: {
                'authorization': token
            }
        })
            .then((res) => {

                if (res.data.status == true) {
                    dispatch(updateUserCarData(res.data.data));
                }
            });
    }
    _checkValid = () => {
        if (!vregistrationNumber && carMake == undefined && carModel == undefined && carEngine == undefined) {
            Toast.show('Please Fill all Fileds');
        }
        else if (!vregistrationNumber) {
            Toast.show('Fill Registeration no.')
        }
        else if (carMakes == undefined || carMakes == null || carMakes == 'Select Car') {
            Toast.show('Please Select Car Make')
        }
        else if (carModels == undefined || carModels == null || carModels == 'ex: Swift') {
            Toast.show('Please Select Car Model')
        }
        else if (carEngines == undefined || carEngines == null || carEngines == 'ex: 1000CC') {
            Toast.show('Please Select Car Engine Size')
        }
        else if (manufactureYear == undefined || manufactureYear == null || manufactureYear == 'ex. 2007') {
            Toast.show('Please select the year of manufacture.')
        }
        else {
            if (props.route.params.mno != null && props.route.params.mno != undefined) {
                _addNew(props.route.params.mno, props.route.params.cc)
            }
            else {
                props.navigation.navigate('AddPhoneNumberProfile', { _addNew: _addNew.bind(this), type: props.route.params.type, mno: props.route.params.mno, cc: props.route.params.cc })

            }
        }
    }
    const _storeData = async (token) => {
        try {
            await AsyncStorage.setItem(
                'token', token
            )
        } catch (error) {
            setLoader(false)
            Toast.show('Token Error signup page')
        }
    }
    const _mobile = (mno, cc, token) => {
        try {
            axios.post(Api + "/api/addMobile", {
                "mno": mno,
                "cc": cc
            }, {
                headers: { 'authorization': token }
            }
            )
                .then(res => {
                    if (res.data.status == true) {
                        _storeData(res.data.token)
                        _getuserData(res.data.token)
                        dispatch(updateUserData(res.data.data))
                    }
                    else {
                        // console.log(res.data.exp)
                    }
                })
                .catch(error => {
                    {
                        setLoader(false)
                        throw (error)
                    }
                });
        }
        catch (err) {
            console.log(err)
        }

    }

    _addNew = async (mno, cc, a = 1) => {

        if (props.route.params.type == 'edit') {
            try {
                setLoader(true)
                const value = await AsyncStorage.getItem('token')
                axios.post(Api + "/api/editcar", {
                    id: props.route.params.id,
                    regno: registrationNumber,
                    carmake: carMakes,
                    modelyear: carModels,
                    enginesize: carEngines,
                    chassisno: chassiNumber,
                    year: manufactureYear
                }, {
                    headers: { 'authorization': value }
                }
                )
                    .then((res) => {
                        if (res.data.status == true) {
                            _getuserData(value)
                            Toast.show(res.data.exp)
                            if (a == 1) {
                                props.navigation.goBack();
                            }
                            setLoader(false)
                        }
                        else if (res.data.status == false) {
                            Toast.show(res.data.exp)
                            if (a == 1) {
                                props.navigation.goBack();
                            }
                            setLoader(false)
                        }
                    })
                    .catch((error) => {
                        Toast.show(error)
                        setLoader(false)
                    })
            }
            catch (err) {
                Toast.show(err)
                setLoader(false)
            }
        }
        else {

            try {
                const value = await AsyncStorage.getItem('token')
                if (props.route.params.mno == null || props.route.params.mno == undefined) {
                    _mobile(mno, cc, value)
                }
                setLoader(true)
                axios.post(Api + "/api/addCar", {
                    regno: registrationNumber,
                    carmake: carMakes,
                    modelyear: carModels,
                    enginesize: carEngines,
                    chassisno: chassiNumber,
                    year: manufactureYear
                }, {
                    headers: { 'authorization': value }
                }
                )
                    .then((res) => {
                        if (res.data.status == true) {
                            _getuserData(value)
                            Toast.show(res.data.exp)
                            if (a == 1) {
                                props.navigation.goBack();
                            }

                            setLoader(false)
                        }
                        else if (res.data.status == false) {
                            Toast.show(res.data.exp)
                            if (a == 1) {
                                props.navigation.goBack();
                            }

                            setLoader(false)
                        }
                    })
                    .catch((error) => {
                        Toast.show(error)
                        setLoader(false)
                    })
            }
            catch (err) {
                Toast.show(err)
                setLoader(false)
            }
        }

    }
    React.useEffect(() => {
        if (registrationNumber.length > 4) {
            setvRegistrationNumber(true);
        } else {
            setvRegistrationNumber(false);
        }
        if (chassiNumber.length > 5) {
            setvChassiNumber(true)
        } else {
            setvChassiNumber(false)
        }

    }, [registrationNumber, chassiNumber])
    return (
        <View style={{ flex: 1, backgroundColor: 'white' }}>

            <StatusBar
                barStyle="light-content"
                backgroundColor="#276EF1"
            />
            {props.route.params.type == 'add' &&
                <View
                    style={{ width: windowHeight - 100, height: windowHeight - 100, backgroundColor: "rgba(202,225,252,0.2)", borderRadius: windowHeight, marginLeft: -windowWidth + 100, marginTop: -windowHeight / 5, zIndex: 1, position: "absolute" }}
                />
            }
            {props.route.params.type == 'add' &&
                <View
                    style={{ width: windowWidth - 100, height: windowWidth - 100, backgroundColor: "rgba(244,224,234,0.2)", borderRadius: windowWidth, zIndex: 1, marginTop: -windowWidth / 3, marginLeft: - windowWidth / 3, position: "absolute", top: windowHeight / 1.7 }}
                />
            }
            <View style={{ backgroundColor: "#276EF1", height: 40 }}></View>
            <SafeAreaView style={{ flex: 1, zIndex: 2 }}>
                <View style={{ flexDirection: "row", marginVertical: 15 }}>
                    <TouchableOpacity style={{ width: 100, padding: 10 }}
                        onPress={() => props.navigation.goBack()}
                    >
                        <Image
                            source={require('../../assets/images/Left.png')}
                            style={{ width: 24, height: 24 }}
                        />
                    </TouchableOpacity>
                    <View style={{ flex: 1, justifyContent: "center" }}>
                        <Text style={{ color: "#34495E", textAlign: "center", fontSize: 22, fontFamily: fontFamilyBold }}>{props.route.params.type == 'add' ? "Add New Car" : "Edit Car"}</Text>
                    </View>
                    <View style={{ width: 100 }}>

                    </View>
                </View>
                <KeyboardAvoidingView style={{ flex: 1 }} behavior={Platform.OS == 'ios' ? "padding" : "height"}>
                    <ScrollView style={{ padding: 15 }}>
                        <View style={{ zIndex: 20 }}>
                            <Input
                                label="WHAT IS YOUR CAR REGISTRATION NUMBER?*"
                                placeholder="ex. KSQ0012"
                                value={registrationNumber}
                                onChangeText={(t) => setRegistrationNumber(t)}
                                isValid={vregistrationNumber}
                                err={'Registration no Length Should Be Greater Than 4 Digits.'}
                            />
                            <View style={{ height: 30 }} />
                        </View>
                        {/* dknowImportMakeCar */}
                        <View style={{ zIndex: 19 }}>
                            <SelectOption
                                label="WHAT IS THE MAKE OF YOUR CAR?"
                                placeholder={carMakes}
                                onChangeText={(t) => setCarMakes(t)}
                                returnArray={(t) => {
                                    dispatch(setDontKnowCarMakes(t))
                                }}
                                data={dknowImportMakeCar}
                            />
                            <View style={{ height: 30 }} />
                        </View>
                        <View style={{ zIndex: 18 }}>
                            <SelectOption
                                label="WHAT IS THE MODEL?*"
                                placeholder={carModels}
                                onChangeText={(t) => setCarModels(t)}
                                returnArray={(t) => {
                                    dispatch(setCarModelRedux(t))
                                }}
                                data={knowcarModel}
                            />
                            <View style={{ height: 30 }} />
                        </View>
                        <View style={{ zIndex: 17 }}>

                            <SelectOption
                                label="WHAT IS THE YEAR OF MANUFACTURE?"
                                placeholder={manufactureYear}
                                onChangeText={(t) => setManufactureYear(t)}
                                returnArray={(t) => {
                                    dispatch(setCarYearlRedux(t))
                                }}
                                data={knowYearofManufacture}
                            />
                            <View style={{ height: 30 }} />
                        </View>
                        <View style={{ zIndex: 16 }}>
                            <SelectOption
                                label="WHAT IS THE ENGINE SIZE?*"
                                placeholder={carEngines}
                                onChangeText={(t) => setCarEngines(t)}
                                returnArray={(t) => {
                                    dispatch(setCarEngineRedux(t))
                                }}
                                data={knwoCarEngine}
                            />

                            <View style={{ height: 40 }} />
                        </View>

                        <View style={{ height: 30 }} />
                        <Button
                            name={title}
                            loading={loader}
                            onPress={() => {
                                // _addNew()
                                _checkValid()
                                setFlag(true)
                            }}
                        />
                        <View style={{ height: 120 }} />
                    </ScrollView>
                </KeyboardAvoidingView>
            </SafeAreaView>
        </View>
    )
}