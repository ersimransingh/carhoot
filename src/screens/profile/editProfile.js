import React from 'react';
import { Alert, TouchableOpacity, View, Text, Image, StyleSheet, ScrollView, SafeAreaView } from "react-native"
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { fontFamilyMedium, fontFamilyRegular, fontFamilyLight, fontFamilyBold } from '../../global/globalStyle'
import Button from '../../components/Button'
import SelectOption from '../../components/SelectOption'
import Header from '../../components/Header'
import Slider from "react-native-slider";
import Input from '../../components/Input'
import axios from 'axios';
import { useSelector, useDispatch } from 'react-redux';
import { updateUserData } from '../../redux'
import AsyncStorage from '@react-native-community/async-storage';
import navigationReducer from '../../redux/navigation/navigationReducer';

import Toast from 'react-native-simple-toast';
import ContactInput from '../../components/ContactInput'
const Tab = createBottomTabNavigator();

export default function EditProfile(props) {
    const dispatch = useDispatch()
    let user = useSelector(state => state.user.userData);
    const [name, setName] = React.useState(user.name);
    const [email, setEmail] = React.useState(user.email);
    const [loading, setLoading] = React.useState(false);
    const [vname, setVname] = React.useState(false);
    const [vemail, setVemail] = React.useState(false);
    const [countryCode, setCountryCode] = React.useState(user.cc ? user.cc : '254');
    const [contact, setContact] = React.useState(user.mobile != null ? user.mobile : '');
    const [contactv, setContactv] = React.useState(false);
    const [countryAlpha, setCountryAlpha] = React.useState(user.cc ? user.alpha2 : 'KE')
    React.useEffect(() => {
        if (contact.length > 7 && contact.length <= 13) {
            setContactv(true)
        } else {
            setContactv(false)
        }
    }, [contact])

    const validateEmail = (email) => {
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
    _storeData = async (token) => {
        try {
            await AsyncStorage.setItem(
                'token', token
            )
        } catch (error) {
            setLoading(false)
            Toast.show('Token Error signup page')
        }
    }
    _updateProfile = async (mno, cc, alpha, a = 1,) => {
        if (vname && vemail) {
            try {
                setLoading(true)
                const value = await AsyncStorage.getItem('token')
                axios.post(Api + "/api/editProfile", {
                    "name": name,
                    "email": email.toLowerCase(),
                    "mno": mno,
                    "cc": cc,
                    "alpha2": alpha
                }, {
                    headers: {
                        'authorization': value
                    }
                })
                    .then(res => {
                        if (res.data.status == true) {
                            _storeData(res.data.token)
                            // console.log(res.data.data)
                            dispatch(updateUserData(res.data.data))
                            setLoading(false)
                            if (a == 1) { props.navigation.navigate('Tabs') }
                            Toast.show('Profile Updated Successfuly ')
                        }
                        else {
                            setLoading(false)
                            Toast.show('Error Occured While Updating Details')
                        }
                    })
                    .catch(error => {
                        {
                            setLoading(false)
                            throw (error)
                        }
                    })
            }
            catch {
                Alert.alert('Error Ocuured')
            }
        }
        else if (vname && !vemail) {
            Alert.alert('Email is Badly Formatted')
        }
        else if (!vname && vemail) {
            Alert.alert('Please Enter Name')
        }
        else {
            Alert.alert('Please Enter Name And Email')
        }
    }
    React.useEffect(() => {
        if (name.length > 3) {
            setVname(true);
        } else {
            setVname(false);
        }
        if (validateEmail(email)) {
            setVemail(true)
        } else {
            setVemail(false)
        }
    }, [name, email])
    return (
        <View style={{ backgroundColor: '#FFFFFF', flex: 1 }}>
            <SafeAreaView style={{ flex: 1 }}>
                <Header
                    backButton={true}
                    {...props}
                />
                <ScrollView style={{ padding: 10, marginTop: 10 }}>
                    <Input
                        label="Name"
                        placeholder="ex. Brian"
                        value={name}
                        onChangeText={(t) => setName(t)}
                        isValid={vname}
                        err={'Name Length Should Be Greater Than 3 Digits.'}
                    />


                    <View style={{ height: 30 }} />
                    <ContactInput
                        label="Phone Number"
                        placeholder="Phone Number"
                        value={contact}
                        valueCountryCode={countryCode}
                        onChangeText={(t) => setContact(t)}
                        onChangeCountryCode={(t) => setCountryCode(t)}
                        isValid={contactv}
                        cc2={countryAlpha}
                        onUpdateCountryAlpha={(t) => setCountryAlpha(t)}
                    />
                    <View
                        style={{ height: 30 }}
                    />
                    <Input
                        label="Email"
                        placeholder="ex. john@deo.com"
                        value={email}
                        onChangeText={(t) => setEmail(t)}
                        isValid={vemail}
                        err={'Please Enter Valid Email'}
                    />

                    <View style={{ height: 30 }} />
                    <Button
                        loading={loading}
                        name="Update Profile Details"
                        onPress={() => {
                            if (contact == user.mobile) {
                                _updateProfile(contact, countryCode)
                            }
                            else {
                                if (contactv && countryCode != undefined && countryCode != null) {
                                    props.navigation.navigate('VerificationCodeProfile', { _addNew: _updateProfile.bind(this), cc: countryCode, mno: contact, alpha: countryAlpha })
                                }
                                else {
                                    Toast.show('Please Fill Valid Mobile no')
                                }
                            }


                        }}
                    />
                </ScrollView>
            </SafeAreaView>
        </View>
    )
}



const styles = StyleSheet.create({
    image: {
        width: 50,
        height: 50,
        marginBottom: 15
    },
    text: {
        color: '#363131',
        fontFamily: fontFamilyRegular,
        fontSize: 12,
        textAlign: 'center'
    },
    boxStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFFFFF',
        borderRadius: 5,
        padding: 20,
        flex: 1
    },
    multipleBoxItemMain: {
        flexDirection: 'row', marginTop: 20
    },
    miniBox1: {
        flex: 1,
        marginRight: 10,
        borderRadius: 5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 2,
    },
    miniBox2: {
        flex: 1, marginLeft: 10,
        borderRadius: 5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 2,
    }
})