import React from 'react';
import { TouchableOpacity, View, Text, Image, StyleSheet, ScrollView, SafeAreaView, Alert } from "react-native"
import AsyncStorage from '@react-native-community/async-storage';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { fontFamilyMedium, fontFamilyRegular, fontFamilyLight, fontFamilyBold } from '../../global/globalStyle'
import Button from '../../components/Button'
import SelectOption from '../../components/SelectOption'
import Header from '../../components/Header'
import Slider from "react-native-slider";
import Input from '../../components/Input'
import axios from 'axios';
import Toast from 'react-native-simple-toast';
import { useSelector, useDispatch } from 'react-redux';
import { updateUserData, updateState } from '../../redux'
const Tab = createBottomTabNavigator();

export default function ChangePassword(props) {
    const dispatch = useDispatch()
    const [currentPassword, setCurrentPassword] = React.useState('');
    const [newPassword, setNewPassword] = React.useState('')
    const [repeatPassword, setRepeatPassword] = React.useState('')

    const [vcurrentPassword, setvCurrentPassword] = React.useState(false);
    const [vnewPassword, setvNewPassword] = React.useState(false);
    const [vrepeatPassword, setvRepeatPassword] = React.useState(false);
    const [flag, setFlag] = React.useState(false)
    const user = useSelector(state => state.user.userData);
    const email = (user.email).toLowerCase()
    const [loading, setLoader] = React.useState(false);
    const Verify = async () => {
        try {
            await AsyncStorage.setItem(
                'token', 'false'
            )
        } catch (error) {
            Toast.show('Token Error signup page')
        }
        dispatch(updateState('auth'))
    }
    const passwordChange = async () => {
        try {
            if (vcurrentPassword && vrepeatPassword) {

                setLoader(true)
                if (user.password == currentPassword) {
                    axios.post(Api + "/api/changepassword", {
                        'email': email,
                        'password': newPassword
                    })
                        .then(res => {

                            if (res.data.status == true) {
                                setLoader(false)
                                let aa = user
                                aa['password'] = newPassword
                                // console.log(aa)
                                dispatch(updateUserData(aa))
                                props.navigation.navigate('ProfileTab')
                                Toast.show('Password Updated Successfully')
                                Verify()
                            }
                            else {
                                setLoader(false)
                                props.navigation.navigate('Login')
                                Toast.show('Changing Password Failed')
                            }
                        })
                        .catch(error => {
                            {
                                setLoader(false)
                                throw (error)

                            }
                        });
                }
                else {
                    setLoader(false)
                    Toast.show('Old Password Mismatch')
                }

            }
            else if (!vcurrentPassword || !vrepeatPassword) {
                // Toast.show("Password dosen't match")
            }
            else {
                // Toast.show('Enter New Password')
            }
        }
        catch (err) {
            console.log(err)
            setLoader(false)
            Toast.show('Error Occured')

        }
    }

    React.useEffect(() => {
        if (currentPassword.length > 5) {
            setvCurrentPassword(true);
        } else {
            setvCurrentPassword(false);
        }
        if (newPassword.length > 5) {
            setvNewPassword(true);
        } else {
            setvNewPassword(false);
        }
        if (repeatPassword.length > 5 && repeatPassword == newPassword) {
            setvRepeatPassword(true);
        } else {
            setvRepeatPassword(false);
        }

    }, [currentPassword, newPassword, repeatPassword])
    return (
        <View style={{ backgroundColor: '#FFFFFF', flex: 1 }}>
            <SafeAreaView style={{ flex: 1 }}>
                <Header
                    backButton={true}
                    {...props}
                />
                <ScrollView style={{ padding: 10, marginTop: 10 }}>
                    <Input
                        label="Current Password"
                        placeholder="Password"
                        value={currentPassword}
                        onChangeText={(t) => setCurrentPassword(t)}
                        isValid={vcurrentPassword}
                        secureText={true}
                        err={'Password Should Be of More Than 5 Digits'}
                    />
                    <View
                        style={{ height: 30 }}
                    />
                    <Input
                        label="New Password"
                        placeholder="Password"
                        value={newPassword}
                        onChangeText={(t) => setNewPassword(t)}
                        isValid={vnewPassword}
                        secureText={true}
                        err={'Password Should Be of More Than 5 Digits'}

                    />
                    {/* {newPassword != repeatPassword && flag ? <Text style={{ color: 'red', fontSize: 12 }}>Password Not Matching</Text> : null} */}
                    <View
                        style={{ height: 30 }}
                    />
                    <Input
                        label="Repeat Password"
                        placeholder="Password"
                        value={repeatPassword}
                        onChangeText={(t) => setRepeatPassword(t)}
                        isValid={vrepeatPassword}
                        secureText={true}
                        err={'Password not Matching'}
                    />
                    <View
                        style={{ height: 30 }}
                    />
                    <Button
                        name="Change Password"
                        loading={loading}
                        onPress={() => {
                            passwordChange()
                            setFlag(true)
                        }}
                    />
                </ScrollView>
            </SafeAreaView>
        </View>
    )
}



const styles = StyleSheet.create({
    image: {
        width: 50,
        height: 50,
        marginBottom: 15
    },
    text: {
        color: '#363131',
        fontFamily: fontFamilyRegular,
        fontSize: 12,
        textAlign: 'center'
    },
    boxStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFFFFF',
        borderRadius: 5,
        padding: 20,
        flex: 1
    },
    multipleBoxItemMain: {
        flexDirection: 'row', marginTop: 20
    },
    miniBox1: {
        flex: 1,
        marginRight: 10,
        borderRadius: 5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 2,
    },
    miniBox2: {
        flex: 1, marginLeft: 10,
        borderRadius: 5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 2,
    }
})