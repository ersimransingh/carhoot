import React from 'react';
import { TouchableOpacity, View, Text, Image, StyleSheet, ScrollView, SafeAreaView, StatusBar } from "react-native"
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { fontFamilyMedium, fontFamilyRegular, fontFamilyLight, fontFamilyBold } from '../../global/globalStyle'
import Button from '../../components/Button'
import SelectOption from '../../components/SelectOption'
import Header from '../../components/Header'
import Slider from "react-native-slider";
import Input from '../../components/Input'
import { useSelector, useDispatch } from 'react-redux';
import { updateLocationsData, updateRequests } from '../../redux'
import Api from '../../api';
import axios from 'axios';
import moment from 'moment';
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-community/async-storage';
import { updateUserCarData, updateUserData } from '../../redux'

export default function OrderHistory(props) {
    const dispatch = useDispatch()
    const dd = useSelector(state => state.user.requests)
    const reduxSteps = useSelector(state => state.steps.activeSteps)
    const myCars = useSelector(state => state.user.carsData)
    const arr = {}

    let [kk, setKK] = React.useState({})
    const user = useSelector(state => state.servicedata.data);
    let dataRedux = user.map((x) => {
        kk[x._id] = x.rescueservicetype
    })
    const _submitRequest = async () => {

        try {
            const value = await AsyncStorage.getItem('token')
            axios.post(Api + "/api/getRescueRequest", {}, {
                headers: { 'authorization': value }
            })
                .then((res) => {
                    if (res.data.status == true) {
                        dispatch(updateRequests(res.data.data))
                    }
                })
                .catch((error) => {
                    Toast.show(JSON.stringify(error))
                })
        }
        catch (err) {
            console.log(err)
        }
    }
    React.useEffect(() => {
        _submitRequest()
    }, [reduxSteps])

    const rate = (rating) => {
        let arr = []
        for (let i = 1; i <= 5; i++) {
            if (i <= rating) {
                arr.push(true);
            } else {
                arr.push(false);
            }

        }
        return (
            <View style={{ padding: 10 }}>

                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                    {
                        arr.map((data, index) => {
                            return (
                                <View
                                    key={index}
                                    style={{ paddingRight: 10 }}

                                >
                                    <Image
                                        source={data ? require('../../assets/images/StarDark.png') : require('../../assets/images/StarLight.png')}
                                        style={{ width: 20, height: 20 }}
                                    />
                                </View>
                            )
                        })
                    }
                </View>
            </View>

        )
    }
    return (
        <View style={{ backgroundColor: '#FFFFFF', flex: 1 }}>
            <StatusBar
                backgroundColor="#276EF1"
                barStyle="light-content"
            />
            <View style={{ backgroundColor: "#276EF1", height: 40 }}></View>
            <SafeAreaView style={{ flex: 1 }}>
                <View style={{ flexDirection: "row", marginVertical: 15 }}>
                    <TouchableOpacity style={{ width: 100, padding: 10 }}
                        onPress={() => props.navigation.goBack()}
                    >
                        <Image
                            source={require('../../assets/images/Left.png')}
                            style={{ width: 24, height: 24 }}
                        />
                    </TouchableOpacity>
                    <View style={{ flex: 1, justifyContent: "center" }}>
                        <Text style={{ color: "#34495E", textAlign: "center", fontSize: 22, fontFamily: fontFamilyBold }}>{"Order History"}</Text>
                    </View>
                    <View style={{ width: 100 }}>

                    </View>
                </View>
                <ScrollView style={{ padding: 10, marginTop: 10 }}>
                    {dd.length == 0 &&
                        <Text style={{ textAlign: 'center', fontFamily: fontFamilyLight }}>No order placed yet </Text>
                    }
                    {dd.map((item, key) => {
                        let importType = "";
                        if (item.importType) {
                            importType = item.importType
                        }
                        let itmeId = item.rescuetype ? item.rescuetype : ""
                        let name = item.requestType ? item.requestType : ""
                        let restype = "Rescue"
                        if (item.importType == 'part' || item.importType == 'car') {
                            itmeId = item.importService;
                            restype = "Import"
                        }
                        if (item.importType == 'part') {
                            name = "Buy Parts"
                        } else if (item.importType == 'car') {
                            name = "Import Car"
                        }
                        if (item.status != 'Intialize') {
                            return (
                                <TouchableOpacity key={key} style={{ backgroundColor: '#F8F8F8', padding: 15, borderRadius: 5, borderWidth: 0, borderColor: '#F5F5F5', marginTop: 20 }}
                                    onPress={() => {
                                        if (restype == 'Rescue') {
                                            props.navigation.navigate('ServiceProgress', {
                                                type: item.rescuetype + '  Request',
                                                price: item.price,
                                                id: itmeId,
                                                oid: item._id,
                                                name: name,
                                                rating: item.rating ? item.rating : 0,
                                                requestType: restype,
                                                latitude: item.latitude,
                                                longitude: item.longitude,
                                                fromOrderPage: true
                                            })
                                        } else if (restype == 'Import') {
                                            if (name == "Import Car") {
                                                props.navigation.navigate('ImportCarProgress', {
                                                    type: item.rescuetype + '  Request',
                                                    price: item.price,
                                                    id: itmeId,
                                                    oid: item._id,
                                                    name: name,
                                                    rating: item.rating ? item.rating : 0,
                                                    requestType: restype,
                                                    latitude: item.latitude,
                                                    longitude: item.longitude,
                                                    fromOrderPage: true
                                                })
                                            } else {
                                                props.navigation.navigate('ImportPartProgress', {
                                                    type: item.rescuetype + '  Request',
                                                    price: item.price,
                                                    id: itmeId,
                                                    oid: item._id,
                                                    name: name,
                                                    rating: item.rating ? item.rating : 0,
                                                    requestType: restype,
                                                    latitude: item.location.lat,
                                                    longitude: item.location.long,
                                                    fromOrderPage: true
                                                })
                                            }

                                        }
                                    }}
                                    disabled={item.status == 'Cancelled'}
                                >
                                    <View style={{ flexDirection: 'row', paddingBottom: 10, borderBottomWidth: 1, borderBottomColor: '#EBF0FF' }}>
                                        <View style={{ flex: 1 }}>
                                            <Text style={{ color: '#276EF1', fontSize: 20, fontFamily: fontFamilyBold }}>{name} Request</Text>
                                            {item.status == 'Pending' &&
                                                <Text style={{ fontFamily: fontFamilyRegular, fontSize: 15, color: '#9D9D9D', marginTop: 6 }}>{item.status}</Text>
                                            }
                                            {item.status == 'Completed' &&
                                                <Text style={{ fontFamily: fontFamilyRegular, fontSize: 15, color: '#1BB934', marginTop: 6 }}>{item.status}</Text>
                                            }
                                            {item.status == 'Cancelled' &&
                                                <Text style={{ fontFamily: fontFamilyRegular, fontSize: 15, color: '#FFC432', marginTop: 6 }}>{item.status}</Text>
                                            }

                                        </View>
                                        <View style={{ justifyContent: 'center' }}>
                                            <Image
                                                source={require('../../assets/images/next.png')}
                                                style={{ width: 7, height: 12 }}
                                            />
                                        </View>
                                    </View>
                                    <View style={{ flexDirection: 'row', paddingTop: 10 }}>
                                        <View style={{ flex: 1 }}>
                                            <Text style={{ color: '#34495E', fontSize: 15, fontFamily: fontFamilyRegular }}>KES {item.price}</Text>
                                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                                <View style={{ marginTop: 5 }}>
                                                    <Text style={{ fontFamily: fontFamilyRegular, fontSize: 15, color: '#34495E' }}>{moment(item.createdAt).format("DD/MM/YYYY")}</Text>
                                                </View>
                                                <View style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'flex-end', }}>
                                                    {item.status == 'Completed' &&
                                                        rate(item.rating)
                                                    }
                                                </View>
                                            </View>
                                        </View>

                                    </View>
                                </TouchableOpacity>)
                        }
                    })}
                    {/* History Item End */}
                    <View style={{ height: 40 }} />
                </ScrollView>
            </SafeAreaView>
        </View>
    )
}



const styles = StyleSheet.create({
    image: {
        width: 50,
        height: 50,
        marginBottom: 15
    },
    text: {
        color: '#363131',
        fontFamily: fontFamilyRegular,
        fontSize: 12,
        textAlign: 'center'
    },
    boxStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFFFFF',
        borderRadius: 5,
        padding: 20,
        flex: 1
    },
    multipleBoxItemMain: {
        flexDirection: 'row', marginTop: 20
    },
    miniBox1: {
        flex: 1,
        marginRight: 10,
        borderRadius: 5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 2,
    },
    miniBox2: {
        flex: 1, marginLeft: 10,
        borderRadius: 5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 2,
    }
})