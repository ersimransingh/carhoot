import React from 'react';
import { TouchableOpacity, View, Text, Image, StyleSheet, SafeAreaView } from "react-native"
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { fontFamilyMedium, fontFamilyRegular, fontFamilyLight } from '../../global/globalStyle'
import Dashboard from './dashboard';
import RescueTab from './rescueTab';
import AutoServiceTab from './autoServiceTab';
import ImportCarTab from './importCarTab';
import ImportServiceParts from './importServiceParts';
import ProfileTab from './profileTab';
import CarShop from './CarShop'
const Tab = createBottomTabNavigator();
import { useSelector, useDispatch } from 'react-redux'
import { getCarMake, setDontKnowCarMakes, setCarYearlRedux, setPartCategoryRedux } from '../../redux'
import SelectOption from '../../components/SelectOptionNew'
import Api from '../../api'
import axios from 'axios'
import Toast from 'react-native-simple-toast'
function MyTabBar({ state, descriptors, navigation }) {
    const dispatch = useDispatch();
    const myCars = useSelector(state => state.user.carsData)
    const dd = useSelector(state => state.user.userData);
    const focusedOptions = descriptors[state.routes[state.index].key].options;
    const [activeIndex, setActiveIndex] = React.useState(null);
    if (focusedOptions.tabBarVisible === false) {
        return null;
    }
    const createSearchableArray = (array) => {
        let arr = [];
        for (let i in array) {
            let a = { name: array[i], value: array[i], show: true }
            arr.push(a);
        }
        return arr;
    }
    const getPartCategories = async () => {
        try {
            axios.get(Api + "/api/part-category")
                .then((res) => {
                    let searchable = createSearchableArray(res.data.Category)
                    dispatch(setPartCategoryRedux(searchable));
                })
                .catch((error) => {
                    Toast.show(error)
                })
        } catch (err) {
            console.log(err);
        }
    }
    const _getMake = async () => {
        try {
            axios.get(Api + "/api/car-makes")
                .then((res) => {
                    let searchable = createSearchableArray(res.data.cars)
                    dispatch(setDontKnowCarMakes(searchable));
                })
                .catch((error) => {
                    Toast.show(error)
                })
        }
        catch (err) {
            console.log(err.message)
            Toast.show(err)
        }
    }
    React.useEffect(() => {
        let years = [];
        for (let i = 2020; i >= 1990; i--) {
            years.push(i + '');
        }
        dispatch(setCarYearlRedux(createSearchableArray(years)))

        _getMake();
        getPartCategories();
    }, [false])
    React.useEffect(() => {

        setActiveIndex(state.index)
    }, [state.index])

    return (
        <View style={{ backgroundColor: 'white' }}>
            <SafeAreaView style={{
                flexDirection: 'row',
                backgroundColor: '#FFFFFF',
                paddingVertical: 8,
                borderTopLeftRadius: 0,
                borderTopRightRadius: 0,
                shadowColor: "#000",
                shadowOffset: {
                    width: 0,
                    height: 2,
                },
                shadowOpacity: 0.23,
                shadowRadius: 2.62,

                elevation: 5,
            }}>
                <TouchableOpacity style={styles.tabBox}
                    onPress={() => navigation.navigate('RescueTab')}
                >
                    <View style={{ height: 38 }}>
                        <Image
                            source={activeIndex == 0 ? require('../../assets/images/tabs/tabIcon3active.png') : require('../../assets/images/tabs/tabIcon3.png')}
                            style={[styles.tabImg, { width: 31, height: 34 }]}
                        />
                    </View>
                    <View>
                        <Text style={[styles.tabText, { color: activeIndex == 0 ? '#276EF1' : '#9098B1' }]}>Rescue</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={styles.tabBox}
                    onPress={() => navigation.navigate('AutoServiceTab')}
                >
                    <View style={{ height: 38 }}>
                        <Image
                            source={activeIndex == 2 ? require('../../assets/images/tabs/tabIcon4active.png') : require('../../assets/images/tabs/tabIcon4.png')}
                            style={[styles.tabImg, { width: 31, height: 34 }]}
                        />
                    </View>
                    <View>
                        <Text style={[styles.tabText, { color: activeIndex == 2 ? '#276EF1' : '#9098B1' }]}>Auto Service</Text>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity style={styles.tabBox}
                    onPress={() => navigation.navigate('CarShop')}
                >
                    <View style={{ height: 38 }}>
                        <Image
                            source={activeIndex == 4 ? require('../../assets/images/tabs/tabIcon1active.png') : require('../../assets/images/tabs/tabIcon1.png')}
                            style={[styles.tabImg, { width: 31, height: 34 }]}
                        />
                    </View>
                    <View>
                        <Text style={[styles.tabText, { color: activeIndex == 4 ? '#276EF1' : '#9098B1' }]}>Auto Shop</Text>
                    </View>

                </TouchableOpacity>

                <TouchableOpacity style={styles.tabBox}
                    onPress={() => navigation.navigate('ProfileTab')}
                >
                    <View style={{ height: 38 }}>
                        <Image
                            source={activeIndex == 3 ? require('../../assets/images/tabs/tabIcon5active.png') : require('../../assets/images/tabs/tabIcon5.png')}
                            style={[styles.tabImg, { width: 31, height: 34 }]}
                        />
                    </View>
                    <View>
                        <Text style={[styles.tabText, { color: activeIndex == 3 ? '#276EF1' : '#9098B1' }]}>Profile</Text>
                    </View>

                </TouchableOpacity>

            </SafeAreaView>
        </View>
    );
}
function Check() {
    return (
        // <View>
        <SelectOption
            label=""
            placeholder={"aaaa"}
            getId={true}
            selectedId={"gg"}
            onChangeText={(t) => {
                alert('AA GEYA')
                console.log(t)
            }}
            data={[
                { name: 'james', value: "james" },
                { name: 'james', value: "james" },
                { name: 'james', value: "james" },
                { name: 'james', value: "james" },

            ]}
        />
        // </View>
    )
}
export default function Tabs(props) {
    return (
        <Tab.Navigator tabBar={props => <MyTabBar {...props} />}>

            <Tab.Screen name="RescueTab" component={RescueTab} {...props} />
            <Tab.Screen name="Dashboard" component={Dashboard} {...props} />
            <Tab.Screen name="AutoServiceTab" component={AutoServiceTab} {...props} />

            <Tab.Screen name="ProfileTab" component={ProfileTab} {...props} />
            <Tab.Screen name="CarShop" component={CarShop} {...props} />
        </Tab.Navigator>
    )
}



const styles = StyleSheet.create({
    tabText: {
        fontSize: 10, color: '#9098B1', fontFamily: fontFamilyRegular, textAlign: 'center'
    },
    tabImg: {
        width: 25, height: 25
    },
    tabBox: {
        flex: 1, paddingVertical: 7, alignItems: 'center',
    }
})