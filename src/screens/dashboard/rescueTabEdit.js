import React, { useEffect } from 'react';
import { Alert, TouchableOpacity, View, Text, Image, StyleSheet, ScrollView, SafeAreaView, TextInput, ActivityIndicator, Modal, StatusBar, Linking } from "react-native"
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { fontFamilyMedium, fontFamilyRegular, fontFamilyLight, fontFamilyBold } from '../../global/globalStyle'
import Dashboard from './dashboard';
import Header from '../../components/Header'

import Toast from 'react-native-simple-toast';
import { useDispatch } from 'react-redux';
const Tab = createBottomTabNavigator();
import axios from 'axios';
import moment from 'moment';
import AsyncStorage from '@react-native-community/async-storage';
import { updateUserCarData, updateUserData, updateRequests } from '../../redux'
import { useSelector } from 'react-redux';
import Api from '../../api';
export default function RescueTabEdit(props) {
    const dispatch = useDispatch()
    const [lastindex, setlastIndex] = React.useState(0)
    const [loading, setLoading] = React.useState(true)
    const [imageLoading, setImageLoading] = React.useState(false);
    const rescueData = useSelector(state => state.servicedata.data)
    const dd = useSelector(state => state.user.userData);
    const [totalImgs, setTotalImgs] = React.useState(0);
    const user = useSelector(state => state.user.userData.mobile);
    const myCars = useSelector(state => state.user.carsData)
    const [search, setSearch] = React.useState('')
    useEffect(() => {

        const length = rescueData.length
        if (length == 1) {
            setlastIndex(length - 1)
        } else if (length % 2 != 0) {
            setlastIndex(length - 1)
        }
    })
    _storeData = async (token) => {
        try {
            await AsyncStorage.setItem(
                'token', token
            )
        } catch (error) {
            setLoading(false)
            Toast.show('Token Error signup page')
        }
    }
    _addNew = async (mno, cc, alpha, a = 1) => {
        try {
            setLoading(true)
            const value = await AsyncStorage.getItem('token')
            axios.post(Api + "/api/editmobile", {
                "mno": mno,
                "cc": cc,
                "alpha2": alpha
            }, {
                headers: {
                    'authorization': value
                }
            }

            )
                .then(res => {

                    if (res.data.status == true) {
                        _storeData(res.data.token)

                        dispatch(updateUserData(res.data.data))
                        setLoading(false)
                        if (a == 1) { props.navigation.navigate('Tabs') }
                        Toast.show('Profile Updated Successfuly ')
                    }
                    else {
                        setLoading(false)
                        Toast.show('Error Occured While Updating Details')
                    }
                })
                .catch(error => {
                    {
                        setLoading(false)
                        throw (error)
                    }
                })
        }
        catch (err) {
            Alert.alert('Error Ocuured', err.message)
        }
    }
    React.useEffect(() => {
        setTimeout(() => {
            setLoading(false)
        }, 2000)

    }, [false])
    const _UpdateAllData = async () => {
        try {
            const value = await AsyncStorage.getItem('token')
            axios.post(Api + "/api/getRescueRequest", {}, {
                headers: { 'authorization': value }
            })
                .then((res) => {
                    if (res.data.status == true) {
                        dispatch(updateRequests(res.data.data))
                        props.navigation.goBack();
                    }
                })
                .catch((error) => {
                    console.log(error)
                })
        }
        catch (err) {
            console.log(err)
        }
    }
    const changeService = async (id, price) => {
        try {
            const value = await AsyncStorage.getItem('token')
            let orderData = props.route.params.orderData;
            let whereTo = "";

            let wtLat = "";
            let wtLong = "";
            if (props.route.params.whereto != '') {
                let wt = JSON.parse(props.route.params.whereto);
                let pla = JSON.parse(orderData.placeid);
                wtLat = wt.lat
                wtLong = wt.long
                let a = { "from": selectedData, "to": pla.to }

            }
            axios.post(Api + "/api/editRescueRequest", {
                "id": orderData._id,
                "type": id,
                "longitude": orderData.longitude,
                "latitude": orderData.latitude,
                "placeid": orderData.placeid,
                "wheretoLat": wtLat,
                "wheretoLang": wtLong,
                "price": price,
                "car": orderData.carmake,
                "serviceType": "Rescue",
                "requestType": id,
                "startTime": moment().add(30, 'm').toDate(),
                "endTime": moment().add(60, 'm').toDate()
            }, {
                headers: { 'authorization': value }
            })
                .then((data) => {
                    _UpdateAllData()
                })
                .catch((err) => {
                    console.log('err', err)
                })
        } catch (err) {
            console.log(err)
        }
    }
    return (
        <View style={{ flex: 1, backgroundColor: 'white' }}>
            <StatusBar
                backgroundColor="#276EF1"
                barStyle="light-content"
            />
            <View style={{ backgroundColor: "#276EF1", height: 40 }}></View>
            <View style={{ backgroundColor: "#F8F8F8", borderBottomLeftRadius: 10, borderBottomRightRadius: 10 }}>
                <View style={{ flexDirection: 'row', padding: 20 }}>
                    <View style={{ flex: 1, justifyContent: 'center' }} >
                        <Image
                            source={require("../../assets/images/blue_logo.png")}
                            style={{ width: 100, height: 19 }}
                        />
                    </View>
                    <View style={{ alignItems: 'flex-end' }}>
                        <View style={{ flexDirection: 'row' }}>
                            <TouchableOpacity style={{ marginRight: 10 }}
                                onPress={() => Linking.openURL("tel:+254720039039")}
                            >
                                <Image
                                    source={require('../../assets/images/phone.png')}
                                    style={{ width: 35, height: 35 }}
                                />
                            </TouchableOpacity>
                            <TouchableOpacity style={{ marginRight: 0 }}
                                onPress={() => {
                                    let url = "whatsapp://send?text=Hello There" +
                                        "&phone=254720039039"
                                    Linking.openURL(url)
                                        .then(() => {
                                            console.log('whatsappopen')
                                        })
                                        .catch((err) => {
                                            console.log(err);
                                        })
                                }}
                            >
                                <Image
                                    source={require('../../assets/images/message.png')}
                                    style={{ width: 35, height: 35 }}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <View style={{ paddingHorizontal: 20, paddingBottom: 20 }}>
                    <View style={{ backgroundColor: 'white', flexDirection: 'row', borderRadius: 5, borderColor: "#276EF1", borderWidth: 1 }}>
                        <View style={{ flex: 1 }}>
                            <TextInput
                                placeholder="Find Rescue Services..."
                                placeholderTextColor="#B2BAC4"
                                style={{ padding: 15, color: "#000000" }}
                                onChangeText={(t) => setSearch(t)}

                            />
                        </View>
                        <View style={{ justifyContent: 'center', alignItems: 'center', marginRight: 15 }}>
                            <Image
                                source={require('../../assets/images/search.png')}
                                style={{ width: 16, height: 16 }}
                            />
                        </View>
                    </View>
                </View>
            </View>
            <SafeAreaView style={{ flex: 1 }}>
                {/* <Header
                    backButton={false}
                    {...props}
                /> */}
                <Modal
                    animationType="none"
                    transparent={true}
                    // visible={false}
                    visible={totalImgs < 6}

                >
                    <View style={{ flex: 1, backgroundColor: '#276EF1', justifyContent: 'center', alignItems: 'center', }}>
                        <ActivityIndicator
                            size="large" color="#FFFFFF"
                        />
                    </View>
                </Modal>
                <ScrollView style={{ padding: 10, marginTop: 10 }}>


                    <View style={{
                        marginTop: 10, flexWrap: "wrap", flexDirection: 'row', justifyContent: lastindex == 0 || lastindex % 2 == 0 ? 'center' : 'flex-start'
                    }}>
                        {
                            rescueData.map((item, key) => {
                                if (item.rescueservicetype != 'Towing service') {
                                    let smallName = item.rescueservicetype.toLowerCase();
                                    if (smallName.indexOf(search.toLowerCase()) > -1) {
                                        return (
                                            <View style={{
                                                width: '48%',
                                                margin: 2,
                                            }} key={key}>
                                                <TouchableOpacity
                                                    style={styles.boxStyle}
                                                    onPress={() => {
                                                        changeService(item._id, item.price);

                                                    }}
                                                >

                                                    <>
                                                        <Image
                                                            source={{ uri: item.icon }}
                                                            style={styles.image}
                                                            resizeMode="stretch"
                                                            onLoadEnd={() => {
                                                                setTotalImgs(totalImgs + 1)
                                                            }}

                                                        />

                                                        <Text style={styles.text}>{item.rescueservicetype}</Text>
                                                    </>

                                                    {imageLoading &&
                                                        <View style={{ width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center', backgroundColor: '#ffffff', position: 'absolute', zIndex: 9909090 }}>
                                                            <ActivityIndicator />
                                                        </View>
                                                    }

                                                </TouchableOpacity>
                                            </View>
                                        )
                                    }
                                }


                            })
                        }
                    </View>


                    <View style={{ height: 30 }} />
                </ScrollView>
                {totalImgs < 6 &&
                    <View style={{ top: 0, left: 0, right: 0, bottom: 0, backgroundColor: '#276EF1', justifyContent: 'center', alignItems: 'center', position: 'absolute', zIndex: 12123123123123123 }}>
                    </View>
                }
            </SafeAreaView>
        </View>
    )

}



const styles = StyleSheet.create({
    image: {
        width: 45,
        height: 45,
        marginBottom: 15
    },
    text: {
        color: '#34495E',
        fontFamily: fontFamilyRegular,
        fontSize: 14,
        textAlign: 'center'
    },
    boxStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F8F8F8',
        paddingVertical: 20,
        borderRadius: 10,
        margin: 10

    },
    multipleBoxItemMain: {
        marginTop: 10, flexWrap: "wrap", flexDirection: 'row'
    },
    miniBox1: {
        width: '47%',
        margin: 5
    },
    miniBox2: {
        // flex: 1, marginLeft: 5
    }
})