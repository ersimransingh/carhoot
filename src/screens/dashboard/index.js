import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { TouchableOpacity, View, Text } from "react-native"
import Tabs from './Tabs';
import RescueIndex from '../rescue/index';
import RescueServiceLocation from '../rescue/serviceLocation';
import ConfirmServicePrice from '../rescue/confirmServicePrice';
import ServiceProgress from '../rescue/serviceProgress';

import RatingforService from '../rescue/ratingforService';
import AutoServiceRequest from '../autoService/autoServiceRequest';
import AutoServiceDetails from '../autoService/autoServiceDetails';
import ImportCarConfirmRequest from '../importCar/importCarConfirmRequest';
import PartConfirmRequest from '../buyParts/partConfirmRequest';
import EditProfile from '../profile/editProfile';
import OrderHistory from '../profile/orderHistory';
import ChangePassword from '../profile/changePassword';
import AddCarProfile from '../profile/AddCar';
import VerificationCodeProfile from '../profile/VerificationCode';
import AddPhoneNumberProfile from '../profile/AddPhoneNumber';
import SelectLocation from '../rescue/SelectLocation'
import SelectDestinationLocation from '../rescue/SelectDestinationLocation'
import RescueTabEdit from '../../screens/dashboard/rescueTabEdit'
import ServiceCategory from '../../screens/autoService/ServiceCategory'
import ImportCarTab from '../../screens/dashboard/importCarTab';
import ImportServiceParts from '../../screens/dashboard/importServiceParts'
import ImportCarProgress from '../importCar/importCarProgress'
import SelectLocationPart from '../buyParts/SelectLocationPart'
import ImportPartProgress from '../buyParts/importPartProgress'
import SearchPageParts from '../buyParts/SearchPageParts';
import EditImportServiceParts from '../dashboard/EditImportServicePart'
const Stack = createStackNavigator();

export default function DashboardIndex(props) {
    return (
        <Stack.Navigator
            screenOptions={{
                headerShown: false
            }}
        >
            <Stack.Screen name="Tabs" component={Tabs} />
            <Stack.Screen name="RescueServiceLocation" component={RescueServiceLocation} />
            <Stack.Screen name="ConfirmServicePrice" component={ConfirmServicePrice}
                options={{
                    gestureEnabled: false
                }}
            />
            <Stack.Screen name="ServiceProgress" component={ServiceProgress}
                options={{
                    gestureEnabled: false
                }}
            />
            <Stack.Screen name="ServiceCategory" component={ServiceCategory} />
            <Stack.Screen name="AutoServiceDetails" component={AutoServiceDetails} />
            <Stack.Screen name="AutoServiceRequest" component={AutoServiceRequest} />

            <Stack.Screen name="RatingforService" component={RatingforService} />
            <Stack.Screen name="ImportCarConfirmRequest" component={ImportCarConfirmRequest} />
            <Stack.Screen name="PartConfirmRequest" component={PartConfirmRequest} />
            <Stack.Screen name="OrderHistory" component={OrderHistory} />
            <Stack.Screen name="ChangePassword" component={ChangePassword} />
            <Stack.Screen name="EditProfile" component={EditProfile} />
            <Stack.Screen name="AddCarProfile" component={AddCarProfile} />
            <Stack.Screen name="VerificationCodeProfile" component={VerificationCodeProfile} />
            <Stack.Screen name="AddPhoneNumberProfile" component={AddPhoneNumberProfile} />
            <Stack.Screen name="SelectLocation" component={SelectLocation} />
            <Stack.Screen name="SelectLocationEdit" component={SelectLocation} />
            <Stack.Screen name="RescueTabEdit" component={RescueTabEdit} />
            <Stack.Screen name="SelectDestinationLocation" component={SelectDestinationLocation} />
            <Stack.Screen name="SelectDestinationLocationEdit" component={SelectDestinationLocation} />
            <Stack.Screen name="SelectLocationPart" component={SelectLocationPart} />
            <Stack.Screen name="SelectLocationPartEdit" component={SelectLocationPart} />
            <Stack.Screen name="ImportPartProgress" component={ImportPartProgress} />
            <Stack.Screen name="SearchPageParts" component={SearchPageParts} />
            <Stack.Screen name="EditImportServiceParts" component={EditImportServiceParts} />
            <Stack.Screen name="ImportCarTab" component={ImportCarTab}

                options={{
                    gestureEnabled: false
                }}
            />
            <Stack.Screen name="ImportCarProgress" component={ImportCarProgress}
                options={{
                    gestureEnabled: false
                }}
            />
            <Stack.Screen name="ImportServiceParts" component={ImportServiceParts} />

        </Stack.Navigator>
    )
}