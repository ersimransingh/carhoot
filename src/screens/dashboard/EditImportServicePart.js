import React from 'react';
import { TouchableOpacity, View, Text, Image, StyleSheet, ScrollView, SafeAreaView, StatusBar, Dimensions } from "react-native"
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { fontFamilyMedium, fontFamilyRegular, fontFamilyLight, fontFamilyBold } from '../../global/globalStyle'
import Button from '../../components/Button'
import SelectOption from '../../components/SelectOption'
import MultipleRadio from '../../components/MultipleRadio'
import Header from '../../components/Header'
import Slider from "react-native-slider";

import Toast from 'react-native-simple-toast'
import { useSelector, useDispatch } from 'react-redux'
import { updateLocationsData, setUserCars, updateRequests } from '../../redux'
import Api from '../../api';
import axios from 'axios';
import moment from 'moment'
import AsyncStorage from '@react-native-community/async-storage'
import {
    setCarPartsRedux, setPartCategoryRedux, setPartNamesRedux, setServicePartsRedux, updateServicePart, setPartSubCategoryRedux,
    setParCategory,
    setPartSubCategory,
    setPartName
} from '../../redux'

const Tab = createBottomTabNavigator();
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
export default function EditImportServiceParts(props) {
    const partId = "5f734c1ff77c365a6c1106b3";
    const dispatch = useDispatch();
    const partsRedux = useSelector(state => state.importParts.serviceParts);
    const partsData = useSelector(state => state.parts)
    const partsReduxNew = useSelector(state => state.importParts.partsCategories);
    const partsCategories = useSelector(state => state.importParts.partsCategory);
    const partsCategoriesSub = useSelector(state => state.importParts.partsSubCategory);
    const partsName = useSelector(state => state.importParts.partsName);
    const [parttype, setPartType] = React.useState(partsData.requestType)
    const allCars = useSelector(state => state.importCar.allCars);
    const [partc, setPartc] = React.useState(partsData.partCategory1)
    const [partcval, setPartcval] = React.useState(partsData.partCategoryId1)
    const [partcsub, setPartcsub] = React.useState(partsData.partSubCategory1)
    const [partcsubval, setPartcsubval] = React.useState(partsData.partSubCategoryId1)
    const [partN, setPartN] = React.useState(partsData.partName)
    const [loading, setLoading] = React.useState(false);
    const [showError, setShowError] = React.useState(false);
    const userCars = useSelector(state => state.importCar.userCars)

    const createSearchableArray = (array) => {
        let arr1 = [];
        for (let i in array) {
            let a = { name: array[i], value: array[i], show: true }
            arr1.push(a);
        }
        return arr1;
    }
    const arr = []
    const carMakesRedux = useSelector(state => state.cardata.carMake)
    const myCars = useSelector(state => state.user.carsData)
    const [selectedCarId, setSelectedCarId] = React.useState(myCars[0]._id)
    const [selectedCarName, setSelectedCarName] = React.useState(myCars[0].carmake + "[ " + myCars[0].regno + " ]")

    const [carname, setcarName] = React.useState(arr.length != 0 ? arr[0].name : '')
    const [carMakes, setCarMakes] = React.useState(arr.length != 0 ? arr[0].value : '');
    React.useEffect(() => {
        let names = []
        for (let i in partsCategoriesSub) {
            if (partsCategoriesSub[i].value == partcsubval) {
                names = partsCategoriesSub[i].partnames.split(",");
            }
        }
        dispatch(setPartNamesRedux(createSearchableArray(names)))
    }, [partcsub, partcsubval])
    React.useEffect(() => {
        let subCat = [];
        for (let y in partsReduxNew) {
            if (partsReduxNew[y]._id == partcval) {

                let sub = partsReduxNew[y].subCategory
                for (let u in sub) {
                    let a = { name: sub[u].partCategory, value: sub[u]._id, show: true, partnames: sub[u].partName };
                    subCat.push(a);
                }
            }
        }
        dispatch(setPartSubCategoryRedux(subCat))
    }, [partc, partcval])


    const onChangeservice = (index) => {
        dispatch(updateServicePart(index, partsRedux[index].status ? false : true))
        // let d = [...parts];
        // d[index].status = d[index].status ? false : true
        // setParts(d)
    }
    const _submitRequest = () => {

        let partItems = [];
        for (let i in partsRedux) {
            if (partsRedux[i].status) {
                partItems.push(partsRedux[i].name);
            }
        }
        if (parttype == 'service') {
            if (partc == 'eg : engine') {

                setShowError(true);
            } else if (partN == "eg : Crankshaft seal") {
                setShowError(true);

            } else if (partcsub == "eg : Gasket and Seal") {
                setShowError(true);
            } else {
                setShowError(false);
                dispatch(setParCategory(partc, partcval))
                dispatch(setPartSubCategory(partcsub, partcsubval))
                dispatch(setPartName(partN))
                props.navigation.navigate("SelectLocationPart", { type: "spareParts" })
            }
        } else {
            if (partItems.length == 0) {
                Toast.show("Please select atleast one service part.")
                setShowError(true);
            } else {
                setShowError(false);
                props.navigation.navigate("SelectLocationPart", { type: "carParts", selectedPartsArr: partItems })
            }
        }
    }
    const _submitReq = async () => {
        try {
            setLoading(true)
            const value = await AsyncStorage.getItem('token')
            axios.post(Api + "/api/getRescueRequest", {

            }, {
                headers: { 'authorization': value }
            }
            )
                .then((res) => {
                    if (res.data.status == true) {
                        dispatch(updateRequests(res.data.data))
                        setLoading(false)
                    }
                })
                .catch((error) => {
                    console.log(error.message)
                    setLoading(false)
                    Toast.show(JSON.stringify(error))
                })
        }
        catch (err) {
            setLoading(false)
            console.log(err)
        }
    }
    const _submitRequestOnline = async () => {
        let partItems = [];
        for (let i in partsRedux) {
            if (partsRedux[i].status) {
                partItems.push(partsRedux[i].name);
            }
        }
        setLoading(true)
        const value = await AsyncStorage.getItem('token')
        let d = {
            importType: "part",
            type: "get",
            carMake: selectedCarId,
            partCategory: partc,
            partName: partN,
            importService: partId
        }
        if (parttype == 'car') {
            d = {
                importType: "part",
                type: "import",
                carMake: selectedCarId,
                partsName: partItems,
                importService: partId
            }
        }


        axios.post(Api + "/api/importservice", d, {
            headers: { 'authorization': value }
        })
            .then((res) => {
                let a = {
                    parttype: parttype,
                    carMakes: selectedCarId,
                    carname: selectedCarName,
                    partCategory: partc,
                    partname: partN,
                    partItems: partItems,
                    oid: res.data.carAdded._id,
                }
                _submitReq()
                props.navigation.navigate('PartConfirmRequest', a)
                setLoading(false)
            })
            .catch((err) => {
                console.log(err)
                setLoading(false)
            })
    }

    return (
        <View style={{ backgroundColor: '#FFFFFF', flex: 1 }}>
            <StatusBar
                barStyle="light-content"
                backgroundColor="#276EF1"
            />

            <View
                style={styles.backgroundBox1}
            />
            <View
                style={styles.backgroundBox2}
            />
            <View style={{ backgroundColor: "#276EF1", height: 40, zIndex: 2 }}></View>
            <SafeAreaView style={{ flex: 1, zIndex: 2 }}>
                <View style={{ flexDirection: "row", paddingTop: 15 }}>
                    <TouchableOpacity style={{ padding: 10, width: 80 }}
                        onPress={() => props.navigation.goBack()}
                    >
                        <Image
                            source={require('../../assets/images/Left.png')}
                            style={{ width: 24, height: 24 }}
                        />
                    </TouchableOpacity>
                    <View style={{ flex: 1, justifyContent: "center" }}>
                        <Text style={{ color: "#34495E", textAlign: "center", fontSize: 22, fontFamily: fontFamilyBold }}>
                            {props.route.params.title}
                        </Text>
                    </View>
                    <View style={{ width: 80 }}>

                    </View>
                </View>
                <ScrollView style={{ padding: 10, marginTop: 10 }}>
                    <View>
                        <Text style={{ fontSize: 15, fontFamily: fontFamilyRegular, color: '#34495E' }}>{props.route.params.description}</Text>
                    </View>
                    <View
                        style={{ padding: 20 }}
                    >
                    </View>
                    {parttype == 'get' &&
                        <View>
                            <View style={{ height: 30 }} />
                            <SelectOption
                                getId={true}
                                label="What is the part category?"
                                placeholder={partc}
                                onChangeText={(t) => {
                                    setPartc(t.name)
                                    setPartcval(t.value)
                                }}
                                returnArray={(t) => {
                                    dispatch(setPartCategoryRedux(t))
                                }}
                                data={partsCategories}
                            />
                            {showError &&
                                <Text style={{ color: 'red', height: partc == 'eg : engine' ? 'auto' : 0 }}>{partc == 'eg : engine' ? "Please Select Category." : ""}</Text>
                            }
                            <View style={{ height: 30 }} />
                            <SelectOption
                                label="What is the sub-category?"
                                getId={true}
                                placeholder={partcsub}
                                onChangeText={(t) => {
                                    setPartcsubval(t.value)
                                    setPartcsub(t.name)
                                }}
                                returnArray={(t) => {
                                    dispatch(setPartSubCategoryRedux(t))
                                }}
                                data={partsCategoriesSub}
                            />
                            {showError &&
                                <Text style={{ color: 'red', height: partcsub == 'eg : Gasket and Seal' ? 'auto' : 0 }}>{partcsub == 'eg : Gasket and Seal' ? "Please Select Category." : ""}</Text>
                            }
                            <View style={{ height: 30 }} />
                            <SelectOption
                                label="WHAT IS THE PART NAME?"
                                placeholder={partN}
                                onChangeText={(t) => setPartN(t)}
                                returnArray={(t) => {
                                    dispatch(setPartNamesRedux(t))
                                }}
                                data={partsName}
                            />
                            {showError &&
                                <Text style={{ color: 'red', height: partN == 'eg : Crankshaft seal' ? 'auto' : 0 }}>{partN == 'eg : Crankshaft seal' ? "Please Select Part Name." : ""}</Text>
                            }
                            <View style={{ height: 30 }} />
                            <Button
                                name="Request for a Quote"
                                onPress={() => _submitRequest()}
                            />
                            <View style={{ height: 30 }} />
                        </View>
                    }
                    {parttype == 'import' &&
                        <View>
                            <View style={{ height: 30 }} />
                            <MultipleRadio
                                label="WHAT SERVICE PART DO YOU NEED?*"
                                data={partsRedux}
                                onChange={(t) => onChangeservice(t)}
                            />
                            {showError &&
                                <Text style={{ color: 'red', height: partsRedux.length == 0 ? 'auto' : 0 }}>{partsRedux.length == 0 ? "Please Select Part Name." : ""}</Text>
                            }
                            <View style={{ height: 30 }} />
                            <Button
                                name="Request for a Quote"
                                onPress={() => _submitRequest()}
                                loading={loading}
                            />
                            <View style={{ height: 30 }} />
                        </View>
                    }
                </ScrollView>
            </SafeAreaView>
        </View>
    )
}



const styles = StyleSheet.create({
    image: {
        width: 50,
        height: 50,
        marginBottom: 15
    },
    text: {
        color: '#363131',
        fontFamily: fontFamilyRegular,
        fontSize: 12,
        textAlign: 'center'
    },
    boxStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFFFFF',
        borderRadius: 5,
        padding: 20,
        flex: 1
    },
    multipleBoxItemMain: {
        flexDirection: 'row', marginTop: 20
    },
    miniBox1: {
        flex: 1,
        marginRight: 10,
        borderRadius: 5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 2,
    },
    miniBox2: {
        flex: 1, marginLeft: 10,
        borderRadius: 5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 2,
    },
    backgroundBox1: {
        width: windowHeight - 100, height: windowHeight - 100, backgroundColor: "rgba(202,225,252,0.2)", borderRadius: windowHeight, marginLeft: -windowWidth + 100, marginTop: -windowHeight / 5, zIndex: 1, position: "absolute"
    },
    backgroundBox2: {
        width: windowWidth - 100, height: windowWidth - 100, backgroundColor: "rgba(244,224,234,0.2)", borderRadius: windowWidth, zIndex: 1, marginTop: -windowWidth / 3, marginLeft: - windowWidth / 3, position: "absolute", top: windowHeight / 1.7
    }
})