import React from 'react';
import { View, Text, Image, StyleSheet, ScrollView, SafeAreaView, Platform, Alert, Modal, ActivityIndicator, StatusBar, TextInput, TouchableOpacity } from "react-native"

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { fontFamilyMedium, fontFamilyRegular, fontFamilyLight, fontFamilyBold } from '../../global/globalStyle'
import Button from '../../components/Button'
import SelectOption from '../../components/SelectOption'
import Header from '../../components/Header'
import Input from '../../components/Input'
import ImagePicker from 'react-native-image-crop-picker';
import Slider from "react-native-slider";
import { useSelector, useDispatch } from 'react-redux';
import { updateUserCarData, updateUserData } from '../../redux'
import { updateState, confirmPin } from '../../redux'
import Toast from 'react-native-simple-toast';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import RNFetchBlob from 'rn-fetch-blob'
import Api from '../../api';
import messaging from '@react-native-firebase/messaging';
import ContactInput from '../../components/ContactInput'
import { BlurView } from "@react-native-community/blur";
const Tab = createBottomTabNavigator();
import auth from '@react-native-firebase/auth';
export default function ProfileTab(props) {
    const dispatch = useDispatch()
    const navState = useSelector(state => state.navigation.navigationPage);
    const user = useSelector(state => state.user.userData);
    const users = useSelector(state => state);
    const [imageModal, setImageModal] = React.useState(false);
    const [confirmModal, setConfirmModal] = React.useState(false);
    const [editModal, setEditModal] = React.useState(false);
    const [imageLoad, setImageLoad] = React.useState(false);
    const [image, setImage] = React.useState(user.image)
    const [removeid, setRemoveid] = React.useState('')
    const [name, setName] = React.useState(user.name);
    const [email, setEmail] = React.useState(user.email);
    const [loading, setLoading] = React.useState(false);
    const [deleteLoading, setDeleteLoading] = React.useState(false)
    const [vname, setVname] = React.useState(false);
    const [vemail, setVemail] = React.useState(false);
    const [countryCode, setCountryCode] = React.useState(user.cc ? user.cc : '254');
    const [contact, setContact] = React.useState(user.mobile != null ? user.mobile : '');
    const [contactv, setContactv] = React.useState(false);
    const [countryAlpha, setCountryAlpha] = React.useState(user.cc ? user.alpha2 : 'KE')
    React.useEffect(() => {
        if (contact.length > 7 && contact.length <= 13) {
            setContactv(true)
        } else {
            setContactv(false)
        }
    }, [contact])
    const _getuserData = async (token) => {
        axios.post(Api + "/api/getcars/", {}, {
            headers: {
                'authorization': token
            }
        })
            .then((res) => {
                if (res.data.status == true) {
                    dispatch(updateUserCarData(res.data.data));
                }
            });
    }
    const Verify = async () => {
        const value = await AsyncStorage.getItem('token')
        let a = await messaging().getToken();
        let d = await axios.post(Api + "/api/logout", {
            deviceToken: a
        }, {
            headers: {
                'authorization': value
            }
        }).then((res) => {
            auth()
                .signOut()
                .then(() => console.log("LOG OUT"))
        })
            .catch((err) => {
                console.log(err);
            })
        try {
            await AsyncStorage.setItem(
                'token', 'false'
            )
        } catch (error) {
            Toast.show('Token Error')
        }
        dispatch(updateState('auth'))
    }
    const _deleteCar = async (_id) => {
        try {
            setDeleteLoading(true)
            const value = await AsyncStorage.getItem('token')
            axios.post(Api + "/api/deletecar", {
                id: _id
            }, {
                headers: { 'authorization': value }
            }
            )
                .then((res) => {
                    if (res.data.status == true) {
                        Toast.show('Car Deleted');
                        _getuserData(value)
                        setRemoveid("");
                    }
                    setDeleteLoading(false)
                })
                .catch((error) => {
                    Toast.show(error)
                    setDeleteLoading(false)
                })
        }
        catch (err) {
            Toast.show(err)
            setDeleteLoading(false)
        }
    }

    const _confirm = (a) => {
        setRemoveid(a);
        setConfirmModal(true);
    }
    const _checkToken = async (value) => {
        await AsyncStorage.setItem('token', value)
        if (value != 'false' && value != null && value != undefined) {

            axios.post(Api + "/api/checkToken", {}, {
                headers: {
                    'authorization': value
                }
            })
                .then(res => {
                    if (res.data.status == true) {
                        let a = res.data.data.user
                        dispatch(updateUserData(a))
                        setImageLoad(false);
                        Toast.show('Profile Pic Updated Successfully')
                    }
                    else {
                        dispatch(updateState('auth'))
                        alert('Session Expired Login Again')
                        setImageLoad(false);
                    }
                })
                .catch(error => {
                    setImageLoad(false);
                    console.log(error.message)
                    throw (error)

                });
        }
    }
    const uploadImage = async (uri) => {
        setImageLoad(true);

        const value = await AsyncStorage.getItem('token')
        let uploadData = new FormData();
        uploadData.append('image', { type: uri.mime, uri: uri.path, name: 'profile.jpg' })


        RNFetchBlob.fetch('POST', Api + '/api/uploadImage', {
            'Content-Type': 'multipart/form-data',
            'authorization': value

        }, [
            { name: 'image', filename: 'image.jpg', type: uri.mime, data: uri.data }
        ]).then((resp) => {
            let dd = JSON.parse(resp.data);
            if (dd.status) {
                _checkToken(dd.token)
            }
        }).catch((err) => {

            setImageLoad(false);
        })
    }
    _storeData = async (token) => {
        try {
            await AsyncStorage.setItem(
                'token', token
            )
        } catch (error) {
            setLoading(false)
            Toast.show('Token Error signup page')
        }
    }
    const _updateProfile = async (mno, cc, alpha, a = 1,) => {
        if (vname && vemail) {
            try {
                setLoading(true)
                const value = await AsyncStorage.getItem('token')
                axios.post(Api + "/api/editProfile", {
                    "name": name,
                    "email": email.toLowerCase(),
                    "mno": mno,
                    "cc": cc,
                    "alpha2": alpha
                }, {
                    headers: {
                        'authorization': value
                    }
                })
                    .then(res => {
                        if (res.data.status == true) {
                            _storeData(res.data.token)
                            dispatch(updateUserData(res.data.data))
                            setLoading(false)
                            Toast.show('Profile Updated Successfuly ')
                            setEditModal(false)
                        }
                        else {
                            setLoading(false)
                            Toast.show('Error Occured While Updating Details')
                        }
                    })
                    .catch(error => {
                        {
                            setLoading(false)
                            throw (error)
                        }
                    })
            }
            catch {
                Alert.alert('Error Ocuured')
            }
        }
        else if (vname && !vemail) {
            Alert.alert('Email is Badly Formatted')
        }
        else if (!vname && vemail) {
            Alert.alert('Please Enter Name')
        }
        else {
            Alert.alert('Please Enter Name And Email')
        }
    }
    const takePhotoFromCamera = () => {
        ImagePicker.openCamera({
            width: 400,
            height: 400,
            cropping: true,
            includeBase64: true
        }).then(image => {

            setImageModal(false)
            uploadImage(image);
        }).catch((err) => {
            console.log(err)
        })
            ;
    }
    const validateEmail = (email) => {
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
    const choosePhotoFromLibrary = () => {
        ImagePicker.openPicker({
            width: 400,
            height: 400,
            cropping: true,
            includeBase64: true
        }).then(image => {
            // setImage(image.path)
            setImageModal(false)
            uploadImage(image);
        }).catch((err) => {
            console.log(err)
        });
    }
    React.useEffect(() => {
        if (name.length > 3) {
            setVname(true);
        } else {
            setVname(false);
        }
        if (validateEmail(email)) {
            setVemail(true)
        } else {
            setVemail(false)
        }
    }, [name, email])
    const _contactPageMove = async () => {
        try {
            let cont = contact.replace(/^0+/, '');
            const confirmation = await auth().signInWithPhoneNumber('+' + countryCode + cont);
            dispatch(confirmPin(confirmation))
            props.navigation.navigate('VerificationCodeProfile', {
                phoneNumber: '+' + countryCode + cont,
                cc: countryCode,
                phone: cont,
                alpha: countryAlpha,
                email: email,
                name: name
            });
            setEditModal(false);
        } catch (err) {
            console.log(err)
        }
    }
    return (
        <View style={{ backgroundColor: '#F8F8F8', flex: 1 }}>
            <StatusBar
                backgroundColor="#276EF1"
                barStyle="light-content"
            />
            <View style={{ backgroundColor: "#276EF1", height: 40 }}></View>
            <Modal
                animationType="slide"
                transparent={true}
                visible={editModal}
                onRequestClose={() => {
                    setEditModal(false)
                }}
            >
                {Platform.OS == 'ios' ?
                    <BlurView style={{ flex: 1, justifyContent: 'center', alignItems: "center" }}
                        blurType="xlight"
                        blurAmount={10}
                        downsampleFactor={25}

                    >
                        <View style={{ padding: 20, backgroundColor: 'white', borderRadius: 10, width: "90%" }}>
                            <View style={{ alignItems: "flex-end" }}>
                                <TouchableOpacity
                                    onPress={() => setEditModal(false)}
                                    style={{ paddingHorizontal: 10, zIndex: 102 }}
                                >
                                    <Image
                                        style={{ width: 16, height: 16 }}
                                        source={require('../../assets/images/close.png')}
                                    />
                                </TouchableOpacity>
                            </View>
                            <Input
                                label="Name"
                                placeholder="ex. Brian"
                                value={name}
                                onChangeText={(t) => setName(t)}
                                isValid={vname}
                                err={'Name Length Should Be Greater Than 3 Digits.'}
                            />
                            <View style={{ height: 30 }} />
                            <ContactInput
                                label="Phone Number"
                                placeholder="Phone Number"
                                value={contact}
                                valueCountryCode={countryCode}
                                onChangeText={(t) => setContact(t)}
                                onChangeCountryCode={(t) => setCountryCode(t)}
                                isValid={contactv}
                                cc2={countryAlpha}
                                onUpdateCountryAlpha={(t) => setCountryAlpha(t)}
                            />


                            <View style={{ height: 30 }} />
                            <Button
                                loading={loading}
                                name="Update"
                                onPress={() => {
                                    if (contact == user.mobile && countryCode == user.cc) {
                                        _updateProfile(contact, countryCode)
                                    }
                                    else {
                                        if (contactv && countryCode != undefined && countryCode != null) {
                                            _contactPageMove();
                                            // props.navigation.navigate('VerificationCodeProfile', { _addNew: _updateProfile.bind(this), cc: countryCode, mno: contact, alpha: countryAlpha })
                                            // setEditModal(false)
                                        }
                                        else {
                                            Toast.show('Please Fill Valid Mobile no')
                                        }
                                    }


                                }}
                            />

                        </View>
                    </BlurView> :
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: "center", backgroundColor: "rgba(255,255,255,0.7)" }}>
                        <View style={{ padding: 20, backgroundColor: 'white', borderRadius: 10, width: "90%", zIndex: 100 }}>
                            <View style={{ alignItems: "flex-end" }}>
                                <TouchableOpacity
                                    onPress={() => setEditModal(false)}
                                    style={{ paddingHorizontal: 10, zIndex: 102 }}
                                >
                                    <Image
                                        style={{ width: 16, height: 16 }}
                                        source={require('../../assets/images/close.png')}
                                    />
                                </TouchableOpacity>
                            </View>
                            <Input
                                label="Name"
                                placeholder="ex. Brian"
                                value={name}
                                onChangeText={(t) => setName(t)}
                                isValid={vname}
                                err={'Name Length Should Be Greater Than 3 Digits.'}
                            />
                            <View style={{ height: 30 }} />
                            <ContactInput
                                label="Phone Number"
                                placeholder="Phone Number"
                                value={contact}
                                valueCountryCode={countryCode}
                                onChangeText={(t) => setContact(t)}
                                onChangeCountryCode={(t) => setCountryCode(t)}
                                isValid={contactv}
                                cc2={countryAlpha}
                                onUpdateCountryAlpha={(t) => setCountryAlpha(t)}
                            />


                            <View style={{ height: 30 }} />
                            <Button
                                loading={loading}
                                name="Update"
                                onPress={() => {
                                    if (contact == user.mobile && countryCode == user.cc) {
                                        _updateProfile(contact, countryCode)
                                    }
                                    else {
                                        if (contactv && countryCode != undefined && countryCode != null) {
                                            _contactPageMove();
                                            // props.navigation.navigate('VerificationCodeProfile', { _addNew: _updateProfile.bind(this), cc: countryCode, mno: contact, alpha: countryAlpha })
                                            // setEditModal(false)
                                        }
                                        else {
                                            Toast.show('Please Fill Valid Mobile no')
                                        }
                                    }


                                }}
                            />

                        </View>
                    </View>
                }
            </Modal>
            <Modal
                animationType="slide"
                transparent={true}
                visible={confirmModal}
                onRequestClose={() => {
                    setConfirmModal(false)
                }}
            >
                {Platform.OS == 'ios' ?
                    <BlurView style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
                        blurType="xlight"
                        blurAmount={10}
                        downsampleFactor={25}

                    >
                        <View style={{ padding: 30, backgroundColor: 'white', justifyContent: 'center', alignItems: 'center', borderRadius: 10 }}

                        >

                            <Text style={{ fontFamily: fontFamilyBold, fontSize: 20, color: '#34495E' }}>Are you sure?</Text>
                            <View style={{ height: 20 }} />
                            <View style={{ flexDirection: 'row' }}>
                                <TouchableOpacity style={{ backgroundColor: "#FF0036", padding: 15, width: 100, borderRadius: 5 }}
                                    onPress={() => {
                                        if (removeid != "") {
                                            _deleteCar(removeid)
                                            setConfirmModal(false)
                                        } else {
                                            Verify()
                                            setConfirmModal(false)
                                        }
                                    }}
                                >
                                    <Text style={{ textAlign: 'center', color: "#FFFFFF", fontSize: 20, fontFamily: fontFamilyBold }}>Yes</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={{ backgroundColor: "#EFF1F7", padding: 15, width: 100, borderRadius: 5, marginLeft: 20 }}
                                    onPress={() => setConfirmModal(false)}
                                >
                                    <Text style={{ textAlign: 'center', color: "#34495E", fontSize: 20, fontFamily: fontFamilyBold }}>No</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </BlurView> :
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: "rgba(255,255,255,0.7)" }}

                    >
                        <View style={{ padding: 30, backgroundColor: 'white', justifyContent: 'center', alignItems: 'center', borderRadius: 10 }}

                        >

                            <Text style={{ fontFamily: fontFamilyBold, fontSize: 20, color: '#34495E' }}>Are you sure?</Text>
                            <View style={{ height: 20 }} />
                            <View style={{ flexDirection: 'row' }}>
                                <TouchableOpacity style={{ backgroundColor: "#FF0036", padding: 15, width: 100, borderRadius: 5 }}
                                    onPress={() => {
                                        if (removeid != "") {
                                            _deleteCar(removeid)
                                            setConfirmModal(false)
                                        } else {
                                            Verify()
                                            setConfirmModal(false)
                                        }
                                    }}
                                >
                                    <Text style={{ textAlign: 'center', color: "#FFFFFF", fontSize: 20, fontFamily: fontFamilyBold }}>Yes</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={{ backgroundColor: "#EFF1F7", padding: 15, width: 100, borderRadius: 5, marginLeft: 20 }}
                                    onPress={() => setConfirmModal(false)}
                                >
                                    <Text style={{ textAlign: 'center', color: "#34495E", fontSize: 20, fontFamily: fontFamilyBold }}>No</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                }
            </Modal>
            <Modal
                animationType="slide"
                transparent={true}
                visible={imageModal}
                onRequestClose={() => {
                    setImageModal(false)
                }}
            >
                <View style={{ flex: 1, backgroundColor: 'rgba(255,255,255,0.8)', justifyContent: 'flex-end' }}>
                    <View style={{ flex: 1 }}></View>
                    <View style={{
                        backgroundColor: 'white', shadowColor: "#000",
                        borderRadius: 30,
                        shadowOffset: {
                            width: 0,
                            height: 3,
                        },
                        shadowOpacity: 0.27,
                        shadowRadius: 4.65,

                        elevation: 6,
                    }}>
                        <View style={{ padding: 15 }}>
                            <View style={{ paddingTop: 20 }}>
                                <Text style={{ fontSize: 30, fontWeight: 'normal', color: '#333', textAlign: 'center' }}>Upload photo</Text>
                                <Text style={{ fontSize: 15, textAlign: 'center', color: '#999' }}>Choose profile photo</Text>
                            </View>
                            <View style={{ paddingTop: 15 }}>
                                <Button
                                    name={"Take photo"}
                                    onPress={() => takePhotoFromCamera()}
                                />
                                <View
                                    style={{ height: 10 }}
                                />
                                <Button
                                    name={"Choose from library"}
                                    onPress={() => choosePhotoFromLibrary()}
                                />
                                <View
                                    style={{ height: 10 }}
                                />
                                <Button
                                    name={"Cancel"}
                                    onPress={() => setImageModal(false)}
                                />
                            </View>
                        </View>
                    </View>
                </View>
            </Modal>
            <SafeAreaView style={{ flex: 1 }}>

                <ScrollView style={{ padding: 10, marginTop: 0 }}>
                    <View style={{ height: 20 }} />
                    <Image
                        source={require("../../assets/images/blue_logo.png")}
                        style={{ width: 100, height: 19 }}
                    />
                    <View style={{ height: 20 }} />


                    <View style={{ alignItems: 'flex-end' }}>

                    </View>
                    <View style={{ backgroundColor: "#276EF1", padding: 15, borderRadius: 10, overflow: "hidden" }}>
                        <View style={{ width: 112, height: 112, backgroundColor: '#327AFE', borderRadius: 56, position: "absolute", right: -30, top: -56, zIndex: 1 }}>

                        </View>
                        <TouchableOpacity style={{ padding: 15, backgroundColor: 'transparent', position: "absolute", right: 0, top: 0, zIndex: 20 }}
                            onPress={() => {
                                setEditModal(true);
                                // props.navigation.navigate('EditProfile')
                            }}
                        >
                            <Image
                                source={require("../../assets/images/edit.png")}
                                style={{ width: 15, height: 15 }}
                            />
                        </TouchableOpacity>
                        <View style={{ flexDirection: 'row', zIndex: 2 }}>
                            <View>
                                <TouchableOpacity
                                    onPress={() => setImageModal(true)}
                                    style={{ width: 70, height: 70, backgroundColor: '#D8D8D8', borderRadius: 50, overflow: 'hidden' }}
                                >
                                    {imageLoad &&
                                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                            <ActivityIndicator />
                                        </View>
                                    }
                                    <View style={{ flex: 1 }}>
                                        {image != '' ?
                                            <Image
                                                source={{ uri: user.image }}
                                                style={{ flex: 1 }}
                                            /> :
                                            <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                                                <Text style={{ fontSize: 20 }}>{user.name.substring(0, 1)}</Text>
                                            </View>
                                        }

                                    </View>

                                </TouchableOpacity>
                            </View>
                            <View style={{ justifyContent: 'center', marginLeft: 20 }}>
                                <Text style={{ color: '#FFFFFF', fontSize: 20, fontFamily: fontFamilyBold, lineHeight: 25 }}>{user.name}</Text>
                                {user.cc &&
                                    <Text style={{ fontFamily: fontFamilyRegular, fontSize: 15, color: '#FFFFFF' }}>+{user.cc} {user.mobile}</Text>
                                }
                            </View>
                        </View>
                    </View>

                    <View style={{ backgroundColor: '#FFFFFF', padding: 15, borderRadius: 10, borderWidth: 0, borderColor: '#F5F5F5', marginTop: 20 }}>
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ justifyConvent: 'center', flex: 1, padding: 10 }}>
                                <Text style={{ color: '#276EF1', fontSize: 20, fontFamily: fontFamilyBold }}>Car Details</Text>
                            </View>
                            <View style={{ justifyContent: 'center' }}>
                                <TouchableOpacity style={{ borderTopWidth: 0, borderTopColor: '#EBF0FF', padding: 10 }}
                                    onPress={() => props.navigation.navigate('AddCarProfile', { type: 'add', mno: user.mobile, cc: user.cc })}
                                >
                                    <Text style={{ color: '#276EF1', fontSize: 13, fontFamily: fontFamilyRegular }}>+ Add New Car</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{ height: 15 }} />
                        {deleteLoading &&
                            <View>
                                <Text>Deleting Car...</Text>
                            </View>
                        }

                        {users.user.carsData.map((item, key) => {
                            return (
                                <View key={key} style={{ flexDirection: 'row', padding: 10 }}>
                                    <View style={{ flex: 1 }}>
                                        <Text style={{ fontFamily: fontFamilyBold, fontSize: 15, color: '#34495E' }}>{item.regno}</Text>
                                    </View>
                                    <View style={{ flex: 1, alignItems: 'flex-end' }}>
                                        <View style={{ flexDirection: 'row' }}>

                                            <TouchableOpacity style={{ marginRight: 10 }}
                                                onPress={() => {
                                                    _confirm(item._id)
                                                    // _deleteCar()
                                                }}
                                            >
                                                <Text style={{ color: '#34495E', fontFamily: fontFamilyRegular, fontSize: 13 }}>Remove</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity style={{ paddingLeft: 10, borderLeftColor: '#DFDFDF', borderLeftWidth: 1 }}
                                                onPress={() => props.navigation.navigate('AddCarProfile', { type: 'edit', id: item._id, enginesize: item.engine_capacity, carmake: item.make, regno: item.regno, modelyear: item.model, chassino: item.chassino, mno: user.mobile, cc: user.cc, year: item.year })}
                                            >
                                                <Text style={{ color: '#276EF1', fontFamily: fontFamilyRegular, fontSize: 13 }}>Edit</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>)
                        })

                        }




                    </View>
                    {/* Edit Profile End */}
                    <View style={{ backgroundColor: '#FFFFFF', paddingHorizontal: 15, borderRadius: 10, borderWidth: 0, borderColor: '#F5F5F5', marginTop: 20 }}>
                        <TouchableOpacity style={{ flexDirection: 'row', paddingVertical: 20, borderBottomWidth: 1, borderBottomColor: '#EBF0FF' }}
                            onPress={() => props.navigation.navigate('OrderHistory')}
                        >
                            <View style={{ flex: 1 }}>
                                <Text style={{ fontFamily: fontFamilyRegular, fontSize: 15, color: '#34495E' }}>Order History</Text>
                            </View>
                            <View style={{ justifyContent: 'center' }}>
                                <Image
                                    source={require('../../assets/images/nextIcon.png')}
                                    style={{ width: 6, height: 10 }}
                                />
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ flexDirection: 'row', paddingVertical: 20, borderBottomWidth: 0, borderBottomColor: '#EBF0FF' }}
                            onPress={() => setConfirmModal(true)}
                        >
                            <View style={{ flex: 1 }}>
                                <Text style={{ fontFamily: fontFamilyRegular, fontSize: 15, color: '#34495E' }}>Logout</Text>
                            </View>
                            <View style={{ justifyContent: 'center' }}>
                                <Image
                                    source={require('../../assets/images/nextIcon.png')}
                                    style={{ width: 6, height: 10 }}
                                />
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={{ height: 30 }} />
                </ScrollView>
            </SafeAreaView>
        </View>
    )
}



const styles = StyleSheet.create({
    image: {
        width: 50,
        height: 50,
        marginBottom: 15
    },
    text: {
        color: '#363131',
        fontFamily: fontFamilyRegular,
        fontSize: 12,
        textAlign: 'center'
    },
    boxStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F8F8F8',
        borderRadius: 5,
        padding: 20,
        flex: 1
    },
    multipleBoxItemMain: {
        flexDirection: 'row', marginTop: 20
    },
    miniBox1: {
        flex: 1,
        marginRight: 10,
        borderRadius: 5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 2,
    },
    miniBox2: {
        flex: 1, marginLeft: 10,
        borderRadius: 5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 2,
    }
})