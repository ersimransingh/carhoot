import React from 'react';
import { TouchableOpacity, View, Text, Image, StyleSheet, ScrollView, SafeAreaView, ActivityIndicator, Modal, Linking, StatusBar, TextInput } from "react-native"
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { fontFamilyMedium, fontFamilyRegular, fontFamilyLight, fontFamilyBold } from '../../global/globalStyle'
import Dashboard from './dashboard';
import Api from '../../api';
import Header from '../../components/Header'

import { useSelector } from 'react-redux';
const Tab = createBottomTabNavigator();

export default function AutoServiceTab(props) {
    const [lastindex, setlastIndex] = React.useState(0)
    const Data = useSelector(state => state.servicedata.autoservice)
    const myCars = useSelector(state => state.user.carsData);
    const dd = useSelector(state => state.user.userData);
    const [imageLoading, setImageLoading] = React.useState(false);
    const [totalImgs, setTotalImgs] = React.useState(0);
    const [search, setSearch] = React.useState('')
    React.useEffect(() => {
        const length = Data.length
        if (length == 1) {
            setlastIndex(length - 1)
        } else if (length % 2 != 0) {
            setlastIndex(length - 1)
        }
    })

    return (
        <View style={styles.mainContainer}>
            <StatusBar
                backgroundColor="#276EF1"
                barStyle="light-content"
            />
            <View style={styles.topSpace}></View>
            <View style={styles.innerContainerStyle}>
                <View style={styles.headerRow}>
                    <View style={styles.headerView1} >
                        <Image
                            source={require("../../assets/images/blue_logo.png")}
                            style={{ width: 100, height: 19 }}
                        />
                    </View>
                    <View style={{ alignItems: 'flex-end' }}>
                        <View style={{ flexDirection: 'row' }}>
                            <TouchableOpacity style={{ marginRight: 10 }}
                                onPress={() => Linking.openURL("tel:+254720039039")}
                            >
                                <Image
                                    source={require('../../assets/images/phone.png')}
                                    style={styles.phoneImage}
                                />
                            </TouchableOpacity>
                            <TouchableOpacity style={{ marginRight: 0 }}
                                onPress={() => {
                                    let url = "whatsapp://send?text=Hello There" +
                                        "&phone=254720039039"
                                    Linking.openURL(url)
                                        .then(() => {
                                            console.log('whatsappopen')
                                        })
                                        .catch((err) => {
                                            console.log(err);
                                        })
                                }}
                            >
                                <Image
                                    source={require('../../assets/images/message.png')}
                                    style={styles.phoneImage}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <View style={styles.outerBoxSearch}>
                    <View style={styles.innerBoxSearch}>
                        <View style={{ flex: 1 }}>
                            <TextInput
                                placeholder="Find Auto Services..."
                                placeholderTextColor="#B2BAC4"
                                style={{ padding: 15, color: "#000000" }}
                                onChangeText={(t) => setSearch(t)}
                            />
                        </View>
                        <View style={styles.searchImageOuter}>
                            <Image
                                source={require('../../assets/images/search.png')}
                                style={{ width: 16, height: 16 }}
                            />
                        </View>
                    </View>
                </View>
            </View>
            <Modal
                animationType="slide"
                transparent={true}
                visible={totalImgs < 10}
            >
                <View style={styles.loadingModal}>
                    <ActivityIndicator
                        size="large" color="#FFFFFF"
                    />
                </View>
            </Modal>
            <SafeAreaView style={{ flex: 1 }}>

                <ScrollView style={styles.scrollViewInner}>
                    <View style={{ marginTop: 10, flexWrap: "wrap", flexDirection: 'row', justifyContent: lastindex == 0 || lastindex % 2 == 0 ? 'center' : 'flex-start' }}>
                        {
                            Data.map((item, key) => {
                                let smallName = item.autoservicetype.toLowerCase();
                                if (smallName.indexOf(search.toLowerCase()) > -1) {
                                    return (
                                        <View style={styles.miniBox1} key={key}>
                                            <TouchableOpacity

                                                onPress={() => {
                                                    if (myCars.length > 0) {
                                                        // AutoServiceRequest
                                                        props.navigation.navigate('ServiceCategory', { item: item, service: "auto" })
                                                    } else {
                                                        props.navigation.navigate('AddCarProfile', { type: 'add', mno: dd.mobile, cc: dd.cc })
                                                    }
                                                    // SelectLocation
                                                }}
                                                style={styles.boxStyle}

                                            >
                                                <Image
                                                    source={{ uri: item.icon }}
                                                    style={styles.image}
                                                    onLoadEnd={() => {
                                                        setTotalImgs(totalImgs + 1)
                                                    }}
                                                    resizeMode="stretch"
                                                />
                                                <Text style={styles.text}>{item.autoservicetype}</Text>
                                                {imageLoading &&
                                                    <View style={{ width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center', backgroundColor: '#ffffff', position: 'absolute', zIndex: 9909090 }}>
                                                        <ActivityIndicator />
                                                    </View>
                                                }
                                            </TouchableOpacity>
                                        </View>)
                                }
                            })
                        }


                    </View>
                    <View
                        style={{ height: 30 }}
                    />
                </ScrollView>
            </SafeAreaView>
        </View>
    )
}



const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: '#F8F8F8'
    },
    topSpace: {
        backgroundColor: "#276EF1", height: 40
    },
    innerContainerStyle: {
        backgroundColor: "#FFFFFF",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,

        elevation: 2,
    },
    headerRow: {
        flexDirection: 'row', padding: 20
    },
    headerView1: {
        flex: 1, justifyContent: 'center'
    },
    headerView1Text: {
        color: "#276EF1", fontSize: 25, fontFamily: fontFamilyBold
    },
    phoneImage: {
        width: 35, height: 35
    },
    outerBoxSearch: {
        paddingHorizontal: 20, paddingBottom: 20
    },
    innerBoxSearch: {
        backgroundColor: 'white', flexDirection: 'row', borderRadius: 5, borderColor: "#276EF1", borderWidth: 1
    },
    searchImageOuter: {
        justifyContent: 'center', alignItems: 'center', marginRight: 15
    },
    loadingModal: {
        flex: 1, backgroundColor: '#276EF1', justifyContent: 'center', alignItems: 'center'
    },
    scrollViewInner: {
        padding: 10, marginTop: 10
    },
    image: {
        width: 50,
        height: 50,
        marginBottom: 15
    },
    text: {
        color: '#363131',
        fontFamily: fontFamilyRegular,
        fontSize: 12,
        textAlign: 'center'
    },
    boxStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFFFFF',
        borderRadius: 0,
        padding: 20,
        margin: 10,
        borderRadius: 10
    },
    multipleBoxItemMain: {
        marginTop: 10, flexWrap: 'wrap', flexDirection: 'row'
    },
    miniBox1: {
        width: '49%',
        margin: 1
    }
})