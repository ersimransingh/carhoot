import React from 'react';
import { View, Text, Image, StyleSheet, ScrollView, SafeAreaView, ActivityIndicator, Modal, StatusBar, Linking, TextInput } from "react-native"
import { TouchableOpacity } from 'react-native-gesture-handler'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { fontFamilyMedium, fontFamilyRegular, fontFamilyLight, fontFamilyBold } from '../../global/globalStyle'
import Dashboard from './dashboard';
import Api from '../../api';
import Header from '../../components/Header'


import { updateUserCarData, updateUserData, setEditableTowing } from '../../redux'
import { useSelector, useDispatch } from 'react-redux';
import { useFocusEffect } from '@react-navigation/native';
const Tab = createBottomTabNavigator();

export default function CarShop(props) {

    const dispatch = useDispatch()
    const [search, setSearch] = React.useState("")
    const user = useSelector(state => state.user.userData);
    const myCars = useSelector(state => state.user.carsData);
    useFocusEffect(
        React.useCallback(() => {
            dispatch(setEditableTowing(false, ""))

        }, [props.route.params])
    );
    const PAGEDATA = [
        {
            name: "Import the car I want",
            description: "You know the make and model of the car you want and need help importing it to Kenya",
            image: require('../../assets/images/carShop/car2.png'),
            size: { width: 74.44, height: 40 },
            type: "known",
            navigate: "ImportCarTab",
            title: "Get me a quote",
            pageDescription: "Carhoot agents will get you options for the car you want to import and share final landing prices for your consideration."
        },
        {
            name: "Buy car spare parts",
            description: "Submit a request for a spare part you need. We will source, verify and purchase on your behalf.",
            image: require('../../assets/images/carShop/car4.png'),
            size: { width: 45, height: 41.48 },
            type: "service",
            navigate: "ImportServiceParts",
            title: "Find service parts",
            pageDescription: "Carhoot agents will give you suggestions on possible cars you can buy based on a few details. We will send you options and landing prices to help you decide."
        }
    ]

    return (
        <View style={styles.mainContainer}>
            <StatusBar
                backgroundColor="#276EF1"
                barStyle="light-content"
            />
            <View style={styles.topSpace}></View>
            <View style={styles.innerContainerStyle}>
                <View style={styles.headerRow}>
                    <View style={styles.headerView1} >
                        <Image
                            source={require("../../assets/images/blue_logo.png")}
                            style={{ width: 100, height: 19 }}
                        />
                    </View>
                    <View style={{ alignItems: 'flex-end' }}>
                        <View style={{ flexDirection: 'row' }}>
                            <TouchableOpacity style={{ marginRight: 10 }}
                                onPress={() => Linking.openURL("tel:+254720039039")}
                            >
                                <Image
                                    source={require('../../assets/images/phone.png')}
                                    style={styles.phoneImage}
                                />
                            </TouchableOpacity>
                            <TouchableOpacity style={{ marginRight: 0 }}
                                onPress={() => {
                                    let url = "whatsapp://send?text=Hello There" +
                                        "&phone=254720039039"
                                    Linking.openURL(url)
                                        .then(() => {
                                            console.log('whatsappopen')
                                        })
                                        .catch((err) => {
                                            console.log(err);
                                        })
                                }}
                            >
                                <Image
                                    source={require('../../assets/images/message.png')}
                                    style={styles.phoneImage}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>

            <SafeAreaView style={{ flex: 1 }}>
                <ScrollView style={styles.scrollViewInner}>
                    {PAGEDATA.map((data, index) => {
                        let dname = data.name.toLowerCase();
                        if (dname.indexOf(search.toLowerCase()) > -1) {

                            return (
                                <TouchableOpacity style={{ flexDirection: 'row', backgroundColor: "#FFFFFF", borderRadius: 10, padding: 15, marginBottom: 25 }} key={"K" + index}
                                    onPress={() => {
                                        if (data.type == "service") {
                                            if (myCars.length > 0) {
                                                props.navigation.navigate(data.navigate, { thetype: data.type, title: data.title, description: data.pageDescription })
                                            } else {
                                                props.navigation.navigate('AddCarProfile', { type: 'add', mno: user.mobile, cc: user.cc })
                                            }
                                        } else {
                                            props.navigation.navigate(data.navigate, { thetype: data.type, title: data.title, description: data.pageDescription })
                                        }
                                    }}
                                >
                                    <View style={{ width: 100, alignItems: "center" }}>
                                        <Image
                                            source={data.image}
                                            style={data.size}
                                        />
                                    </View>
                                    <View style={{ flex: 1 }}>
                                        <Text style={{ color: "#34495E", fontFamily: fontFamilyRegular, fontSize: 14, marginBottom: 6 }}>{data.name}</Text>
                                        <Text style={{ color: "#34495E", fontFamily: fontFamilyRegular, fontSize: 12 }}>{data.description}</Text>
                                    </View>
                                </TouchableOpacity>
                            )
                        }
                    })}
                    <View
                        style={{ height: 30 }}
                    />
                </ScrollView>
            </SafeAreaView>
        </View>
    )
}



const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: '#F8F8F8'
    },
    topSpace: {
        backgroundColor: "#276EF1", height: 40
    },
    innerContainerStyle: {
        backgroundColor: "#FFFFFF",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,

        elevation: 2,
    },
    headerRow: {
        flexDirection: 'row', padding: 20
    },
    headerView1: {
        flex: 1, justifyContent: 'center'
    },
    headerView1Text: {
        color: "#276EF1", fontSize: 25, fontFamily: fontFamilyBold
    },
    phoneImage: {
        width: 35, height: 35
    },
    outerBoxSearch: {
        paddingHorizontal: 20, paddingBottom: 20
    },
    innerBoxSearch: {
        backgroundColor: 'white', flexDirection: 'row', borderRadius: 5, borderColor: "#276EF1", borderWidth: 1
    },
    searchImageOuter: {
        justifyContent: 'center', alignItems: 'center', marginRight: 15
    },
    loadingModal: {
        flex: 1, backgroundColor: '#276EF1', justifyContent: 'center', alignItems: 'center'
    },
    scrollViewInner: {
        padding: 10, marginTop: 10
    },
    image: {
        width: 50,
        height: 50,
        marginBottom: 15
    },
    text: {
        color: '#363131',
        fontFamily: fontFamilyRegular,
        fontSize: 12,
        textAlign: 'center'
    },
    boxStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFFFFF',
        borderRadius: 0,
        padding: 20,
        margin: 10,
        borderRadius: 10
    },
    multipleBoxItemMain: {
        marginTop: 10, flexWrap: 'wrap', flexDirection: 'row'
    },
    miniBox1: {
        width: '49%',
        margin: 1
    }
})