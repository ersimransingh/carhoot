import React from 'react';
import { View, Text } from 'react-native';

export function Thumb(props) {
    return (
        <View style={{
            width: 12 * 2,
            height: 12 * 2,
            borderRadius: 12,
            borderWidth: 2,
            borderColor: '#276EF1',
            backgroundColor: '#ffffff',
        }} />
    )
}
export function Rail(props) {
    return (
        <View style={{
            flex: 1,
            height: 8,
            borderRadius: 4,
            backgroundColor: 'white',
            borderColor: "#276EF1",
            borderWidth: 1
        }} />
    )
}
export function RailSelected(props) {
    return (
        <View style={{
            height: 8,
            backgroundColor: '#276EF1',
            borderRadius: 4,
            borderColor: "#276EF1",
            borderWidth: 1
        }} />
    )
}
export function Label(props) {
    return (
        <View style={{
            alignItems: 'center',
            padding: 8,
            backgroundColor: '#276EF1',
            borderRadius: 4,
        }}>
            <Text style={{
                fontSize: 16,
                color: '#fff',
            }}>{props.text}</Text>
        </View>
    )
}
export function Notch(props) {
    return (
        <View style={{
            width: 8,
            height: 8,
            borderLeftColor: 'transparent',
            borderRightColor: 'transparent',
            borderTopColor: '#276EF1',
            borderLeftWidth: 4,
            borderRightWidth: 4,
            borderTopWidth: 8,
        }} />
    )
}