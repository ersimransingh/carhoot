import React from 'react';
import { View, Text, Image, StyleSheet, ScrollView, SafeAreaView, StatusBar, Dimensions, ActivityIndicator } from "react-native"
import { TouchableOpacity } from 'react-native-gesture-handler'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { fontFamilyMedium, fontFamilyRegular, fontFamilyLight, fontFamilyBold } from '../../global/globalStyle'
import Button from '../../components/Button'
import SelectOption from '../../components/SelectOption'
import MultipleRadio from '../../components/MultipleRadio'
import Header from '../../components/Header'
import Slider from "react-native-slider";

import Toast from 'react-native-simple-toast'
import { useSelector, useDispatch } from 'react-redux'
import { updateLocationsData, setUserCars, updateRequests } from '../../redux'
import Api from '../../api';
import axios from 'axios';
import moment from 'moment'
import AsyncStorage from '@react-native-community/async-storage'
import {
    setCarPartsRedux, setPartCategoryRedux, setPartNamesRedux, setServicePartsRedux, updateServicePart, setPartSubCategoryRedux,
    setParCategory,
    setPartSubCategory,
    setPartName
} from '../../redux'

const Tab = createBottomTabNavigator();
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
export default function ImportServiceParts(props) {
    const partId = "5f734c1ff77c365a6c1106b3";
    const dispatch = useDispatch();
    const partsRedux = useSelector(state => state.importParts.serviceParts);
    const partsData = useSelector(state => state.parts)
    const partsReduxNew = useSelector(state => state.importParts.partsCategories);
    const partsCategories = useSelector(state => state.importParts.partsCategory);
    const partsCategoriesSub = useSelector(state => state.importParts.partsSubCategory);

    const partsName = useSelector(state => state.importParts.partsName);

    const [parttype, setPartType] = React.useState(props.route.params.thetype)
    const allCars = useSelector(state => state.importCar.allCars);
    const [partc, setPartc] = React.useState(partsData.partCategory1)
    const [partcval, setPartcval] = React.useState(partsData.partCategoryId1)
    const [partcsub, setPartcsub] = React.useState(partsData.partSubCategory1)
    const [partcsubval, setPartcsubval] = React.useState(partsData.partSubCategoryId1)
    const [partN, setPartN] = React.useState(partsData.partName)
    const [loading, setLoading] = React.useState(false);
    const [showError, setShowError] = React.useState(false);
    const userCars = useSelector(state => state.importCar.userCars)
    const [isLoading, setIsLoading] = React.useState(false);
    const createSearchableArray = (array) => {
        let arr1 = [];
        for (let i in array) {
            let a = { name: array[i], value: array[i], show: true }
            arr1.push(a);
        }
        return arr1;
    }
    const arr = []

    const myCars = useSelector(state => state.user.carsData)
    const [selectedCarId, setSelectedCarId] = React.useState(myCars[0]._id)
    const [selectedCarName, setSelectedCarName] = React.useState(myCars[0].carmake + "[ " + myCars[0].regno + " ]")

    const [carname, setcarName] = React.useState(arr.length != 0 ? arr[0].name : '')
    const [carMakes, setCarMakes] = React.useState(arr.length != 0 ? arr[0].value : '');
    React.useEffect(() => {
        try {
            setIsLoading(true);

            axios.post(Api + "/api/get-part", { category: partc })
                .then((res) => {
                    let searchable = createSearchableArray(res.data.parts)
                    dispatch(setPartNamesRedux(searchable));
                    setIsLoading(false);
                })
                .catch((error) => {
                    Toast.show(error)
                    setIsLoading(false);
                })
        } catch (err) {
            console.log(err);
            setIsLoading(false);
        }
    }, [partc, partcval])


    const onChangeservice = (index) => {
        dispatch(updateServicePart(index, partsRedux[index].status ? false : true))
    }
    const _submitRequest = () => {
        let partItems = [];
        for (let i in partsRedux) {
            if (partsRedux[i].status) {
                partItems.push(partsRedux[i].name);
            }
        }
        if (parttype == 'service') {
            if (partc == 'eg : engine') {

                setShowError(true);
            } else if (partN == "eg : Crankshaft seal") {
                setShowError(true);

            } else {
                setShowError(false);
                dispatch(setParCategory(partc, partcval))
                dispatch(setPartSubCategory("", ""))
                dispatch(setPartName(partN))
                props.navigation.navigate("SelectLocationPart", { type: "spareParts" })
            }
        } else {
            if (partItems.length == 0) {
                Toast.show("Please select atleast one service part.")
                setShowError(true);
            } else {
                setShowError(false);
                props.navigation.navigate("SelectLocationPart", { type: "carParts", selectedPartsArr: partItems })
            }
        }
    }
    const _submitReq = async () => {
        try {
            setLoading(true)
            const value = await AsyncStorage.getItem('token')
            axios.post(Api + "/api/getRescueRequest", {

            }, {
                headers: { 'authorization': value }
            }
            )
                .then((res) => {
                    if (res.data.status == true) {
                        dispatch(updateRequests(res.data.data))
                        setLoading(false)
                    }
                })
                .catch((error) => {
                    console.log(error.message)
                    setLoading(false)
                    Toast.show(JSON.stringify(error))
                })
        }
        catch (err) {
            setLoading(false)
            console.log(err)
        }
    }

    return (
        <View style={{ backgroundColor: '#FFFFFF', flex: 1 }}>
            {isLoading &&
                <View style={{ backgroundColor: "rgba(0,0,0,0.6)", position: "absolute", top: 0, bottom: 0, left: 0, right: 0, zIndex: 1000, justifyContent: "center", alignItems: "center" }}>
                    <ActivityIndicator
                        size="large"
                        color="#276EF1"
                    />
                </View>
            }
            <StatusBar
                barStyle="light-content"
                backgroundColor="#276EF1"
            />

            <View
                style={styles.backgroundBox1}
            />
            <View
                style={styles.backgroundBox2}
            />
            <View style={{ backgroundColor: "#276EF1", height: 40, zIndex: 2 }}></View>
            <SafeAreaView style={{ flex: 1, zIndex: 2 }}>
                <View style={{ flexDirection: "row", paddingTop: 15 }}>
                    <TouchableOpacity style={{ padding: 10, width: 80 }}
                        onPress={() => props.navigation.goBack()}
                    >
                        <Image
                            source={require('../../assets/images/Left.png')}
                            style={{ width: 24, height: 24 }}
                        />
                    </TouchableOpacity>
                    <View style={{ flex: 1, justifyContent: "center" }}>
                        <Text style={{ color: "#34495E", textAlign: "center", fontSize: 22, fontFamily: fontFamilyBold }}>
                            {props.route.params.title}
                        </Text>
                    </View>
                    <View style={{ width: 80 }}>

                    </View>
                </View>
                <ScrollView style={{ padding: 10, marginTop: 10 }}>
                    <View>
                        <Text style={{ fontSize: 15, fontFamily: fontFamilyRegular, color: '#34495E' }}>{props.route.params.description}</Text>
                    </View>
                    <View
                        style={{ padding: 20 }}
                    >

                    </View>
                    {parttype == 'service' &&
                        <View>
                            <View style={{ zIndex: 20 }}>
                                <View style={{ height: 30 }} />
                                <SelectOption
                                    getId={true}
                                    label="What is the part category?"
                                    placeholder={partc}
                                    onChangeText={(t) => {
                                        setPartc(t.name)
                                        setPartcval(t.value)
                                    }}
                                    returnArray={(t) => {
                                        dispatch(setPartCategoryRedux(t))
                                    }}
                                    data={partsCategories}
                                />
                                {showError &&
                                    <Text style={{ color: 'red', height: partc == 'eg : engine' ? 'auto' : 0 }}>{partc == 'eg : engine' ? "Please Select Category." : ""}</Text>
                                }
                            </View>
                            <View style={{ zIndex: 18 }}>
                                <View style={{ height: 30 }} />
                                <SelectOption
                                    label="WHAT IS THE PART NAME?"
                                    placeholder={partN}
                                    onChangeText={(t) => setPartN(t)}
                                    returnArray={(t) => {
                                        dispatch(setPartNamesRedux(t))
                                    }}
                                    data={partsName}
                                />
                                {showError &&
                                    <Text style={{ color: 'red', height: partN == 'eg : Crankshaft seal' ? 'auto' : 0 }}>{partN == 'eg : Crankshaft seal' ? "Please Select Part Name." : ""}</Text>
                                }
                            </View>
                            <View style={{ height: 30 }} />
                            <Button
                                name="Get car part quote"
                                onPress={() => _submitRequest()}
                                gesture={true}
                            />
                            <View style={{ height: 100 }} />
                        </View>
                    }
                    {parttype == 'car' &&
                        <View>
                            <View style={{ height: 30 }} />
                            <MultipleRadio
                                label="WHAT SERVICE PART DO YOU NEED?*"
                                data={partsRedux}
                                onChange={(t) => onChangeservice(t)}
                            />
                            {showError &&
                                <Text style={{ color: 'red', height: partsRedux.length == 0 ? 'auto' : 0 }}>{partsRedux.length == 0 ? "Please Select Part Name." : ""}</Text>
                            }
                            <View style={{ height: 30 }} />
                            <Button
                                name="Get car part quote"
                                onPress={() => _submitRequest()}
                                loading={loading}
                                gesture={true}
                            />
                            <View style={{ height: 30 }} />
                        </View>
                    }
                </ScrollView>
            </SafeAreaView>
        </View>
    )
}



const styles = StyleSheet.create({
    image: {
        width: 50,
        height: 50,
        marginBottom: 15
    },
    text: {
        color: '#363131',
        fontFamily: fontFamilyRegular,
        fontSize: 12,
        textAlign: 'center'
    },
    boxStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFFFFF',
        borderRadius: 5,
        padding: 20,
        flex: 1
    },
    multipleBoxItemMain: {
        flexDirection: 'row', marginTop: 20
    },
    miniBox1: {
        flex: 1,
        marginRight: 10,
        borderRadius: 5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 2,
    },
    miniBox2: {
        flex: 1, marginLeft: 10,
        borderRadius: 5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 2,
    },
    backgroundBox1: {
        width: windowHeight - 100, height: windowHeight - 100, backgroundColor: "rgba(202,225,252,0.2)", borderRadius: windowHeight, marginLeft: -windowWidth + 100, marginTop: -windowHeight / 5, zIndex: 1, position: "absolute"
    },
    backgroundBox2: {
        width: windowWidth - 100, height: windowWidth - 100, backgroundColor: "rgba(244,224,234,0.2)", borderRadius: windowWidth, zIndex: 1, marginTop: -windowWidth / 3, marginLeft: - windowWidth / 3, position: "absolute", top: windowHeight / 1.7
    }
})