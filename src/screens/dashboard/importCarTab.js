import React from 'react';
import { TouchableOpacity, View, Text, Image, StyleSheet, ScrollView, SafeAreaView, StatusBar, Dimensions, TextInput, KeyboardAvoidingView, Platform } from "react-native"
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { fontFamilyMedium, fontFamilyRegular, fontFamilyLight, fontFamilyBold } from '../../global/globalStyle'
import Button from '../../components/Button'
import { useSelector, useDispatch } from 'react-redux'
import SelectOption from '../../components/SelectOption'
import Header from '../../components/Header'
import Input from '../../components/Input'
import Slider from "react-native-slider";
import RangeSlider from 'rn-range-slider';
import { Thumb, Rail, RailSelected, Notch, Label } from './SliderUI/SliderUI'
import axios from 'axios'
import Api from '../../api'
import AsyncStorage from '@react-native-community/async-storage'
import { useFocusEffect } from '@react-navigation/native';
import Toast from 'react-native-simple-toast'
import {
    setDontKnowCarMakes, setBodyType as setBodyTypeRedux, setFuelType as setFuelTypeRedux,
    setCarEngineRedux,
    setCarYearlRedux,
    setCarModelRedux,
    setCarColorRedux,
    updateRequests,
    setPartEditable
} from '../../redux'
const Tab = createBottomTabNavigator();
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
export default function ImportCarTab(props) {
    const dispatch = useDispatch();
    const knowcarModel = useSelector(state => state.importCar.knowcarModel);
    const knwoCarEngine = useSelector(state => state.importCar.knwoCarEngine);
    const knowYearofManufacture = useSelector(state => state.importCar.knowYearofManufacture);
    const knowColor = useSelector(state => state.importCar.knowColor);
    const dknowminmaxMilage = useSelector(state => state.importCar.dknowminmaxMilage);
    const dknowminMaxBudget = useSelector(state => state.importCar.dknowminMaxBudget);
    const [loading, setLoading] = React.useState(false)
    const [dataLoading, setDataLoading] = React.useState(false);
    const [knownUnknown, setKnownUnknown] = React.useState(props.route.params.thetype)
    const [carMakes, setCarMakes] = React.useState('ex. Audi')
    const [carModels, setCarModels] = React.useState('ex. 001')
    const [carEngines, setCarEngines] = React.useState('ex. 1000 cc')
    const [manufactureYear, setManufactureYear] = React.useState('ex. 2017')
    const [carColor, setCarColor] = React.useState('')
    const [bodyType, setBodyType] = React.useState('ex. saloon')
    const [fuelType, setFuelType] = React.useState('ex. Petrol')
    const [milage, setMilage] = React.useState(dknowminmaxMilage.min)
    const [budget, setBudget] = React.useState(dknowminMaxBudget.min)
    const [showError, setShowError] = React.useState(false);
    const importData = useSelector(state => state.importCar.importCarMakes);
    const dknowImportMakeCar = useSelector(state => state.importCar.dknowImportMakeCar);
    const dknowBodyType = useSelector(state => state.importCar.dknowBodyType);
    const dknowFuelType = useSelector(state => state.importCar.dknowFuelType);
    const allCars = useSelector(state => state.importCar.allCars);
    const [budgetlow, setbudgetLow] = React.useState(1000000);
    const [budgethigh, setbudgetHigh] = React.useState(9000000);
    const [milagelow, setmilageLow] = React.useState(10000);
    const [milagehigh, setmilageHigh] = React.useState(130000);
    const isEditable = useSelector(state => state.servicedata.isEditing)
    const [workAuto, setWorkAuto] = React.useState(false);
    const towingId = useSelector(state => state.servicedata.towingId)
    const _submitData = () => {
        setShowError(false);
        if (knownUnknown == 'known') {
            if (carMakes == 'ex. Audi') {
                setShowError(true);
            } else if (carModels == 'ex. 001') {
                setShowError(true);
            } else if (carEngines == 'ex. 1000 cc') {
                setShowError(true);
            } else if (manufactureYear == "ex. 2017") {
                setShowError(true);
            }
            else {
                gotoNextPage();
            }
        } else {
            if (carMakes == 'ex. Audi') {
                setShowError(true);
            } else if (bodyType == 'ex. saloon') {
                setShowError(true);
            } else if (fuelType == 'ex. Petrol') {
                setShowError(true);
            } else {
                gotoNextPage();
            }
        }
    }
    const _submitReq = async () => {
        try {
            setLoading(true)
            const value = await AsyncStorage.getItem('token')
            axios.post(Api + "/api/getRescueRequest", {

            }, {
                headers: { 'authorization': value }
            }
            )
                .then((res) => {
                    if (res.data.status == true) {
                        console.log("===>>>>", res.data.data)
                        dispatch(updateRequests(res.data.data))
                        setLoading(false)
                    }
                })
                .catch((error) => {
                    console.log(error.message)
                    setLoading(false)
                    Toast.show(JSON.stringify(error))
                })
        }
        catch (err) {
            setLoading(false)
            console.log(err)
        }
    }
    useFocusEffect(
        React.useCallback(() => {

            if (isEditable == true) {


            }
        }, [isEditable])
    );
    const gotoNextPage = async () => {
        setLoading(true)
        const car = "5f734be7f77c365a6c1106b2"
        const value = await AsyncStorage.getItem('token')
        let price = "0";
        let a = {
            importType: "car",
            type: "know",
            carMakes: carMakes,
            carModel: carModels,
            engineSize: carEngines,
            year: manufactureYear,
            color: carColor,
            importService: car,
            budget: { min: price, max: price },
            candf: { min: price, max: price },
            duty: 0,
        }
        if (knownUnknown == 'unknown') {
            a = {
                importType: "car",
                type: "donot",
                carMakes: carMakes,
                bodyType: bodyType,
                fuelType: fuelType,
                milage: JSON.stringify({ min: milagelow, max: milagehigh }),
                importService: car,
                budget: { min: budgetlow, max: budgethigh },
                candf: { min: budgetlow, max: budgethigh },
                duty: 0,
                year: 2007
            }
        }

        if (isEditable == true) {
            a['id'] = towingId;
            axios.post(Api + "/api/editimportservice", a, {
                headers: { 'authorization': value }
            })
                .then((res) => {

                    _submitReq()
                    setLoading(false)
                    props.navigation.navigate('ImportCarConfirmRequest', {
                        knowUnknown: knownUnknown,
                        carMakes: carMakes,
                        carModels: carModels,
                        carEngines: carEngines,
                        manufactureYear: manufactureYear,
                        carColor: carColor,
                        bodyType: bodyType,
                        fuelType: fuelType,
                        milage: milage,
                        budget: budget,
                        oid: towingId
                    })

                })
                .catch((err) => {
                    setLoading(false)
                    console.log(err)
                    Toast.show("OOPS Error Occured");
                })
        } else {
            axios.post(Api + "/api/importservice", a, {
                headers: { 'authorization': value }
            })
                .then((res) => {
                    _submitReq()
                    setLoading(false)
                    props.navigation.navigate('ImportCarConfirmRequest', {
                        knowUnknown: knownUnknown,
                        carMakes: carMakes,
                        carModels: carModels,
                        carEngines: carEngines,
                        manufactureYear: manufactureYear,
                        carColor: carColor,
                        bodyType: bodyType,
                        fuelType: fuelType,
                        milage: milage,
                        budget: budget,
                        oid: res.data.carAdded._id
                    })
                })
                .catch((err) => {
                    setLoading(false)
                    console.log(err)
                })
        }

    }

    const createSearchableArray = (array) => {
        let arr = [];
        for (let i in array) {
            let a = { name: array[i], value: array[i], show: true }
            arr.push(a);
        }
        return arr;
    }

    React.useEffect(() => {
        setDataLoading(true);
        axios.post(Api + "/api/car-models", { carMake: carMakes }, {})
            .then((res) => {
                if (res.data.status) {
                    dispatch(setCarModelRedux(createSearchableArray(res.data.models)));
                    if (isEditable != true || workAuto == true) {
                        if (!res.data.models.includes(carMakes)) {
                            setCarModels('ex. 001')
                            setCarEngines('ex. 1000 cc')

                        }
                    }
                }
                setDataLoading(false);
            })
            .catch((err) => {
                console.log(err);
                setDataLoading(false);
            })

    }, [carMakes])
    React.useEffect(() => {
        setDataLoading(true);
        axios.post(Api + "/api/engine-capacity", { carMake: carMakes, carModel: carModels }, {})
            .then((res) => {
                if (res.data.status) {
                    dispatch(setCarEngineRedux(createSearchableArray(res.data.engines)));
                    if (isEditable != true || workAuto == true) {
                        if (!res.data.engines.includes(carEngines)) {
                            setCarEngines('ex. 1000 cc')
                        }
                    }
                }
                setDataLoading(false);
            })
            .catch((err) => {
                console.log(err);
                setDataLoading(false);
            })
    }, [carModels])
    React.useEffect(() => {
        setTimeout(() => {
            setWorkAuto(true);
        }, 1000)
    }, [])
    return (
        <View style={styles.mainContainer}>
            <StatusBar
                barStyle="light-content"
                backgroundColor="#276EF1"
            />

            <View
                style={styles.bgBox1}
            />
            <View
                style={styles.bgBox2}
            />

            <View style={styles.topBlueLayer}></View>
            <SafeAreaView style={{ flex: 1, zIndex: 2 }}>
                <View style={styles.headerView1}>
                    <TouchableOpacity style={styles.headerBackTouch}
                        onPress={() => props.navigation.goBack()}
                    >
                        <Image
                            source={require('../../assets/images/Left.png')}
                            style={{ width: 24, height: 24 }}
                        />
                    </TouchableOpacity>
                    <View style={{ flex: 1, justifyContent: "center" }}>
                        <Text style={styles.headerText}>
                            {props.route.params.title}
                        </Text>
                    </View>
                    <View style={{ width: 80 }}>

                    </View>
                </View>
                <KeyboardAvoidingView style={{ flex: 1 }} behavior={Platform.OS == 'ios' ? "padding" : "height"}>
                    <ScrollView style={{ padding: 10, marginTop: 0 }}>
                        <View>
                            <Text style={styles.description}>{props.route.params.description}</Text>
                        </View>

                        {knownUnknown == 'known' &&
                            <View>
                                <View style={{ zIndex: 20 }}>
                                    <View style={{ height: 30 }} />
                                    <SelectOption
                                        label="WHAT IS THE MAKE OF THE CAR YOU WANT?*"
                                        placeholder={carMakes}
                                        onChangeText={(t) => setCarMakes(t)}
                                        returnArray={(t) => {
                                            dispatch(setDontKnowCarMakes(t))
                                        }}
                                        data={dknowImportMakeCar}
                                    />
                                    {showError &&
                                        <Text style={{ color: 'red', height: carMakes == 'ex. Audi' ? 'auto' : 0 }}>{carMakes == 'ex. Audi' ? "Please Select Car Make." : ""}</Text>
                                    }
                                </View>
                                <View style={{ zIndex: 19 }}>
                                    <View style={{ height: 30 }} />
                                    <SelectOption
                                        label="WHAT IS THE MODEL?*"
                                        placeholder={carModels}
                                        onChangeText={(t) => setCarModels(t)}
                                        returnArray={(t) => {
                                            dispatch(setCarModelRedux(t))
                                        }}
                                        data={knowcarModel}
                                    />
                                    {showError &&
                                        <Text style={{ color: 'red', height: carModels == 'ex. 001' ? 'auto' : 0 }}>{carModels == 'ex. 001' ? "Please Select Car Model." : ""}</Text>
                                    }
                                </View>
                                <View style={{ zIndex: 18 }}>
                                    <View style={{ height: 30 }} />
                                    <SelectOption
                                        label="WHAT IS THE ENGINE SIZE?*"
                                        placeholder={carEngines}
                                        onChangeText={(t) => setCarEngines(t)}
                                        returnArray={(t) => {
                                            dispatch(setCarEngineRedux(t))
                                        }}
                                        data={knwoCarEngine}
                                    />
                                    {showError &&
                                        <Text style={{ color: 'red', height: carEngines == 'ex. 1000 cc' ? 'auto' : 0 }}>{carEngines == 'ex. 1000 cc' ? "Please Select Car Engine Size." : ""}</Text>
                                    }
                                </View>
                                <View style={{ zIndex: 17 }}>
                                    <View style={{ height: 30 }} />
                                    <SelectOption
                                        label="WHAT IS THE YEAR OF MANUFACTURE?"
                                        placeholder={manufactureYear}
                                        onChangeText={(t) => setManufactureYear(t)}
                                        returnArray={(t) => {
                                            dispatch(setCarYearlRedux(t))
                                        }}
                                        data={knowYearofManufacture}
                                    />
                                    {showError &&
                                        <Text style={{ color: 'red', height: manufactureYear == 'ex. 2017' ? 'auto' : 0 }}>{manufactureYear == 'ex. 2017' ? "Please Select Car Manufacture Year." : ""}</Text>
                                    }
                                </View>
                                <View style={{ zIndex: 16 }}>
                                    <View style={{ height: 30 }} />
                                    <View style={{ backgroundColor: '#F8F8F8', borderRadius: 5, borderWidth: 1, borderColor: '#EFF1F7' }}>
                                        <TextInput
                                            placeholder={"ex : Red"}
                                            style={{ fontSize: 15, fontFamily: fontFamilyRegular, padding: 15, fontFamily: fontFamilyRegular, color: '#9D9D9D' }}
                                            placeholderTextColor="#B2BAC4"
                                            onChangeText={(t) => setCarColor(t)}
                                            value={carColor}
                                        />
                                    </View>
                                </View>
                                <View style={{ height: 30 }} />
                                <Button
                                    name="Get car import quote"
                                    onPress={() => {
                                        _submitData()
                                    }}
                                />
                                <View style={{ height: 120 }} />
                            </View>
                        }
                        {knownUnknown == 'unknown' &&
                            <View>
                                <View style={{ zIndex: 20 }}>
                                    <View style={{ height: 30 }} />
                                    <SelectOption
                                        label="WHAT IS THE MAKE OF THE CAR YOU WANT?*"
                                        placeholder={carMakes}
                                        onChangeText={(t) => setCarMakes(t)}
                                        returnArray={(t) => {
                                            dispatch(setDontKnowCarMakes(t))
                                        }}
                                        data={dknowImportMakeCar}
                                    />
                                    {showError &&
                                        <Text style={{ color: 'red', height: carMakes == 'ex. Audi' ? 'auto' : 0 }}>{carMakes == 'ex. Audi' ? "Please Select Car Make." : ""}</Text>
                                    }
                                </View>
                                <View style={{ zIndex: 19 }}>
                                    <View style={{ height: 30 }} />
                                    <SelectOption
                                        label="WHAT IS THE BODY TYPE?*"
                                        placeholder={bodyType}
                                        onChangeText={(t) => setBodyType(t)}
                                        returnArray={(t) => {
                                            dispatch(setBodyTypeRedux(t))
                                        }}
                                        data={dknowBodyType}
                                    />
                                    {showError &&
                                        <Text style={{ color: 'red', height: bodyType == 'ex. saloon' ? 'auto' : 0 }}>{bodyType == 'ex. saloon' ? "Please Select Car Body Type." : ""}</Text>
                                    }
                                </View>
                                <View style={{ zIndex: 18 }}>
                                    <View style={{ height: 30 }} />
                                    <SelectOption
                                        label="WHAT IS THE FUEL TYPE?*"
                                        placeholder={fuelType}
                                        onChangeText={(t) => setFuelType(t)}
                                        returnArray={(t) => {
                                            dispatch(setFuelTypeRedux(t))
                                        }}
                                        data={dknowFuelType}
                                    />
                                    {showError &&
                                        <Text style={{ color: 'red', height: fuelType == 'ex. Petrol' ? 'auto' : 0 }}>{fuelType == 'ex. Petrol' ? "Please Select Fuel Type." : ""}</Text>
                                    }
                                </View>
                                <View style={{ height: 30 }} />
                                <View>
                                    <View style={{ flex: 1 }}>
                                        <Text style={styles.rangeHeader}>WHAT IS THE MILEAGE RANGE?</Text>
                                    </View>
                                    <View style={styles.budgetRow}>
                                        <View style={{ flex: 1, marginLeft: 5 }}>
                                            <Text style={styles.budgetLeft}>{milagelow}km</Text>
                                        </View>
                                        <View style={{ flex: 1, marginRight: 5 }}>
                                            <Text style={styles.budgetRight}>{milagehigh}km</Text>
                                        </View>

                                    </View>
                                </View>
                                <RangeSlider
                                    style={{ marginTop: 8 }}
                                    min={1000}
                                    max={150000}
                                    step={1}
                                    low={milagelow}
                                    high={milagehigh}
                                    floatingLabel={true}
                                    onValueChanged={(low, high) => {
                                        setmilageLow(low);
                                        setmilageHigh(high);
                                    }}
                                    disableRange={false}
                                    renderThumb={() => {
                                        return (
                                            <Thumb />
                                        )
                                    }}
                                    renderRail={() => {
                                        return (
                                            <Rail />
                                        )
                                    }}
                                    renderRailSelected={() => {
                                        return (
                                            <RailSelected />
                                        )
                                    }}
                                    renderLabel={(t) => {
                                        return (
                                            <Label text={t} />
                                        )
                                    }}
                                    renderNotch={() => {
                                        return (
                                            <Notch />
                                        )
                                    }}
                                />
                                <View style={{ height: 30 }} />
                                <View>
                                    <View style={{ flex: 1 }}>
                                        <Text style={styles.rangeHeader}>WHAT IS YOUR BUDGET RANGE?</Text>
                                    </View>
                                    <View style={styles.budgetRow}>
                                        <View style={{ flex: 1, marginLeft: 5 }}>
                                            <Text style={styles.budgetLeft}>{budgetlow}ksh</Text>
                                        </View>
                                        <View style={{ flex: 1, marginRight: 5 }}>
                                            <Text style={styles.budgetRight}>{budgethigh}ksh</Text>
                                        </View>

                                    </View>
                                </View>
                                <RangeSlider
                                    style={{ marginTop: 8 }}
                                    min={400000}
                                    max={10000000}
                                    step={100}
                                    low={budgetlow}
                                    high={budgethigh}
                                    floatingLabel={true}
                                    onValueChanged={(low, high) => {
                                        setbudgetLow(low);
                                        setbudgetHigh(high);
                                    }}
                                    disableRange={false}
                                    renderThumb={() => {
                                        return (
                                            <Thumb />
                                        )
                                    }}
                                    renderRail={() => {
                                        return (
                                            <Rail />
                                        )
                                    }}
                                    renderRailSelected={() => {
                                        return (
                                            <RailSelected />
                                        )
                                    }}
                                    renderLabel={(t) => {
                                        return (
                                            <Label text={t} />
                                        )
                                    }}
                                    renderNotch={() => {
                                        return (
                                            <Notch />
                                        )
                                    }}
                                />

                                <View style={{ height: 30 }} />
                                <Button
                                    name={knownUnknown == 'unknown' ? "Help me find a car" : "Send me a quote"}
                                    onPress={() => {
                                        _submitData()
                                    }}
                                    loading={loading}
                                />
                                <View style={{ height: 30 }} />
                            </View>
                        }
                    </ScrollView>
                </KeyboardAvoidingView>
            </SafeAreaView>
        </View>
    )
}



const styles = StyleSheet.create({
    image: {
        width: 50,
        height: 50,
        marginBottom: 15
    },
    text: {
        color: '#363131',
        fontFamily: fontFamilyRegular,
        fontSize: 12,
        textAlign: 'center'
    },
    boxStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFFFFF',
        borderRadius: 5,
        padding: 20,
        flex: 1
    },
    multipleBoxItemMain: {
        flexDirection: 'row', marginTop: 20
    },
    miniBox1: {
        flex: 1,
        marginRight: 10,
        borderRadius: 5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 2,
    },
    miniBox2: {
        flex: 1, marginLeft: 10,
        borderRadius: 5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 2,
    },
    mainContainer: {
        backgroundColor: '#FFFFFF', flex: 1
    },
    bgBox1: {
        width: windowHeight - 100, height: windowHeight - 100, backgroundColor: "rgba(202,225,252,0.2)", borderRadius: windowHeight, marginLeft: -windowWidth + 100, marginTop: -windowHeight / 5, zIndex: 1, position: "absolute"
    },
    bgBox2: {
        width: windowWidth - 100, height: windowWidth - 100, backgroundColor: "rgba(244,224,234,0.2)", borderRadius: windowWidth, zIndex: 1, marginTop: -windowWidth / 3, marginLeft: - windowWidth / 3, position: "absolute", top: windowHeight / 1.7
    },
    topBlueLayer: {
        backgroundColor: "#276EF1", height: 40, zIndex: 2
    },
    headerView1: {
        flexDirection: "row", paddingTop: 15
    },
    headerBackTouch: {
        padding: 10, width: 80
    },
    headerText: {
        color: "#34495E", textAlign: "center", fontSize: 22, fontFamily: fontFamilyBold
    },
    description: {
        fontSize: 15, fontFamily: fontFamilyRegular, color: '#34495E'
    },
    rangeHeader: {
        color: '#34495E', fontFamily: fontFamilyRegular
    },
    budgetLeft: {
        color: '#276EF1', fontFamily: fontFamilyRegular
    },
    budgetRight: {
        color: '#276EF1', fontFamily: fontFamilyRegular, textAlign: 'right'
    },
    budgetRow: {
        flexDirection: "row", marginTop: 5
    }
})