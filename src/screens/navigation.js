import React from 'react';
import { View, Text, Vibration, AppState } from 'react-native'
import { NavigationContainer } from '@react-navigation/native'
import AuthIndex from './auth/index'
import CarIndex from './car/index'
import DashboardIndex from './dashboard/index'
import { useSelector, useDispatch } from 'react-redux';
import { updateState } from '../redux';
import { updateStates, updateRequests } from '../redux/'
import AsyncStorage from '@react-native-community/async-storage';
import messaging from '@react-native-firebase/messaging';
import { updateSteps, addSteps } from '../redux';
import Toast from 'react-native-simple-toast';
import Api from '../api';
import SplashScreen from '../screens/auth/SplashScreen'
import SignUp from '../screens/auth/SignUp'

const axios = require('axios').default
export default function Navigation() {
    const dispatch = useDispatch()
    let navState = useSelector(state => state.navigation.navigationPage);
    const reduxSteps = useSelector(state => state.steps.activeSteps)
    const reques = useSelector(state => state.user.requests)

    async function requestUserPermission() {
        const authStatus = await messaging().requestPermission();
        const enabled =
            authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
            authStatus === messaging.AuthorizationStatus.PROVISIONAL;
        if (enabled) {

        }
    }
    const _submitRequest = async () => {
        try {
            const value = await AsyncStorage.getItem('token')
            axios.post(Api + "/api/getRescueRequest", {}, {
                headers: { 'authorization': value }
            })
                .then((res) => {
                    if (res.data.status == true) {
                        dispatch(updateRequests(res.data.data))
                    }
                })
                .catch((error) => {
                    console.log(error)
                })
        }
        catch (err) {
            console.log(err)
        }
    }

    React.useEffect(() => {
        let asta = AppState.addEventListener("change", (state) => {
            if (state == 'active') {
                _submitRequest()
            }
        })
        return () => asta;
    }, []);
    React.useEffect(() => {
        const unsubscribe = messaging().setBackgroundMessageHandler(async remoteMessage => {
            // console.log('Forground BACKGROUND', remoteMessage);
            _submitRequest()

        });
        return unsubscribe;
    }, [])

    React.useEffect(() => {
        const unsubscribe = messaging().onMessage(async remoteMessage => {
            _submitRequest()
            Vibration.vibrate(1000);
            Toast.show('Service status update')
        });
        return unsubscribe;
    }, []);
    React.useEffect(() => {
        requestUserPermission()
    }, [false])

    const [loaded, setLoaded] = React.useState(null);



    return (
        <NavigationContainer>
            {navState == 'splash' &&
                <SplashScreen />
                // <SignUp />
            }
            {navState == 'auth' &&
                <AuthIndex />
            }
            {navState == 'Dashboard' &&
                <DashboardIndex />
            }
            {navState == 'Car' &&
                <CarIndex />
            }
        </NavigationContainer>
    )

}