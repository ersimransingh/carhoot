import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import AddCar from './AddCar';
import AddPhoneNumber from './AddPhoneNumber';
import VerificationCode from './VerificationCode';
const Stack = createStackNavigator();

export default function CarIndex() {
    return (
        <Stack.Navigator
            screenOptions={{
                headerShown: false
            }}
        >
            <Stack.Screen name="AddCar" component={AddCar} />
            <Stack.Screen name="AddPhoneNumber" component={AddPhoneNumber} />
            <Stack.Screen name="VerificationCode" component={VerificationCode} />
        </Stack.Navigator>
    )
}