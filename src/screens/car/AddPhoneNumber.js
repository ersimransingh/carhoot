import React from 'react';
import { View, Text, StatusBar, SafeAreaView, KeyboardAvoidingView, Platform } from 'react-native'
import Header from '../../components/Header'
import Input from '../../components/Input'
import ContactInput from '../../components/ContactInput'
import Button from '../../components/Button'
import { fontFamilyMedium, fontFamilyRegular, fontFamilyLight } from '../../global/globalStyle'
import { ScrollView } from 'react-native-gesture-handler';
export default function AddPhoneNumber(props) {
    const [countryCode, setCountryCode] = React.useState('254');
    const [contact, setContact] = React.useState('');
    const [contactv, setContactv] = React.useState(false);
    const [countryAlpha, setCountryAlpha] = React.useState('KE')
    React.useEffect(() => {
        if (contact.length > 7 && contact.length <= 13) {
            setContactv(true)
        } else {
            setContactv(false)
        }
    }, [contact])
    return (
        <View style={{ flex: 1, backgroundColor: 'white' }}>
            <StatusBar
                barStyle="light-content"
            />
            <SafeAreaView style={{ flex: 1 }}>
                <Header
                    backButton={true}
                    {...props}
                />
                <KeyboardAvoidingView style={{ flex: 1 }} behavior={Platform.OS == 'ios' ? "padding" : "height"}>
                    <ScrollView style={{ padding: 15 }}>
                        <View style={{ height: 30 }} />
                        <Text style={{ fontFamily: fontFamilyMedium, fontSize: 16, color: '#9D9D9D' }}>Phone Number</Text>
                        <Text style={{ fontFamily: fontFamilyRegular, fontSize: 16, color: '#9D9D9D' }}>Please enter your phone number to allow access to the services we offer.</Text>
                        <Text style={{ fontFamily: fontFamilyMedium, fontSize: 16, color: '#9D9D9D', marginTop: 10 }}>WHY?</Text>
                        <Text style={{ fontFamily: fontFamilyLight, fontSize: 16, color: '#9D9D9D' }}>Our services require phone contact for</Text>
                        <ContactInput
                            label=""
                            placeholder="Phone Number"
                            value={contact}
                            valueCountryCode={countryCode}
                            onChangeText={(t) => setContact(t)}
                            onChangeCountryCode={(t) => setCountryCode(t)}
                            isValid={contactv}
                            cc2={countryAlpha}
                            onUpdateCountryAlpha={(t) => setCountryAlpha(t)}
                        />

                        <View style={{ height: 30 }} />
                        <Button
                            name="Send Verification Code"
                            onPress={() => props.navigation.navigate('VerificationCode', { contact: countryCode + " " + contact })}
                        />
                    </ScrollView>
                </KeyboardAvoidingView>
            </SafeAreaView>
        </View>
    )
}