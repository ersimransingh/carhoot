import React from 'react';
import { View, Text, StatusBar, SafeAreaView, KeyboardAvoidingView, Platform } from 'react-native'
import Header from '../../components/Header'
import Input from '../../components/Input'
import OTP from '../../components/OTP'
import Button from '../../components/Button'
import { fontFamilyMedium, fontFamilyRegular, fontFamilyLight } from '../../global/globalStyle'
import { ScrollView } from 'react-native-gesture-handler';
import { useSelector, useDispatch } from 'react-redux';
import { updateState } from '../../redux'

export default function VerificationCode(props) {

    const dispatch = useDispatch()
    const navState = useSelector(state => state.navigation.navigationPage);
    const [code, setCode] = React.useState('');
    const Verify = () => {

        dispatch(updateState('Dashboard'))
    }
    const [seconds, setSeconds] = React.useState(45)
    const [reset, setReset] = React.useState(true)
    const [dis, setDis] = React.useState(true)

    React.useEffect(() => {
        let interval = null;
        if (reset) {
            interval = setInterval(() => {
                setSeconds(seconds => seconds - 1);
            }, 1000);
            if (seconds == 0) {
                clearInterval(interval)

                setDis(false)
                setReset(false)
            }
        }
        return () => {
            clearInterval(interval)
        };
    }, [reset, seconds]);

    return (
        <View style={{ flex: 1, backgroundColor: 'white' }}>
            <StatusBar
                barStyle="light-content"
            />
            <SafeAreaView style={{ flex: 1 }}>
                <Header
                    backButton={true}
                    {...props}
                />
                <KeyboardAvoidingView style={{ flex: 1 }} behavior={Platform.OS == 'ios' ? "padding" : "height"}>
                    <ScrollView style={{ padding: 15 }}>
                        <View style={{ height: 30 }} />
                        <Text style={{ fontFamily: fontFamilyMedium, fontSize: 16, color: '#9D9D9D' }}>Verification Code</Text>
                        <Text style={{ fontFamily: fontFamilyLight, fontSize: 16, color: '#9D9D9D' }}>Please enter the 4 digit verification code send to you via SMS on {this.props.params.contact}</Text>
                        <View style={{ height: 30 }} />
                        <OTP
                            onChangeText={(t) => setCode(t)}
                        />
                        <TouchableOpacity style={{ paddingTop: 30 }}
                            disabled={dis}
                            onPress={() => {
                                setDis(true)
                            }}
                        >{seconds == 0
                            &&
                            <Text style={{ color: '#0AC97E', fontFamily: fontFamilyLight, fontSize: 16, textAlign: 'center' }}>Resend code
                                    </Text>
                            }
                            {seconds > 0 &&
                                <Text style={{ color: '#0AC97E', fontFamily: fontFamilyLight, fontSize: 16, textAlign: 'center' }}>Resend code in {'00:'}{
                                    seconds.length == 1 || seconds == 0 ? '0' + seconds : seconds
                                }</Text>
                            }
                        </TouchableOpacity>
                        <View style={{ height: 30 }} />
                        <Button
                            name="Verify Code"
                            onPress={() => {
                                Verify()
                            }}
                        />
                    </ScrollView>
                </KeyboardAvoidingView>
            </SafeAreaView>
        </View>
    )
}