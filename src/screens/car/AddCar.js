import React from 'react';
import { View, Text, StatusBar, SafeAreaView, KeyboardAvoidingView, Platform } from 'react-native'
import Header from '../../components/Header'
import Input from '../../components/Input'
import SelectOption from '../../components/SelectOption'
import Devider from '../../components/Devider'
import Button from '../../components/Button'
import { ScrollView } from 'react-native-gesture-handler';
export default function AddCar(props) {
    const [registrationNumber, setRegistrationNumber] = React.useState('');
    const [vregistrationNumber, setvRegistrationNumber] = React.useState(false);
    const [chassiNumber, setChassiNumber] = React.useState('');
    const [vchassiNumber, setvChassiNumber] = React.useState(false);
    const [carMakes, setCarMakes] = React.useState('ex. Audi')
    const [carModels, setCarModels] = React.useState('ex. 001')
    const [carEngines, setCarEngines] = React.useState('ex. 1000 cc')
    const carMake = [
        { name: 'Audi', value: '1' },
        { name: 'BMW', value: '2' },
        { name: 'Ford', value: '3' },
        { name: 'Audi2', value: '4' },
        { name: 'BMW2', value: '5' },
        { name: 'Ford2', value: '6' },
        { name: 'Audi3', value: '7' },
        { name: 'BMW3', value: '8' },
        { name: 'Ford3', value: '9' },
    ]
    const carModel = [
        { name: '001', value: '1' },
        { name: '002', value: '2' },
        { name: '003', value: '3' },
        { name: '004', value: '4' },
        { name: '005', value: '5' },
        { name: '006', value: '6' },
    ]
    const carEngine = [
        { name: '1000 CC', value: '1' },
        { name: '2000 CC', value: '2' },
        { name: '3000 CC', value: '3' },
        { name: '1000 DD', value: '4' },
        { name: '2000 DD', value: '5' },
        { name: '3000 DD', value: '6' },
    ]
    React.useEffect(() => {
        if (registrationNumber.length > 4) {
            setvRegistrationNumber(true);
        } else {
            setvRegistrationNumber(false);
        }
        if (chassiNumber.length > 5) {
            setvChassiNumber(true)
        } else {
            setvChassiNumber(false)
        }
    }, [registrationNumber, chassiNumber])
    return (
        <View style={{ flex: 1, backgroundColor: 'white' }}>
            <StatusBar
                barStyle="light-content"
            />
            <SafeAreaView style={{ flex: 1 }}>
                <Header
                    backButton={false}
                    {...props}
                />
                <KeyboardAvoidingView style={{ flex: 1 }} behavior={Platform.OS == 'ios' ? "padding" : "height"}>
                    <ScrollView style={{ padding: 15 }}>
                        <Input
                            label="WHAT IS YOUR CAR REGISTRATION NUMBER?*"
                            placeholder="ex. KSQ0012"
                            value={registrationNumber}
                            onChangeText={(t) => setRegistrationNumber(t)}
                            isValid={vregistrationNumber}
                        />
                        <View style={{ height: 30 }} />
                        <SelectOption
                            label="WHAT IS THE MAKE OF YOUR CAR?"
                            placeholder={carMakes}
                            onChangeText={(t) => setCarMakes(t)}
                            data={carMake}
                        />
                        <View style={{ height: 30 }} />
                        <SelectOption
                            label="WHAT IS THE MODEL OF YOUR CAR?"
                            placeholder={carModels}
                            onChangeText={(t) => setCarModels(t)}
                            data={carModel}
                        />
                        <View style={{ height: 30 }} />
                        <SelectOption
                            label="WHAT IS THE ENGINE SIZE OF YOUR CAR?"
                            placeholder={carEngines}
                            onChangeText={(t) => setCarEngines(t)}
                            data={carEngine}
                        />
                        <View style={{ height: 40 }} />
                        <Devider />
                        <View style={{ height: 40 }} />
                        <Input
                            label="WHAT IS THE CHASIS NUMBER OF YOUR CAR?*"
                            placeholder="ex. KSQ0012"
                            value={chassiNumber}
                            onChangeText={(t) => setChassiNumber(t)}
                            isValid={vchassiNumber}
                        />
                        <View style={{ height: 30 }} />
                        <Button
                            name="Add New Car"
                            onPress={() => props.navigation.navigate('AddPhoneNumber')}
                        />
                        <View style={{ height: 30 }} />
                    </ScrollView>
                </KeyboardAvoidingView>
            </SafeAreaView>
        </View>
    )
}