import React from 'react';
import { View, Dimensions, Text, KeyboardAvoidingView, Platform, Image, SafeAreaView, ScrollView, TouchableOpacity, Linking, Modal } from 'react-native';
import MapView, { PROVIDER_GOOGLE, PROVIDER_DEFAULT, Marker } from 'react-native-maps'
import { fontFamilyMedium, fontFamilyRegular, fontFamilyLight, fontFamilyBold, fontFamilySemiBold, buttonColor } from '../../global/globalStyle'
import Header from '../../components/Header'
import Button from '../../components/Button'
import Input from '../../components/Input'
import SelectOption from '../../components/SelectOption'
import Api from '../../api';
import axios from 'axios';
import { useSelector, useDispatch } from 'react-redux';
import { updateLocationsData, updateRequests } from '../../redux'
import moment from 'moment'
import AsyncStorage from '@react-native-community/async-storage';
import { updateUserCarData, updateUserData, updateOpenPageSteps } from '../../redux'
import MapViewDirections from 'react-native-maps-directions';
import { BlurView } from "@react-native-community/blur";
import Toast from 'react-native-simple-toast';
import { MapStyle } from '../../global/mapStyle'

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;


function Widget(props) {
    return (
        <View style={{ flexDirection: 'row' }}>
            <View style={{ paddingRight: 25 }}>
                <Image
                    source={props.checked ? require('../../assets/images/check.png') : require('../../assets/images/uncheck.png')}
                    style={{ width: 20, height: 20 }}
                />
                {!props.isLast &&
                    <View style={{ borderLeftColor: '#276EF1', borderLeftWidth: props.checked ? 1 : 0, borderStyle: "solid", marginLeft: 9, flex: 1 }}>


                    </View>
                }
            </View>
            <View style={{ flex: 1, paddingBottom: 20 }}>
                <Text style={{ fontSize: 12, fontFamily: fontFamilySemiBold, lineHeight: 15 }}>{props.title}</Text>
                <Text style={{ fontSize: 12, fontFamily: fontFamilyLight, lineHeight: 20 }}>{props.description}</Text>

            </View>

        </View>
    )
}

export default function ImportCarProgress(props) {
    const dispatch = useDispatch()
    const [loader, setLoader] = React.useState(false)
    const [loading, setLoading] = React.useState(true);
    const [seteps, setSeteps] = React.useState('[]');

    const [dis, setDis] = React.useState(true)
    const [rating, setRating] = React.useState(props.route.params.rating ? props.route.params.rating : 0)
    const dd = useSelector(state => state.user.requests);
    const [activeData, setActiveData] = React.useState('[]')
    const reques = useSelector(state => state.user.requests)
    const stepsPage = useSelector(state => state.steps.openPageSteps)
    const [price, setPrice] = React.useState(props.route.params.price ? props.route.params.price : 'N/A')
    const [minutes, setMinutes] = React.useState(0)
    const [seconds, setSeconds] = React.useState(0)
    const [whereto, setWhereto] = React.useState('')
    const [isActive, setIsActive] = React.useState(false);
    const [confirmModal, setConfirmModal] = React.useState(false);
    const [stepsId, setStepsId] = React.useState('')
    const reset = () => {
        setIsActive(false);
        setSeconds(0)
    }
    const [ratingModal, setRatingModal] = React.useState(false);
    const [serviceType, setServiceType] = React.useState("Import");
    React.useEffect(() => {
        dispatch(updateOpenPageSteps([]))
        let oid = props.route.params.oid;
        let data = [];
        let price = 0;
        for (let i in reques) {
            if (reques[i]._id == oid) {
                data = reques[i].stepsdone
                price = reques[i].price
            }
        }
        setPrice(price)
        dispatch(updateOpenPageSteps(data))
    }, [reques])
    React.useEffect(() => {
        let interval = null;
        if (isActive) {
            interval = setInterval(() => {
                setSeconds(seconds => seconds - 1);
                if (seconds <= 0) {
                    reset()
                }
            }, 1000);
        }
        return () => clearInterval(interval);
    }, [isActive, seconds]);
    React.useEffect(() => {
        setIsActive(true);
    }, [false])
    let [kk, setKK] = React.useState({})
    const user = useSelector(state => state.servicedata.data);
    let dataRedux = user.map((x) => {
        kk[x._id] = x.rescueservicetype
    })

    const [arr, setArr] = React.useState('[]');
    const _getSteps = async () => {
        setLoading(true);
        const value = await AsyncStorage.getItem('token')
        try {

            axios.get(Api + '/api/getrescuesteps/' + stepsId, {},)
                .then((res) => {
                    if (res.data.steps.length != 0) {
                        let places = res.data.steps;
                        setArr(JSON.stringify(places))
                        setLoading(false)
                    } else {
                        setLoading(false)
                    }
                })
                .catch((error) => {
                    console.log(error)
                    setLoading(false)
                })
        }
        catch (err) {
            Toast.show(err)
            setLoading(false)
        }
    }
    React.useEffect(() => {
        _getSteps()
    }, [stepsId])
    const _submitReq = async () => {
        try {
            setLoader(true)
            const value = await AsyncStorage.getItem('token')
            axios.post(Api + "/api/getRescueRequest", {

            }, {
                headers: { 'authorization': value }
            })
                .then((res) => {
                    if (res.data.status == true) {
                        dispatch(updateRequests(res.data.data))
                        setLoader(false)
                    }
                })
                .catch((error) => {
                    console.log(error.message)
                    setLoader(false)
                    Toast.show(JSON.stringify(error))
                })
        }
        catch (err) {
            setLoader(false)
            console.log(err)
        }
    }
    const rate = (r) => {
        let arr = []
        for (let i = 1; i <= 5; i++) {
            if (i <= rating) {
                arr.push(true);
            } else {
                arr.push(false);
            }
        }
        return (
            <View style={{ padding: 10, marginVertical: 20 }}>
                {/* <Text style={{ fontFamily: fontFamilyLight, fontSize: 16, textAlign: 'center' }}>Please rate your hoot experience</Text> */}
                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: 15 }}>
                    {
                        arr.map((data, index) => {
                            return (
                                <TouchableOpacity
                                    key={index}
                                    style={{ paddingRight: 10 }}
                                    onPress={() => setRating(index + 1)}
                                >
                                    <Image
                                        source={data ? require('../../assets/images/StarDark.png') : require('../../assets/images/StarLight.png')}
                                        style={{ width: 35, height: 34 }}
                                    />
                                </TouchableOpacity>
                            )
                        })
                    }
                </View>
                <View style={{ marginVertical: 30 }}>
                    <Text style={{ fontFamily: fontFamilyBold, color: '#34495E', fontSize: 20, textAlign: 'center' }}>Rate your service</Text>
                </View>
                <View style={{ marginTop: 15 }}>
                    <Button
                        name={"Submit Rating"}
                        onPress={() => {
                            submitRating()
                        }}
                    />
                </View>
            </View>

        )
    }
    const submitRating = () => {
        if (rating > 0) {
            let oid = props.route.params.oid;
            axios.post(Api + '/api/addRating', {
                id: oid,
                rating: rating,
                type: "Import"
            })
                .then((res) => {
                    Toast.show('Thanks for your valuable rating')
                    setRatingModal(false);
                })
                .catch((err) => {
                    console.log(err);
                })


        } else {
            Toast.show('Submit Your Rating')
        }
    }
    const _submitRequest = async () => {
        let data = {}
        for (let i in dd) {
            if (dd[i]._id == props.route.params.oid) {
                data = dd[i]
            }
        }
        let d = {
            type: data.rescuetype + '  Request',
            price: data.price,
            id: props.route.params.id,
            oid: props.route.params.oid,
            name: data.requestType
        }

        try {
            setLoader(true)
            const value = await AsyncStorage.getItem('token')
            axios.post(Api + "/api/changeStatus", {
                "type": "Resuce",
                "id": props.route.params.oid
            }, {
                headers: { 'authorization': value }
            }
            )
                .then((res) => {
                    if (res.data.status == true) {
                        Toast.show('Your request has been received')
                        _submitReq()
                        setLoader(false)
                        props.navigation.navigate('ServiceProgress', d)
                    }
                })
                .catch((error) => {
                    setLoader(false)
                    Toast.show(error)
                })
        }

        catch (err) {
            setLoader(false)
            console.log(err)
        }

    }
    const [location, setLocation] = React.useState('');
    const [carMakes, setCarMakes] = React.useState('Select Car');
    const [modalVisible, setModalVesible] = React.useState(false);
    let whereTo = props.route.params.whereto ? JSON.parse(props.route.params.whereto) : ''
    const cancleRequest = async () => {
        let oid = props.route.params.oid;
        const value = await AsyncStorage.getItem('token')
        axios.post(Api + "/api/changeStatus", {
            "type": "Import",
            "id": props.route.params.oid,
            "status": "Cancelled"

        }, {
            headers: { 'authorization': value }
        }
        )
            .then(() => {
                if (props.route.params.fromOrderPage) {
                    props.navigation.navigate("OrderHistory");
                }
                else {
                    props.navigation.navigate("Tabs")
                }
            })
            .catch((err) => {
                console.log('err', err)
            })
    }
    React.useEffect(() => {

        for (let i in dd) {
            let id = dd[i]._id;
            if (props.route.params.oid == id) {
                console.log('DTA', dd[i]);
                if (props.route.params.name == 'Towing service') {
                    setWhereto(JSON.stringify(dd[i].whereto));
                }
                if (dd[i].type == 'Auto Service') {
                    setServiceType('Auto Service')
                    setStepsId(props.route.params.id);
                } else if (dd[i].type == "Rescue Service") {
                    setServiceType('Rescue Service')
                    setStepsId(dd[i].rescuetype);
                } else {
                    setStepsId(props.route.params.id);
                }

                var startTime = moment();
                var endTime = moment(dd[i].startTime);
                // calculate total duration
                var duration = moment.duration(endTime.diff(startTime));
                // duration in hours
                var hours = parseInt(duration.asHours());
                // duration in minutes
                var minutes = parseInt(duration.asMinutes()) % 60;
                let second = minutes * 60;
                setMinutes(minutes);
                let diff = endTime.diff(startTime, 'seconds')
                console.log('diff', diff);
                setSeconds(diff);

            }
        }
    }, [dd])
    React.useEffect(() => {
        if (stepsPage.length != 0) {
            if (!props.route.params.rating && stepsPage.length == JSON.parse(arr).length) {
                setModalVesible(false);
                setRatingModal(true);
            }
        }
    }, [dd, stepsPage, arr])
    return (
        <View style={{ flex: 1 }} >
            <Modal
                animationType="slide"
                transparent={true}
                visible={ratingModal}
                onRequestClose={() => {
                    setRatingModal(false)
                }}
            >
                <View style={{ backgroundColor: 'rgba(0,0,0,0.5)', flex: 1, padding: 20, justifyContent: 'center' }}>
                    <View style={{ backgroundColor: 'white', borderRadius: 5 }}>
                        {rate(3)}
                    </View>
                </View>
            </Modal>
            <Modal
                animationType="slide"
                transparent={true}
                visible={confirmModal}
                onRequestClose={() => {
                    setConfirmModal(false)
                }}
            >
                {Platform.OS == 'ios' ?
                    <BlurView style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
                        blurType="xlight"
                        blurAmount={10}
                        downsampleFactor={25}

                    >
                        <View style={{ padding: 30, backgroundColor: 'white', justifyContent: 'center', alignItems: 'center', borderRadius: 10 }}

                        >

                            <Text style={{ fontFamily: fontFamilyBold, fontSize: 20, color: '#34495E' }}>Are you sure?</Text>
                            <View style={{ height: 20 }} />
                            <View style={{ flexDirection: 'row' }}>
                                <TouchableOpacity style={{ backgroundColor: "#FF0036", padding: 15, width: 100, borderRadius: 5 }}
                                    onPress={() => {
                                        cancleRequest()
                                        setConfirmModal(false)
                                    }}
                                >
                                    <Text style={{ textAlign: 'center', color: "#FFFFFF", fontSize: 20, fontFamily: fontFamilyBold }}>Yes</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={{ backgroundColor: "#EFF1F7", padding: 15, width: 100, borderRadius: 5, marginLeft: 20 }}
                                    onPress={() => setConfirmModal(false)}
                                >
                                    <Text style={{ textAlign: 'center', color: "#34495E", fontSize: 20, fontFamily: fontFamilyBold }}>No</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </BlurView> :
                    <View style={{ flex: 1, backgroundColor: "rgba(255,255,255,0.7)", justifyContent: 'center', alignItems: 'center' }}>
                        <View style={{ padding: 30, backgroundColor: 'white', justifyContent: 'center', alignItems: 'center', borderRadius: 10 }}

                        >

                            <Text style={{ fontFamily: fontFamilyBold, fontSize: 20, color: '#34495E' }}>Are you sure?</Text>
                            <View style={{ height: 20 }} />
                            <View style={{ flexDirection: 'row' }}>
                                <TouchableOpacity style={{ backgroundColor: "#FF0036", padding: 15, width: 100, borderRadius: 5 }}
                                    onPress={() => {
                                        cancleRequest()
                                        setConfirmModal(false)
                                    }}
                                >
                                    <Text style={{ textAlign: 'center', color: "#FFFFFF", fontSize: 20, fontFamily: fontFamilyBold }}>Yes</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={{ backgroundColor: "#EFF1F7", padding: 15, width: 100, borderRadius: 5, marginLeft: 20 }}
                                    onPress={() => setConfirmModal(false)}
                                >
                                    <Text style={{ textAlign: 'center', color: "#34495E", fontSize: 20, fontFamily: fontFamilyBold }}>No</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                }
            </Modal>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    setModalVesible(false)
                }}
            >
                <BlurView style={{ flex: 1 }}
                    blurType="xlight"
                    blurAmount={10}
                    downsampleFactor={25}
                    reducedTransparencyFallbackColor="white"
                >
                    <View style={{ margin: 20 }}>
                        <ScrollView style={{ backgroundColor: 'white', elevation: 10 }}>
                            <View style={{ alignItems: 'flex-end', backgroundColor: 'white' }}>
                                <TouchableOpacity
                                    style={{ padding: 15 }}
                                    onPress={() => setModalVesible(false)}
                                >
                                    <Image
                                        style={{ width: 15, height: 15 }}
                                        source={require('../../assets/images/close.png')}
                                    />
                                </TouchableOpacity>

                            </View>
                            {dd.map((data, key) => {
                                if (data._id == props.route.params.oid) {
                                    return (
                                        <View style={{ backgroundColor: 'white' }} key={key}>

                                            <View style={{ flexDirection: 'row', padding: 15, borderBottomWidth: 1, borderBottomColor: "#E0E6ED" }}>
                                                <View>
                                                    <Image
                                                        source={require('../../assets/images/money.png')}
                                                        style={{ width: 45, height: 45 }}
                                                    />
                                                </View>
                                                <View style={{ justifyContent: 'center', paddingLeft: 15 }}>
                                                    <Text style={{ color: "#34495E", fontSize: 18, fontFamily: fontFamilyBold }}>Ksh {data.budget.min} - {data.budget.max}</Text>
                                                </View>
                                            </View>
                                            <View style={{ flexDirection: 'row', padding: 15, borderBottomWidth: 1, borderBottomColor: "#E0E6ED" }}>
                                                <View>
                                                    <Image
                                                        source={require('../../assets/images/locationPriceIcon.png')}
                                                        style={{ width: 45, height: 45 }}
                                                    />
                                                </View>
                                                <View style={{ justifyContent: 'center', paddingLeft: 15, flex: 1 }}>
                                                    <Text style={{ color: "#34495E", fontSize: 15, fontFamily: fontFamilyRegular }}>C & F: Ksh {data.candf.min} - {data.candf.max}</Text>
                                                </View>
                                            </View>

                                            <View style={{ flexDirection: 'row', padding: 15, borderBottomWidth: 1, borderBottomColor: "#E0E6ED" }}>
                                                <View>
                                                    <Image
                                                        source={require('../../assets/images/serviceIcon.png')}
                                                        style={{ width: 45, height: 45 }}
                                                    />
                                                </View>
                                                <View style={{ justifyContent: 'center', paddingLeft: 15, flex: 1 }}>
                                                    <Text style={{ color: "#34495E", fontSize: 15, fontFamily: fontFamilyRegular }}>Duty : {data.duty}</Text>
                                                </View>
                                            </View>

                                            <View style={{ flexDirection: 'row', padding: 15, borderBottomWidth: 1, borderBottomColor: "#E0E6ED" }}>
                                                <View>
                                                    <Image
                                                        source={require('../../assets/images/servicePayment.png')}
                                                        style={{ width: 45, height: 45 }}
                                                    />
                                                </View>
                                                <View style={{ justifyContent: 'center', paddingLeft: 15, flex: 1 }}>
                                                    <Text style={{ color: "#34495E", fontSize: 15, fontFamily: fontFamilyRegular }}>Bank Account</Text>
                                                </View>
                                            </View>
                                            <View style={{ flexDirection: 'row', padding: 15, borderBottomWidth: 1, borderBottomColor: "#E0E6ED" }}>
                                                <View>
                                                    <Image
                                                        source={require('../../assets/images/servicePayment.png')}
                                                        style={{ width: 45, height: 45 }}
                                                    />
                                                </View>
                                                <View style={{ justifyContent: 'center', paddingLeft: 15, flex: 1 }}>
                                                    <Text style={{ color: "#34495E", fontSize: 15, fontFamily: fontFamilyRegular }}>{data.carMake}</Text>
                                                </View>
                                            </View>
                                        </View>
                                    )
                                }
                            })}
                            <View style={{ padding: 20 }}>
                                {
                                    JSON.parse(arr).map((item, key) => {
                                        let se = JSON.parse(arr);
                                        return (
                                            <Widget
                                                key={key}
                                                checked={stepsPage.includes(item._id) ? true : false}
                                                title={item.head}
                                                description={item.subhead}
                                                isLast={key == se.length - 1 ? true : false}
                                            />
                                        )
                                    })
                                }

                            </View>
                        </ScrollView>
                    </View>
                </BlurView>
            </Modal>
            {dd.map((data, key) => {
                if (data._id == props.route.params.oid) {
                    return (
                        <View style={{ flex: 1 }}>
                            <View style={{ flex: 0.8 }}>
                                <TouchableOpacity
                                    onPress={() => {
                                        if (props.route.params.fromOrderPage) {
                                            props.navigation.goBack()
                                        } else {
                                            if (serviceType == 'Auto Service') {
                                                props.navigation.navigate("Tabs", { screen: 'AutoServiceTab' })
                                            } else {
                                                props.navigation.navigate("Tabs", { screen: 'CarShop' })
                                            }
                                        }
                                    }}
                                    style={{ position: "absolute", zIndex: 103, padding: 20, marginTop: 30 }}
                                >
                                    <Image
                                        source={require("../../assets/images/home_icon.png")}
                                        style={{ width: 50, height: 50 }}
                                    />
                                </TouchableOpacity>
                                <View style={{ flex: 1 }}>
                                    <Image source={{ uri: data.imageUrl ? data.imageUrl : "" }}
                                        style={{ flex: 1 }}
                                        resizeMode="cover"
                                    />
                                </View>
                            </View>
                            <View style={{ flex: 1, backgroundColor: 'white' }} key={key}>
                                <View style={{ backgroundColor: '#276EF1', padding: 8 }}>
                                    <Text style={{ color: 'white', fontSize: 13, fontFamily: fontFamilyRegular, textAlign: 'center' }}>
                                        Import delivery in 5-8 weeks
                                </Text>
                                </View>
                                <ScrollView>
                                    <View style={{ flexDirection: 'row', padding: 15, borderBottomWidth: 1, borderBottomColor: "#E0E6ED" }}>
                                        <View>
                                            <Image
                                                source={require('../../assets/images/money.png')}
                                                style={{ width: 45, height: 45 }}
                                            />
                                        </View>
                                        <View style={{ justifyContent: 'center', paddingLeft: 15 }}>
                                            <Text style={{ color: "#34495E", fontSize: 18, fontFamily: fontFamilyBold }}>Ksh {data.budget.min} - {data.budget.max}</Text>
                                        </View>
                                    </View>
                                    <View style={{ flexDirection: 'row', padding: 15, borderBottomWidth: 1, borderBottomColor: "#E0E6ED" }}>
                                        <View>
                                            <Image
                                                source={require('../../assets/images/locationPriceIcon.png')}
                                                style={{ width: 45, height: 45 }}
                                            />
                                        </View>
                                        <View style={{ justifyContent: 'center', paddingLeft: 15, flex: 1 }}>
                                            <Text style={{ color: "#34495E", fontSize: 15, fontFamily: fontFamilyRegular }}>C & F: Ksh {data.candf.min} - {data.candf.max}</Text>
                                        </View>
                                    </View>

                                    <View style={{ flexDirection: 'row', padding: 15, borderBottomWidth: 1, borderBottomColor: "#E0E6ED" }}>
                                        <View>
                                            <Image
                                                source={require('../../assets/images/serviceIcon.png')}
                                                style={{ width: 45, height: 45 }}
                                            />
                                        </View>
                                        <View style={{ justifyContent: 'center', paddingLeft: 15, flex: 1 }}>
                                            <Text style={{ color: "#34495E", fontSize: 15, fontFamily: fontFamilyRegular }}>Duty : {data.duty}</Text>
                                        </View>
                                    </View>
                                    <View style={{ flexDirection: 'row', padding: 15, borderBottomWidth: 1, borderBottomColor: "#E0E6ED" }}>
                                        <View>
                                            <Image
                                                source={require('../../assets/images/locationPriceIcon.png')}
                                                style={{ width: 45, height: 45 }}
                                            />
                                        </View>
                                        <View style={{ justifyContent: 'center', paddingLeft: 15 }}>
                                            <Text style={{ color: "#34495E", fontSize: 15, fontFamily: fontFamilyRegular }}>{moment(data.updatedAt).format("DD-MMM-YYYY")}</Text>
                                        </View>
                                    </View>
                                    <View style={{ flexDirection: 'row', padding: 15, borderBottomWidth: 1, borderBottomColor: "#E0E6ED" }}>
                                        <View>
                                            <Image
                                                source={require('../../assets/images/servicePayment.png')}
                                                style={{ width: 45, height: 45 }}
                                            />
                                        </View>
                                        <View style={{ justifyContent: 'center', paddingLeft: 15, flex: 1 }}>
                                            <Text style={{ color: "#34495E", fontSize: 15, fontFamily: fontFamilyRegular }}>Bank Account</Text>
                                        </View>
                                    </View>
                                    <View style={{ flexDirection: 'row', padding: 15, borderBottomWidth: 1, borderBottomColor: "#E0E6ED" }}>
                                        <View>
                                            <Image
                                                source={require('../../assets/images/servicePayment.png')}
                                                style={{ width: 45, height: 45 }}
                                            />
                                        </View>
                                        <View style={{ justifyContent: 'center', paddingLeft: 15, flex: 1 }}>
                                            <Text style={{ color: "#34495E", fontSize: 15, fontFamily: fontFamilyRegular }}>{data.carMakes}</Text>
                                        </View>
                                    </View>

                                </ScrollView>
                                <View style={{ padding: 15 }}>
                                    <View style={{ flexDirection: "row" }}>
                                        <View>
                                            <TouchableOpacity
                                                style={{ padding: 15, borderColor: stepsPage.length > 1 ? "#ccc" : "#276EF1", borderWidth: 1, borderRadius: 5 }}
                                                disabled={stepsPage.length > 1 ? true : false}
                                                onPress={() => setConfirmModal(true)}
                                            >
                                                <Text style={{ fontFamily: fontFamilyBold, fontSize: 22, color: stepsPage.length > 1 ? "#ccc" : "#276EF1" }}>Cancel Request</Text>
                                            </TouchableOpacity>
                                        </View>
                                        <View style={{ justifyContent: 'center', flex: 1 }}>
                                            <View style={{ flexDirection: 'row' }}>
                                                <View style={{ flex: 1 }} />
                                                <View style={{ alignItems: 'flex-end' }}>
                                                    <TouchableOpacity style={{ marginRight: 10 }}
                                                        onPress={() => setModalVesible(true)}
                                                    >
                                                        <Image
                                                            source={require('../../assets/images/showProgress.png')}
                                                            style={{ width: 45, height: 45 }}
                                                        />
                                                    </TouchableOpacity>
                                                </View>
                                                <View style={{ alignItems: 'flex-end' }}>
                                                    <TouchableOpacity style={{ marginRight: 10 }}
                                                        onPress={() => Linking.openURL("tel:+254720039039")}
                                                    >
                                                        <Image
                                                            source={require('../../assets/images/phone.png')}
                                                            style={{ width: 45, height: 45 }}
                                                        />
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </View>
                    )
                }
            })}
        </View>

    )
}