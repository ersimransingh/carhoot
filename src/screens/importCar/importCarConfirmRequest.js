import React from 'react';
import { View, Dimensions, Text, KeyboardAvoidingView, Platform, Image, SafeAreaView, ScrollView, TouchableOpacity, StatusBar } from 'react-native';
import MapView, { PROVIDER_GOOGLE, PROVIDER_DEFAULT, Marker } from 'react-native-maps'
import { fontFamilyMedium, fontFamilyRegular, fontFamilyLight, fontFamilyBold } from '../../global/globalStyle'
import Header from '../../components/Header'
import Button from '../../components/Button'
import Input from '../../components/Input'
import SelectOption from '../../components/SelectOption'
import Api from '../../api';
import axios from 'axios';
import { useSelector, useDispatch } from 'react-redux';
import { updateLocationsData, updateRequests, setEditableTowing } from '../../redux'
import moment from 'moment'
import AsyncStorage from '@react-native-community/async-storage';
import { updateUserCarData, updateUserData } from '../../redux'
import MapViewDirections from 'react-native-maps-directions';

import Toast from 'react-native-simple-toast';
import { MapStyle } from '../../global/mapStyle';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const car = "5f734be7f77c365a6c1106b2"

export default function AutoServiceDetails(props) {
    const [longitude, setLongitude] = React.useState(0);
    const [latitude, setLatitude] = React.useState(0);
    const dispatch = useDispatch()
    const [loader, setLoader] = React.useState(false)
    const dd = useSelector(state => state.user.requests);
    const [activeData, setActiveData] = React.useState('[]')
    const _submitReq = async () => {
        try {
            setLoader(true)
            const value = await AsyncStorage.getItem('token')
            axios.post(Api + "/api/getRescueRequest", {

            }, {
                headers: { 'authorization': value }
            }
            )
                .then((res) => {
                    if (res.data.status == true) {
                        dispatch(updateRequests(res.data.data))
                        setLoader(false)
                    }
                })
                .catch((error) => {
                    console.log(error.message)
                    setLoader(false)
                    Toast.show(JSON.stringify(error))
                })
        }
        catch (err) {
            setLoader(false)
            console.log(err)
        }
    }
    React.useEffect(() => {
        for (let i in dd) {
            if (dd[i]._id == props.route.params.oid) {
                setLongitude(dd[i].longitude)
                setLatitude(dd[i].latitude)
            }
        }
    }, [dd])

    const _submitRequest = async () => {
        let data = {}
        for (let i in dd) {
            if (dd[i]._id == props.route.params.oid) {
                data = dd[i]
            }
        }
        let d = {
            type: 'Import Car Request',
            id: car,
            oid: props.route.params.oid,
            name: "Import Car",
            // latitude: parseFloat(props.route.params.latitude),
            // longitude: parseFloat(props.route.params.longitude),
        }

        try {
            setLoader(true)
            const value = await AsyncStorage.getItem('token')
            axios.post(Api + "/api/changeStatus", {
                "type": "Import",
                "id": props.route.params.oid,
                "status": "Pending"

            }, {
                headers: { 'authorization': value }
            }
            )
                .then((res) => {
                    if (res.data.status == true) {
                        Toast.show('Your request has been received')
                        _submitReq()
                        setLoader(false)
                        props.navigation.navigate('ImportCarProgress', d)
                    }
                })
                .catch((error) => {
                    setLoader(false)
                    Toast.show(error)
                })
        }

        catch (err) {
            setLoader(false)
            console.log(err)
        }

    }
    const [location, setLocation] = React.useState('');
    const [carMakes, setCarMakes] = React.useState('Select Car');

    return (
        <View style={{ flex: 1 }} >
            <StatusBar
                barStyle="dark-content"
            />

            {dd.map((data, key) => {
                if (data._id == props.route.params.oid) {
                    var startTime = moment();
                    var endTime = moment(data.startTime);
                    var duration = moment.duration(endTime.diff(startTime));
                    var hours = parseInt(duration.asHours());
                    var minutes = parseInt(duration.asMinutes()) % 60;
                    return (
                        <View style={{ flex: 1 }}>
                            <View style={{ flex: 0.8 }}>
                                <TouchableOpacity
                                    onPress={() => props.navigation.navigate("Tabs")}
                                    style={{ position: "absolute", zIndex: 103, padding: 20, marginTop: 30 }}
                                >
                                    <Image
                                        source={require("../../assets/images/home_icon.png")}
                                        style={{ width: 50, height: 50 }}
                                    />
                                </TouchableOpacity>
                                <View style={{ flex: 1 }}>
                                    <Image source={{ uri: data.imageUrl ? data.imageUrl : "" }}
                                        style={{ flex: 1 }}
                                        resizeMode="cover"
                                    />
                                </View>
                            </View>
                            <View style={{ flex: 1, backgroundColor: 'white' }} key={key}>
                                <View style={{ backgroundColor: '#276EF1', padding: 8 }}>
                                    <Text style={{ color: 'white', fontSize: 13, fontFamily: fontFamilyRegular, textAlign: 'center' }}>
                                        Import delivery in 5-8 weeks
                                </Text>
                                </View>
                                <ScrollView>
                                    <View style={{ flexDirection: 'row', padding: 15, borderBottomWidth: 1, borderBottomColor: "#E0E6ED" }}>
                                        <View>
                                            <Image
                                                source={require('../../assets/images/money.png')}
                                                style={{ width: 45, height: 45 }}
                                            />
                                        </View>
                                        <View style={{ justifyContent: 'center', paddingLeft: 15 }}>
                                            <Text style={{ color: "#34495E", fontSize: 18, fontFamily: fontFamilyBold }}>Ksh {data.budget.min} - {data.budget.max}</Text>
                                        </View>
                                    </View>
                                    <View style={{ flexDirection: 'row', padding: 15, borderBottomWidth: 1, borderBottomColor: "#E0E6ED" }}>
                                        <View>
                                            <Image
                                                source={require('../../assets/images/money.png')}
                                                // source={require('../../assets/images/locationPriceIcon.png')}
                                                style={{ width: 45, height: 45 }}
                                            />
                                        </View>
                                        <View style={{ justifyContent: 'center', paddingLeft: 15, flex: 1 }}>
                                            <Text style={{ color: "#34495E", fontSize: 15, fontFamily: fontFamilyRegular }}>C & F: Ksh {data.candf.min} - {data.candf.max}</Text>
                                        </View>
                                    </View>

                                    <View style={{ flexDirection: 'row', padding: 15, borderBottomWidth: 1, borderBottomColor: "#E0E6ED" }}>
                                        <View>
                                            <Image
                                                source={require('../../assets/images/money.png')}
                                                // source={require('../../assets/images/serviceIcon.png')}
                                                style={{ width: 45, height: 45 }}
                                            />
                                        </View>
                                        <View style={{ justifyContent: 'center', paddingLeft: 15, flex: 1 }}>
                                            <Text style={{ color: "#34495E", fontSize: 15, fontFamily: fontFamilyRegular }}>Duty : {data.duty}</Text>
                                        </View>
                                    </View>


                                    <View style={{ flexDirection: 'row', padding: 15, borderBottomWidth: 1, borderBottomColor: "#E0E6ED" }}>
                                        <View>
                                            <Image
                                                source={require('../../assets/images/locationPriceIcon.png')}
                                                style={{ width: 45, height: 45 }}
                                            />
                                        </View>
                                        <View style={{ justifyContent: 'center', paddingLeft: 15 }}>
                                            <Text style={{ color: "#34495E", fontSize: 15, fontFamily: fontFamilyRegular }}>{moment(data.updatedAt).format("DD-MMM-YYYY")}</Text>
                                        </View>
                                    </View>
                                    <View style={{ flexDirection: 'row', padding: 15, borderBottomWidth: 1, borderBottomColor: "#E0E6ED" }}>
                                        <View>
                                            <Image
                                                source={require('../../assets/images/servicePayment.png')}
                                                style={{ width: 45, height: 45 }}
                                            />
                                        </View>
                                        <View style={{ justifyContent: 'center', paddingLeft: 15, flex: 1 }}>
                                            <Text style={{ color: "#34495E", fontSize: 15, fontFamily: fontFamilyRegular }}>Bank Account</Text>
                                        </View>
                                        <TouchableOpacity
                                            style={{ justifyContent: 'center' }}
                                            disabled={true}
                                        >
                                            <Text style={{ color: "#E0E6ED", fontFamily: fontFamilyRegular }}>Change</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ flexDirection: 'row', padding: 15, borderBottomWidth: 1, borderBottomColor: "#E0E6ED" }}>
                                        <View>
                                            <Image
                                                source={require('../../assets/images/servicePayment.png')}
                                                style={{ width: 45, height: 45 }}
                                            />
                                        </View>
                                        <View style={{ justifyContent: 'center', paddingLeft: 15, flex: 1 }}>
                                            <Text style={{ color: "#34495E", fontSize: 15, fontFamily: fontFamilyRegular }}>{data.carMakes}</Text>
                                        </View>
                                        <TouchableOpacity
                                            style={{ justifyContent: 'center' }}
                                            disabled={false}
                                            onPress={() => {
                                                dispatch(setEditableTowing(true, data._id))
                                                props.navigation.navigate('ImportCarTab', { oldDta: data });
                                            }}
                                        >
                                            <Text style={{ color: "#276EF1", fontFamily: fontFamilyRegular }}>Change</Text>
                                        </TouchableOpacity>
                                    </View>

                                </ScrollView>
                                <View style={{ marginBottom: 15 }}>
                                    <View style={{ padding: 15 }}>
                                        <Button
                                            name="Get import invoice"
                                            onPress={() => {
                                                _submitRequest()
                                            }}
                                            loading={loader}
                                        />
                                    </View>
                                </View>
                            </View>
                        </View>
                    )
                }
            })}
        </View>
    )
}