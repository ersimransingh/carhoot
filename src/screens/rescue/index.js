import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import RescueServiceLocation from './serviceLocation';
const Stack = createStackNavigator();

export default function RescueIndex() {
    return (
        <>
            <Stack.Screen name="RescueServiceLocation" component={RescueServiceLocation} />

        </>
    )
}