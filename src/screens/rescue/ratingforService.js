import React from 'react';
import { View, Dimensions, Text, KeyboardAvoidingView, Platform, Image, SafeAreaView } from 'react-native';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps'
import { fontFamilyMedium, fontFamilyRegular, fontFamilyLight, fontFamilySemiBold } from '../../global/globalStyle'
import Header from '../../components/Header'
import Button from '../../components/Button'
import Input from '../../components/Input'
import SelectOption from '../../components/SelectOption'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import Api from '../../api';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import Toast from 'react-native-simple-toast';
// import { socket } from '../../socket/_socket';
import { useSelector, useDispatch } from 'react-redux';
import { updateRequests } from '../../redux'

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

function Widget(props) {
    return (
        <View style={{ flexDirection: 'row', marginTop: 20 }}>
            <View style={{ paddingRight: 25 }}>
                <Image
                    source={props.checked ? require('../../assets/images/check.png') : require('../../assets/images/uncheck.png')}
                    style={{ width: 20, height: 20 }}
                />
            </View>
            <View style={{ flex: 1 }}>
                <Text style={{ fontSize: 12, fontFamily: fontFamilySemiBold, lineHeight: 15 }}>{props.title}</Text>
                <Text style={{ fontSize: 12, fontFamily: fontFamilyLight, lineHeight: 20 }}>{props.description}</Text>
            </View>

        </View>
    )
}

export default function RatingforService(props) {

    const [steps, setSteps] = React.useState(props.route.params.steps)
    const dispatch = useDispatch()
    const _submitRequest = async () => {

        try {
            const value = await AsyncStorage.getItem('token')
            axios.post(Api + "/api/getRescueRequest", {

            }, {
                headers: { 'authorization': value }
            }
            )
                .then((res) => {
                    if (res.data.status == true) {
                        dispatch(updateRequests(res.data.data))
                    }
                })
                .catch((error) => {
                    console.log(error.message)
                    Toast.show(JSON.stringify(error))
                })
        }

        catch (err) {
            console.log(err)
        }
    }

    const [rating, setRating] = React.useState(4)
    let [kk, setKK] = React.useState({})
    const user = useSelector(state => state.servicedata.data);
    let dataRedux = user.map((x) => {
        kk[x._id] = x.rescueservicetype
    })

    let [arr, setArr] = React.useState([])
    let [ll, setLL] = React.useState([])

    const _getSteps = async () => {

        const value = await AsyncStorage.getItem('token')
        try {
            axios.get(Api + '/api/getrescuesteps/' + props.route.params.id, {},)
                .then((res) => {
                    if (res.data.steps.length != 0) {
                        let places = res.data.steps;
                        setLL(places.length)
                        setArr(places)
                    }
                })
                .catch((error) => {
                    console.log(error)
                })
        }
        catch (err) {
            Toast.show(err)
        }
    }
    React.useEffect(() => {
        _getSteps()
    }, [false])

    const rate = () => {
        let arr = []
        for (let i = 1; i <= 5; i++) {
            if (i <= rating) {
                arr.push(true);
            } else {
                arr.push(false);
            }
        }
        return (
            <View style={{ padding: 10 }}>
                <Text style={{ fontFamily: fontFamilyLight, fontSize: 16, textAlign: 'center' }}>Please rate your hoot experience</Text>
                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: 15 }}>
                    {
                        arr.map((data, index) => {
                            return (
                                <TouchableOpacity
                                    key={index}
                                    style={{ paddingRight: 10 }}
                                    onPress={() => setRating(index + 1)}
                                >
                                    <Image
                                        source={data ? require('../../assets/images/StarDark.png') : require('../../assets/images/StarLight.png')}
                                        style={{ width: 35, height: 34 }}
                                    />
                                </TouchableOpacity>
                            )
                        })
                    }
                </View>
            </View>

        )
    }
    return (
        <SafeAreaView style={{ flex: 1 }} >
            <Header
                backButton={true}
                {...props}
            />
            <ScrollView style={{ padding: 20 }}>
                <View>
                    <Text style={{ fontFamily: fontFamilyMedium, fontSize: 16 }}>Service: <Text style={{ color: '#0AC97E' }}>{kk[props.route.params != undefined ? props.route.params.type.split(' ')[0] : null]} </Text></Text>
                    <View
                        style={{ height: 15 }}
                    />
                    <Text style={{ fontFamily: fontFamilyMedium, fontSize: 16 }}>Cost: <Text style={{ color: '#0AC97E' }}>KES {props.route.params != undefined ? props.route.params.price : null}</Text></Text>
                </View>
                <View>
                    {
                        arr.map((item, key) => {
                            return (
                                <Widget
                                    key={key}
                                    checked={steps >= key ? true : false}
                                    title={item.head}
                                    description={item.subhead}
                                />
                            )
                        })
                    }

                    {ll <= steps + 1 ? rate() : null}
                </View>
                <View style={{ height: 15 }} />
                {ll > steps + 1 ? <Button
                    name="Close Report"
                    onPress={() => props.navigation.navigate('Tabs')}
                /> : null}

                <View style={{ height: 40 }} />
            </ScrollView>

        </SafeAreaView>

    )
}