import React from 'react';
import { View, Text, StatusBar, Image, TouchableOpacity, Dimensions, SafeAreaView, StyleSheet, Alert, Linking } from 'react-native'
import Geolocation from '@react-native-community/geolocation';
import SelectLocationNew from '../../components/SelectLocationNew'
import MapView, { Marker, Overlay, PROVIDER_GOOGLE, PROVIDER_DEFAULT } from 'react-native-maps'
import Button from '../../components/Button';
import { MapStyle } from '../../global/mapStyle'
import { useRoute } from '@react-navigation/native';
import { updateUserData } from '../../redux';
import axios from 'axios'
import Api from '../../api';
import { useSelector, useDispatch } from 'react-redux';
import { updateStates, updateRequests, setEditableTowing } from '../../redux/'
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment';


export default function SelectLocation(props) {
    const dispatch = useDispatch()
    const [longitude, setLongitude] = React.useState(76.711810156703)
    const [latitude, setLatitude] = React.useState(30.69465395251688)
    const [selectedData, setSelectedData] = React.useState('Current Location')
    const [lat, setLat] = React.useState(31)
    const [long, setLong] = React.useState(75)
    const [longitudeDelta, setLongitudeDetla] = React.useState(0.0022208690643310547);
    const [latitudeDelta, setlatitudeDetla] = React.useState(0.0035607585813046683);
    const mapRef = React.createRef()
    const route = useRoute();
    const [locationLoading, setLocationLoading] = React.useState(false);

    const _gotoNextPage = async () => {
        let oldparams = props.route.params;
        oldparams.lat = latitude
        oldparams.long = longitude
        oldparams.lname = selectedData
        oldparams.whereto = ""
        let service = props.route.params.type;
        if (props.route.params.service == 'auto') {
            props.navigation.navigate("AutoServiceRequest", oldparams);
        } else {
            if (service == 'Towing service') {
                let a = { "lat": lat, "long": long }
                let nameLoc = await getLocationFromLatLongReturn(lat, long);
                oldparams.whereto = JSON.stringify(a)
                oldparams.dlname = "Enter Destination";
                props.navigation.navigate("RescueServiceLocation", oldparams);
                // props.navigation.navigate("SelectDestinationLocation", oldparams);
            } else {
                props.navigation.navigate("RescueServiceLocation", oldparams);
            }
        }
    }
    const getLocationFromLatLongReturn = async (lat, long) => {
        let locationName = await axios.get('https://maps.googleapis.com/maps/api/geocode/json?address=' + lat + ',' + long + '&key=AIzaSyAB7cS3Wb_fyoN_GObjGhud-3PMNNRmfwc')
        return locationName.data.results[0].formatted_address
    }
    const getLocationFromLatLong = (lat, long) => {
        fetch('https://maps.googleapis.com/maps/api/geocode/json?address=' + lat + ',' + long + '&key=AIzaSyAB7cS3Wb_fyoN_GObjGhud-3PMNNRmfwc')
            .then((response) => response.json())
            .then((responseJson) => {
                console.log('response json', responseJson);
                setSelectedData(responseJson.results[0].formatted_address)
            })
    }
    React.useEffect(() => {
        try {
            setLocationLoading(true)
            Geolocation.setRNConfiguration({ authorizationLevel: 'whenInUse', skipPermissionRequests: false, });
            Geolocation.getCurrentPosition((x) => {

                if (route.name != "SelectLocationEdit") {
                    console.log('cords', x.coords);
                    setLatitude(x.coords.latitude)
                    setLongitude(x.coords.longitude)
                    getLocationFromLatLong(x.coords.latitude, x.coords.longitude)
                } else {
                    setLongitude(props.route.params.orderData.longitude);
                    setLatitude(props.route.params.orderData.latitude);
                    setSelectedData(props.route.params.orderData.placeid)
                }
                setLat(x.coords.latitude)
                setLong(x.coords.longitude)
                setLocationLoading(false)
            },
                err => {
                    if (err.code == 2) {
                        if (route.name != "SelectLocationEdit") {
                            setLatitude(-1.2851404)
                            setLongitude(36.8219813)
                            getLocationFromLatLong(-1.2851404, 36.8219813)
                        }
                        Alert.alert("Alert", "GPS not enabled so we are not able to get your current location automatically.",
                            [
                                {
                                    text: "Cancel",
                                    onPress: () => console.log("Cancel Pressed"),
                                    style: "cancel"
                                },
                                { text: "Open Setting", onPress: () => Linking.openSettings() }
                            ])
                    } else if (err.code == 3) {
                        Alert.alert("Unable to get location please activate High Accuracy Location from settings.")
                    } else {
                        Alert.alert(err.message)
                    }
                    console.log('sbcd', err)
                    setLocationLoading(false)
                },
                { enableHighAccuracy: false, timeout: 50000 }
            )
        } catch (err) {
            console.log('sbcd', err)
            setLocationLoading(false)
        }
    }, [false])
    const _UpdateAllData = async () => {
        try {
            const value = await AsyncStorage.getItem('token')
            axios.post(Api + "/api/getRescueRequest", {}, {
                headers: { 'authorization': value }
            })
                .then((res) => {
                    if (res.data.status == true) {
                        dispatch(updateRequests(res.data.data))
                        props.navigation.goBack();
                    }
                })
                .catch((error) => {
                    console.log(error)
                })
        }
        catch (err) {
            console.log(err)
        }
    }
    const updateLocation = async () => {
        try {
            const value = await AsyncStorage.getItem('token')
            let orderData = props.route.params.orderData;
            if (props.route.params.requestType == 'Auto') {
                let carMake = "";
                let carModel = "";
                let carEngine = "";
                let dta = {
                    id: orderData._id,
                    type: orderData.rescuetype,
                    longitude: longitude,
                    latitude: latitude,
                    placeid: selectedData,
                    whereto: "",
                    price: orderData.price,
                    car: orderData.carmake,
                    serviceType: "Auto",
                    serviceDate: orderData.serviceDate,
                    pickUpService: orderData.pickUpService,
                    requestType: orderData.rescuetype,
                    categoryName: orderData.categoryName,
                    categoryTrim: orderData.categoryTrim,
                    // carMake: carMake,
                    // carModel: carModel,
                    // carEngine: carEngine
                }
                axios.post(Api + "/api/editRescueRequest", dta, {
                    headers: { 'authorization': value }
                })
                    .then((data) => {
                        _UpdateAllData()
                    })
                    .catch((err) => {
                        console.log('err', err)
                    })
            } else {
                let whereTo = "";
                let place = selectedData;
                let wtLat = "";
                let wtLong = "";
                if (props.route.params.whereto != '') {
                    let wt = JSON.parse(props.route.params.whereto);
                    let pla = JSON.parse(orderData.placeid);
                    wtLat = wt.lat
                    wtLong = wt.long
                    let a = { "from": selectedData, "to": pla.to }
                    place = JSON.stringify(a);
                }
                axios.post(Api + "/api/editRescueRequest", {
                    "id": orderData._id,
                    "type": props.route.params.id,
                    "longitude": longitude,
                    "latitude": latitude,
                    "placeid": place,
                    "wheretoLat": wtLat,
                    "wheretoLang": wtLong,
                    "price": orderData.price,
                    "car": orderData.carmake,
                    "serviceType": "Rescue",
                    "requestType": props.route.params.id,
                    "startTime": moment().add(30, 'm').toDate(),
                    "endTime": moment().add(60, 'm').toDate()
                }, {
                    headers: { 'authorization': value }
                })
                    .then((data) => {
                        _UpdateAllData()
                    })
                    .catch((err) => {
                        console.log('err', err)
                    })


            }
        } catch (err) {
            console.log(err)
        }
    }
    const _backCurrentLocation = () => {
        Geolocation.getCurrentPosition((x) => {
            setLatitude(x.coords.latitude)
            setLongitude(x.coords.longitude)

            if (mapRef.current) {
                mapRef.current.animateToRegion({
                    latitude: x.coords.latitude,
                    longitude: x.coords.longitude,
                    latitudeDelta: latitudeDelta,
                    longitudeDelta: longitudeDelta
                })
                getLocationFromLatLong(x.coords.latitude, x.coords.longitude)
            } else {
                _backCurrentLocation()
            }
        },
            err => {
                console.log(err)
            },
        )
    }
    return (
        <View style={styles.mainContainer}>
            <StatusBar
                backgroundColor="#276EF1"
                barStyle="light-content"
            />
            <View style={styles.topBar}></View>
            <View style={styles.mainHeader}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ justifyContent: 'center' }}>
                        <TouchableOpacity
                            style={{ padding: 15, paddingLeft: 0 }}
                            onPress={() => props.navigation.goBack()}
                        >
                            <Image
                                source={require('../../assets/images/Left.png')}
                                style={{ width: 24, height: 24 }}
                            />
                        </TouchableOpacity>
                    </View>
                    <View style={{ flex: 1, justifyContent: 'center', zIndex: 20 }}>
                        <SelectLocationNew
                            label={props.route.params.service != 'auto' ? "Set Rescue Location" : "Where will we find your car?"}
                            placeholder={selectedData}
                            nonAbsolute={true}
                            onSelect={(t) => {

                                mapRef.current.animateToRegion({
                                    latitude: t.cords.lat,
                                    longitude: t.cords.long,
                                    latitudeDelta: latitudeDelta,
                                    longitudeDelta: longitudeDelta
                                }, 1000)


                                setLongitude(t.cords.long)
                                setLatitude(t.cords.lat);
                                setSelectedData(t.name)
                            }}
                        />
                    </View>
                </View>
            </View>
            <View style={styles.bottomButtonView}>
                <Button
                    name={props.route.params.requestType != 'Auto' ? "Find my car here" : "Confirm car location"}
                    onPress={() => {
                        if (route.name == "SelectLocationEdit") {
                            updateLocation()
                        } else {
                            _gotoNextPage()
                        }
                    }}
                />
            </View>
            <View style={styles.currentLocationIcon}>
                <TouchableOpacity
                    style={styles.currentLocationTouch}
                    onPress={() => _backCurrentLocation()}
                >
                    <Image
                        source={require('../../assets/images/currentLocation.png')}
                        style={{ width: 26, height: 26 }}
                    />
                </TouchableOpacity>
            </View>
            {locationLoading &&
                <Text style={{ padding: 10 }}>Getting your location please wait...</Text>
            }
            {latitude != 30.69465395251688 &&
                <SafeAreaView style={{ flex: 1, zIndex: 0 }}>
                    <Image
                        source={require('../../assets/images/currentLocationIcon.png')}
                        style={styles.mapcurrentlocation}
                    />
                    <MapView
                        style={{ flex: 1, zIndex: -2 }}
                        provider={PROVIDER_GOOGLE}
                        ref={mapRef}
                        initialRegion={{
                            latitude: parseFloat(latitude),
                            longitude: parseFloat(longitude),
                            latitudeDelta: latitudeDelta,
                            longitudeDelta: longitudeDelta,
                        }}
                        region={{
                            latitude: parseFloat(latitude),
                            longitude: parseFloat(longitude),
                            latitudeDelta: latitudeDelta,
                            longitudeDelta: longitudeDelta,
                        }}
                        customMapStyle={MapStyle}
                        onRegionChangeComplete={(d) => {
                            setLatitude(d.latitude)
                            setLongitude(d.longitude)
                            setLongitudeDetla(d.longitudeDelta)
                            setlatitudeDetla(d.latitudeDelta)
                            getLocationFromLatLong(d.latitude, d.longitude)

                        }}
                        cacheEnabled={false}
                    >
                        <Marker
                            coordinate={{
                                latitude: lat,
                                longitude: long,
                            }}
                            pinColor="#0AC97E"
                            title={"Your Location"}
                        >
                            <View style={styles.markerView}>
                            </View>
                        </Marker>
                    </MapView>
                </SafeAreaView>
            }
        </View>
    )
}
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1
    },
    topBar: {
        backgroundColor: "#276EF1", height: 40
    },
    mainHeader: {
        padding: 15, backgroundColor: '#F8F8F8', zIndex: 1
    },
    bottomButtonView: {
        position: 'absolute', width: '100%', bottom: 0, padding: 20, zIndex: 101
    },
    currentLocationIcon: {
        position: 'absolute', bottom: 100, padding: 20, right: 0, zIndex: 100
    },
    currentLocationTouch: {
        padding: 10, backgroundColor: '#FFFFFF', borderRadius: 50, zIndex: 34343
    },
    mapcurrentlocation: {
        width: 36, height: 45, position: 'absolute', alignSelf: 'center', marginTop: windowHeight / 2.9
    },
    markerView: {
        width: 14, height: 14, backgroundColor: "#276EF1", borderColor: 'rgba(255, 196, 50, 0.3)', borderWidth: 3, borderRadius: 7
    }
});