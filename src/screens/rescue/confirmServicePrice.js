import React from 'react';
import { View, Dimensions, Text, KeyboardAvoidingView, Platform, Image, SafeAreaView, ScrollView, TouchableOpacity, StatusBar } from 'react-native';
import MapView, { PROVIDER_GOOGLE, PROVIDER_DEFAULT, Marker } from 'react-native-maps'
import { fontFamilyMedium, fontFamilyRegular, fontFamilyLight, fontFamilyBold } from '../../global/globalStyle'
import Header from '../../components/Header'
import Button from '../../components/Button'
import Input from '../../components/Input'
import SelectOption from '../../components/SelectOption'
import Api from '../../api';
import axios from 'axios';
import { useSelector, useDispatch } from 'react-redux';
import { updateLocationsData, updateRequests, setEditableTowing } from '../../redux'
import moment from 'moment'
import AsyncStorage from '@react-native-community/async-storage';
import { updateUserCarData, updateUserData } from '../../redux'
import MapViewDirections from 'react-native-maps-directions';
import { useFocusEffect } from '@react-navigation/native';
import Toast from 'react-native-simple-toast';
import { MapStyle } from '../../global/mapStyle';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;


export default function ConfirmServicePrice(props) {
    const mapRef = React.createRef()
    const dispatch = useDispatch()
    const [loader, setLoader] = React.useState(false)
    const dd = useSelector(state => state.user.requests);
    const [activeData, setActiveData] = React.useState('[]')
    const [showDirection, setShowDirection] = React.useState(false);
    const userCars = useSelector(state => state.importCar.userCars)
    const _submitReq = async () => {
        try {
            setLoader(true)
            const value = await AsyncStorage.getItem('token')
            axios.post(Api + "/api/getRescueRequest", {

            }, {
                headers: { 'authorization': value }
            }
            )
                .then((res) => {
                    if (res.data.status == true) {
                        dispatch(updateRequests(res.data.data))
                        setLoader(false)
                    }
                })
                .catch((error) => {
                    console.log(error.message)
                    setLoader(false)
                    Toast.show(JSON.stringify(error))
                })
        }
        catch (err) {
            setLoader(false)
            console.log(err)
        }
    }

    const _submitRequest = async () => {
        let data = {}
        for (let i in dd) {
            if (dd[i]._id == props.route.params.oid) {
                data = dd[i]
            }
        }
        let d = {
            type: data.rescuetype + '  Request',
            price: data.price,
            id: props.route.params.id,
            oid: props.route.params.oid,
            name: data.requestType,
            latitude: parseFloat(lat),
            longitude: parseFloat(long),
        }

        try {
            setLoader(true)
            const value = await AsyncStorage.getItem('token')
            axios.post(Api + "/api/changeStatus", {
                "type": "Rescue",
                "id": props.route.params.oid,
                "status": "Pending"

            }, {
                headers: { 'authorization': value }
            })
                .then((res) => {
                    if (res.data.status == true) {
                        console.log("===>>>>", res.data.data)
                        Toast.show('Your request has been received')
                        _submitReq()
                        setLoader(false)
                        props.navigation.navigate('ServiceProgress', d)
                    }
                })
                .catch((error) => {
                    setLoader(false)
                    Toast.show(error)
                })
        }

        catch (err) {
            setLoader(false)
            console.log(err)
        }

    }
    const [allLatLongs, setAllLatLongs] = React.useState("{}");
    let whereTo = props.route.params.whereto ? JSON.parse(props.route.params.whereto) : ''

    const [long, setLong] = React.useState(props.route.params.longitude);
    const [lat, setLat] = React.useState(props.route.params.latitude);

    const [location, setLocation] = React.useState('');
    const [carMakes, setCarMakes] = React.useState('Select Car');
    const [latDelta, setLatDelta] = React.useState(0.0035607585813046683);
    const [longDelta, setLongDelta] = React.useState(0.0022208690643310547);
    const [showIt, setShowIt] = React.useState(false);
    React.useEffect(() => {
        const timer = setTimeout(() => {
            setShowIt(true);
        }, 2000);
        return () => clearTimeout(timer);
    }, [false]);
    const changeCoords = () => {
        mapRef.current.fitToCoordinates(JSON.parse(allLatLongs), { edgePadding: { top: 10, right: 10, bottom: 10, left: 10 }, animated: false })
    }
    const getLocation = () => {

    }
    useFocusEffect(
        React.useCallback(() => {
            dd.map((data, key) => {
                if (data._id == props.route.params.oid) {
                    if (mapRef.current) {
                        setLat(parseFloat(data.latitude))
                        setLong(parseFloat(data.longitude))
                        setActiveData("[]")
                    }
                }
            })


        }, [dd])
    );
    return (
        <View style={{ flex: 1 }} >
            <StatusBar
                barStyle="dark-content"
            />
            <View style={{ flex: 0.8 }}>
                <TouchableOpacity
                    onPress={() => props.navigation.navigate("Tabs")}
                    style={{ position: "absolute", zIndex: 103, padding: 20, marginTop: 30 }}
                >
                    <Image
                        source={require("../../assets/images/home_icon.png")}
                        style={{ width: 50, height: 50 }}
                    />
                </TouchableOpacity>
                {showIt &&
                    <MapView
                        style={{ flex: 1 }}
                        provider={PROVIDER_GOOGLE}
                        loadingEnabled={false}
                        ref={mapRef}
                        initialRegion={{
                            latitude: lat,
                            longitude: long,
                            latitudeDelta: latDelta,
                            longitudeDelta: longDelta,
                        }}
                        region={{
                            latitude: lat,
                            longitude: long,
                            latitudeDelta: latDelta,
                            longitudeDelta: longDelta,
                        }}
                        cacheEnabled={false}
                        customMapStyle={MapStyle}
                    >
                        <Marker
                            coordinate={{
                                latitude: lat,
                                longitude: long,
                            }}
                            pinColor="#0AC97E"
                            title={"Rescue Location"}
                        >
                            <Image
                                source={require('../../assets/images/currentLocationIcon.png')}
                                style={{ width: 36, height: 45 }}
                            />
                        </Marker>
                        {props.route.params.whereto != "" &&
                            <Marker
                                coordinate={{
                                    latitude: whereTo.lat,
                                    longitude: whereTo.long,
                                }}
                                pinColor="#276EF1"
                                title={"Destination Location"}

                            >
                                <Image
                                    source={require('../../assets/images/whereToPin.png')}
                                    style={{ width: 36, height: 45 }}
                                />
                            </Marker>
                        }
                        {props.route.params.whereto != "" &&
                            <MapViewDirections
                                origin={{ longitude: long, latitude: lat }}
                                destination={{ longitude: whereTo.long, latitude: whereTo.lat }}
                                apikey={"AIzaSyANEd-gpHYzA7Mc2hq9r9PgYxpWuUNeHL0"}
                                strokeWidth={3}
                                strokeColor="#276EF1"
                                onReady={result => {
                                    mapRef.current.fitToCoordinates(result.coordinates, {
                                        edgePadding: {
                                            right: (width / 20),
                                            bottom: (height / 20),
                                            left: (width / 20),
                                            top: (height / 20),
                                        }
                                    });
                                }}
                            />
                        }
                    </MapView>
                }
            </View>

            {dd.map((data, key) => {
                if (data._id == props.route.params.oid) {
                    var startTime = moment();
                    var endTime = moment(data.startTime);
                    var duration = moment.duration(endTime.diff(startTime));
                    let carName = "";
                    for (let i in userCars) {
                        if (userCars[i].value == data.carmake) {
                            carName = userCars[i].name;
                        }
                    }
                    return (
                        <View style={{ flex: 1, backgroundColor: 'white' }} key={key}>
                            <View style={{ backgroundColor: '#276EF1', padding: 8 }}>
                                <Text style={{ color: 'white', fontSize: 13, fontFamily: fontFamilyRegular, textAlign: 'center' }}>
                                    It will take {parseInt(duration.asMinutes())} minutes to reach you
                                </Text>
                            </View>
                            <ScrollView>
                                <View style={{ flexDirection: 'row', padding: 15, borderBottomWidth: 1, borderBottomColor: "#E0E6ED" }}>
                                    <View>
                                        <Image
                                            source={require('../../assets/images/money.png')}
                                            style={{ width: 45, height: 45 }}
                                        />
                                    </View>
                                    <View style={{ justifyContent: 'center', paddingLeft: 15 }}>
                                        <Text style={{ color: "#34495E", fontSize: 18, fontFamily: fontFamilyBold }}>KES {data.price}</Text>
                                    </View>
                                </View>
                                <View style={{ flexDirection: 'row', padding: 15, borderBottomWidth: 1, borderBottomColor: "#E0E6ED" }}>
                                    <View>
                                        <Image
                                            source={require('../../assets/images/serviceIcon.png')}
                                            style={{ width: 45, height: 45 }}
                                        />
                                    </View>
                                    <View style={{ justifyContent: 'center', paddingLeft: 15, flex: 1 }}>
                                        <Text style={{ color: "#34495E", fontSize: 15, fontFamily: fontFamilyRegular }}>{data.requestType}</Text>
                                    </View>
                                    {data.requestType != "Towing service" &&
                                        <TouchableOpacity
                                            style={{ justifyContent: 'center' }}
                                            onPress={() => {
                                                let oldParams = props.route.params
                                                oldParams['orderData'] = data
                                                props.navigation.navigate("RescueTabEdit", oldParams)
                                            }}
                                        >
                                            <Text style={{ color: "#276EF1", fontFamily: fontFamilyRegular }}>Change</Text>
                                        </TouchableOpacity>
                                    }
                                </View>
                                <View style={{ flexDirection: 'row', padding: 15, borderBottomWidth: 1, borderBottomColor: "#E0E6ED" }}>
                                    <View>
                                        <Image
                                            source={require('../../assets/images/locationPriceIcon.png')}
                                            style={{ width: 45, height: 45 }}
                                        />
                                    </View>
                                    <View style={{ justifyContent: 'center', paddingLeft: 15, flex: 1 }}>
                                        <Text style={{ color: "#34495E", fontSize: 15, fontFamily: fontFamilyRegular }}>{props.route.params.whereto != '' ? JSON.parse(data.placeid).from : data.placeid}</Text>
                                    </View>
                                    <TouchableOpacity
                                        style={{ justifyContent: 'center' }}
                                        onPress={() => {
                                            let oldParams = props.route.params
                                            oldParams['orderData'] = data
                                            props.navigation.navigate("SelectLocationEdit", oldParams)
                                        }}
                                    >
                                        <Text style={{ color: "#276EF1", fontFamily: fontFamilyRegular }}>Change</Text>
                                    </TouchableOpacity>
                                </View>
                                {props.route.params.whereto != '' &&
                                    <View style={{ flexDirection: 'row', padding: 15, borderBottomWidth: 1, borderBottomColor: "#E0E6ED" }}>
                                        <View>
                                            <Image
                                                source={require('../../assets/images/ToService.png')}
                                                style={{ width: 45, height: 45 }}
                                            />
                                        </View>
                                        <View style={{ justifyContent: 'center', paddingLeft: 15, flex: 1 }}>
                                            <Text style={{ color: "#34495E", fontSize: 15, fontFamily: fontFamilyRegular }}>{props.route.params.whereto != '' ? JSON.parse(data.placeid).to : data.placeid}</Text>
                                        </View>
                                        <TouchableOpacity
                                            style={{ justifyContent: 'center' }}
                                            onPress={() => {
                                                dispatch(setEditableTowing(true, data._id))
                                                let oldParams = props.route.params
                                                oldParams['orderData'] = data
                                                let oldparams = props.route.params;
                                                let a = { "lat": whereTo.lat, "long": whereTo.long }
                                                oldparams.whereto = JSON.stringify(a)
                                                oldparams.dlname = JSON.parse(data.placeid).to
                                                props.navigation.navigate("RescueServiceLocation", oldparams);
                                            }}
                                        >
                                            <Text style={{ color: "#276EF1", fontFamily: fontFamilyRegular }}>Change</Text>
                                        </TouchableOpacity>

                                    </View>
                                }


                                <View style={{ flexDirection: 'row', padding: 15, borderBottomWidth: 1, borderBottomColor: "#E0E6ED" }}>
                                    <View>
                                        <Image
                                            source={require('../../assets/images/locationPriceIcon.png')}
                                            style={{ width: 45, height: 45 }}
                                        />
                                    </View>
                                    <View style={{ justifyContent: 'center', paddingLeft: 15 }}>
                                        <Text style={{ color: "#34495E", fontSize: 15, fontFamily: fontFamilyRegular }}>{carName}</Text>
                                    </View>
                                </View>
                                <View style={{ flexDirection: 'row', padding: 15, borderBottomWidth: 1, borderBottomColor: "#E0E6ED" }}>
                                    <View>
                                        <Image
                                            source={require('../../assets/images/locationPriceIcon.png')}
                                            style={{ width: 45, height: 45 }}
                                        />
                                    </View>
                                    <View style={{ justifyContent: 'center', paddingLeft: 15 }}>
                                        <Text style={{ color: "#34495E", fontSize: 15, fontFamily: fontFamilyRegular }}>{moment(data.updatedAt).format("DD-MMM-YYYY HH:mm")}</Text>
                                    </View>
                                </View>
                                <View style={{ flexDirection: 'row', padding: 15, borderBottomWidth: 1, borderBottomColor: "#E0E6ED" }}>
                                    <View>
                                        <Image
                                            source={require('../../assets/images/servicePayment.png')}
                                            style={{ width: 45, height: 45 }}
                                        />
                                    </View>
                                    <View style={{ justifyContent: 'center', paddingLeft: 15, flex: 1 }}>
                                        <Text style={{ color: "#34495E", fontSize: 15, fontFamily: fontFamilyRegular }}>M-Pesa/Cash</Text>
                                    </View>

                                </View>

                            </ScrollView>
                            <View style={{ marginBottom: 15 }}>
                                <View style={{ padding: 15 }}>
                                    <Button
                                        name="Confirm Rescue Order"
                                        onPress={() => {
                                            _submitRequest()
                                        }}
                                        loading={loader}
                                    />
                                </View>
                            </View>
                        </View>
                    )
                }
            })}
        </View>
    )
}