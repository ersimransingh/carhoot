import React from 'react';
import { View, Text, StatusBar, Image, TouchableOpacity, Dimensions, SafeAreaView } from 'react-native'
import Geolocation from '@react-native-community/geolocation';
import SelectLocationNew from '../../components/SelectLocationNew'
import MapView, { Marker, Overlay, PROVIDER_GOOGLE, PROVIDER_DEFAULT } from 'react-native-maps'
import Button from '../../components/Button';
import { MapStyle } from '../../global/mapStyle'
import { useRoute } from '@react-navigation/native';
import { updateUserData } from '../../redux';
import axios from 'axios'
import Api from '../../api';
import { useSelector, useDispatch } from 'react-redux';
import { updateStates, updateRequests } from '../../redux/'
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment';

export default function SelectDestinationLocation(props) {
    const dispatch = useDispatch()
    const [longitude, setLongitude] = React.useState(props.route.params.whereto != '' ? JSON.parse(props.route.params.whereto).long : "")
    const [latitude, setLatitude] = React.useState(props.route.params.whereto != '' ? JSON.parse(props.route.params.whereto).lat : "")
    const [selectedData, setSelectedData] = React.useState(props.route.params.dlname)
    const [lat, setLat] = React.useState(31)
    const [long, setLong] = React.useState(75)
    const [longitudeDelta, setLongitudeDetla] = React.useState(0.0022208690643310547);
    const [latitudeDelta, setlatitudeDetla] = React.useState(0.0035607585813046683);
    const mapRef = React.createRef()
    const route = useRoute();
    const _gotoNextPage = () => {
        let oldparams = props.route.params;
        let a = { "lat": latitude, "long": longitude }
        oldparams.whereto = JSON.stringify(a)
        oldparams.dlname = selectedData
        props.navigation.navigate("RescueServiceLocation", oldparams);
    }

    const getLocationFromLatLong = (lat, long) => {
        fetch('https://maps.googleapis.com/maps/api/geocode/json?address=' + lat + ',' + long + '&key=AIzaSyAB7cS3Wb_fyoN_GObjGhud-3PMNNRmfwc')
            .then((response) => response.json())
            .then((responseJson) => {
                setSelectedData(responseJson.results[0].formatted_address)
                // console.log('ADDRESS GEOCODE is BACK!! => ' + JSON.stringify(responseJson.results[0].formatted_address));

            })
    }
    React.useEffect(() => {
        Geolocation.getCurrentPosition((x) => {
            if (route.name != "SelectDestinationLocationEdit") {
                setLatitude(x.coords.latitude)
                setLongitude(x.coords.longitude)
                getLocationFromLatLong(x.coords.latitude, x.coords.longitude)
            } else {
                setLongitude(props.route.params.orderData.longitude);
                setLatitude(props.route.params.orderData.latitude);
                setSelectedData(props.route.params.orderData.placeid)
            }
            setLat(x.coords.latitude)
            setLong(x.coords.longitude)
        },
            err => {
                console.log(err)
            },
        )
    }, [false])
    const _UpdateAllData = async () => {
        try {
            const value = await AsyncStorage.getItem('token')
            axios.post(Api + "/api/getRescueRequest", {}, {
                headers: { 'authorization': value }
            })
                .then((res) => {
                    if (res.data.status == true) {
                        dispatch(updateRequests(res.data.data))
                        props.navigation.goBack();
                    }
                })
                .catch((error) => {
                    console.log(error)
                })
        }
        catch (err) {
            console.log(err)
        }
    }
    const updateLocation = async () => {
        try {
            const value = await AsyncStorage.getItem('token')
            let orderData = props.route.params.orderData;
            let place = selectedData;
            let wtLat = "";
            let wtLong = "";
            if (props.route.params.whereto != '') {
                let wt = JSON.parse(props.route.params.whereto);
                let pla = JSON.parse(orderData.placeid);
                wtLat = wt.lat
                wtLong = wt.long
                let a = { "from": pla.from, "to": selectedData }
                place = JSON.stringify(a);
            }
            axios.post(Api + "/api/editRescueRequest", {
                "id": orderData._id,
                "type": props.route.params.id,
                "longitude": orderData.longitude,
                "latitude": orderData.latitude,
                "placeid": place,
                "wheretoLat": latitude,
                "wheretoLang": longitude,
                "price": orderData.price,
                "car": orderData.carmake,
                "serviceType": "Rescue",
                "requestType": props.route.params.id,
                "startTime": moment().add(30, 'm').toDate(),
                "endTime": moment().add(60, 'm').toDate()
            }, {
                headers: { 'authorization': value }
            })
                .then((data) => {
                    _UpdateAllData()
                })
                .catch((err) => {
                    console.log('err', err)
                })
        } catch (err) {
            console.log(err)
        }
    }
    const _backCurrentLocation = () => {
        Geolocation.getCurrentPosition((x) => {
            setLatitude(x.coords.latitude)
            setLongitude(x.coords.longitude)
            if (mapRef.current) {
                mapRef.current.animateToRegion({
                    latitude: x.coords.latitude,
                    longitude: x.coords.longitude,
                    latitudeDelta: latitudeDelta,
                    longitudeDelta: longitudeDelta
                })
                getLocationFromLatLong(x.coords.latitude, x.coords.longitude)
            }
        },
            err => {
                console.log(err)
            },
        )
    }
    return (
        <View style={{ flex: 1 }}>
            <StatusBar
                backgroundColor="#276EF1"
                barStyle="light-content"
            />
            <View style={{ backgroundColor: "#276EF1", height: 40 }}></View>
            <View style={{ padding: 15, backgroundColor: '#F8F8F8', zIndex: 103 }}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ justifyContent: 'center' }}>
                        <TouchableOpacity
                            style={{ padding: 15, paddingLeft: 0 }}
                            onPress={() => props.navigation.goBack()}
                        >
                            <Image
                                source={require('../../assets/images/Left.png')}
                                style={{ width: 24, height: 24 }}
                            />
                        </TouchableOpacity>
                    </View>
                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <SelectLocationNew
                            label={props.route.params.service != 'auto' ? "Select destination location" : ""}
                            placeholder={selectedData}
                            onSelect={(t) => {
                                mapRef.current.animateToRegion({
                                    latitude: t.cords.lat,
                                    longitude: t.cords.long,
                                    latitudeDelta: latitudeDelta,
                                    longitudeDelta: longitudeDelta
                                })
                                setLongitude(t.cords.long)
                                setLatitude(t.cords.lat);
                                setSelectedData(t.name)
                            }}
                        />
                    </View>
                </View>
            </View>
            <View style={{ position: 'absolute', width: '100%', bottom: 0, padding: 20, zIndex: 101 }}>
                <Button
                    name={props.route.params.service != 'auto' ? "Select rescue location" : "Done"}
                    onPress={() => {
                        if (route.name == "SelectDestinationLocationEdit") {
                            updateLocation()
                        } else {
                            _gotoNextPage()
                        }
                    }}
                />
            </View>
            <View style={{ position: 'absolute', bottom: 100, padding: 20, right: 0, zIndex: 100 }}>
                <TouchableOpacity
                    style={{ padding: 10, backgroundColor: '#FFFFFF', borderRadius: 50, zIndex: 34343 }}
                    onPress={() => _backCurrentLocation()}
                >
                    <Image
                        source={require('../../assets/images/currentLocation.png')}
                        style={{ width: 26, height: 26 }}
                    />
                </TouchableOpacity>
            </View>
            {latitude != 30.69465395251688 &&
                <SafeAreaView style={{ flex: 1, zIndex: 0 }}>
                    <Image
                        source={require('../../assets/images/whereToPin.png')}
                        style={{ width: 36, height: 45, position: 'absolute', alignSelf: 'center', marginTop: windowHeight / 2.9 }}
                    />
                    <MapView
                        style={{ flex: 1, zIndex: -2 }}
                        provider={PROVIDER_GOOGLE}
                        ref={mapRef}
                        initialRegion={{
                            latitude: latitude,
                            longitude: longitude,
                            latitudeDelta: latitudeDelta,
                            longitudeDelta: longitudeDelta,
                        }}
                        customMapStyle={MapStyle}
                        onRegionChangeComplete={(d) => {
                            setLatitude(d.latitude)
                            setLongitude(d.longitude)
                            setLongitudeDetla(d.longitudeDelta)
                            setlatitudeDetla(d.latitudeDelta)
                            getLocationFromLatLong(d.latitude, d.longitude)
                        }}
                        cacheEnabled={true}
                    >
                        <Marker
                            coordinate={{
                                latitude: lat,
                                longitude: long,
                            }}
                            pinColor="#0AC97E"
                            title={"Your Location"}
                        >
                            <View style={{ width: 14, height: 14, backgroundColor: "#276EF1", borderColor: 'rgba(255, 196, 50, 0.3)', borderWidth: 3, borderRadius: 7 }}>
                            </View>
                        </Marker>
                    </MapView>
                </SafeAreaView>
            }
        </View>
    )
}