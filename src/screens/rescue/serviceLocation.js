import React from 'react';
import { View, Dimensions, Text, KeyboardAvoidingView, Platform, SafeAreaView, ScrollView, StatusBar, TouchableWithoutFeedback, Image } from 'react-native';
import MapView, { Marker, PROVIDER_GOOGLE, PROVIDER_DEFAULT } from 'react-native-maps'
import { fontFamilyMedium, fontFamilyRegular, fontFamilyLight } from '../../global/globalStyle'
import { TouchableOpacity } from 'react-native-gesture-handler'
import Button from '../../components/Button'
import Input from '../../components/Input'
import SelectOption from '../../components/SelectOptionNew'
import SelectLocation from '../../components/SelectLocation'
import SelectLocationNew from '../../components/SelectLocationNew'
import navigationReducer from '../../redux/navigation/navigationReducer';
import Geolocation from '@react-native-community/geolocation';
import { useSelector, useDispatch } from 'react-redux';
import { originLatLong } from '../../globalVariable'
import { updateLocationsData, setUserCars, updateRequests, setEditableTowing } from '../../redux'
import Api from '../../api';
import axios from 'axios';
import moment from 'moment'
import AsyncStorage from '@react-native-community/async-storage'
import Toast from 'react-native-simple-toast';
import { useRoute } from '@react-navigation/native';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
import { useFocusEffect } from '@react-navigation/native';

function Garage(props) {
    return (
        <TouchableOpacity style={{
            flexDirection: 'row',
            borderBottomColor: props.selected ? "#276EF1" : '#E0E6ED',
            borderBottomWidth: 1,
            padding: 15,
            backgroundColor: "transparent",
            borderColor: '#276EF1',
            borderWidth: props.selected ? 1 : 0
        }}
            onPress={() => props.onPress(props.id)}

        >
            <View style={{ paddingRight: 20, justifyContent: 'center', alignItems: 'center' }}>
                <Image
                    source={require('../../assets/images/garageNew.png')}
                    style={{ width: 45, height: 45 }}
                />
            </View>
            <View style={{ flex: 1, justifyContent: 'center' }}>
                <Text style={{ fontFamily: fontFamilyRegular, fontSize: 14, color: '#313030' }}>{props.name}</Text>
                <Text style={{ fontFamily: fontFamilyLight, fontSize: 14, color: '#313030' }}>{props.address}</Text>
            </View>
        </TouchableOpacity>
    )
}

export default function RescueServiceLocation(props) {
    const dispatch = useDispatch()
    const locationsData = useSelector(state => state.location.locationPlaces)
    const [longitude, setLongitude] = React.useState(props.route.params.long)
    const [latitude, setLatitude] = React.useState(props.route.params.lat)
    const [wlongitude, setwLongitude] = React.useState(props.route.params.whereto != '' ? JSON.parse(props.route.params.whereto).long : "")
    const [wlatitude, setwLatitude] = React.useState(props.route.params.whereto != '' ? JSON.parse(props.route.params.whereto).lat : "")
    const [search, setSearch] = React.useState()
    const [lt, setLt] = React.useState()
    const [selectedData, setSelectedData] = React.useState(props.route.params.lname)
    const [wselectedData, setwSelectedData] = React.useState(props.route.params.dlname)
    const [defPlace, setDefplace] = React.useState()
    const [deflocation, setdefLocation] = React.useState()
    const [data, setData] = React.useState([])
    const [garages, setGarages] = React.useState('[]')
    const userCars = useSelector(state => state.importCar.userCars)
    const [loader, setLoader] = React.useState(false)
    const [selectedGarage, setSelectedGarage] = React.useState(-1)
    const isEditable = useSelector(state => state.servicedata.isEditing)
    const towingId = useSelector(state => state.servicedata.towingId)
    const route = useRoute();
    const _getDefaultPlace = async (latitude, longitude) => {
        try {
            axios.post("https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + latitude + "," + longitude + "&radius=1500&key=AIzaSyAB7cS3Wb_fyoN_GObjGhud-3PMNNRmfwc")
                .then((res) => {
                    setData([])
                    if (res.data.results.length != 0) {
                        let places = res.data.results;
                        for (let y in places) {
                            let a = { name: places[y].name + places[y].vicinity, value: places[y].place_id, show: true, long: JSON.stringify(places[y].geometry.location) };
                            data.push(a);
                            setDefplace(a.value)

                        }

                        dispatch(updateLocationsData(data))

                    }
                })
                .catch((error) => {
                    console.log(error)
                })
        }
        catch (err) {
            Toast.show(err)
        }
    }


    const getGarage = () => {
        axios.get(Api + '/api/garages')
            .then((data) => {
                setGarages(JSON.stringify(data.data.garageList));
            })
    }
    React.useEffect(() => {
        getGarage()
    }, [false])



    const arr = []
    const [location, setLocation] = React.useState('');
    const myCars = useSelector(state => state.user.carsData)
    const [selectedCarId, setSelectedCarId] = React.useState("")
    const [selectedCarName, setSelectedCarName] = React.useState("Choose your car")
    React.useEffect(() => {
        let dta = [];
        for (let i in myCars) {
            let a = { name: myCars[i].make + "[ " + myCars[i].regno + " ]", value: myCars[i]._id, show: true }
            dta.push(a)
        }
        dispatch(setUserCars(dta));
    }, [false])
    useFocusEffect(
        React.useCallback(() => {
            if (props.route.params.whereto != '') {
                setwLongitude(props.route.params.whereto != '' ? JSON.parse(props.route.params.whereto).long : "")
                setwLatitude(props.route.params.whereto != '' ? JSON.parse(props.route.params.whereto).lat : "")
                setwSelectedData(props.route.params.dlname ? props.route.params.dlname : "")
            }
        }, [props.route.params])
    );

    const [carname, setcarName] = React.useState(arr.length != 0 ? arr[0].name : '')
    const [carMakes, setCarMakes] = React.useState(arr.length != 0 ? arr[0].value : '');
    const _submitReq = async () => {
        try {
            setLoader(true)
            const value = await AsyncStorage.getItem('token')
            axios.post(Api + "/api/getRescueRequest", {

            }, {
                headers: { 'authorization': value }
            }
            )
                .then((res) => {
                    if (res.data.status == true) {
                        dispatch(updateRequests(res.data.data))
                        setLoader(false)
                    }
                })
                .catch((error) => {
                    console.log(error.message)
                    setLoader(false)
                    Toast.show(JSON.stringify(error))
                })
        }
        catch (err) {
            setLoader(false)
            console.log(err)
        }
    }
    const submitData = async () => {
        try {
            //calculate price
            let distance = 0;
            let seconds = 0;
            let price = 0;

            let getTimeandDistance = await axios.get("https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=" + originLatLong.lat + "," + originLatLong.long + "&destinations=" + latitude + "," + longitude + "&key=AIzaSyAB7cS3Wb_fyoN_GObjGhud-3PMNNRmfwc")
            if (getTimeandDistance.data.rows[0].elements[0].status == "OK") {
                distance = parseInt(getTimeandDistance.data.rows[0].elements[0].distance.value / 1000);
                seconds = getTimeandDistance.data.rows[0].elements[0].duration.value;
                price = (distance * 10);
                price += props.route.params.price ? parseInt(props.route.params.price) : 0
            }
            let endTime = moment().add(parseInt(seconds) + 2400, 'seconds').toDate();
            // Calculate Price 


            if (selectedCarName == 'Choose your car') {
                Toast.show("Please choose your car.")
            } else if (props.route.params.whereto != '' && wselectedData == 'Enter Destination') {
                Toast.show("Please choose destination location.")
            } else if (getTimeandDistance.data.rows[0].elements[0].status != "OK") {
                Toast.show("Sorry we are not providing service at your location.")
            } else if (distance > 100) {
                Toast.show("We are not providing service at your location.")
            } else {
                const value = await AsyncStorage.getItem('token')
                setLoader(true)
                let whereTo = "";
                let place = selectedData;
                if (props.route.params.whereto != '') {
                    let priceWT = 0;
                    let secondsWT = 0;
                    let distanceWT = 0;
                    let getTimeandDistanceWT = await axios.get("https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=" + latitude + "," + longitude + "&destinations=" + wlongitude + "," + wlatitude + "&key=AIzaSyAB7cS3Wb_fyoN_GObjGhud-3PMNNRmfwc")
                    if (getTimeandDistanceWT.data.rows[0].elements[0].status == "OK") {
                        distanceWT = parseInt(getTimeandDistanceWT.data.rows[0].elements[0].distance.value / 1000);
                        secondsWT = getTimeandDistanceWT.data.rows[0].elements[0].duration.value;
                        priceWT = (distanceWT * 100);
                        priceWT += props.route.params.price ? parseInt(props.route.params.price) : 0
                    }
                    endTime = moment().add(parseInt(seconds) + parseInt(secondsWT) + 1200, 'seconds').toDate();
                    let wt = { long: wlongitude, lat: wlatitude }
                    whereTo = JSON.stringify(wt);
                    let a = { "from": selectedData, "to": wselectedData }
                    place = JSON.stringify(a);
                    price = priceWT
                }
                if (isEditable) {
                    axios.post(Api + "/api/editRescueRequest", {
                        "id": towingId,
                        "type": props.route.params.id,
                        "longitude": longitude,
                        "latitude": latitude,
                        "placeid": place,
                        "wheretoLat": wlatitude,
                        "wheretoLang": wlongitude,
                        "price": props.route.params.price,
                        "car": selectedCarId,
                        "serviceType": "Rescue",
                        "requestType": props.route.params.type ? props.route.params.type : "",
                        "startTime": moment().add(seconds, 'seconds').toDate(),
                        "endTime": endTime
                    }, {
                        headers: { 'authorization': value }
                    })
                        .then((res) => {

                            if (res.data.status == true) {
                                _submitReq()
                                setLoader(false)
                                let oldParam = props.route.params;
                                oldParam.carname = selectedCarName
                                oldParam.longitude = longitude
                                oldParam.latitude = latitude
                                oldParam.oid = towingId
                                oldParam.whereto = whereTo
                                props.navigation.navigate('ConfirmServicePrice', oldParam)
                                setTimeout(() => {
                                    dispatch(setEditableTowing(false, ""))
                                }, 1000)
                            }

                        })
                        .catch((err) => {
                            console.log('err', err)
                        })
                } else {
                    axios.post(Api + "/api/addRescueRequest", {
                        "type": props.route.params.id,
                        "longitude": longitude,
                        "latitude": latitude,
                        "placeid": place,
                        "whereto": whereTo,
                        "price": props.route.params.price,
                        "car": selectedCarId,
                        "serviceType": "Rescue",
                        "requestType": props.route.params.type ? props.route.params.type : "",
                        "startTime": moment().add(seconds, 'seconds').toDate(),
                        "endTime": endTime
                    }, {
                        headers: { 'authorization': value }
                    })
                        .then((res) => {

                            if (res.data.status == true) {

                                // Toast.show('Request Submitted')
                                _submitReq()
                                setLoader(false)
                                let oldParam = props.route.params;
                                oldParam.carname = selectedCarName
                                oldParam.longitude = longitude
                                oldParam.latitude = latitude
                                oldParam.oid = res.data.data._id
                                oldParam.whereto = whereTo
                                props.navigation.navigate('ConfirmServicePrice', oldParam)
                                // props.navigation.navigate('ConfirmServicePrice', { carname: selectedCarName, longitude: longitude, latitude: latitude, id: props.route.params.id, oid: res.data.data._id, whereto: whereTo })
                                // props.navigation.navigate('ServiceProgress', { type: res.data.data.rescuetype + '  Request', price: props.route.params.price, id: props.route.params.id, oid: res.data.data._id, name: props.route.params.type })
                            }
                        })
                        .catch((error) => {
                            console.log(error)
                            setLoader(false)
                            // Toast.show(error)
                        })
                }

            }
        }

        catch (err) {
            setLoader(false)
            console.log(err)
        }
    }
    const getLoc = (t) => {
        try {
            axios.post('https://maps.googleapis.com/maps/api/place/details/json?place_id=' + t + '&key=AIzaSyAB7cS3Wb_fyoN_GObjGhud-3PMNNRmfwc')
                .then((res) => {
                    if (res.data.result.length != 0) {
                        setLatitude(res.data.result.geometry.location.lat)
                        setLongitude(res.data.result.geometry.location.lng)

                    }

                })
                .catch((error) => {
                    console.log(error)
                })
        }
        catch (err) {
            console.log(err)
        }
    }
    return (
        <View style={{ flex: 1, backgroundColor: 'white' }} >
            <StatusBar
                backgroundColor="#276EF1"
                barStyle="light-content"
            />
            <View style={{ backgroundColor: "#276EF1", height: 40 }}></View>

            <View style={{ backgroundColor: '#F8F8F8', zIndex: 100 }}>
                <View style={{ flexDirection: 'row' }}>
                    <View>
                        <TouchableOpacity
                            style={{ padding: 15 }}
                            onPress={() => props.navigation.goBack()}
                        >
                            <Image
                                source={require('../../assets/images/Left.png')}
                                style={{ width: 24, height: 24 }}
                            />
                        </TouchableOpacity>
                        {props.route.params.whereto != '' &&
                            <View style={{ padding: 15, alignItems: 'flex-end' }}>
                                {!isEditable &&
                                    <Image
                                        source={require('../../assets/images/towingStepsService.png')}
                                        style={{ width: 15, height: 70 }}
                                    />
                                }
                            </View>
                        }
                    </View>
                    <View style={{ flex: 1 }}>
                        <View style={{ padding: 15, marginTop: 5 }}>
                            <Text style={{ fontFamily: fontFamilyRegular }}>{props.route.params.whereto != '' ? "Confirm Dropoff address and car" : "Confirm rescue address and car"}</Text>
                            <View style={{ height: 20 }} />
                            {!isEditable &&
                                <View style={{ zIndex: 20 }}>
                                    <SelectLocationNew
                                        label=""
                                        placeholder={selectedData}
                                        onSelect={(t) => {
                                            setLongitude(t.cords.long)
                                            setLatitude(t.cords.lat);
                                            setSelectedData(t.name)
                                        }}
                                    />
                                </View>
                            }
                            {props.route.params.whereto != '' &&
                                <View style={{ zIndex: 19 }}>
                                    <View style={{ height: 20 }} />
                                    <SelectLocationNew
                                        label=""
                                        placeholder={wselectedData}
                                        onSelect={(t) => {
                                            setSelectedGarage(-1)
                                            setwLongitude(t.cords.long)
                                            setwLatitude(t.cords.lat);
                                            setwSelectedData(t.name)
                                        }}
                                    />
                                </View>
                            }
                            <View style={{ height: 20 }} />
                            {!isEditable &&
                                <View style={{ zIndex: 18 }} >
                                    <SelectOption
                                        label=""
                                        placeholder={selectedCarName}
                                        getId={true}
                                        selectedId={selectedCarId}
                                        onChangeText={(t) => {
                                            setSelectedCarId(t.value)
                                            setSelectedCarName(t.name)
                                        }}
                                        data={userCars}
                                    />
                                </View>
                            }
                        </View>
                    </View>
                </View>
            </View>
            <View style={{ height: 15 }} />
            <View style={{ flex: 1, zIndex: 0 }}>
                <ScrollView>
                    {!isEditable &&
                        <View style={{ zIndex: 0 }}>
                            <TouchableOpacity style={{ flexDirection: 'row', padding: props.route.params.whereto != '' ? 5 : 5, backgroundColor: props.route.params.whereto != '' ? "white" : '#FFFFFF', zIndex: -1 }}
                                // disabled={true}
                                onPress={() => props.navigation.navigate('SelectLocation', props.route.params)}
                            >
                                <View>
                                    <Image
                                        source={require('../../assets/images/mapLocationReturn.png')}
                                        style={{ width: 45, height: 45 }}
                                    />
                                </View>
                                <View style={{ justifyContent: "center", paddingLeft: 20 }}>
                                    <Text style={{ color: props.route.params.whereto != '' ? "#276EF1" : '#276EF1', fontSize: 15, fontFamily: fontFamilyRegular }}>Set location from map</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    }
                    {props.route.params.whereto != '' &&
                        <View style={{ zIndex: 0, marginTop: 10 }}>
                            <TouchableOpacity style={{ flexDirection: 'row', padding: 5, backgroundColor: '#FFFFFF', zIndex: -1 }}
                                onPress={() => {
                                    let oldparams = props.route.params;
                                    let a = { "lat": wlatitude, "long": wlongitude }
                                    oldparams.whereto = JSON.stringify(a)
                                    oldparams.dlname = wselectedData
                                    props.navigation.navigate('SelectDestinationLocation', oldparams)
                                }}
                            >
                                <View>
                                    <Image
                                        source={require('../../assets/images/towingLocationDrop.png')}
                                        style={{ width: 45, height: 45 }}
                                    />
                                </View>
                                <View style={{ justifyContent: "center", paddingLeft: 20 }}>
                                    <Text style={{ color: '#FFC432', fontSize: 15, fontFamily: fontFamilyRegular }}>Set Destination location from map</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    }
                    {props.route.params.whereto != '' &&
                        <View style={{ padding: 8 }}>
                            <Text style={{ color: '#276EF1', fontFamily: fontFamilyRegular, marginVertical: 8 }}>Recommended Garages</Text>
                            <View style={{ backgroundColor: "#F8F8F8", marginTop: 10 }}>

                                {JSON.parse(garages).map((data, key) => {
                                    // console.log('abcd', data);
                                    return (
                                        <Garage
                                            key={key}
                                            name={data.name}
                                            address={data.address}
                                            onPress={(t) => {
                                                setSelectedGarage(t)
                                                setwLongitude(data.locationCoordinates.long)
                                                setwLatitude(data.locationCoordinates.lat);
                                                setwSelectedData(data.name)
                                            }}
                                            id={key}
                                            selected={selectedGarage == key ? true : false}
                                            {...props}
                                        />
                                    )
                                })}
                            </View>
                        </View>
                    }
                </ScrollView>
            </View>
            <View style={{ padding: 20 }}>
                <Button
                    name="Get Rescue Quote"
                    onPress={() => submitData()}
                    loading={loader}

                />
            </View>
        </View>

    )
}