import React from 'react';
import { View, Dimensions, Text, KeyboardAvoidingView, Platform, Image, SafeAreaView, ScrollView, StatusBar, StyleSheet, TouchableOpacity } from 'react-native';

import MapView, { PROVIDER_GOOGLE, PROVIDER_DEFAULT, Marker } from 'react-native-maps'
import { fontFamilyMedium, fontFamilyRegular, fontFamilyLight, fontFamilyBold } from '../../global/globalStyle'
import Header from '../../components/Header'
import Button from '../../components/Button'
import Input from '../../components/Input'
import SelectOption from '../../components/SelectOption'
import Api from '../../api';
import axios from 'axios';
import { useSelector, useDispatch } from 'react-redux';
import { updateLocationsData, updateRequests } from '../../redux'
import moment from 'moment'
import AsyncStorage from '@react-native-community/async-storage';
import { updateUserCarData, updateUserData } from '../../redux'
import MapViewDirections from 'react-native-maps-directions';

import Toast from 'react-native-simple-toast';
import { MapStyle } from '../../global/mapStyle';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;


export default function AutoServiceDetails(props) {
    const [longitude, setLongitude] = React.useState(props.route.params.longitude);
    const [latitude, setLatitude] = React.useState(props.route.params.latitude);
    const dispatch = useDispatch()
    const [loader, setLoader] = React.useState(false)
    const dd = useSelector(state => state.user.requests);
    const [activeData, setActiveData] = React.useState('[]')
    const _submitReq = async () => {
        try {
            setLoader(true)
            const value = await AsyncStorage.getItem('token')
            axios.post(Api + "/api/getRescueRequest", {

            }, {
                headers: { 'authorization': value }
            }
            )
                .then((res) => {
                    if (res.data.status == true) {
                        dispatch(updateRequests(res.data.data))
                        setLoader(false)
                    }
                })
                .catch((error) => {
                    console.log(error.message)
                    setLoader(false)
                    Toast.show(JSON.stringify(error))
                })
        }
        catch (err) {
            setLoader(false)
            console.log(err)
        }
    }
    React.useEffect(() => {
        for (let i in dd) {
            if (dd[i]._id == props.route.params.oid) {
                setLongitude(dd[i].longitude)
                setLatitude(dd[i].latitude)
            }
        }
    }, [dd])

    const _submitRequest = async () => {
        let data = {}
        for (let i in dd) {
            if (dd[i]._id == props.route.params.oid) {
                data = dd[i]
            }
        }
        let d = {
            type: data.rescuetype + '  Request',
            price: data.price,
            id: props.route.params.lastObj._id,
            oid: props.route.params.oid,
            name: data.requestType,
            latitude: parseFloat(latitude),
            longitude: parseFloat(longitude),
        }

        try {
            setLoader(true)
            const value = await AsyncStorage.getItem('token')
            axios.post(Api + "/api/changeStatus", {
                "type": "Rescue",
                "id": props.route.params.oid,
                "status": "Pending"

            }, {
                headers: { 'authorization': value }
            }
            )
                .then((res) => {
                    if (res.data.status == true) {
                        Toast.show('Your request has been received')
                        _submitReq()
                        setLoader(false)
                        props.navigation.navigate('ServiceProgress', d)
                    }
                })
                .catch((error) => {
                    setLoader(false)
                    Toast.show(error)
                })
        }

        catch (err) {
            setLoader(false)
            console.log(err)
        }

    }
    const [location, setLocation] = React.useState('');
    const [carMakes, setCarMakes] = React.useState('Select Car');
    const [lat, setLat] = React.useState(0.0035607585813046683);
    const [long, setLong] = React.useState(0.0022208690643310547);
    const userCars = useSelector(state => state.importCar.userCars)
    return (
        <View style={{ flex: 1 }} >
            <StatusBar
                barStyle="dark-content"
            />
            <View style={{ flex: 0.8 }}>

                <TouchableOpacity
                    onPress={() => props.navigation.navigate("Tabs")}
                    style={styles.homeIcon}
                >
                    <Image
                        source={require("../../assets/images/home_icon.png")}
                        style={styles.homeiconImg}
                    />
                </TouchableOpacity>

                <MapView
                    style={{ flex: 1, zIndex: 1 }}
                    provider={PROVIDER_GOOGLE}
                    loadingEnabled={false}
                    initialRegion={{
                        latitude: parseFloat(latitude),
                        longitude: parseFloat(longitude),
                        latitudeDelta: lat,
                        longitudeDelta: long,
                    }}
                    region={{
                        latitude: parseFloat(latitude),
                        longitude: parseFloat(longitude),
                        latitudeDelta: lat,
                        longitudeDelta: long,
                    }}
                    cacheEnabled={false}
                    customMapStyle={MapStyle}
                >
                    <Marker
                        coordinate={{
                            latitude: latitude ? parseFloat(latitude) : 0,
                            longitude: longitude ? parseFloat(longitude) : 0,
                        }}
                        pinColor="#0AC97E"
                        title={"Pick Up Location"}
                    >
                        <Image
                            source={require('../../assets/images/currentLocationIcon.png')}
                            style={{ width: 36, height: 45 }}
                        />
                    </Marker>

                </MapView>
            </View>
            {dd.map((data, key) => {
                if (data._id == props.route.params.oid) {
                    var startTime = moment();
                    var endTime = moment(data.startTime);
                    var duration = moment.duration(endTime.diff(startTime));

                    let carName = "";
                    for (let i in userCars) {
                        if (userCars[i].value == data.carmake) {
                            carName = userCars[i].name;
                        }
                    }
                    return (
                        <View style={styles.secondHalf} key={key}>
                            <View style={styles.timerView}>
                                <Text style={styles.timerText}>
                                    Schedule pickup for {moment(data.serviceDate).format("DD-MM-YYYY [at] HH:mm")}
                                </Text>
                            </View>
                            <ScrollView>
                                <View style={styles.listItemView}>
                                    <View>
                                        <Image
                                            source={require('../../assets/images/money.png')}
                                            style={styles.listItemImg}
                                        />
                                    </View>
                                    <View style={styles.listItemPriceView}>
                                        <Text style={styles.listItemPriceText}>KES {data.price}</Text>
                                    </View>
                                </View>


                                <View style={styles.listItemView}>
                                    <View>
                                        <Image
                                            source={require('../../assets/images/serviceIcon.png')}
                                            style={styles.listItemImg}
                                        />
                                    </View>
                                    <View style={styles.listItemPriceView}>
                                        <Text style={styles.listItemNormalText}>{data.requestType}</Text>
                                    </View>
                                    <TouchableOpacity
                                        style={{ justifyContent: 'center' }}
                                        onPress={() => {
                                            props.navigation.navigate("Tabs")
                                        }}
                                    >
                                        <Text style={{ color: "#276EF1", fontFamily: fontFamilyRegular }}>Change</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.listItemView}>
                                    <View>
                                        <Image
                                            source={require('../../assets/images/locationPriceIcon.png')}
                                            style={styles.listItemImg}
                                        />
                                    </View>
                                    <View style={styles.listItemPriceView}>
                                        <Text style={styles.listItemNormalText}>{data.categoryName}</Text>
                                    </View>
                                </View>
                                <View style={styles.listItemView}>
                                    <View>
                                        <Image
                                            source={require('../../assets/images/locationPriceIcon.png')}
                                            style={styles.listItemImg}
                                        />
                                    </View>
                                    <View style={styles.listItemPriceView}>
                                        <Text style={styles.listItemNormalText}>{data.categoryTrim}</Text>
                                    </View>
                                </View>
                                <View style={styles.listItemView}>
                                    <View>
                                        <Image
                                            source={require('../../assets/images/locationPriceIcon.png')}
                                            style={styles.listItemImg}
                                        />
                                    </View>
                                    <View style={styles.listItemPriceView}>
                                        <Text style={styles.listItemNormalText}>{data.placeid}</Text>
                                    </View>
                                    <TouchableOpacity
                                        style={{ justifyContent: 'center' }}
                                        onPress={() => {
                                            let oldParams = props.route.params
                                            oldParams['orderData'] = data
                                            oldParams['requestType'] = 'Auto'
                                            props.navigation.navigate("SelectLocationEdit", oldParams)
                                        }}
                                    >
                                        <Text style={{ color: "#276EF1", fontFamily: fontFamilyRegular }}>Change</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.listItemView}>
                                    <View>
                                        <Image
                                            source={require('../../assets/images/locationPriceIcon.png')}
                                            style={styles.listItemImg}
                                        />
                                    </View>
                                    <View style={styles.listItemPriceView}>
                                        <Text style={styles.listItemNormalText}>{carName}</Text>
                                    </View>
                                </View>
                                <View style={styles.listItemView}>
                                    <View>
                                        <Image
                                            source={require('../../assets/images/locationPriceIcon.png')}
                                            style={styles.listItemImg}
                                        />
                                    </View>
                                    <View style={styles.listItemPriceView}>
                                        <Text style={styles.listItemNormalText}>{moment(data.updatedAt).format("DD-MMM-YYYY HH:mm")}</Text>
                                    </View>
                                </View>


                                <View style={styles.listItemView}>
                                    <View>
                                        <Image
                                            source={require('../../assets/images/servicePayment.png')}
                                            style={styles.listItemImg}
                                        />
                                    </View>
                                    <View style={styles.listItemPriceView}>
                                        <Text style={styles.listItemNormalText}>M-Pesa / Cash</Text>
                                    </View>
                                </View>

                            </ScrollView>
                            <View style={{ marginBottom: 15 }}>
                                <View style={{ padding: 15 }}>
                                    <Button
                                        name="Confirm service request"
                                        onPress={() => {
                                            _submitRequest()
                                        }}
                                        loading={loader}
                                    />
                                </View>
                            </View>
                        </View>
                    )
                }
            })}
        </View>
    )
}

const styles = StyleSheet.create({
    homeIcon: {
        position: "absolute", zIndex: 10000003, padding: 20, marginTop: 30
    },
    homeiconImg: {
        width: 50, height: 50
    },
    secondHalf: {
        flex: 1, backgroundColor: 'white'
    },
    timerView: {
        backgroundColor: '#276EF1', padding: 8
    },
    timerText: {
        color: 'white', fontSize: 13, fontFamily: fontFamilyRegular, textAlign: 'center'
    },
    listItemView: {
        flexDirection: 'row', padding: 15, borderBottomWidth: 1, borderBottomColor: "#E0E6ED"
    },
    listItemImg: {
        width: 45, height: 45
    },
    listItemPriceView: {
        justifyContent: 'center', paddingLeft: 15, flex: 1
    },
    listItemPriceText: {
        color: "#34495E", fontSize: 18, fontFamily: fontFamilyBold
    },
    listItemNormalText: {
        color: "#34495E", fontSize: 15, fontFamily: fontFamilyRegular
    }
});