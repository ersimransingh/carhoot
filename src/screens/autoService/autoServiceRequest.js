import React from 'react';
import { View, Dimensions, Text, KeyboardAvoidingView, Platform, ScrollView, StatusBar, Image } from 'react-native';
import MapView, { PROVIDER_GOOGLE, PROVIDER_DEFAULT, Marker } from 'react-native-maps'
import { fontFamilyMedium, fontFamilyRegular, fontFamilyLight, fontFamilyBold } from '../../global/globalStyle'
import Header from '../../components/Header'
import Button from '../../components/Button'
import Input from '../../components/Input'
import SelectOption from '../../components/SelectOptionNew'
import navigationReducer from '../../redux/navigation/navigationReducer';
import { SafeAreaView } from 'react-native-safe-area-context';
import Geolocation from '@react-native-community/geolocation';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
import AsyncStorage from '@react-native-community/async-storage'
import SelectLocationNew from '../../components/SelectLocationNew'
import { updateLocationsData, setUserCars, updateRequests, setServicesSearchable, setServiceDetails, updateServiceDetails } from '../../redux'
import { useSelector, useDispatch } from 'react-redux';
import { TouchableOpacity } from 'react-native-gesture-handler';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';
import Toast from 'react-native-simple-toast';
import Api from '../../api';
import axios from 'axios';
import { originLatLong } from '../../globalVariable'
export default function AutoServiceRequest(props) {
    const dispatch = useDispatch()
    const [location, setLocation] = React.useState(props.route.params.lname);
    const [fromLocation, setFromLocation] = React.useState(JSON.stringify({ "lat": 42.0553757, "long": -87.8522143 }))
    const [loading, setLoader] = React.useState(false);
    const [carMakes1, setCarMakes1] = React.useState('Pick up car for service');
    const [longitude, setLongitude] = React.useState(props.route.params.long);
    const [latitude, setLatitude] = React.useState(props.route.params.lat);
    const [selectedData, setSelectedData] = React.useState(props.route.params.lname);
    const [date, setDate] = React.useState("")
    const [showdate, setShowDate] = React.useState(false)
    const userCars = useSelector(state => state.importCar.userCars)
    const Data = useSelector(state => state.servicedata.autoservice)
    const searchableService = useSelector(state => state.servicedata.servicesSearchable)
    const serviceDetails = useSelector(state => state.servicedata.serviceDetails)
    const arr = []
    const carMakesRedux = useSelector(state => state.cardata.carMake)
    const myCars = useSelector(state => state.user.carsData)
    const [selectedCarId, setSelectedCarId] = React.useState('')
    const [selectedCarName, setSelectedCarName] = React.useState("Choose your car")
    const [selectedService, setSelectedService] = React.useState('');
    const [selectedServiceId, setSelectedServiceId] = React.useState(props.route.params.item._id)
    const [datetime, setDatetime] = React.useState(Platform.OS == "ios" ? "datetime" : "date");
    const [androidDate, setAndroidDate] = React.useState(new Date());
    const [androidTime, setAndroidTime] = React.useState(new Date());
    const [showAndroidDate, setShowAndroidDate] = React.useState(false);
    const [showAndroidTime, setShowAndroidTime] = React.useState(false);
    const serviceTrim = useSelector(state => state.servicedata.serviceTrim)
    const createSearchableArray = (array) => {
        let arr = [];
        for (let i in array) {
            let a = { name: array[i], value: array[i], show: true }
            arr.push(a);
        }
        return arr;
    }
    const createMultiselectArray = (array) => {
        let arr = [];
        for (let i in array) {
            let a = { name: array[i], id: i + 1, status: false }
            arr.push(a);
        }
        return arr;
    }
    React.useEffect(() => {
        let services = [];
        for (let j in Data) {
            let a = { name: Data[j].autoservicetype, value: Data[j]._id, show: true }
            services.push(a);
        }
        dispatch(setServicesSearchable(services))
        let dta = [];
        for (let i in myCars) {
            let a = { name: myCars[i].make + "[ " + myCars[i].regno + " ]", value: myCars[i]._id, show: true }
            dta.push(a)
        }
        dispatch(setUserCars(dta));

    }, [false])
    const [carname, setcarName] = React.useState(arr.length != 0 ? arr[0].name : '')
    const [carMakes, setCarMakes] = React.useState(arr.length != 0 ? arr[0].value : '');
    const [pickupFor, setPickupFor] = React.useState([
        { name: 'Service at Premises', value: "Service at Premises" },
        { name: 'Pick Up Car', value: "Pick Up Car" },

    ])
    // get long lat
    const _submitReq = async () => {

        try {
            setLoader(true)
            const value = await AsyncStorage.getItem('token')
            axios.post(Api + "/api/getRescueRequest", {

            }, {
                headers: { 'authorization': value }
            }
            )
                .then((res) => {
                    if (res.data.status == true) {
                        dispatch(updateRequests(res.data.data))
                        setLoader(false)
                    }
                })
                .catch((error) => {
                    console.log(error.message)
                    setLoader(false)
                    Toast.show(JSON.stringify(error))
                })
        }

        catch (err) {
            setLoader(false)
            console.log(err)
        }

    }
    const _submitRequest = async () => {
        try {
            let distance = 1000;
            let getTimeandDistance = await axios.get("https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=" + originLatLong.lat + "," + originLatLong.long + "&destinations=" + latitude + "," + longitude + "&key=AIzaSyAB7cS3Wb_fyoN_GObjGhud-3PMNNRmfwc")
            console.log(getTimeandDistance.data.rows[0].elements[0].status);
            if (getTimeandDistance.data.rows[0].elements[0].status == "OK") {
                distance = parseInt(getTimeandDistance.data.rows[0].elements[0].distance.value / 1000);
            }
            if (selectedCarName == 'Choose your car') {
                Toast.show("Please select car")
            } else if (date == '') {
                Toast.show("Please select service Date")
            } else if (carMakes1 == 'Pick up car for service') {
                Toast.show("Please select service type")
            } else if (distance > 100) {
                Toast.show("We are not providing service at your location.")
            }
            else {
                let cName = "";
                let cTrim = "";
                for (let o in serviceDetails) {
                    if (serviceDetails[o].status) {
                        cName = serviceDetails[o].name
                    }
                }
                for (let p in serviceTrim) {
                    if (serviceTrim[p].status) {
                        cTrim = serviceTrim[p].name
                    }
                }
                let carMake = "";
                let carModel = "";
                let carEngine = "";
                for (let u in myCars) {
                    if (myCars[u]._id == selectedCarId) {
                        carMake = myCars[u].make;
                        carModel = myCars[u].model;
                        carEngine = myCars[u].engine_capacity
                    }
                }
                let d = props.route.params.item;
                setLoader(true)
                const value = await AsyncStorage.getItem('token')
                let dta = {
                    type: d._id,
                    longitude: longitude,
                    latitude: latitude,
                    placeid: selectedData,
                    whereto: "",
                    price: 0,
                    car: selectedCarId,
                    serviceType: "Auto",
                    serviceDate: date,
                    pickUpService: carMakes1,
                    requestType: props.route.params.item.autoservicetype,
                    categoryName: cName,
                    categoryTrim: cTrim,
                    carMake: carMake,
                    carModel: carModel,
                    carEngine: carEngine
                }
                axios.post(Api + "/api/addRescueRequest", dta, {
                    headers: { 'authorization': value }
                })
                    .then((res) => {
                        if (res.status == 200) {
                            _submitReq()
                            setLoader(false)
                            dta.dateserviceDate = "";
                            props.navigation.navigate('AutoServiceDetails', {
                                type: res.data.data.rescuetype + '  Request',
                                oid: res.data.data._id,
                                dta: res.data.data,
                                lastObj: d,
                                submitted: dta,
                                carname: selectedCarId,
                                latitude: latitude,
                                longitude: longitude
                            })
                        } else {
                            setLoader(false)
                        }

                    })
                    .catch((err) => {
                        console.warn(err)
                        setLoader(false)
                    })
            }
        }

        catch (err) {
            setLoader(false)
            console.log(err)
        }

    }

    // get long lat end
    const updateData = (index) => {
        let val = serviceDetails[index].status ? false : true
        dispatch(updateServiceDetails(index, val))
    }

    return (
        <View style={{ flex: 1, backgroundColor: 'white' }} >
            <StatusBar
                backgroundColor="#276EF1"
                barStyle="light-content"
            />
            <View style={{ backgroundColor: "#276EF1", height: 40 }}></View>
            <View style={{ backgroundColor: '#F8F8F8', zIndex: 100 }}>
                <View style={{ flexDirection: 'row' }}>
                    <View>
                        <TouchableOpacity
                            style={{ padding: 15 }}
                            onPress={() => props.navigation.goBack()}
                        >
                            <Image
                                source={require('../../assets/images/Left.png')}
                                style={{ width: 24, height: 24 }}
                            />
                        </TouchableOpacity>
                    </View>
                    <View style={{ flex: 1 }}>
                        <View style={{ padding: 15, marginTop: 5 }}>
                            <Text style={{ fontFamily: fontFamilyRegular, marginBottom: 10 }}>Confirm location and service details</Text>
                            <SelectLocationNew
                                label=""
                                placeholder={selectedData}
                                onSelect={(t) => {
                                    setFromLocation(JSON.stringify(t.cords));
                                    setLongitude(t.cords.long)
                                    setLatitude(t.cords.lat);
                                    setSelectedData(t.name)
                                }}
                            />
                            <View style={{ height: 20 }} />
                            <View style={{ zIndex: 21 }}>
                                <SelectOption
                                    label=""
                                    placeholder={selectedCarName}
                                    getId={true}
                                    selectedId={selectedCarId}
                                    onChangeText={(t) => {
                                        setSelectedCarId(t.value)
                                        setSelectedCarName(t.name)
                                    }}
                                    data={userCars}
                                />
                            </View>
                            <View style={{ height: 20 }} />
                            <View style={{ zIndex: 20 }}>
                                <SelectOption
                                    label=""
                                    placeholder={carMakes1}
                                    onChangeText={(t) => setCarMakes1(t)}
                                    data={pickupFor}
                                />
                            </View>
                            <View style={{ height: 20 }} />
                            <View style={{ zIndex: 19 }}>
                                <TouchableOpacity style={{ backgroundColor: '#F5F5F5', borderRadius: 0, borderWidth: 1, borderColor: '#276EF1', height: 45, backgroundColor: 'white', flexDirection: "row" }}
                                    onPress={() => {
                                        if (date == '') {
                                            setDate(new Date())
                                        }
                                        if (Platform.OS == "ios") {
                                            setShowDate(showdate ? false : true)
                                        } else {
                                            setShowAndroidDate(true);
                                        }
                                    }}
                                >
                                    <View style={{ flex: 1, justifyContent: "center", paddingLeft: 15 }}>
                                        <Text style={{ fontFamily: fontFamilyRegular, color: '#34495E', fontSize: 15 }}>{date == "" ? "Service Date" : moment(date).format('DD-MMM-YYYY HH:mm')}</Text>
                                    </View>
                                    <View style={{ justifyContent: "center", paddingRight: 15 }}>
                                        <Image
                                            source={require('../../assets/images/Calendar.png')}
                                            style={{ width: 14, height: 15.56 }}
                                        />
                                    </View>
                                </TouchableOpacity>
                                {showdate && Platform.OS == "ios" && (
                                    <View style={{ alignItems: "flex-end" }}>
                                        <TouchableOpacity style={{ padding: 5 }}
                                            onPress={() => {
                                                setShowDate(false)
                                            }}
                                        >
                                            <Text style={{ color: "#276EF1", fontWeight: "bold" }}>Done</Text>
                                        </TouchableOpacity>
                                    </View>
                                )}
                                {showAndroidDate &&
                                    <DateTimePicker
                                        value={androidDate}
                                        mode={"date"}
                                        display="default"
                                        minimumDate={new Date()}
                                        maximumDate={new Date(new Date().getTime() + (10 * 24 * 60 * 60 * 1000))}
                                        onChange={(t) => {
                                            if (t.type == 'set') {
                                                setShowAndroidDate(false);
                                                setAndroidDate(new Date(t.nativeEvent.timestamp));
                                                setShowAndroidTime(true);
                                            } else {
                                                setShowAndroidDate(false);
                                            }
                                        }}
                                    />
                                }
                                {showAndroidTime &&
                                    <DateTimePicker
                                        value={androidTime}
                                        mode={"time"}
                                        display="default"
                                        minimumDate={new Date()}
                                        maximumDate={new Date(new Date().getTime() + (10 * 24 * 60 * 60 * 1000))}
                                        onChange={async (t) => {
                                            if (t.type == 'set') {
                                                setShowAndroidDate(false);
                                                setShowAndroidTime(false);
                                                let date = moment(androidDate).format("DD-MM-YYYY");
                                                let time = moment(t.nativeEvent.timestamp).format("HH:mm");
                                                let newDate = moment(date + " " + time, "DD-MM-YYYY HH:mm").toDate()
                                                setDate(newDate)
                                                console.log(moment(t.nativeEvent.timestamp).format("HH:mm"))
                                                setAndroidTime(new Date(t.nativeEvent.timestamp))

                                            } else {
                                                setShowAndroidTime(false);
                                            }
                                        }}
                                    />
                                }
                                {showdate && (
                                    <DateTimePicker
                                        value={date}
                                        mode={datetime}
                                        display="default"
                                        minimumDate={new Date()}
                                        maximumDate={new Date(new Date().getTime() + (10 * 24 * 60 * 60 * 1000))}
                                        onChange={(t) => {
                                            if (Platform.OS == "ios") {
                                                setDate(new Date(t.nativeEvent.timestamp))
                                            } else {
                                                if (t.type == 'set') {
                                                    if (datetime == "date") {
                                                        setAndroidDate(new Date(t.nativeEvent.timestamp));
                                                        setDatetime('time');
                                                    } else {
                                                        setAndroidTime(new Date(t.nativeEvent.timestamp))
                                                        setShowDate(false);
                                                        setDate(new Date(t.nativeEvent.timestamp))
                                                        let date = moment(androidDate).format("DD-MM-YYYY");
                                                        let time = moment(androidTime).format("HH:mm")
                                                        let newDate = moment(date + " " + time, "DD-MM-YYYY HH:mm").toDate()
                                                        setDate(newDate)
                                                        setDatetime('date');
                                                    }
                                                } else {
                                                    setDatetime('date');
                                                    setShowDate(false);
                                                }
                                            }

                                        }}
                                    />
                                )}
                            </View>
                        </View>
                    </View>
                </View>

            </View>
            <View style={{ marginTop: 15, backgroundColor: "#FFFFFF", flex: 1 }}>
                <View style={{ flex: 1 }}>
                    <View style={{ zIndex: 0, position: "relative" }}>
                        <TouchableOpacity style={{ flexDirection: 'row', padding: 5, backgroundColor: 'transparent', zIndex: -1 }}
                            onPress={() => props.navigation.navigate('SelectLocation', props.route.params)}
                        >
                            <View>
                                <Image
                                    source={require('../../assets/images/mapLocationReturn.png')}
                                    style={{ width: 45, height: 45 }}
                                />
                            </View>
                            <View style={{ justifyContent: "center", paddingLeft: 20 }}>
                                <Text style={{ color: '#276EF1', fontSize: 15, fontFamily: fontFamilyRegular }}>Set location from map</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ padding: 15 }}>
                    <Button
                        name="Get service quote"
                        loading={loading}
                        onPress={() => _submitRequest()}
                    />
                </View>
            </View>
        </View>

    )
}