import React from 'react';
import { View, Text, StatusBar, SafeAreaView, Image, ScrollView, ActivityIndicator } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler'
import { fontFamilyBold, fontFamilyRegular } from '../../global/globalStyle';
import { useSelector, useDispatch } from 'react-redux';
import { updateLocationsData, setUserCars, updateRequests, setServicesSearchable, setServiceDetails, updateServiceDetails, updateServiceTrim, setServiceTrim } from '../../redux'
import Button from '../../components/Button'
import moment from 'moment';
import Toast from 'react-native-simple-toast';
import Api from '../../api';
import axios from 'axios';

export default function ServiceCategory(props) {
    const dispatch = useDispatch()
    const [selectedServiceId, setSelectedServiceId] = React.useState(props.route.params.item._id)
    const [loading, setLoading] = React.useState(false);
    const Data = useSelector(state => state.servicedata.autoservice)
    const searchableService = useSelector(state => state.servicedata.servicesSearchable)
    const serviceDetails = useSelector(state => state.servicedata.serviceDetails)
    const serviceTrim = useSelector(state => state.servicedata.serviceTrim)

    const updateData = (index) => {
        let all = [];
        for (let i in serviceDetails) {
            let a = serviceDetails[i];
            a.status = false;
            all.push(a);
        }
        dispatch(setServiceDetails(all))
        let val = serviceDetails[index].status ? false : true
        dispatch(updateServiceDetails(index, val))
    }
    const updateTrim = (index) => {
        let all = [];
        for (let i in serviceTrim) {
            let a = serviceTrim[i];
            a.status = false;
            all.push(a);
        }
        dispatch(setServiceTrim(all));
        let val = serviceTrim[index].status ? false : true
        dispatch(updateServiceTrim(index, val))
    }
    const submitData = () => {
        let selectedService = false;
        let selectedTrim = false;
        for (let i in serviceDetails) {
            if (serviceDetails[i].status) {
                selectedService = true;
            }
        }
        for (let j in serviceTrim) {
            if (serviceTrim[j].status) {
                selectedTrim = true;
            }
        }
        if (!selectedService) {
            Toast.show("Please select atleast one service.")
        } else if (!selectedTrim) {
            Toast.show("Please select atleast one service trim.")
        } else {
            props.navigation.navigate('SelectLocation', props.route.params)
        }
    }
    const _getDetailsandTrim = async () => {
        setLoading(true);
        axios.get(Api + "/api/service-price/" + props.route.params.item.autoservicetype)
            .then((res) => {
                setLoading(false);
                if (res.data.status) {
                    let arr = [];
                    let arrTrim = [];
                    for (let i in res.data.serviceType) {
                        let a = { name: res.data.serviceType[i].name, company: res.data.serviceType[i].detail, status: false }
                        arr.push(a);
                    }
                    for (let i in res.data.serviceTrim) {
                        let a = { name: res.data.serviceTrim[i].name, company: res.data.serviceTrim[i].detail, status: false }
                        arrTrim.push(a);
                    }

                    dispatch(setServiceDetails(arr));
                    dispatch(setServiceTrim(arrTrim));
                } else {
                    console.log(res.data)
                }

            })
            .catch((err) => {
                setLoading(false);
                console.log(err)
            })


    }
    React.useEffect(() => {

        dispatch(setServiceDetails([]));
        dispatch(setServiceTrim([]));
        _getDetailsandTrim()
    }, [false])
    return (

        <View style={{ flex: 1, backgroundColor: 'white' }}>
            {loading &&
                <View style={{ backgroundColor: 'rgba(0,0,0,0.7)', position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, zIndex: 10, justifyContent: 'center' }}>
                    <ActivityIndicator
                        size="large"
                        color="#276EF1"
                    />
                </View>
            }
            <StatusBar
                barStyle="light-content"
                backgroundColor="#276EF1"
            />
            <View style={{ backgroundColor: "#276EF1", height: 40 }}></View>
            <SafeAreaView style={{ flex: 1, zIndex: 2 }}>
                <View style={{ flexDirection: "row", paddingVertical: 15, backgroundColor: "#F8F8F8" }}>
                    <TouchableOpacity style={{ padding: 10 }}
                        onPress={() => props.navigation.goBack()}
                    >
                        <Image
                            source={require('../../assets/images/Left.png')}
                            style={{ width: 24, height: 24 }}
                        />
                    </TouchableOpacity>
                    <View style={{ flex: 1, justifyContent: "center" }}>
                        <Text style={{ color: "#34495E", textAlign: "left", fontSize: 15, fontFamily: fontFamilyRegular }}>
                            Select service details
                        </Text>
                    </View>
                    <View style={{ width: 100 }}>

                    </View>
                </View>

                <View style={{ flex: 1, marginTop: 10, backgroundColor: "#F8F8F8", paddingHorizontal: 15 }}>
                    <View style={{ flex: 1, paddingVertical: 10 }}>
                        <Text style={{ fontFamily: fontFamilyRegular, color: '#276EF1', fontSize: 15 }}>Select Service Details</Text>
                        <ScrollView style={{ padding: 15 }}>

                            {serviceDetails.map((data, key) => {
                                return (
                                    <TouchableOpacity style={{ flexDirection: 'row', padding: 15, borderWidth: data.status ? 1 : 0, borderRadius: 5, backgroundColor: "#EEEEEE", borderColor: "#276EF1", marginBottom: 10 }} key={key}
                                        onPress={() => updateData(key)}
                                    >
                                        <View style={{ flex: 1 }}>
                                            <Text style={{ color: '#34495E', fontFamily: fontFamilyBold, fontSize: 15 }}>{data.name}</Text>
                                            <Text style={{ color: '#34495E', fontFamily: fontFamilyRegular, fontSize: 13, marginTop: 10 }}>{data.company}</Text>
                                        </View>

                                    </TouchableOpacity>
                                )
                            })}
                        </ScrollView>
                    </View>
                    <View style={{ flex: 0.8, paddingVertical: 10 }}>
                        <Text style={{ fontFamily: fontFamilyRegular, color: '#276EF1', fontSize: 15 }}>Select Service Trim</Text>
                        <ScrollView style={{ padding: 15 }}>

                            {serviceTrim.map((data, key) => {
                                return (
                                    <TouchableOpacity style={{ flexDirection: 'row', padding: 15, borderWidth: data.status ? 1 : 0, borderRadius: 5, backgroundColor: "#EEEEEE", borderColor: "#276EF1", marginBottom: 10 }} key={key}
                                        onPress={() => updateTrim(key)}
                                    >
                                        <View style={{ flex: 1 }}>
                                            <Text style={{ color: '#34495E', fontFamily: fontFamilyBold, fontSize: 15 }}>{data.name}</Text>
                                            <Text style={{ color: '#34495E', fontFamily: fontFamilyRegular, fontSize: 13, marginTop: 10 }}>{data.company}</Text>
                                        </View>

                                    </TouchableOpacity>
                                )
                            })}
                        </ScrollView>
                    </View>

                </View>
                {/* Button */}
                <View style={{ padding: 15, backgroundColor: "#F8F8F8" }}>
                    <Button
                        name="Select service details"
                        onPress={() => submitData()}
                    />
                </View>
            </SafeAreaView>
        </View>
    )
}