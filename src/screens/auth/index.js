import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import TermsConditions from './TermsConditions';
import Login from './Login';
import SignUp from './SignUp';
import VerifyCode from './VerifyCode';
import SlideShow from './SlideShow'
const Stack = createStackNavigator();

export default function AuthIndex() {

    const [screen, setScreen] = React.useState('login');
    return (
        <Stack.Navigator
            screenOptions={{
                headerShown: false
            }}
        >
            {screen &&
                <>
                    <Stack.Screen name="SlideShow" component={SlideShow} />
                    <Stack.Screen name="SignUp" component={SignUp} />
                    <Stack.Screen name="Login" component={Login} />
                    <Stack.Screen name="TermsConditions" component={TermsConditions} />
                    <Stack.Screen name="VerifyCode" component={VerifyCode} />
                </>
            }
        </Stack.Navigator>
    )
}