import React from 'react'
import { ActivityIndicator, Dimensions, Alert, View, Text, ImageBackground, ScrollView, SafeAreaView, StatusBar, Image, TouchableOpacity, KeyboardAvoidingView, Platform } from 'react-native';

import Button from '../../components/Button'

import { useSelector, useDispatch } from 'react-redux';
import { updateUserData, updateRequests, updateState, getCarMake, getCarModels, updateUserCarData, getRescueData, getAutoServiceData, confirmPin } from '../../redux';
import ContactInput from '../../components/ContactInput'
import styles from './AuthStyles'

import auth from '@react-native-firebase/auth';
import Toast from 'react-native-simple-toast';
export default function Login({ navigation }) {
    const dispatch = useDispatch()
    const [old, setOld] = React.useState(false);
    const [email, setEmail] = React.useState('');
    const [password, setPassword] = React.useState('');
    const window = Dimensions.get('window')
    const validateEmail = (email) => {
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
    const [vemail, setVemail] = React.useState(false);
    const [vpassword, setVpassword] = React.useState(false);
    const [loading, setLoader] = React.useState(false);
    const [countryCode, setCountryCode] = React.useState(254);
    const [contact, setContact] = React.useState('');
    const [contactv, setContactv] = React.useState(false);
    const [countryAlpha, setCountryAlpha] = React.useState('KE')
    const [showErr, setShowErr] = React.useState(false);
    const [err, setErr] = React.useState('');
    async function signInWithPhoneNumber() {
        try {
            if (contact == '') {
                setShowErr(true);
                setErr("Please enter contact number first.")
            }
            else if (contact.length < 7 || contact.length > 13) {
                setShowErr(true);
                setErr("Contact number should be 7 to 13 digits.")
            } else {
                setLoader(true);
                setShowErr(false);
                let cont = contact.replace(/^0+/, '');
                const confirmation = await auth().signInWithPhoneNumber('+' + countryCode + cont);
                dispatch(confirmPin(confirmation))
                navigation.navigate('VerifyCode', { phoneNumber: '+' + countryCode + cont, cc: countryCode, phone: cont });
                setLoader(false);
            }

        }
        catch (err) {
            setShowErr(false);
            alert(err + " Call us on +254 720 039 039.");
            console.log(err);
            setLoader(false);
        }
    }
    return (
        <View style={styles.loginContainer}>
            <ImageBackground
                source={require('../../assets/images/loginBg.png')}
                style={styles.loginBGImageStyle}
                imageStyle={styles.loginBgImage}
            >
                <SafeAreaView style={{ flex: 1 }}>
                    <KeyboardAvoidingView style={{ flex: 1 }} behavior={Platform.OS == 'ios' ? "padding" : 'height'}>
                        <ScrollView
                            showsVerticalScrollIndicator={false}
                            showsHorizontalScrollIndicator={false}
                        >
                            <View style={{ height: window.width / 10 }} />
                            <Image
                                source={require("../../assets/images/blue_logo.png")}
                                style={{ width: 100, height: 19 }}
                            />
                            <View style={{ height: window.width / 2.4 }} />
                            <Text style={styles.loginGetStarted}>Get {'\n'}Started!</Text>
                            <View style={{ height: window.width / 10 }} />
                            <ContactInput
                                label="What is your phone number ?"
                                placeholder="Phone Number"
                                value={contact}
                                valueCountryCode={countryCode}
                                onChangeText={(t) => setContact(t)}
                                onChangeCountryCode={(t) => setCountryCode(t)}
                                isValid={contactv}
                                onUpdateCountryAlpha={(t) => setCountryAlpha(t)}
                                cc2={countryAlpha}
                                onDone={() => signInWithPhoneNumber()}
                                isonLogin={true}
                            />
                            {showErr &&
                                <Text style={styles.loginErrorStyle}>{err}</Text>
                            }
                            <View style={{ height: 20 }} />
                            <Button
                                loading={loading}
                                name="Get Started"
                                onPress={() => signInWithPhoneNumber()}
                            />
                        </ScrollView>
                    </KeyboardAvoidingView>
                </SafeAreaView>
            </ImageBackground>
        </View>
    )
}