import React from 'react'
import { Alert, View, Text, Image, ImageBackground, ScrollView, SafeAreaView, StatusBar, TouchableOpacity, Dimensions, KeyboardAvoidingView, Platform, Modal } from 'react-native';
import { global, fontFamilyRegular, fontFamilyBold } from '../../global/globalStyle'
import Button from '../../components/Button'
import Input from '../../components/Input'
import Api from '../../api';
import { useDispatch } from 'react-redux';
import axios from 'axios';
import Toast from 'react-native-simple-toast';
import { updateState, updateUserData, updateRequests, updateUserCarData, getCarMake, getCarModels, getAutoServiceData, getRescueData } from '../../redux'
import AsyncStorage from '@react-native-community/async-storage';
import messaging from '@react-native-firebase/messaging';
import styles from './AuthStyles'
import { BlurView } from "@react-native-community/blur";
const window = Dimensions.get('window')
export default function SignUp(props) {
    const [name, setName] = React.useState('');
    const [email, setEmail] = React.useState('');
    const [password, setPassword] = React.useState('');
    const [loading, setLoading] = React.useState(false);

    const validateEmail = (email) => {
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
    const ref_input2 = React.useRef();
    const ref_input3 = React.useRef();
    const [vname, setVname] = React.useState(false);
    const [vemail, setVemail] = React.useState(false);
    const [showErrors, setShowErrors] = React.useState(false);
    const [vpassword, setVpassword] = React.useState(false);
    const dispatch = useDispatch()

    _storeData = async (token) => {
        try {
            await AsyncStorage.setItem(
                'token', token
            )
        } catch (error) {
            Toast.show('Token Error signup page')
        }
    }
    const _getautoService = (value) => {
        axios.post(Api + "/api/getautoService", {}, {
            headers: { 'authorization': value }
        }
        )
            .then((res) => {
                if (res.data.status == true) {
                    dispatch(getAutoServiceData(res.data.data))
                }
                else {
                    console.log('rescue data error')
                }
            })
            .catch((err) => {
                console.log(err)
            })
    }
    const _getRescueData = async () => {
        const value = await AsyncStorage.getItem('token')
        _submitRequest()
        _getautoService(value)
        try {
            axios.post(Api + "/api/getrescueService", {}, {
                headers: { 'authorization': value }
            }
            )
                .then((res) => {
                    if (res.data.status == true) {
                        dispatch(getRescueData(res.data.data))
                    }
                    else {
                        console.log('rescue data error')
                    }
                })
                .catch((err) => {
                    console.log(err)
                })
        }
        catch (err) {
            console.log(err)
        }
    }
    const _getuserData = (token) => {
        axios.post(Api + "/api/getcars/", {}, {
            headers: {
                'authorization': token
            }
        })
            .then((res) => {
                if (res.data.status == true) {

                    dispatch(updateUserCarData(res.data.data))
                }
            })
    }
    const _checkToken = async () => {
        const value = await AsyncStorage.getItem('token');
        if (value != 'false' && value != null && value != undefined) {
            axios.post(Api + "/api/checkToken/", {}, {
                headers: {
                    'authorization': value
                }
            })
                .then(res => {
                    if (res.data.status == true) {
                        let a = res.data.data.user
                        _getuserData(value)
                        // _getModels()
                        _getRescueData()
                        dispatch(updateUserData(a))

                    }
                    else {
                        dispatch(updateState('auth'))
                        Toast.show('Session Expired Login Again')
                    }
                })
                .catch(error => { throw (error) });

        }
    }
    const _submitRequest = async () => {

        try {
            const value = await AsyncStorage.getItem('token')
            axios.post(Api + "/api/getRescueRequest", {

            }, {
                headers: { 'authorization': value }
            })
                .then((res) => {
                    if (res.data.status == true) {
                        dispatch(updateRequests(res.data.data))
                    }
                })
                .catch((error) => {
                    console.log(error.message)
                    Toast.show(JSON.stringify(error))
                })
        }

        catch (err) {
            console.log(err)
        }

    }
    const Register = async () => {
        let a = await messaging().getToken();
        if (!vname && !vemail) {
            setVemail(null);
            setVname(null);
            setShowErrors(true);
        }
        else if (!vname) {
            setVname(null);
        }
        else if (!vemail) {
            setVemail(null);
        }
        else {

            try {
                setLoading(true)
                axios.post(Api + "/api/signup", {
                    "name": name,
                    "password": "Admin@1313",
                    "email": email.toLowerCase(),
                    "deviceToken": a,
                    'cc': props.route.params.cc,
                    'mobile': props.route.params.phone
                })
                    .then(res => {
                        console.log(res.data)
                        if (res.data.status == true) {
                            _storeData(res.data.token)
                            _checkToken()
                            setName('')
                            setEmail('')
                            setPassword('')
                            // _getModels()
                            _getRescueData()
                            _getuserData(res.data.token)
                            setTimeout(() => {
                                dispatch(updateState('Dashboard'))
                                setLoading(false)
                            }, 1500)

                            setModalVisible(true);
                            setLoading(false);
                        }
                        else {
                            setLoading(false)
                            Toast.show("Email already taken, Please use another one.")
                        }
                    })
                    .catch((error) => {
                        {
                            console.log(error)
                            setLoading(false)
                            // throw (error)
                        }
                    });
            } catch (err) {
                console.log(err)
            }
        }
    }
    React.useEffect(() => {
        if (name.length > 2) {
            setVname(name);
        } else {
            setVname(false);
        }
        if (validateEmail(email)) {

            setVemail(email)
        } else {
            setVemail(false)
        }
    }, [name, email, password])
    const [modalVisible, setModalVisible] = React.useState(false);
    return (
        <View style={{ backgroundColor: 'white', flex: 1 }}>
            <Modal
                animationType="fade"
                transparent={true}
                visible={modalVisible}
            >
                {Platform.OS == 'ios' ?
                    <BlurView style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
                        blurType="xlight"
                        blurAmount={10}
                        downsampleFactor={25}
                    >
                        <View style={{ padding: 30, backgroundColor: 'white', justifyContent: 'center', alignItems: 'center', borderRadius: 30 }}
                            onPress={() => setModalVisible(false)}
                        >
                            <Image
                                source={require('../../assets/images/signUpDone.png')}
                                style={{ width: 60, height: 60 }}
                            />
                            <Text style={{ fontFamily: fontFamilyBold, fontSize: 20, color: '#34495E' }}>Signed Up</Text>
                        </View>
                    </BlurView> :
                    <View style={{ flex: 1, backgroundColor: "rgba(255,255,255,0.7)", justifyContent: 'center', alignItems: 'center' }}>
                        <View style={{ padding: 30, backgroundColor: 'white', justifyContent: 'center', alignItems: 'center', borderRadius: 30 }}
                            onPress={() => setModalVisible(false)}
                        >
                            <Image
                                source={require('../../assets/images/signUpDone.png')}
                                style={{ width: 60, height: 60 }}
                            />
                            <Text style={{ fontFamily: fontFamilyBold, fontSize: 20, color: '#34495E' }}>Signed Up</Text>
                        </View>
                    </View>
                }
            </Modal>
            <ImageBackground style={{ flex: 1, backgroundColor: 'white' }}
                source={require('../../assets/images/signUp.png')}
                style={{ flex: 1, padding: 15 }}
                imageStyle={styles.loginBgImage}
            >
                <SafeAreaView style={{ flex: 1 }}>
                    <KeyboardAvoidingView style={{ flex: 1 }} behavior={Platform.OS == 'ios' ? "padding" : 'height'}>
                        <ScrollView
                            showsVerticalScrollIndicator={false}
                            showsHorizontalScrollIndicator={false}
                        >
                            <View style={{ height: window.width / 10 }} />
                            <Image
                                source={require("../../assets/images/blue_logo.png")}
                                style={{ width: 100, height: 19 }}
                            />
                            <View style={{ height: window.width / 2.4 }} />
                            <Text style={[styles.loginGetStarted, { fontSize: 30 }]}>Let's complete {'\n'}setting up your account.</Text>
                            <View style={{ height: window.width / 10 }} />
                            <Input
                                label=""
                                placeholder="Name"
                                value={name}
                                onChangeText={(t) => setName(t)}
                                isValid={vname}
                                returnKeyType={'next'}
                                onSubmitEditing={() => ref_input2.current.focus()}
                                blurOnSubmit={false}
                                err={'Name Should Be Of Atleast 3 digits'}
                            />
                            <View style={{ height: 30 }} />
                            <Input
                                label=""
                                placeholder="Email"
                                value={email}
                                returnKeyType={'done'}
                                onChangeText={(t) => setEmail(t)}
                                isValid={vemail}
                                r={ref_input2}
                                err={'Please Enter Valid Email'}
                            />
                            <View style={{ height: 30 }} />
                            <View>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={styles.signUpAgree}>By signing up I agree to the </Text>
                                    <TouchableOpacity
                                        onPress={() => props.navigation.navigate("TermsConditions")}
                                    >
                                        <Text style={[styles.signUpAgree, { color: '#276EF1' }]}>Terms &amp; Conditions</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={{ height: 30 }} />

                            <Button
                                loading={loading}
                                name="Sign Up"
                                onPress={() => {
                                    Register()
                                }}
                            />
                        </ScrollView>
                    </KeyboardAvoidingView>
                </SafeAreaView>
            </ImageBackground>
        </View >
    )
}