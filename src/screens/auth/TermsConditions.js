import React from 'react'
import { View, Text, ImageBackground, ScrollView, SafeAreaView, StatusBar, Dimensions } from 'react-native';
import { global, fontFamilyRegular, fontFamilyBold } from '../../global/globalStyle'
import Button from '../../components/Button'
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
export default function TermsConditions({ navigation }) {

    return (
        <View style={{ flex: 1, backgroundColor: 'white' }}>
            <View
                style={{ width: windowHeight - 100, height: windowHeight - 100, backgroundColor: "rgba(202,225,252,0.2)", borderRadius: windowHeight, marginLeft: -windowWidth + 100, marginTop: -windowHeight / 5, zIndex: 1, position: "absolute" }}
            />
            <View
                style={{ width: windowWidth - 100, height: windowWidth - 100, backgroundColor: "rgba(244,224,234,0.2)", borderRadius: windowWidth, zIndex: 1, marginTop: -windowWidth / 3, marginLeft: - windowWidth / 3, position: "absolute", top: windowHeight / 1.7 }}
            />
            <SafeAreaView
                style={{ flex: 1, zIndex: 20000 }}
            >

                <View style={{ flex: 1 }}>
                    <ScrollView style={{
                        padding: 15,
                        zIndex: 20000
                    }}>
                        <View>
                            <Text style={{ color: '#276EF1', fontSize: 25, fontFamily: fontFamilyBold }}>Carhoot</Text>
                        </View>
                        <View style={{ height: 30 }} />
                        <View style={{ paddingBottom: 10 }}>
                            <Text style={{ color: '#34495E', fontSize: 32, fontFamily: fontFamilyBold, lineHeight: 44 }}>Terms and condition</Text>
                        </View>
                        <View style={{
                            backgroundColor: 'transparent', borderRadius: 5, marginTop: 15
                        }}>
                            <Text style={{ color: '#34495E', fontFamily: fontFamilyBold, fontSize: 18, lineHeight: 30 }}>BACKGROUND</Text>
                            <Text style={{ color: '#34495E', fontSize: 13, fontFamily: fontFamilyRegular, lineHeight: 20 }}>
                                These Terms of Use, together with any and all other documents referred to herein, set out the terms of use under which you may use this Application, (“Our Application”). You must read, agree and accept all the Terms set out in this agreement which includes the terms and conditions set out below. You will be required to read and accept these Terms of Use when signing up for an Account. If you do not accept all the terms of this agreement, then Carhoot is unwilling to allow you to use the Carhoot platform. By using any of the services you become a Service Provider on the Carhoot platform and you agree to be bound by the terms and conditions of this agreement with respect to the provision of such services. If you do not agree to be bound by the terms of this agreement, do not use Carhoot. We may amend this agreement at any time by posting the amended terms on the Carhoot application. You may not have access to the Carhoot application before accepting these terms. These Terms of Use do not apply to the sale of services.
                            {'\n'}
                            </Text>
                            <Text style={{ color: '#34495E', fontFamily: fontFamilyBold, fontSize: 18, lineHeight: 30 }}>Definitions and Interpretation</Text>
                            <Text style={{ color: '#34495E', fontSize: 13, fontFamily: fontFamilyRegular, lineHeight: 20 }}>
                                In these Terms of Use, unless the context otherwise requires, the following expressions have the following meanings:
                            {"\n"}
                            “Account”  means an account required for a User to access and/or use certain areas of Application, as detailed in Clause 4;
                            {"\n"}
                            “Content” means any and all text, images, audio, video, scripts, code, software, databases and any other form of information capable of being stored on a computer that appears on, or forms part of, Our Site;
                            {"\n"}
                            “Carhoot”  means all versions of the Carhoot applications.
                            {"\n"}
                            “Carhoot Equipment”  means all equipment owned by the licensee of the Carhoot Applications.
                            {"\n"}
                            “Carhoot Support  Desk”  means the in-person support and the call-centre available to all current and prospective SafeBoda Users and Service providers in the various locations where Carhoot is operational.
                            {"\n"}
                            “Service Provider”  means any individual or business offers goods or services to Customers/Users on the Carhoot platform.
                            {"\n"}
                            “User”  means a user or consumer of any Carhoot Applications and/or a beneficiary to Services extended through our Application.
                            {"\n"}
                            “User Content”  means any content submitted to Our Application by Users including, but not limited to include personal information, images and all data
                            {"\n"}
                            “We/Us/Our”  means all licensees of the Carhoot applications.
                            {'\n'}
                            </Text>
                            <Text style={{ color: '#34495E', fontFamily: fontFamilyBold, fontSize: 18, lineHeight: 30 }}>1.	Information About Us</Text>
                            <Text style={{ color: '#34495E', fontSize: 13, fontFamily: fontFamilyRegular, lineHeight: 20 }}>
                                More  information  about  Carhoot  is  available  at  our  website  -
                                www.carhoot.app, or on request by emailing jambo@carhoot.app
                                {'\n'}
                            </Text>
                            <Text style={{ color: '#34495E', fontFamily: fontFamilyBold, fontSize: 18, lineHeight: 30 }}>2.	Access to Our Application</Text>
                            <Text style={{ color: '#34495E', fontSize: 13, fontFamily: fontFamilyRegular, lineHeight: 20 }}>
                                2.1.    We reserve the right to discontinue, stop or preclude anyone from accessing our Application at any time and for any period. Will not be liable to you in any way if such access is made unavailable to you at any time.
                            {"\n"}
                            2.2.    It is the responsibility of the Carhoot user to make any and all arrangements necessary in order to access Our Application. Any User who is unable to access the Application should contact us for clarification and guidance.
                            {"\n"}
                            2.3.    Access to Our Application is provided “as is” and on an “as available” basis. We may alter, suspend or discontinue Our Application (or any part of it) at any time and without notice. We will not be liable to you in any way if Our Application (or any part of it) is unavailable at any time and for any period.
                            {'\n'}
                            </Text>
                            <Text style={{ color: '#34495E', fontFamily: fontFamilyBold, fontSize: 18, lineHeight: 30 }}>3.	Terms of Use</Text>
                            <Text style={{ color: '#34495E', fontSize: 13, fontFamily: fontFamilyRegular, lineHeight: 20 }}>
                                3.1.    Carhoot Service providers remain fully independent business people. The Carhoot application is a mechanism for a Service Providers to connect with Users, to whom they may wish to provide Services.
                            {"\n"}
                            3.2.    Neither the provision of access to the Carhoot application, nor any other action by Carhoot, constitutes an employment agreement or contract for services.
                            {"\n"}
                            3.3.    Carhoot Service providers shall take reasonable precaution in all actions and interactions with any party they may interact with through the use of the services.
                            {"\n"}
                            3.4.    Carhoot Service providers are required to adhere to the Carhoot Code of Conduct in the relevant country when using the Application or any other Carhoot equipment. The Code of Conduct will be provided to the Carhoot Service providers during the registration process and will be updated from time to time. It is the responsibility of the Carhoot Service providers to ensure that they are familiar with the latest version of the Code of Conduct.
                            {"\n"}
                            3.5.    The Carhoot application provides fair and transparent prices for both Service Providers and Users. Carhoot Service providers  agree to adhere to the pricing set out by the Carhoot application.
                            {"\n"}
                            3.9.    We reserve the right to charge a commission or other service fee to Carhoot Service providers and usersfor access to the Carhoot platform. Such charges will be communicated in advance. It is the responsibility of the users and service providers to clearly monitor all communications and ensure that they are aware of any changes in the structure of pricing.
                            {"\n"}
                            3.10.    Any questions about the agreement, the practices of Carhoot, or your dealings with us please contact us at jambo@carhoot.app
                            {'\n'}
                            </Text>
                            <Text style={{ color: '#34495E', fontFamily: fontFamilyBold, fontSize: 18, lineHeight: 30 }}>4.	Accounts</Text>
                            <Text style={{ color: '#34495E', fontSize: 13, fontFamily: fontFamilyRegular, lineHeight: 20 }}>
                                4.1.    Once a User is approved, he/she will be provided with an account.
                            {"\n"}
                            4.2.    When registering as a User  and creating an Account, the information the User provides must be accurate and complete. If any of the information changes at a later date, it is the User's responsibility to ensure that the Account is kept up-to-date.
                            {"\n"}
                            4.3.    It is the responsibility of the User to protect their account details and ensure that no-one else gains access. Any unauthorised access to a User account shall be the responsibility of the User and shall be liable for any resulting financial loss. If a User believes their Account is being used without their permission, please contact Us immediately on the Carhoot Customer Care Line. Carhoot will not be liable for any unauthorised use of any Account.
                            {"\n"}
                            4.4.    Any personal information provided in a Driver's Account will be collected, used, and held in accordance with the rights and obligations under the law, as set out in Clause 17.
                            {"\n"}
                            4.5.    Carhoot reserves the right to suspend or deactivate a User’s account at any time.
                            {"\n"}
                            </Text>
                            <Text style={{ color: '#34495E', fontFamily: fontFamilyBold, fontSize: 18, lineHeight: 30 }}>5.	Carhoot Equipment</Text>
                            <Text style={{ color: '#34495E', fontSize: 13, fontFamily: fontFamilyRegular, lineHeight: 20 }}>
                                5.1.    A registered Carhoot Service provider may, under some circumstances, be eligible to use Carhoot equipment, including but not limited to Carhoot -branded helmets, Carhoot -branded reflectors, Carhoot -Vehicle repair equipment
                            {"\n"}
                            5.2.    We reserve the right to charge the Carhoot Service provider for use of any arhoot equipment. Such charges will be clearly communicated to the Carhoot Service provider  at the point of collection of the equipment.
                            {"\n"}
                            5.3.    The charge in Clause 5.2 shall not be the purchase price of the equipment and neither shall it comprise of any arrangement to mean that the ownership of the equipment has transferred to the Service provider. For avoidance of doubt the equipment shall at all times remain the property of Carhoot.
                            {"\n"}
                            5.4.    Carhoot Service provider commit to take good care of all Carhoot Equipment at all times and to return Carhoot Equipment to Carhoot if at any point they decide to cease using the Carhoot Application.
                            {"\n"}
                            5.5.    Carhoot Service provider commit only to use any Carhoot Equipment while providing services through the Carhoot Application. Any Carhoot Service provider found to be using Carhoot Equipment while providing services through other mobile platform will be liable to have their Carhoot Account suspended and will be required to return all Carhoot Equipment to Carhoot.
                            {"\n"}
                            5.6.    Upon loss of the Carhoot Equipment the Service provider shall be liable to pay a replacement fee that shall be communicated at the time of the loss.
                            {"\n"}
                            </Text>
                            <Text style={{ color: '#34495E', fontFamily: fontFamilyBold, fontSize: 18, lineHeight: 30 }}>6.	Intellectual Property Rights</Text>
                            <Text style={{ color: '#34495E', fontSize: 13, fontFamily: fontFamilyRegular, lineHeight: 20 }}>
                                6.1.    All Content included on Our Site and the copyright and other intellectual property rights subsisting in that Content, unless specifically labelled otherwise, belongs to or has been licensed by us. All Content (including User Content) is protected by applicable Kenyan and international intellectual property laws and treaties.
                            {"\n"}
                            6.2.    You may not reproduce, copy, distribute, sell, rent, sub-licence, store, or in any other manner re-use Content from Our Application unless given express written permission to do so by Us.
                            {"\n"}
                            6.3.    A User may:
                            {"\n"}
                            6.3.1.     Access, view and use Our Application in a web browser (including any web browsing capability built into other types of software or app);
                            {"\n"}
                            6.3.2.     Download Our Application (or any part of it) for caching;
                            {"\n"}
                            6.3.3.     Print one copy of any page(s) from Our Application;
                            {"\n"}
                            6.3.4.     Download extracts from the pages on Our Application; and
                            {"\n"}
                            6.3.5.     Save pages from Our Application for later and/or offline viewing.
                            {"\n"}
                            6.4.    Our status as the owner and author of the Content on Our Application (or that of identified licensors, as appropriate) must always be acknowledged.
                            {"\n"}
                            6.5.    A User may not use any Content saved or downloaded from Our Application for commercial purposes without first obtaining a licence from Us (or our licensors, as appropriate) to do so.
                            {"\n"}
                            </Text>
                            <Text style={{ color: '#34495E', fontFamily: fontFamilyBold, fontSize: 18, lineHeight: 30 }}>7.	User Content</Text>
                            <Text style={{ color: '#34495E', fontSize: 13, fontFamily: fontFamilyRegular, lineHeight: 20 }}>
                                7.1. User Content on Our Site includes (but is not necessarily limited to) product reviews, comments etc.
                            {"\n"}
                            7.2. An Account is required if a User wishes to submit User Content. Please refer to Clause 3 for more information.
                            {"\n"}
                            7.3. A User agrees to be solely responsible for their User Content. Specifically they agree, represent and warrant that they have the right to submit the User Content and that all such User Content will comply with Our Acceptable Usage Policy, detailed below in Clause 10.
                            {"\n"}
                            7.4. A User agrees to be liable to us and will, to the fullest extent permissible by law, indemnify us for any breach of the warranties given by us.
                            {"\n"}
                            7.5. A User will be responsible for any loss or damage suffered by us as a result of such breach.
                            {"\n"}
                            7.6. A User (or your licensors, as appropriate) shall retain ownership of their User Content and all intellectual property rights subsisting therein. When a User submits User Content they grant Us an unconditional, non-exclusive, fully transferable, royalty-free, perpetual, worldwide license to use, store, archive, syndicate, publish, transmit, adapt, edit, reproduce, distribute, prepare derivative works from, display, perform and sub-license the User Content for the purposes of operating and promoting Our Application. In addition, the User also grants Other Users the right to copy and quote their User Content within Our Application.
                            {"\n"}
                            7.7. We may reject, reclassify, or remove any User Content from Our Site where, in Our sole opinion, it violates Our Acceptable Usage Policy, or If We receive a complaint from a third party and determine that the User Content in question should be removed as a result.
                            {"\n"}
                            </Text>
                            <Text style={{ color: '#34495E', fontFamily: fontFamilyBold, fontSize: 18, lineHeight: 30 }}>8.	Links to Our Application</Text>
                            <Text style={{ color: '#34495E', fontSize: 13, fontFamily: fontFamilyRegular, lineHeight: 20 }}>
                                8.1. A User may use or create a link to our Application provided that:
                            {"\n"}
                            8.2. It is done in a fair and legal manner;
                            {"\n"}
                            8.3. It is not done in a manner that suggests any form of association, endorsement or approval on Our part where none exists;
                            {"\n"}
                            8.4. A User does not use any logos or trademarks displayed on Our Application without Our express written permission; and
                            {"\n"}
                            8.5. A User  does not use our Application in a manner that causes damage to Our reputation or to take unfair advantage of it.
                            {"\n"}
                            8.6. Framing or embedding of Our Application on other websites is not permitted without Our express written permission. Please contact Us at support@carhoot.app for further information.
                            {"\n"}
                            8.7. A User shall not link to Our Application from any other site the main content of which contains material that:
                            {"\n"}
                            8.7.1. is sexually explicit; obscene, deliberately offensive, hateful or otherwise inflammatory;
                            {"\n"}
                            8.7.2. promotes violence;
                            {"\n"}
                            8.7.3. promotes or assists in any form of unlawful activity;
                            {"\n"}
                            8.7.4. discriminates against, or is in any way defamatory of, any person, group or class of persons, race, sex, religion, nationality, disability, sexual orientation, or age;
                            {"\n"}
                            8.7.5. Is intended or is otherwise likely to threaten, harass, annoy, alarm, inconvenience, upset, or embarrass another person;
                            {"\n"}
                            8.7.6. Is calculated or is otherwise likely to deceive another person;
                            {"\n"}
                            8.7.7. Is intended or is otherwise likely to infringe (or to threaten to infringe) another person’s privacy;
                            {"\n"}
                            8.7.8. misleadingly impersonates any person or otherwise misrepresents the identity or affiliation of a particular person in a way that is calculated to deceive
                            {"\n"}
                            8.7.9. implies any form of affiliation with Us where none exists;
                            {"\n"}
                            8.7.10. infringes, or assists in the infringement of, the intellectual property rights (including, but not limited to, copyright, trademarks and database rights) of any other party; or
                            {"\n"}
                            8.7.11. Is made in breach of any legal duty owed to a third party including, but not limited to, contractual duties and duties of confidence.
                            {"\n"}
                            8.8. The content restrictions in sub-Clause 8.7 do not apply to content submitted to sites by other users or drivers provided that the primary purpose of the site accords with the provisions of sub-Clause 8.7. A User, for example, is not prohibited from posting links on general-purpose social networking sites merely because another user may post such content. A User, however, is prohibited from posting links on websites which focus on or encourage the submission of such content from users.
                            {"\n"}
                            </Text>
                            <Text style={{ color: '#34495E', fontFamily: fontFamilyBold, fontSize: 18, lineHeight: 30 }}>9.	Links to Other Sites</Text>
                            <Text style={{ color: '#34495E', fontSize: 13, fontFamily: fontFamilyRegular, lineHeight: 20 }}>
                                9.1.    Links to other sites may be included on Our Application. Unless expressly stated, these sites are not under Our control. We neither assume nor accept responsibility or liability for the content of third-party sites. The inclusion of a link to another site on Our Application is for information only and does not imply any endorsement of the sites themselves or of those in control of them.
                                
                                {'\n'}
                            </Text>
                            <Text style={{ color: '#34495E', fontFamily: fontFamilyBold, fontSize: 18, lineHeight: 30 }}>10. Disclaimers</Text>
                            <Text style={{ color: '#34495E', fontSize: 13, fontFamily: fontFamilyRegular, lineHeight: 20 }}>
                                10.1. Insofar as is permitted by law, we make no representation, warranty, or guarantee that Our Application will meet your requirements, not infringe on the rights of third parties, be compatible with all software and hardware, or that it be secure.
                                {'\n'}
                            </Text>
                            <Text style={{ color: '#34495E', fontFamily: fontFamilyBold, fontSize: 18, lineHeight: 30 }}>11. Our Liability</Text>
                            <Text style={{ color: '#34495E', fontSize: 13, fontFamily: fontFamilyRegular, lineHeight: 20 }}>
                                11.1.    To the fullest extent permissible by law, We accept no liability to any User for any loss or damage, whether foreseeable or otherwise, in contract, tort (including negligence), for breach of statutory duty, or otherwise, arising out of or in connection with the use of (or inability to use) Our Application or the use of or reliance upon any Content (including User Content) included on Our Application.
                            {"\n"}
                            11.2.    To the fullest extent permissible by law, we exclude all representations, warranties, and guarantees (whether express or implied) that may apply to Our Application or any Content included on Our Application.
                            {"\n"}
                            11.3.    Our Application is intended for non-commercial use only. If you are a business user, we accept no liability for loss of profits, sales, business or revenue; loss of business opportunity, goodwill or reputation; loss of anticipated savings; business interruption; or for any indirect or consequential loss or damage.
                            {"\n"}
                            11.4.    Reasonable skill and care shall be exercised to ensure that Our Application is free from viruses and other malware. However, We accept no liability for any loss or damage resulting from a virus or other malware, a distributed denial of service attack, or other harmful material or event that may adversely affect your hardware, software, data or other material that occurs as a result of your use of Our Application (including the downloading of any Content from it) or any other site referred to on Our Application.
                            {"\n"}
                            11.5.    We neither assume nor accept responsibility or liability arising out of any disruption or non-availability of Our Application resulting from external causes including, but not limited to, ISP equipment failure, host equipment failure, communications network failure, natural events, acts of war, or legal restrictions and censorship.
                            {"\n"}

                            </Text>
                            <Text style={{ color: '#34495E', fontFamily: fontFamilyBold, fontSize: 18, lineHeight: 30 }}>12. Acceptable Usage Policy</Text>
                            <Text style={{ color: '#34495E', fontSize: 13, fontFamily: fontFamilyRegular, lineHeight: 20 }}>
                                12.1.    A User may only use Our Application in a manner that is lawful and that complies with the provisions of this Clause 12.
                            {"\n"}
                            12.2.    A User must ensure that he or she complies fully with any and all local, national or international laws and/or regulations;
                            {"\n"}
                            12.3.    A User shall Not use Our Application in any way, or for any purpose, that is unlawful or fraudulent; or for any purpose, that is intended to harm any person or persons in any way.
                            {"\n"}
                            12.4.    We reserve the right to suspend or terminate access to Our Application if a user is in material breach of the provisions of this Clause 12 or any of the other provisions of these Terms of Use. Specifically, we may take one or more of the following actions:
                            {"\n"}
                            12.4.1.     suspend, whether temporarily or permanently, your Account and/or your right to access Our Site;
                            {"\n"}
                            12.4.2.     remove any Driver Content submitted by a Driver that violates this Acceptable Usage Policy;
                            {"\n"}
                            12.4.3.     issue you with a written warning;
                            {"\n"}
                            12.4.4.     take legal proceedings against you for reimbursement of any and all relevant costs on an indemnity basis resulting from your breach;
                            {"\n"}
                            12.4.5.     take further legal action against you as appropriate;
                            {"\n"}
                            12.4.6.     disclose such information to law enforcement authorities as required or as We deem reasonably necessary; and/or
                            {"\n"}
                            12.4.7.     any other actions which We deem reasonably appropriate (and lawful).
                            {"\n"}
                            12.5.    We hereby exclude any and all liability arising out of any actions (including, but not limited to those set out above) that We may take in response to breaches of these Terms of Use.
                            {"\n"}
                            </Text>
                            <Text style={{ color: '#34495E', fontFamily: fontFamilyBold, fontSize: 18, lineHeight: 30 }}>13. Changes to these Terms of Use</Text>
                            <Text style={{ color: '#34495E', fontSize: 13, fontFamily: fontFamilyRegular, lineHeight: 20 }}>
                                13.1.    We may alter these Terms of Use at any time. If We do so, details of the changes will be highlighted at the top of this page and a prompt may pop up at the time of your next usage of our application after such changes have been made. Any such changes will become binding on the user upon first use of Our Application after the changes have been implemented. A User is therefore advised to check this page from time to time to acquaint themselves with any new changes that may have been affected.
                                {'\n'}
                            </Text>
                            <Text style={{ color: '#34495E', fontFamily: fontFamilyBold, fontSize: 18, lineHeight: 30 }}>14. Contacting Us</Text>
                            <Text style={{ color: '#34495E', fontSize: 13, fontFamily: fontFamilyRegular, lineHeight: 20 }}>
                                14.1.    To contact Us, please email Us at jambo@carhoot.app or using any of the methods provided on Our contact page at www.carhoot.app
                                {'\n'}
                            </Text>
                            <Text style={{ color: '#34495E', fontFamily: fontFamilyBold, fontSize: 18, lineHeight: 30 }}>15. Communications from Us</Text>
                            <Text style={{ color: '#34495E', fontSize: 13, fontFamily: fontFamilyRegular, lineHeight: 20 }}>
                                15.1.    By signing up for an Account User agrees to receive communications from carhoot or any carhoot partner. Such notices may relate to matters including, but not limited to, service changes, changes to these Terms of Use, Our Terms of Sale, and changes to your Account.
                            {"\n"}
                            15.2.    For questions or complaints about communications from Us , please contact Us at jambo@carhoot.app or via our official line.
                            {'\n'}
                            </Text>
                            <Text style={{ color: '#34495E', fontFamily: fontFamilyBold, fontSize: 18, lineHeight: 30 }}>16. Law and Jurisdiction</Text>
                            <Text style={{ color: '#34495E', fontSize: 13, fontFamily: fontFamilyRegular, lineHeight: 20 }}>
                                16.1.     These Terms and Conditions, and the relationship between the User and Us (whether contractual or otherwise) shall be governed by, and construed in accordance with the laws of the Kenya.
                            {'\n'}
                            </Text>
                        </View>
                        <View style={{ height: 40 }} />
                    </ScrollView>
                </View>
                <View style={{ paddingVertical: 10, paddingHorizontal: 15, marginTop: 0, marginBottom: 20 }}>
                    <Button
                        loading={false}
                        name="Done"
                        onPress={() => navigation.navigate('SignUp')}
                    />
                </View>
            </SafeAreaView>
        </View>
    )
}