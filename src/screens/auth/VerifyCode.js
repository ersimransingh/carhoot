import React from 'react'
import { Alert, View, Text, ImageBackground, ScrollView, SafeAreaView, StatusBar, Image, KeyboardAvoidingView, Platform, Modal } from 'react-native';
import { global, fontFamilyRegular, fontFamilyLight, fontFamilyBold } from '../../global/globalStyle'
import Button from '../../components/Button'
import { useSelector, useDispatch } from 'react-redux';
import OTP from '../../components/OTP'
import Api from '../../api';
import axios from 'axios';
import Toast from 'react-native-simple-toast';
import auth from '@react-native-firebase/auth';
import { TouchableOpacity } from 'react-native-gesture-handler';
import styles from './AuthStyles'
import messaging from '@react-native-firebase/messaging';
import { BlurView } from "@react-native-community/blur";
import AsyncStorage from '@react-native-community/async-storage'
import RNRestart from 'react-native-restart';
import { updateUserData, updateRequests, updateState, getCarMake, getCarModels, updateUserCarData, getRescueData, getAutoServiceData, confirmPin } from '../../redux';
export default function VerifyCode(props) {
    const confirmCode = useSelector(state => state.user.confirmPin);
    const [seconds, setSeconds] = React.useState(45)
    const [reset, setReset] = React.useState(true)
    const [dis, setDis] = React.useState(true)
    const [loading, setLoading] = React.useState(false);
    React.useEffect(() => {
        let interval = null;
        if (reset) {
            interval = setInterval(() => {
                setSeconds(seconds => seconds - 1);
            }, 1000);
            if (seconds == 0) {
                clearInterval(interval)

                setDis(false)
                setReset(false)
            }
        }
        return () => {
            clearInterval(interval)
        };
    }, [reset, seconds]);
    const [code, setCode] = React.useState('');
    const [otp, setOtp] = React.useState('');
    const [phone, SetPhone] = React.useState(props.route.params.phoneNumber)
    const dispatch = useDispatch()
    const [codeValid, setCodeValid] = React.useState(true);
    const [codeValidAll, setCodeValidAll] = React.useState(null)
    const _storeData = async (token) => {
        try {
            await AsyncStorage.setItem(
                'token', token
            )
            _checkToken()
        } catch (error) {
            setLoading(false)

            Toast.show('Token Error signup page')
        }
    }
    const _checkToken = async () => {
        const value = await AsyncStorage.getItem('token');
        if (value != 'false' && value != null && value != undefined) {
            axios.post(Api + "/api/checkToken/", {}, {
                headers: {
                    'authorization': value
                }
            })
                .then(res => {
                    if (res.data.status == true) {
                        let a = res.data.data.user
                        dispatch(updateUserData(a))
                        // _getModels()
                        _getRescueData()
                        _getuserData(value)
                        setModalVisible(true);
                        setLoading(false)
                        setTimeout(() => {
                            setModalVisible(false);
                            dispatch(updateState('Dashboard'))
                            setLoading(false)
                        }, 3000)
                    }
                    else {
                        dispatch(updateState('auth'))
                        Toast.show('Session Expired Login Again')
                    }
                })
                .catch(error => { throw (error) });

        }
    }

    const _getRescueData = async () => {

        const value = await AsyncStorage.getItem('token')
        _submitRequest()
        _getautoService(value)
        try {
            axios.post(Api + "/api/getrescueService", {}, {
                headers: { 'authorization': value }
            }
            )
                .then((res) => {
                    if (res.data.status == true) {
                        dispatch(getRescueData(res.data.data))
                    }
                    else {
                        console.log('rescue data error')
                    }
                })
                .catch((err) => {
                    console.log("ERR", err)
                })
        }
        catch (err) {
            console.log("ERR", err)
        }
    }
    const _submitRequest = async () => {

        try {
            const value = await AsyncStorage.getItem('token')
            axios.post(Api + "/api/getRescueRequest", {

            }, {
                headers: { 'authorization': value }
            }
            )
                .then((res) => {
                    if (res.data.status == true) {
                        dispatch(updateRequests(res.data.data))
                    }
                })
                .catch((error) => {
                    console.log("ERR 3", error.message)
                    Toast.show(JSON.stringify(error))
                })
        }
        catch (err) {
            console.log("err 4", err)
        }
    }
    const _getautoService = (value) => {
        axios.post(Api + "/api/getautoService", {}, {
            headers: { 'authorization': value }
        })
            .then((res) => {
                if (res.data.status == true) {
                    dispatch(getAutoServiceData(res.data.data))
                }
                else {
                    console.log('rescue data error')
                }
            })
            .catch((err) => {
                console.log("ERR 5", err)
            })
    }
    const _getuserData = (token) => {
        axios.post(Api + "/api/getcars/", {}, {
            headers: {
                'authorization': token
            }
        })
            .then((res) => {
                if (res.data.status == true) {
                    dispatch(updateUserCarData(res.data.data))
                }
            })
    }

    React.useEffect(() => {
        if (code.length == 6) {
            setCodeValid(false);
        } else {
            setCodeValid(true);
            setCodeValidAll(true);
        }
    }, [code])

    React.useEffect(() => {
        auth().onAuthStateChanged((user) => {
            if (user) {
                checkUserinDB()
            }
        });
    }, [])
    const checkUserinDB = async () => {
        let a = await messaging().getToken();
        try {
            axios.post(Api + "/api/siginWithPhoneNumber", { cc: props.route.params.cc, phoneNumber: props.route.params.phone, deviceToken: a })
                .then((res) => {
                    if (res.data.status == true) {
                        if (res.data.token) {

                            _storeData(res.data.token)
                            dispatch(updateState('Dashboard'))
                        } else {
                            props.navigation.navigate('SignUp', { cc: props.route.params.cc, phone: props.route.params.phone })
                            setModalVisible(false);
                            setLoading(false)
                        }
                    } else {
                        setModalVisible(false);
                        props.navigation.navigate('SignUp', { cc: props.route.params.cc, phone: props.route.params.phone })
                        setLoading(false)
                    }
                })
        } catch (err) {
            setModalVisible(false);
            setLoading(false);
            props.navigation.navigate('SignUp', { cc: props.route.params.cc, phone: props.route.params.phone })
        }
    }
    // React.useEffect(() => {
    //     checkUserinDB();
    // }, [])
    const onsubmit = async () => {
        setLoading(true)
        try {
            await confirmCode.confirm(code)
            checkUserinDB()
        } catch (err) {
            Toast.show("Invalid Code")
            setCodeValidAll(false);
            console.log("ERR", err);
            // alert(JSON.stringify(err))
            setLoading(false)
        }
    }
    const resendOtp = async () => {
        try {
            setSeconds(45)
            const confirmation = await auth().signInWithPhoneNumber(props.route.params.phoneNumber);
            dispatch(confirmPin(confirmation))
            setReset(true)
            setSeconds(45)
        } catch (err) {
            alert(err + "Call us on +254 720 039 039.");
            console.log(err);
        }
    }
    const [modalVisible, setModalVisible] = React.useState(false);
    return (
        <View style={styles.verifyCodeContainer}>
            <Modal
                animationType="fade"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    setModalVisible(false);
                }}
            >
                <BlurView style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
                    blurType="xlight"
                    blurAmount={10}
                    downsampleFactor={25}
                >
                    <View style={{ padding: 30, backgroundColor: 'white', justifyContent: 'center', alignItems: 'center', borderRadius: 30 }}>
                        <Image
                            source={require('../../assets/images/signUpDone.png')}
                            style={{ width: 60, height: 60 }}
                        />
                        <Text style={{ fontFamily: fontFamilyBold, fontSize: 20, color: '#34495E' }}>Logged In</Text>
                    </View>
                </BlurView>
            </Modal>
            <SafeAreaView>
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    showsHorizontalScrollIndicator={false}
                >
                    <View>
                        <TouchableOpacity style={{ padding: 15 }}
                            onPress={() => props.navigation.goBack()}
                        >
                            <Image
                                source={require('../../assets/images/Left.png')}
                                style={{ width: 24, height: 24 }}
                            />
                        </TouchableOpacity>
                        <View style={{ padding: 15 }}>
                            <Text style={styles.verifyCodeText1}>Verification Code</Text>
                            <Text style={styles.verifyCodeText2}>We will send a 6 digit code to +{props.route.params.cc} {props.route.params.phone} that will allow you confirm your account.</Text>
                            <View style={{ height: 100 }}></View>
                            <OTP
                                onChangeText={(t) => setCode(t)}
                                codeValid={codeValidAll == null ? true : codeValidAll}
                            />
                            <View style={{ height: 40 }}></View>
                            <Button
                                name="Next"
                                onPress={() => {
                                    onsubmit()
                                }}
                                loading={loading}
                                disable={codeValid}
                            />
                            <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 30 }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ color: '#34495E', fontFamily: fontFamilyRegular, fontSize: 15 }}>Don't get the code? </Text>
                                    <TouchableOpacity
                                        onPress={() => {
                                            setDis(true)
                                            resendOtp()
                                        }}
                                        disabled={dis}
                                    >
                                        {seconds == 0 &&
                                            <Text style={{ color: '#276EF1', fontFamily: fontFamilyRegular, fontSize: 15, textAlign: 'center' }}>RESEND
                                     </Text>
                                        }
                                        {seconds > 0 &&
                                            <Text style={{ color: '#276EF1', fontFamily: fontFamilyRegular, fontSize: 15, textAlign: 'center' }}>{'00:'}{
                                                seconds < 10 ? '0' + seconds : seconds
                                            }</Text>
                                        }
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>

                    </View>
                </ScrollView>
            </SafeAreaView>
        </View>
    )
}