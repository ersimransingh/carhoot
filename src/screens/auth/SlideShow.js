import React from 'react';
import { View, Text, Image, SafeAreaView, Dimensions, TouchableOpacity } from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import AsyncStorage from '@react-native-community/async-storage'
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height;
import styles from './AuthStyles';

const Slides = [
    {
        key: 1,
        title: '24 hour recovery services',
        text: 'Get a tow truck or a recovery expert to your location within 30 minutes.',
        image: require('../../assets/images/slide1.png'),
        backgroundColor: '#59b2ab',
    },
    {
        key: 2,
        title: 'On-demand auto services by vetted professionals',
        text: 'Have your car picked up for quality guaranteed services by our partners.',
        image: require('../../assets/images/slide2.png'),
        backgroundColor: '#febe29',
    },
    {
        key: 3,
        title: 'Buy genuine spare parts or import a car',
        text: 'We will help with purchasing parts under warranty or importing a new car.',
        image: require('../../assets/images/slide3.png'),
        backgroundColor: '#22bcb5',
    }
]

export default function SlideShow(props) {
    const checkPage = async () => {
        try {
            let a = await AsyncStorage.getItem('sliders');
            if (a == "true") {
                props.navigation.navigate('Login')
            }
        } catch (err) {
            console.log(err);
        }
    }
    React.useEffect(() => {
        checkPage()
    }, [false]);
    const slideRef = React.useRef();
    const _renderItem = ({ item }) => {
        return (
            <View key={"a" + item.key} style={styles.introMainContainer}>
                <SafeAreaView>

                    <Image
                        source={item.image}
                        style={styles.introImageStyle}
                        resizeMode={'contain'}

                    />
                    <View style={{ padding: 20 }}>
                        <Text style={styles.introTextTitle}>{item.title}</Text>
                        <Text style={styles.introTextDescription}>{item.text}</Text>
                    </View>
                </SafeAreaView>
            </View>
        )
    }
    return (
        <View style={{ flex: 1, backgroundColor: 'white' }}>
            <AppIntroSlider
                ref={slideRef}
                renderItem={_renderItem}
                data={Slides}
                showSkipButton={true}
                dotStyle={{ backgroundColor: '#E0E6ED' }}
                activeDotStyle={{ backgroundColor: '#276EF1', width: 25 }}
                renderPagination={(activeIndex) => {
                    return (
                        <View style={{ marginBottom: 20 }}>
                            {/* FOR PAGINATION */}
                            <View style={{ alignItems: 'center' }}>
                                <View style={{ flexDirection: 'row' }}>
                                    {Slides.map((data, index) => {
                                        return (
                                            <View key={index} style={{ padding: 5, backgroundColor: activeIndex == index ? "#276EF1" : '#E0E6ED', borderRadius: 50, marginHorizontal: 5, width: activeIndex == index ? 30 : 0 }}></View>
                                        )
                                    })}


                                </View>
                            </View>

                            {/* FOR BUTTONS */}
                            <View style={{ padding: 10, marginTop: 50 }}>
                                {Slides.length - 1 == activeIndex &&
                                    <TouchableOpacity style={{ paddingVertical: 15, paddingHorizontal: 25, flexDirection: 'row', backgroundColor: '#276EF1', justifyContent: 'center', alignItems: 'center', borderRadius: 50 }}
                                        onPress={() => {
                                            AsyncStorage.setItem('sliders', "true")
                                            props.navigation.navigate('Login')
                                        }}
                                    >
                                        <Text style={styles.introNextButtonText}>Get Started</Text>
                                    </TouchableOpacity>
                                }
                                {Slides.length - 1 != activeIndex &&


                                    <View style={{ flexDirection: 'row' }}>
                                        <View style={{ flex: 1, alignItems: 'flex-start' }}>
                                            <TouchableOpacity style={{ paddingVertical: 15, paddingHorizontal: 25, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', borderRadius: 50 }}
                                                onPress={() => slideRef.current.goToSlide(Slides.length - 1)}
                                            >
                                                <Text style={[styles.introNextButtonText, { color: '#34495E' }]}>SKIP</Text>

                                            </TouchableOpacity>
                                        </View>
                                        <View style={{ flex: 1, alignItems: 'flex-end' }}>
                                            <TouchableOpacity style={{ paddingVertical: 15, paddingHorizontal: 25, flexDirection: 'row', backgroundColor: '#276EF1', justifyContent: 'center', alignItems: 'center', borderRadius: 50 }}
                                                onPress={() => slideRef.current.goToSlide(activeIndex + 1)}
                                            >
                                                <Text style={styles.introNextButtonText}>NEXT</Text>
                                                <Image
                                                    source={require('../../assets/images/nextButton.png')}
                                                    style={{ width: 20, height: 12, marginLeft: 10 }}
                                                />
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                }
                            </View>
                        </View>
                    )
                }}
            />

        </View>
    )
}