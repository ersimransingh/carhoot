import { StyleSheet, Dimensions } from 'react-native'
import { bluecolor, fontFamilyBold, fontFamilyLight, fontFamilyMedium, fontFamilyRegular, textColor } from '../../global/globalStyle'
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height;

export default StyleSheet.create({
    splashScreenContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: bluecolor,
    },
    splashScreenText: {
        textAlign: 'center',
        fontSize: 40,
        color: 'white',
        fontFamily: fontFamilyBold
    },
    introMainContainer: {
        flex: 1,
        backgroundColor: 'white'
    },
    introImageStyle: {
        width: width,
        alignSelf: 'stretch',
        height: height / 2.3
    },
    introTextTitle: {
        fontSize: 22,
        lineHeight: 32,
        fontFamily: fontFamilyBold,
        color: textColor,
        textAlign: 'center'
    },
    introTextDescription: {
        fontSize: 15,
        lineHeight: 24,
        fontFamily: fontFamilyRegular,
        color: '#8086A5',
        textAlign: 'center'
    },
    introNextButtonText: {
        color: "#FFFFFF",
        fontSize: 15,
        fontFamily: fontFamilyBold
    },
    loginContainer: {
        backgroundColor: 'white',
        flex: 1
    },
    loginBGImageStyle: {
        flex: 1, padding: 15
    },
    loginErrorStyle: {
        fontFamily: fontFamilyRegular, marginTop: 10, color: '#FF0036'
    },
    loginBgImage: {
        width: width / 1.3,
        height: width / 1.3,
        left: undefined,
        resizeMode: "contain",
    },
    loginLogoText: {
        color: '#276EF1',
        fontFamily: fontFamilyBold,
        fontSize: 25
    },
    loginGetStarted: {
        color: '#276EF1',
        fontFamily: fontFamilyBold,
        fontSize: 32,
        lineHeight: 44,
        color: '#34495E'
    },
    verifyCodeContainer: {
        flex: 1,
        backgroundColor: 'white'
    },
    verifyCodeText1: {
        fontSize: 26,
        color: "#34495E",
        lineHeight: 36,
        fontFamily: fontFamilyBold
    },
    verifyCodeText2: {
        fontSize: 15,
        color: "#8086A5",
        lineHeight: 24,
        fontFamily: fontFamilyRegular
    },
    signUpAgree: {
        color: '#34495E',
        fontFamily: fontFamilyRegular,
        fontSize: 12,
        lineHeight: 24
    }
})