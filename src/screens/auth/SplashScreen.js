import React from 'react';
import { View, Image, Text, StyleSheet, ActivityIndicator } from 'react-native';
import { useSelector, useDispatch } from 'react-redux'
import {
    updateUserData, updateState, updateUserCarData, getCarMake, getCarModels, getRescueData, getAutoServiceData, updateRequests
    , setPartsCategoriesAll
} from '../../redux'
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios'
import Api from '../../api'
import styles from './AuthStyles'
import Toast from 'react-native-simple-toast'
export default function SplashScreen() {
    const dispatch = useDispatch()
    const user = useSelector(state => state);

    const setCategories = () => {
        axios.get(Api + "/api/getSubCategory")
            .then((data) => {
                dispatch(setPartsCategoriesAll(data.data.detail))
            })
            .catch((err) => {
                console.log(err);
            })
    }
    React.useEffect(() => {
        setCategories();
    }, [false])

    const _getuserData = (token) => {
        axios.post(Api + "/api/getcars/", {}, {
            headers: {
                'authorization': token
            }
        })
            .then((res) => {
                if (res.data.status == true) {

                    dispatch(updateUserCarData(res.data.data))
                }
            })
    }
    const _submitRequest = async () => {

        try {
            const value = await AsyncStorage.getItem('token')
            axios.post(Api + "/api/getRescueRequest", {

            }, {
                headers: { 'authorization': value }
            }
            )
                .then((res) => {
                    if (res.data.status == true) {
                        dispatch(updateRequests(res.data.data))
                    }
                })
                .catch((error) => {
                    console.log(error.message)
                    Toast.show(JSON.stringify(error))
                })
        }

        catch (err) {
            console.log(err)
        }

    }

    const _getautoService = (value) => {
        axios.post(Api + "/api/getautoService", {}, {
            headers: { 'authorization': value }
        })
            .then((res) => {
                if (res.data.status == true) {
                    dispatch(getAutoServiceData(res.data.data))
                }
                else {
                    console.log('rescue data error')
                }
            })
            .catch((err) => {
                console.log(err.message)
            })
    }
    const _getRescueData = async () => {
        const value = await AsyncStorage.getItem('token')
        _getautoService(value)
        try {
            axios.post(Api + "/api/getrescueService", {}, {
                headers: { 'authorization': value }
            }
            )
                .then((res) => {
                    if (res.data.status == true) {
                        dispatch(getRescueData(res.data.data))
                    }
                    else {
                        console.log('rescue data error')
                    }
                })
                .catch((err) => {
                    console.log(err.message)
                })
        }
        catch (err) {
            console.log(err.message)
        }
    }

    _checkToken = async () => {
        const value = await AsyncStorage.getItem('token');
        if (value != 'false' && value != null && value != undefined) {
            // console.log(value)
            axios.post(Api + "/api/checkToken", {}, {
                headers: {
                    'authorization': value
                }
            })
                .then(res => {
                    if (res.data.status == true) {
                        let a = res.data.data.user
                        _getuserData(value)
                        // _getModels()
                        _getRescueData()
                        dispatch(updateUserData(a))
                        setTimeout(() => {
                            dispatch(updateState('Dashboard'))
                        }, 2000)

                    }
                    else {
                        dispatch(updateState('auth'))
                        // alert('Session Expired Login Again')
                    }
                })
                .catch(error => {
                    console.log(error.message)
                    throw (error)

                });

        } else {
            setTimeout(() => {
                dispatch(updateState('auth'))
            }, 2000)

        }
    }
    React.useEffect(() => {
        _checkToken();
    }, [user])

    return (
        <View style={styles.splashScreenContainer}>
            <Image
                source={require("../../assets/images/white_logo.png")}
                style={{ width: 200, height: 38 }}
            />
            <View style={{ height: 50 }} />
            <ActivityIndicator
                size="large"
                color="#FFFFFF"
            />
        </View>
    )
}
