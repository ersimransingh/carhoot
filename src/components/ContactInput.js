import React, { useState } from 'react';
import { View, Text, TextInput, Image, Modal, ScrollView } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler'
import { buttonColor, fontFamilyRegular } from '../global/globalStyle'
import CountryPicker from 'react-native-country-picker-modal'

export default function ContactInput(props) {
    const [showmodal, setShowmodal] = React.useState(false);
    const [search, setSearch] = React.useState('');
    const [alphaCode, setAlphaCode] = useState(props.cc2)
    const [countryCode, setCountryCode] = useState(props.cc)
    const [showModalCountry, setShowmodalCountry] = React.useState(false);
    return (
        <View style={{}}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={showmodal}
                onRequestClose={() => {
                    setShowmodal(false);
                }}
            >
                <View style={{ flex: 1, backgroundColor: 'rgba(0,0,0,0.5)' }}>
                    <TouchableOpacity style={{ flex: 1 }} onPress={() => setShowmodal(false)}></TouchableOpacity>
                    <View style={{ flex: 2, backgroundColor: '#ffffff' }}>
                        {/* <View style={{}}>
                            <Text style={{ textAlign: 'center', color: '#0AC97E', fontFamily: fontFamilyRegular }}>{props.label}</Text>
                        </View> */}
                        <View>
                            <TextInput
                                placeholder="Search"
                                style={{ padding: 15, borderBottomColor: '#000', borderBottomWidth: 2, color: '#000000' }}
                                value={search}
                                onChangeText={(t) => setSearch(t)}
                            />
                        </View>
                        <ScrollView>
                            {/* {countryCodes.map((data, index) => {
                                return (
                                    <TouchableOpacity style={{ borderBottomWidth: 1, borderBottomColor: '#ccc', padding: 20 }}
                                        key={'a' + index}
                                        onPress={() => {
                                            setShowmodal(false);
                                            props.onChangeCountryCode(data.phoneCode)
                                        }}
                                    >
                                        <Text style={{ color: '#000000', fontSize: 16 }}>+{data.phoneCode}  -- <Text>{data.countryCode}</Text></Text>
                                    </TouchableOpacity>
                                )
                            })} */}

                        </ScrollView>
                    </View>
                </View>
            </Modal>
            <Text style={{ color: '#34495E', fontSize: 13, fontFamily: fontFamilyRegular, marginBottom: 10 }}>{props.label}</Text>
            <View style={{ backgroundColor: '#F8F8F8', borderRadius: 5, borderWidth: 1, borderColor: '#EFF1F7', flexDirection: 'row', padding: props.isonLogin ? 10 : 0 }}>
                <TouchableOpacity style={{ justifyContent: "center", alignItems: "center", padding: 10, flexDirection: 'row', borderRightColor: '#EFF1F7', borderRightWidth: 1 }}
                    onPress={() => setShowmodalCountry(true)}
                >
                    <CountryPicker
                        withCallingCode={true}
                        onSelect={(value) => {
                            setCountryCode(value['callingCode'][0])
                            setAlphaCode(value.cca2)
                            props.onUpdateCountryAlpha ? props.onUpdateCountryAlpha(value.cca2) : ''
                            props.onChangeCountryCode(value['callingCode'][0])
                        }}
                        cca2={alphaCode}
                        translation='eng'
                        withFilter={true}
                        countryCode={alphaCode}
                        withCallingCodeButton={false}
                        withFlagButton={false}
                        visible={showModalCountry}
                        onClose={() => {
                            setShowmodalCountry(false);
                        }}
                    />
                    <Text style={{ fontFamily: fontFamilyRegular, color: '#34495E', fontSize: 15 }}>+{props.valueCountryCode}</Text>
                    <Image
                        source={require('../assets/images/downArrayN.png')}
                        style={{ width: 12.5, height: 8.5, marginLeft: 6 }}
                    />
                </TouchableOpacity>
                <View style={{ flex: 1, justifyContent: 'center', marginLeft: 10 }}>
                    <TextInput
                        placeholder={props.placeholder}
                        style={{ fontSize: 15, fontFamily: fontFamilyRegular, padding: 10, paddingLeft: 0, color: '#34495E' }}
                        placeholderTextColor="#9D9D9D"
                        onChangeText={(t) => props.onChangeText(t)}
                        value={props.value}
                        secureTextEntry={props.secureText ? true : false}
                        maxLength={13}
                        keyboardType='phone-pad'
                        returnKeyType="done"
                        onSubmitEditing={() => props.onDone ? props.onDone() : ""}
                    />
                </View>
                {props.isValid &&
                    <View style={{ padding: 15 }}>
                        <Image
                            source={require('../assets/images/check.png')}
                            style={{ width: 20, height: 20 }}
                        />
                    </View>
                }
            </View>
        </View>
    )
}