import React from 'react';
import { View, Text, TextInput, Image, Modal, FlatList } from 'react-native';
import { cos } from 'react-native-reanimated';
import { buttonColor, fontFamilyRegular } from '../global/globalStyle'
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler'

export default function SelectOption(props) {
    const [showmodal, setShowmodal] = React.useState(false);
    const [update, setUpdate] = React.useState(1);
    let allData = props.data;
    let allDataOld = props.data;
    React.useEffect(() => {
        if (props.showmodal) {
            props.showmodal = showmodal;
        }

    }, [showmodal])
    const search = (text) => {
        let t = text.toLowerCase();
        let data = [];
        for (let i in allData) {
            let smallName = allData[i].name.toString().toLowerCase();
            if (smallName.indexOf(t) > -1) {
                allData[i].show = true;
                data.push(allData[i]);
            } else {
                allData[i].show = false;
                data.push(allData[i]);
            }
        }
        props.returnArray ? props.returnArray(data) : '';
    }
    let placeholder = '';
    if (props.selectedId) {
        for (let i in allDataOld) {
            if (allData[i].value == props.selectedId) {
                placeholder = allDataOld[i].name
            }
        }
        if (placeholder == '') {
            placeholder = props.placeholder;
        }
    } else {
        placeholder = props.placeholder;
    }

    React.useEffect(() => {
        if (!showmodal) {
            let data = [];
            for (let i in allData) {
                allData[i].show = true
                data.push(allData[i])
            }
            props.returnArray ? props.returnArray(data) : '';
        }
    }, [showmodal])
    return (
        <View style={{ position: 'relative' }}>
            {props.label != '' &&
                <Text style={{ color: '#9D9D9D', fontSize: 13, fontFamily: fontFamilyRegular, }}>{props.label}</Text>
            }
            <>
                <TouchableOpacity style={{ backgroundColor: '#F5F5F5', borderRadius: 0, borderWidth: 1, borderColor: '#276EF1', flexDirection: 'row', height: 45, backgroundColor: 'white' }}
                    onPress={() => setShowmodal(showmodal ? false : true)}
                >
                    <View style={{ flex: 1, justifyContent: 'center', paddingLeft: 15 }}>
                        {showmodal &&
                            <TextInput
                                placeholder="Search Here"
                                onChangeText={(t) => search(t)}
                                style={{ color: '#000000' }}
                            />
                        }
                        {!showmodal &&
                            <Text style={{ color: '#34495E', fontFamily: fontFamilyRegular }}>{placeholder}</Text>
                        }


                    </View>
                    <View style={{ padding: 15, justifyContent: 'center', alignItems: 'center' }}>
                        <Image
                            source={require('../assets/images/downArrow.png')}
                            style={{ width: 13, height: 7.42 }}
                        />
                    </View>
                </TouchableOpacity>
                {props.selectedId == '' && props.flag == true ? <Text style={{ color: 'red', padding: 5, fontSize: 12 }}> Please Select One Option </Text> : null}
                <View >
                    {showmodal &&
                        <View style={{ maxHeight: 200, borderRadius: 0, borderWidth: 0, borderColor: '#E0E6ED', top: 0, left: 0, right: 0, backgroundColor: 'white', zIndex: 1, position: 'absolute' }}>

                            <ScrollView nestedScrollEnabled={true}>

                                {props.data.map((data, index) => {
                                    if (data.show) {
                                        return (
                                            <TouchableOpacity style={{ borderBottomWidth: 1, borderBottomColor: '#E0E6ED', padding: 20 }}
                                                key={'a' + index}
                                                onPress={() => {

                                                    setShowmodal(false);
                                                    props.onChangeText(props.getId ? { value: data.value, name: data.name } : data.name)
                                                }}
                                            >
                                                <Text style={{ color: '#34495E', fontSize: 16, fontFamily: fontFamilyRegular }}>{data.name}</Text>
                                            </TouchableOpacity>
                                        )
                                    }
                                })}
                                {props.data.length == 0 &&
                                    <Text style={{ color: '#ccc' }}>No Records Found</Text>
                                }

                            </ScrollView>
                        </View>
                    }
                </View>
            </>
        </View>
    )
}


// {/* <FlatList 
//                                     scrollEnabled={true}
//                                      nestedScrollEnabled={true}
//                                     data={props.data}
//                                     keyExtractor={(item, index) => item.index = index + ''}
//                                     renderItem={({item}) => {
//                                         return(
//                                         <TouchableOpacity style={{ borderBottomWidth: 1, borderBottomColor: '#ccc', padding: 20 }}
//                                                 key={'a' + item.index}
//                                                 onPress={() => {

//                                                     setShowmodal(false);
//                                                     props.onChangeText(props.getId ? { value: item.value, name: item.name } : item.name)
//                                                 }}
//                                             >
//                                                 <Text style={{ color: '#000000', fontSize: 16 }}>{item.name}</Text>
//                                             </TouchableOpacity>
//                                         )
//                                     }}
//                                 /> */}