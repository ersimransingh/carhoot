import React from 'react';
import { View, Text, TextInput, Image, Modal, ScrollView, StyleSheet } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler'
import { cos } from 'react-native-reanimated';
import { buttonColor, fontFamilyRegular } from '../global/globalStyle'
export default function SelectLocation(props) {
    const [showmodal, setShowmodal] = React.useState(false);
    const [update, setUpdate] = React.useState(1);
    let allData = props.data;
    let allDataOld = props.data;
    let placeholder = '';
    if (props.selectedId) {
        for (let i in allDataOld) {
            if (allData[i].value == props.selectedId) {
                placeholder = allDataOld[i].name
            }
        }
        if (placeholder == '') {
            placeholder = props.placeholder;
        }
    } else {
        placeholder = props.placeholder;
    }

    React.useEffect(() => {
        if (!showmodal) {
            let data = [];
            for (let i in allData) {
                allData[i].show = true
                data.push(allData[i])
            }
            props.returnArray ? props.returnArray(data) : '';
        }
    }, [showmodal])
    return (
        <View style={{}}>
            <Text style={styles.mainLabel}>{props.label}</Text>
            <View>
                <TouchableOpacity style={styles.inputTouch}
                    onPress={() => setShowmodal(showmodal ? false : true)}
                >
                    <View style={styles.innerInputView}>
                        {showmodal &&
                            <TextInput
                                placeholder="Search Here"
                                autoFocus={true}
                                onChangeText={(t) => props.inputText(t)}
                                style={{ color: '#000000' }}
                            />
                        }
                        {!showmodal &&
                            <Text style={styles.placeholderText}>{placeholder}</Text>
                        }


                    </View>
                    <View style={styles.View1}>
                        <Image
                            source={require('../assets/images/downArrow.png')}
                            style={{ width: 24, height: 24 }}
                        />
                    </View>
                </TouchableOpacity>
                <View>
                    {showmodal &&
                        <View style={styles.header}>
                            <ScrollView nestedScrollEnabled={true}>
                                {props.data.map((data, index) => {
                                    return (
                                        <TouchableOpacity style={{ borderBottomWidth: 1, borderBottomColor: '#ccc', padding: 20 }}
                                            key={'a' + index}
                                            onPress={() => {
                                                setShowmodal(false);
                                                props.onChangeText(props.getId ? { value: data.value, name: data.name } : data.name)
                                                props.selectedDataObj(props.selectedDataObj ? JSON.stringify(data) : '')
                                            }}
                                        >
                                            <Text style={{ color: '#000000', fontSize: 16 }}>{data.name}</Text>
                                        </TouchableOpacity>
                                    )
                                })}


                            </ScrollView>
                        </View>
                    }
                </View>
            </View>
        </View >
    )
}
const styles = StyleSheet.create({
    mainLabel: {
        color: '#9D9D9D', fontSize: 13, fontFamily: fontFamilyRegular, marginBottom: 10
    },
    inputTouch: {
        backgroundColor: '#F5F5F5', borderRadius: 5, borderWidth: 1, borderColor: '#D6D6D6', flexDirection: 'row'
    },
    innerInputView: {
        flex: 1, justifyContent: 'center', paddingLeft: 15
    },
    placeholderText: {
        color: '#9D9D9D', fontFamily: fontFamilyRegular
    },
    View1: {
        padding: 14, justifyContent: 'center', alignItems: 'center'
    },
    header: {
        maxHeight: 200, borderRadius: 5, borderWidth: 1, borderColor: '#ccc', top: 0, left: 0, right: 0, backgroundColor: 'white', zIndex: 1
    }
});