import React from 'react';
import { View, Text, TouchableOpacity, TextInput, Image, Keyboard, StyleSheet } from 'react-native';
import { buttonColor, fontFamilyRegular, fontFamilyBold, fontFamilySemiBold } from '../global/globalStyle'
export default function OTP(props) {
    const input1 = React.useRef(null);
    const input2 = React.useRef(null);
    const input3 = React.useRef(null);
    const input4 = React.useRef(null);
    const input5 = React.useRef(null);
    const input6 = React.useRef(null);

    const [otp1, setOtp1] = React.useState('')
    const [otp2, setOtp2] = React.useState('')
    const [otp3, setOtp3] = React.useState('')
    const [otp4, setOtp4] = React.useState('')
    const [otp5, setOtp5] = React.useState('')
    const [otp6, setOtp6] = React.useState('')
    const [codeValid, setCodeValid] = React.useState(props.codeValid);

    React.useEffect(() => {
        if (otp1.length > 0) {
            input2.current.focus()
        }
        if (otp2.length > 0) {
            input3.current.focus()
        }
        if (otp3.length > 0) {
            input4.current.focus()
        }
        if (otp4.length > 0) {
            input5.current.focus()
        }
        if (otp5.length > 0) {
            input6.current.focus()
        }
        if (otp6.length > 0) {
            Keyboard.dismiss()
        }
        props.onChangeText(otp1 + otp2 + otp3 + otp4 + otp5 + otp6);

    }, [otp1, otp2, otp3, otp4, otp5, otp6])

    return (
        <View style={{}}>
            <Text style={{ color: '#9D9D9D', fontSize: 13, fontFamily: fontFamilyRegular, marginBottom: 10 }}>{props.label}</Text>
            <View style={{ flexDirection: 'row' }}>
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <TextInput
                        style={[styles.textInputStyle, { borderColor: props.codeValid == false ? "#FF0036" : "#EFF1F7" }]}
                        keyboardType="number-pad"
                        maxLength={1}
                        autoFocus={true}
                        ref={input1}
                        onChangeText={(t) => setOtp1(t)}
                    />
                </View>
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <TextInput
                        style={[styles.textInputStyle, { borderColor: props.codeValid == false ? "#FF0036" : "#EFF1F7" }]}
                        keyboardType="number-pad"
                        maxLength={1}
                        ref={input2}
                        onChangeText={(t) => setOtp2(t)}
                        onKeyPress={({ nativeEvent }) => {
                            if (nativeEvent.key === 'Backspace') {
                                input1.current.focus()
                            }
                        }}
                    />
                </View>
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <TextInput
                        style={[styles.textInputStyle, { borderColor: props.codeValid == false ? "#FF0036" : "#EFF1F7" }]}
                        keyboardType="number-pad"
                        maxLength={1}
                        ref={input3}
                        onChangeText={(t) => setOtp3(t)}
                        onKeyPress={({ nativeEvent }) => {
                            if (nativeEvent.key === 'Backspace') {
                                input2.current.focus()
                            }
                        }}
                    />
                </View>
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <TextInput
                        style={[styles.textInputStyle, { borderColor: props.codeValid == false ? "#FF0036" : "#EFF1F7" }]}
                        keyboardType="number-pad"
                        maxLength={1}
                        ref={input4}
                        onChangeText={(t) => setOtp4(t)}
                        onKeyPress={({ nativeEvent }) => {
                            if (nativeEvent.key === 'Backspace') {
                                input3.current.focus()
                            }
                        }}
                    />
                </View>
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <TextInput
                        style={[styles.textInputStyle, { borderColor: props.codeValid == false ? "#FF0036" : "#EFF1F7" }]}
                        keyboardType="number-pad"
                        maxLength={1}
                        ref={input5}
                        onChangeText={(t) => setOtp5(t)}
                        onKeyPress={({ nativeEvent }) => {
                            if (nativeEvent.key === 'Backspace') {
                                input4.current.focus()
                            }
                        }}
                    />
                </View>
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <TextInput
                        style={[styles.textInputStyle, { borderColor: props.codeValid == false ? "#FF0036" : "#EFF1F7" }]}
                        keyboardType="number-pad"
                        maxLength={1}
                        ref={input6}
                        onChangeText={(t) => setOtp6(t)}
                        onKeyPress={({ nativeEvent }) => {
                            if (nativeEvent.key === 'Backspace') {
                                input5.current.focus()
                            }
                        }}
                    />
                </View>
            </View>
            <View style={{ height: 10 }} />
            {!props.codeValid &&
                <Text style={{ fontFamily: fontFamilyRegular, color: '#FF0036', lineHeight: 24, fontSize: 15 }}>Please provide a valid verification code.</Text>
            }
        </View>
    )
}


const styles = StyleSheet.create({
    textInputStyle: {
        color: '#34495E',
        fontFamily:
            fontFamilyBold,
        padding: 0,

        width: 50,
        height: 50,
        backgroundColor: '#F8F8F8',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#EFF1F7',
        justifyContent: 'center',
        alignContent: 'center',
        textAlign: 'center',
        fontSize: 18
    }
})