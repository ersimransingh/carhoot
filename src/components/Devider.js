import React from 'react';
import { View, Text, } from 'react-native';
import { global, fontFamilyRegular } from '../global/globalStyle'
export default function Devider(props) {
    return (
        <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 1, borderBottomColor: '#EBF0FF', borderBottomWidth: 1, marginBottom: 10 }}></View>
            <View style={{ alignItems: 'center', justifyContent: 'center', paddingHorizontal: 15 }}>
                <Text style={{ color: '#9098B1', fontSize: 14, fontFamily: fontFamilyRegular }}>OR</Text>
            </View>
            <View style={{ flex: 1, borderBottomColor: '#EBF0FF', borderBottomWidth: 1, marginBottom: 10 }}></View>
        </View>
    )
}