import React from 'react';
import { View, Text, TouchableOpacity, TextInput, Image } from 'react-native';
import { buttonColor, fontFamilyRegular } from '../global/globalStyle'
export default function Input(props) {
    return (
        <View style={{}}>
            {props.label != "" &&
                <Text style={{ color: '#9D9D9D', fontSize: 13, fontFamily: fontFamilyRegular, marginBottom: 10 }}>{props.label}</Text>
            }
            <View style={{ backgroundColor: '#F8F8F8', borderRadius: 5, borderWidth: 1, borderColor: '#EFF1F7', flexDirection: 'row' }}>
                <View style={{ flex: 1 }}>
                    <TextInput
                        returnKeyType={props.returnKeyType}
                        onSubmitEditing={props.onSubmitEditing}
                        ref={props.r}
                        placeholder={props.placeholder}
                        style={{ fontSize: 15, fontFamily: fontFamilyRegular, padding: 20, fontFamily: fontFamilyRegular, color: '#000000' }}
                        placeholderTextColor="#B2BAC4"
                        onChangeText={(t) => props.onChangeText(t)}
                        value={props.value}
                        secureTextEntry={props.secureText ? true : false}
                    />
                </View>
                {props.isValid &&
                    <View style={{ padding: 15 }}>
                        <Image
                            source={require('../assets/images/check.png')}
                            style={{ width: 20, height: 20 }}
                        />
                    </View>
                }
                {!props.isValid && props.value.length > 1 &&
                    <View style={{ padding: 15 }}>
                        <Image
                            source={require('../assets/images/false.png')}
                            style={{ width: 20, height: 20 }}
                        />
                    </View>
                }
            </View>
            {props.isValid == null && <View><Text style={{ paddingTop: 7, fontSize: 12, color: 'red' }} >{props.err}</Text></View>}
            {props.isValid == false && props.value.length > 1 && <View><Text style={{ padding: 7, fontSize: 12, color: 'red' }} >{props.err}</Text></View>}
        </View>
    )
}