import React from 'react';
import { View, Text, TextInput, Image, StyleSheet } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler'
import { buttonColor, fontFamilyRegular, fontFamilyBold } from '../global/globalStyle'
export default function MultipleRadio(props) {
    return (
        <View style={{}}>
            <Text style={styles.labelStyle}>{props.label}</Text>
            {props.data.map((dd, index) => {
                return (
                    <TouchableOpacity
                        key={index}
                        style={[styles.touchStyle, { borderWidth: dd.status ? 1 : 0 }]}
                        onPress={() => props.onChange(index)}
                    >
                        <View style={styles.textView}>
                            <Text style={styles.textViewText}>{dd.name}</Text>
                        </View>
                    </TouchableOpacity>
                )
            })}
        </View>
    )
}

const styles = StyleSheet.create({
    labelStyle: {
        color: '#34495E', fontSize: 13, fontFamily: fontFamilyRegular, marginBottom: 10
    },
    touchStyle: {
        flexDirection: 'row', padding: 15, backgroundColor: "#EEEEEE", borderRadius: 5, marginBottom: 10, borderColor: "#276EF1"
    },
    textView: {
        flex: 1, paddingLeft: 10, justifyContent: 'center'
    },
    textViewText: {
        color: "#34495E", fontSize: 15, fontFamily: fontFamilyBold
    }
});