import React from 'react';
import { View, Text, TextInput, Image, Modal, FlatList } from 'react-native';
import { cos } from 'react-native-reanimated';
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler'
import { buttonColor, fontFamilyRegular } from '../global/globalStyle'
export default function SelectOption(props) {
    const [showmodal, setShowmodal] = React.useState(false);
    const [update, setUpdate] = React.useState(1);
    let allData = props.data;
    let allDataOld = props.data;
    const search = (text) => {
        let t = text.toLowerCase();
        let data = [];
        for (let i in allData) {
            let smallName = allData[i].name.toString().toLowerCase();
            if (smallName.indexOf(t) > -1) {
                allData[i].show = true;
                data.push(allData[i]);
            } else {
                allData[i].show = false;
                data.push(allData[i]);
            }
        }
        props.returnArray ? props.returnArray(data) : '';
    }
    let placeholder = '';
    if (props.selectedId) {
        for (let i in allDataOld) {
            if (allData[i].value == props.selectedId) {
                placeholder = allDataOld[i].name
            }
        }
        if (placeholder == '') {
            placeholder = props.placeholder;
        }
    } else {
        placeholder = props.placeholder;
    }

    React.useEffect(() => {
        if (!showmodal) {
            let data = [];
            for (let i in allData) {
                allData[i].show = true
                data.push(allData[i])
            }
            props.returnArray ? props.returnArray(data) : '';
        }
    }, [showmodal])
    return (
        <>

            <Text style={{ color: '#34495E', fontSize: 13, fontFamily: fontFamilyRegular, marginBottom: 10 }}>{props.label}</Text>
            <>
                <TouchableOpacity style={{ backgroundColor: '#F8F8F8', borderRadius: 5, borderWidth: 1, borderColor: '#EFF1F7', flexDirection: 'row' }}
                    onPress={() => setShowmodal(showmodal ? false : true)}
                >
                    <View style={{ flex: 1, justifyContent: 'center', paddingLeft: 15 }}>
                        {showmodal &&
                            <TextInput
                                placeholder="Search Here"
                                onChangeText={(t) => search(t)}
                                style={{ color: '#000000' }}
                            />
                        }
                        {!showmodal &&
                            <Text style={{ color: '#9D9D9D', fontFamily: fontFamilyRegular }}>{placeholder}</Text>
                        }


                    </View>
                    <View style={{ padding: 23, justifyContent: 'center', alignItems: 'center' }}>
                        <Image
                            source={require('../assets/images/downArrow.png')}
                            style={{ width: 13, height: 7.42 }}
                        />
                    </View>
                </TouchableOpacity>
                {props.selectedId == '' && props.flag == true ? <Text style={{ color: 'red', padding: 5, fontSize: 12 }}> Please Select One Option </Text> : null}
                <View>
                    {showmodal &&
                        <View style={{ maxHeight: 200, borderRadius: 5, borderWidth: 1, borderColor: '#ccc', top: 0, left: 0, right: 0, backgroundColor: 'white', zIndex: 1, position: "absolute" }}>
                            <ScrollView nestedScrollEnabled={true}>

                                {props.data.map((data, index) => {
                                    if (data.show) {
                                        return (
                                            <TouchableOpacity style={{ borderBottomWidth: 1, borderBottomColor: '#ccc', padding: 20 }}
                                                key={'a' + index}
                                                onPress={() => {
                                                    setShowmodal(false);
                                                    props.onChangeText(props.getId ? { value: data.value, name: data.name } : data.name)
                                                }}
                                            >
                                                <Text style={{ color: '#000000', fontSize: 16 }}>{data.name}</Text>
                                            </TouchableOpacity>
                                        )
                                    }
                                })}
                                {props.data.length == 0 &&
                                    <Text style={{ color: '#ccc' }}>No Records Found</Text>
                                }

                            </ScrollView>
                        </View>
                    }
                </View>
            </>
        </>
    )
}