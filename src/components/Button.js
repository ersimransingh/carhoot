import React from 'react';
import { View, Text, ActivityIndicator, TouchableOpacity } from 'react-native';
import { TouchableOpacity as TouchableOpacityGesture } from 'react-native-gesture-handler'
import { buttonColor, fontFamilyBold, fontFamilyRegular } from '../global/globalStyle'
export default function Button(props) {
    if (props.gesture) {
        return (
            <TouchableOpacityGesture style={{ backgroundColor: props.disable ? "#B2BAC4" : buttonColor, paddingVertical: 18, borderRadius: 5 }}
                onPress={props.onPress}
                disabled={props.disable}
            >
                {props.loading ? <ActivityIndicator

                    animating={true}
                    color='white'
                /> :
                    <Text style={{ color: props.textColor ? props.textColor : 'white', fontSize: 22, fontFamily: fontFamilyBold, textAlign: 'center' }}>{props.name}</Text>
                }


            </TouchableOpacityGesture>
        )
    } else {
        return (
            <TouchableOpacity style={{ backgroundColor: props.disable ? "#B2BAC4" : buttonColor, paddingVertical: 18, borderRadius: 5 }}
                onPress={props.onPress}
                disabled={props.disable}
            >
                {props.loading ? <ActivityIndicator

                    animating={true}
                    color='white'
                /> :
                    <Text style={{ color: props.textColor ? props.textColor : 'white', fontSize: 22, fontFamily: fontFamilyBold, textAlign: 'center' }}>{props.name}</Text>
                }


            </TouchableOpacity>
        )
    }
}