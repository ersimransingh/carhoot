import React from 'react';
import { View, Text, TextInput, Image, Modal, Platform } from 'react-native';
import { cos } from 'react-native-reanimated';
import { buttonColor, fontFamilyRegular } from '../global/globalStyle'
import { useSelector, useDispatch } from 'react-redux';
import { updateLocationsData } from '../redux'
import Toast from 'react-native-simple-toast';
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler';
import axios from 'axios';

export default function SelectLocationNew(props) {
    const [showmodal, setShowmodal] = React.useState(false);
    const [update, setUpdate] = React.useState(1);
    const [locations, setLocations] = React.useState('[]');
    const _getSearchPlace = async (t) => {
        try {
            axios.post("https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" + t + "&key=AIzaSyAB7cS3Wb_fyoN_GObjGhud-3PMNNRmfwc")
                .then((res) => {
                    let data = [];
                    if (res.data.predictions.length != 0) {
                        let places = res.data.predictions;
                        for (let y in places) {
                            let a = { name: places[y].description, value: places[y].place_id, show: true };
                            data.push(a);
                        }
                        setLocations(JSON.stringify(data))
                    }
                })
                .catch((error) => {
                    console.log(error)
                })
        }
        catch (err) {
            Toast.show(err)
        }
    }
    const abst = React.useState(props.nonAbsolute ? true : false)
    const getLoc = (val, name) => {
        try {
            axios.post('https://maps.googleapis.com/maps/api/place/details/json?place_id=' + val + '&key=AIzaSyAB7cS3Wb_fyoN_GObjGhud-3PMNNRmfwc')
                .then((res) => {
                    if (res.data.result.length != 0) {
                        let cordinates = { long: res.data.result.geometry.location.lng, lat: res.data.result.geometry.location.lat }
                        let main = { name: name, cords: cordinates };
                        props.onSelect(main);
                    }

                })
                .catch((error) => {
                    console.log(error)
                })
        }
        catch (err) {
            console.log(err)
        }
    }
    React.useEffect(() => {
        if (!showmodal) {

        }
    }, [showmodal])
    let placeholder = props.placeholder;
    return (
        <View style={{ zIndex: 1000 }}>
            {props.label != '' &&
                <Text style={{ color: '#000000', fontSize: 13, fontFamily: fontFamilyRegular, marginBottom: 7 }}>{props.label}</Text>
            }
            <View>
                <TouchableOpacity style={{ backgroundColor: '#F5F5F5', borderRadius: 0, borderWidth: 1, borderColor: '#276EF1', flexDirection: 'row', height: 45, backgroundColor: 'white' }}
                    onPress={() => setShowmodal(showmodal ? false : true)}
                >
                    <View style={{ flex: 1, justifyContent: 'center', paddingLeft: 15 }}>
                        {showmodal &&
                            <TextInput
                                placeholder="Search Here"
                                autoFocus={true}
                                onChangeText={(t) => _getSearchPlace(t)}
                                style={{ color: '#000000' }}
                            />
                        }
                        {!showmodal &&
                            <Text style={{ color: '#34495E', fontFamily: fontFamilyRegular }}>{placeholder}</Text>
                        }
                    </View>
                    <View style={{ justifyContent: 'center', alignItems: 'center', }}>
                        <Image
                            source={require('../assets/images/searchActive.png')}
                            style={{ width: 16, height: 16, marginRight: 15 }}
                        />
                    </View>
                </TouchableOpacity>
                <View style={{ zIndex: 121212121 }}>
                    {showmodal &&
                        <View style={{ maxHeight: 200, borderRadius: 0, borderWidth: JSON.parse(locations).length ? 1 : 0, borderColor: '#ccc', backgroundColor: 'white', position: Platform.OS == 'ios' ? 'absolute' : "relative", zIndex: 50 }}>
                            <ScrollView
                                nestedScrollEnabled={true}
                            >

                                {JSON.parse(locations).map((data, index) => {
                                    return (
                                        <TouchableOpacity style={{ borderBottomWidth: 1, borderBottomColor: '#ccc', padding: 20, zIndex: 51 }}
                                            key={'a' + index}
                                            onPress={() => {
                                                setShowmodal(false);
                                                getLoc(data.value, data.name)
                                            }}
                                        >
                                            <Text style={{ color: '#000000', fontSize: 16 }}>{data.name}</Text>
                                        </TouchableOpacity>
                                    )
                                })}
                            </ScrollView>
                        </View>
                    }
                </View>
            </View>
        </View >
    )
}