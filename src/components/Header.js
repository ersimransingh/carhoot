import React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';

export default function Header(props) {

    return (
        <View style={{ flexDirection: "row", paddingVertical: 0, }}>
            {props.backButton &&
                <TouchableOpacity style={{ paddingLeft: 10, paddingVertical: 8 }}
                    onPress={() => props.navigation.goBack()}
                >
                    <Image
                        source={require('../assets/images/back.png')}
                        style={{ width: 24, height: 24 }}
                    />
                </TouchableOpacity>
            }
            <View style={{ justifyContent: 'center', paddingLeft: 10 }}>

            </View>
        </View >
    )
}